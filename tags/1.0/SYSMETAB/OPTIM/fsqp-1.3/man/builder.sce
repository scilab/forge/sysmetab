mode(-1);

DIRM=get_absolute_file_path('builder.sce');
// adding this directory to %helps
// this is important here for link search during 
// translation from xml to html 
add_help_chapter('FSQP: Constrained minimax optimization problems solver',DIRM+'fsqp')


// process the xml files to produce html files 
xmltohtml(DIRM+'fsqp');


