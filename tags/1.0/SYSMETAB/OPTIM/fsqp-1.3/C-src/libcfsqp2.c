#include <mex.h> 
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc cintfsqp1;
extern Gatefunc cintfsqp0;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway,cintfsqp1,"x_is_new"},
  {(Myinterfun)sci_gateway,cintfsqp0,"set_x_is_new"},
};
 
int C2F(libcfsqp2)()
{
  Rhs = Max(0, Rhs);
  (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  return 0;
}
