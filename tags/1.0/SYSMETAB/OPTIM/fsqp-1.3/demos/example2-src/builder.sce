// builder for a shared library for example2.c functions 
//=======================================================

link_name =["objmad","cnmad"];
flag  = "c";		// ext1c is a C function 
files = ['example2.o' ];   // objects files for ext1c 
libs  = [];		// other libs needed for linking 

// the next call generates files (Makelib,loader.sce) used
// for compiling and loading ext1c and performs the compilation

ilib_for_link(link_name,files,libs,flag);

