function f=f_1(x)
  f=x(10);
endfunction

function g=grf_1(x)
  g=[0,0,0,0,0,0,0,0,0,1];
endfunction

function g=G_1(x)
  g=(1-x(10))^2-z(t1,x);
endfunction

function g=G_2(x)
  g=z(t2,x)-(1+x(10))^2;
endfunction

function g=G_3(x)
  g=z(t3,x)-(x(10))^2;
endfunction

function w=grG_1(x)
  w=[-grz(t1,x),-2*(1-x(10))*ones(t1)]
endfunction

function w=grG_2(x)
  w=[grz(t2,x),-2*(1+x(10))*ones(t2)]
endfunction

function w=grG_3(x)
  w=[grz(t3,x),-2*x(10)*ones(t3)]
endfunction

