exdir=get_absolute_file_path('example3.sce');

modefsqp=100;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=7.e-6;
udelta=0.e0;
nf=1;
neqn=1;
nineqn=1;
nineq=1;
neq=1;

x0=[1;5;5;1];

bl=ones(x0);
bu=5*bl;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];

//[1] fsqp with Scilab functions
//=======================================================

getf(exdir+'/example3.sci');  //Loading Scilab fcts

//Scilab functions obj71,cntr71,grob71,grcn71 are called by fsqp:
x=fsqp(x0,ipar,rpar,[bl,bu],obj71,cntr71,grob71,grcn71)
srpar=[0,0,0];mesh_pts=[];

//Scilab functions obj71,cntr71,grob71,grcn71 are called by srfsqp:
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj71,cntr71,grob71,grcn71)

//[2] fsqp with  C functions 
//=======================================================
exec(exdir+'/example3-src/loader.sce') 

x=fsqp(x0,ipar,rpar,[bl,bu],'obj71','cntr71','grob71','grcn71');

//Check
//nineqn non linear inequality
for j=1:nineqn
	cntr71(j,x)
end

//nineq-nineqn linear inequality
for j=nineqn+1:nineqn+(nineq-nineqn)
	cntr71(j,x)
end

//neqn nonlinear equality
for j=nineqn+(nineq-nineqn)+1:nineqn+(nineq-nineqn)+neqn
	cntr71(j,x)
end

//neq-neqn linear equality
for j=nineqn+(nineq-nineqn)+neqn+1:nineqn+(nineq-nineqn)+neqn+neq-neqn
	cntr71(j,x)
end

//[2] fsqp with  lists 
//=======================================================

getf(exdir+'/listutils.sci')

getf(exdir+'/example3_list.sci'); //Loading Scilab functions f_1, g_1 and h_1
//f_1 returns the objective (R-valued)
//g_1 returns non linear regular inequality constaint (R-valued)
//h_1 returns non linear regular equality constaint (R-valued)

// List of objectives: 1 reg, 0 SR
list_obj=list(list(f_1),list()) 
// List of constraints: 1 nonlin regular inequ and 1 non lin regular equality
list_cntr=list(list(g_1),list(),list(),list(),list(h_1),list())

//lists of gradients functions
list_grobj=list(list(grf_1),list())
list_grcn=list(list(grg_1),list(),list(),list(),list(grh_1),list())

x0=[1;5;5;1];
//fsqp parameters obtained by findparam
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-7;epsneq=7.e-6;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=ones(x0);
bu=5*bl;

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)
