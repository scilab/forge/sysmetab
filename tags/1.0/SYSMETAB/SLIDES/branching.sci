function [x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input)
n1=5;
//
// Cumom�res de poids 1
//
M1_ijv=[1,1,-(v(1)+v(2)+v(3))
2,2,-(v(1)+v(2)+v(3))
3,2,v(2)
3,1,v(2)
3,3,-(v(4)+v(4))
4,1,v(1)
4,2,v(3)
4,3,v(4)
4,4,-v(5)
5,2,v(1)
5,1,v(3)
5,3,v(4)
5,5,-v(5)];
M1=sparse(M1_ijv(:,1:2),M1_ijv(:,3),[n1,n1]);
b1_ijv=[1,1,v(6).*x1_input(1,:)
2,1,v(6).*x1_input(2,:)];
b1_1=s_full(b1_ijv(:,1:2),b1_ijv(:,3),[n1,1]);
[M1_handle,M1_rank]=lufact(M1);
x1=lusolve(M1_handle,-[b1_1]);
dg1_dv_ijv=[1,6,x1_input(1,:)
1,1,-x1(1,:)
1,2,-x1(1,:)
1,3,-x1(1,:)
2,6,x1_input(2,:)
2,1,-x1(2,:)
2,2,-x1(2,:)
2,3,-x1(2,:)
3,2,x1(2,:)+x1(1,:)
3,4,-x1(3,:)-x1(3,:)
4,1,x1(1,:)
4,3,x1(2,:)
4,4,x1(3,:)
4,5,-x1(4,:)
5,1,x1(2,:)
5,3,x1(1,:)
5,4,x1(3,:)
5,5,-x1(5,:)];
dg1_dv_1=s_full(dg1_dv_ijv(:,1:2),dg1_dv_ijv(:,3),[n1,6]);
dx1_dv=zeros(n1,6,1);
dx1_dv(:,:,1)=lusolve(M1_handle,-dg1_dv_1);
ludel(M1_handle);
n2=2;
//
// Cumom�res de poids 2
//
M2_ijv=[1,1,-(v(1)+v(2)+v(3))
2,1,v(1)+v(3)
2,2,-v(5)];
M2=sparse(M2_ijv(:,1:2),M2_ijv(:,3),[n2,n2]);
b2_ijv=[1,1,v(6).*x2_input(1,:)
2,1,v(4).*x1(3,:).*x1(3,:)];
b2_1=s_full(b2_ijv(:,1:2),b2_ijv(:,3),[n2,1]);
[M2_handle,M2_rank]=lufact(M2);
x2=lusolve(M2_handle,-[b2_1]);
dg2_dv_ijv=[1,6,x2_input(1,:)
1,1,-x2(1,:)
1,2,-x2(1,:)
1,3,-x2(1,:)
2,1,x2(1,:)
2,3,x2(1,:)
2,4,x1(3,:).*x1(3,:)
2,5,-x2(2,:)];
dg2_dv_1=s_full(dg2_dv_ijv(:,1:2),dg2_dv_ijv(:,3),[n2,6]);
db2_dx1_ijv=[2,3,x1(3,:).*v(4)+x1(3,:).*v(4)];
db2_dx1_1=sparse(db2_dx1_ijv(:,1:2),db2_dx1_ijv(:,3),[n2,n1]);
dx2_dv=zeros(n2,6,1);
dx2_dv(:,:,1)=lusolve(M2_handle,-(dg2_dv_1+db2_dx1_1*dx1_dv(:,:,1)));
ludel(M2_handle);
endfunction
function [cost,grad]=costAndGrad(v)
[x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input);
e_label=(C1*x1+C2*x2)-yobs;
e_flux=E*v-vobs;
cost=0.5*(sum(delta.*e_flux.^2)+sum(alpha(:,1).*e_label(:,1).^2));
grad=(delta.*e_flux)'*E+(alpha(:,1).*e_label(:,1))'*(C1*dx1_dv(:,:,1)+C2*dx2_dv(:,:,1));
endfunction
function [A,b,N,H,w]=fluxSubspace(_fluxes)
//
// Construction des matrices (dont la matrice de stoechiom�trie) permettant de calculer le changement de variable affine des flux.
//
N_ijv=[1,1,-1
1,2,-1
1,3,-1
1,6,1
2,2,1+1
2,4,-1-1
3,1,1
3,3,1
3,4,1
3,5,-1];
N=s_full(N_ijv(:,1:2),N_ijv(:,3),[3,6]);
//
// Flux impos�s dans l'interface
//
[H,w]=fluxConstraints(_fluxes);
b=[zeros(3,1);w];
A=[N;H];
endfunction
rr//
// Matrices d'observation (cumom�res)
//
//
// Matrices d'observation (cumom�res)
//
C1_ijv=[1,5,1
2,4,1];
C1=sparse(C1_ijv(:,1:2),C1_ijv(:,3),[3,5]);
C2_ijv=[3,2,1];
C2=sparse(C2_ijv(:,1:2),C2_ijv(:,3),[3,2]);
//
// Matrice d'observation (flux)
//
//
// Matrice d'observation (flux)
//
E_ijv=[];
E=sparse(E_ijv(:,1:2),E_ijv(:,3),[0,6]);
//
// Matrices d'entr�e
//
//
// Matrices d'entr�e
//
D1_ijv=[1,1,1
1,3,1
2,2,1
2,3,1];
D1=sparse(D1_ijv(:,1:2),D1_ijv(:,3),[2,3]);
D2_ijv=[1,3,1];
D2=sparse(D2_ijv(:,1:2),D2_ijv(:,3),[1,3]);
flux_names=["v1";"v2";"v3";"v4";"v5";"Entree"];
flux_ids=["v1";"v2";"v3";"v4";"v5";"v6"];
