<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer les �quations du syst�me
    sous forme de texte lisible par un humain. La sortie n'est pas destin�e
    � un logiciel particulier, mais on pourra s'inspirer du code ci-dessous
    pour �crire une feuille de style destin�e par exemple � Maple ou autre.
-->


<xsl:stylesheet   version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"  
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml">

    <xsl:param name="weight">0</xsl:param>

    <xsl:output method="text"  encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="carbon-labeling-system">
     <xsl:apply-templates select="//equation">
        <xsl:sort select="@weight"/>
      </xsl:apply-templates>
    </xsl:template>    
    
    
    <xsl:template match="equation/m:apply">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>=</xsl:text>
        <xsl:apply-templates select="*[3]"/>   
        <xsl:text>&#xA;</xsl:text>     
    </xsl:template>
    
    <xsl:template match="m:apply[m:plus]">
        <xsl:if test="count(*)&gt;2">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>+</xsl:text>
            </xsl:if>
        </xsl:for-each> 
        <xsl:if test="count(*)&gt;2">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="m:apply[m:minus]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>
   
   <xsl:template match="m:apply[m:times]">
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>*</xsl:text>
            </xsl:if>
        </xsl:for-each> 
    </xsl:template>
    

    <xsl:template match="m:apply[m:diff]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>'</xsl:text>
    </xsl:template>

    <xsl:template match="m:ci">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="m:cn">
        <xsl:value-of select="."/>
    </xsl:template>


</xsl:stylesheet>
