<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"    version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml">
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:key name="dynamicSpecies" match="sbml:species[(@constant='false') or not(@constant)]" use="@id"/>
    <xsl:key name="boundarySpecies" match="sbml:species[@boundaryCondition='true']" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    
    
    <xsl:template match="sbml:reaction">
        <sbml:reaction>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </sbml:reaction>
    </xsl:template>
    
    <xsl:template match="sbml:listOfReactants">
        <sbml:listOfReactants>
            <xsl:variable name="CR" select="str:split(substring-before(../sbml:notes/xhtml:html/xhtml:body,'>'),'+')"/>
            <xsl:for-each select="sbml:speciesReference">
                <xsl:variable name="i" select="1+sum(preceding-sibling::sbml:speciesReference/@stoichiometry)"/>
                <xsl:choose>
                    <xsl:when test="number(@stoichiometry)=1">
                        <sbml:speciesReference species="{@species}" stoichiometry="{@stoichiometry}" carbons="{normalize-space(exslt:node-set($CR)[$i])}"/>
                    </xsl:when>
                    <xsl:when test="number(@stoichiometry)=2">
                        <sbml:speciesReference species="{@species}" stoichiometry="{@stoichiometry}" carbons="{normalize-space(exslt:node-set($CR)[$i])},{normalize-space(exslt:node-set($CR)[($i)+1])}"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </sbml:listOfReactants>
    </xsl:template>

    <xsl:template match="sbml:listOfProducts">
        <sbml:listOfProducts>
            <xsl:variable name="CR" select="str:split(substring-after(../sbml:notes/xhtml:html/xhtml:body,'>'),'+')"/>
            <xsl:for-each select="sbml:speciesReference">
                <xsl:variable name="i" select="1+sum(preceding-sibling::sbml:speciesReference/@stoichiometry)"/>
                <xsl:choose>
                    <xsl:when test="@stoichiometry=1">
                        <sbml:speciesReference species="{@species}" stoichiometry="{@stoichiometry}" carbons="{normalize-space(exslt:node-set($CR)[$i])}"/>
                    </xsl:when>
                    <xsl:when test="number(@stoichiometry)=2">
                        <sbml:speciesReference species="{@species}" stoichiometry="{@stoichiometry}" carbons="{normalize-space(exslt:node-set($CR)[$i])},{normalize-space(exslt:node-set($CR)[($i)+1])}"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </sbml:listOfProducts>
    </xsl:template>

   <xsl:template match="sbml:annotation"/>
   <xsl:template match="sbml:notes"/>



</xsl:stylesheet>
