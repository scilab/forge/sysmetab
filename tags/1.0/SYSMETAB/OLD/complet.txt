re1 : 
re2 : 
re3 : Hexokinase
re4 : 
re5 : 
re6 : Gluconate Kinase
re7 : Glucose-6-phosphate Déshydrogénase
re8 : Glucose déshydrogénase
re9_f : Phosphoglucose Isomérase (forward)
re9_b : Phosphoglucose Isomérase (backward)
re10 : PhosphoGlucose Mutase
re11 : 
re12 : 
re13_f : Ribulose-5-phosphate épimérase (forward)
re13_b : Ribulose-5-phosphate épimérase (backward)
re14_f : Ribulose-5-phosphate isomérase (forward)
re14_b : Ribulose-5-phosphate isomérase (backward)
re15_f : Transcétolase 1 (forward)
re15_b : Transcétolase 1 (backward)
re16 : 6-phosphogluconate Déshydrogénase
re17_f : Transcétolase 2 (forward)
re17_b : Transcétolase 2 (backward)
re18 : Phosphofructo Kinase
re19 : Fructose 1,6 BisPhosphatase
re20_f : Fructose 1,6 BiPhosphate Aldolase (forward)
re20_b : Fructose 1,6 BiPhosphate Aldolase (backward)
re21_f : Triose Phosphate Isomérase (forward)
re21_b : Triose Phosphate Isomérase (backward)
re22_f : Glycéraldéhyde-3-phosphate déshydrogénase (forward)
re22_b : Glycéraldéhyde-3-phosphate déshydrogénase (backward)
re23_f : Phosphoglycérate Kinase (forward)
re23_b : Phosphoglycérate Kinase (backward)
re24_f : Phosphoglycérate Mutase (forward)
re24_b : Phosphoglycérate Mutase (backward)
re25_f : Enolase (forward)
re25_b : Enolase (backward)
re26 : Pyruvate Kinase
re28 : 6-phosphogluconate Déshydratase
re29_f : 2-céto-3-désoxy-6-phosphogluconate Aldolase (forward)
re29_b : 2-céto-3-désoxy-6-phosphogluconate Aldolase (backward)
re30 : Pyruvate Déshydrogénase
re31 : PHB synthase
re32_f : Aconitase (forward)
re32_b : Aconitase (backward)
re33_f : Isocitrate Déshydrogénase (forward)
re33_b : Isocitrate Déshydrogénase (backward)
re34 : a-cétoglutarate déshydrogénase
re35_f : Succinyl-CoA Synthétase (forward)
re35_b : Succinyl-CoA Synthétase (backward)
re36_f : Succinate déshydrogénase (forward)
re36_b : Succinate déshydrogénase (backward)
re37_f : Fumarase (forward)
re37_b : Fumarase (backward)
re38_f : Malate déshydrogénase (forward)
re38_b : Malate déshydrogénase (backward)
re39 : Citrate Synthase
re40_f : Pyruvate Carboxylase (forward)
re40_b : Pyruvate Carboxylase (backward)
re41_f : Enzyme Malique-NAD+ dépendante (forward)
re41_b : Enzyme Malique-NAD+ dépendante (backward)
re42_f : Enzyme Malique-NADP+ dépendante (forward)
re42_b : Enzyme Malique-NADP+ dépendante (backward)
re44 : 
re45 : 
re46 : 
re47 : 
re48 : 
re49 : 
re50 : 
re51 : 
re52 : 
re53 : 
re54 : 
re55_f : Transaldolase (forward)
re55_b : Transaldolase (backward)

s1 : Glucose (6 carbones) : métabolite entrant

s2 : Glucose6P (6 carbones)
0=-re7-re9_f-re10-re45+re3+re9_b
d(s2_1)/dt=-re7*s2_1-re9_f*s2_1-re10*s2_1-re45*s2_1+re3*s77_1+re9_b*s5_1
d(s2_2)/dt=-re7*s2_2-re9_f*s2_2-re10*s2_2-re45*s2_2+re3*s77_2+re9_b*s5_2
d(s2_3)/dt=-re7*s2_3-re9_f*s2_3-re10*s2_3-re45*s2_3+re3*s77_3+re9_b*s5_3
d(s2_4)/dt=-re7*s2_4-re9_f*s2_4-re10*s2_4-re45*s2_4+re3*s77_4+re9_b*s5_4
d(s2_5)/dt=-re7*s2_5-re9_f*s2_5-re10*s2_5-re45*s2_5+re3*s77_5+re9_b*s5_5
d(s2_6)/dt=-re7*s2_6-re9_f*s2_6-re10*s2_6-re45*s2_6+re3*s77_6+re9_b*s5_6

s3 : Gluconate (0 carbone) : métabolite sortant

s4 : 6PGluconate (6 carbones)
0=-re16-re28+re6+re7
d(s4_1)/dt=-re16*s4_1-re28*s4_1+re6*s78_1+re7*s2_1
d(s4_2)/dt=-re16*s4_2-re28*s4_2+re6*s78_2+re7*s2_2
d(s4_3)/dt=-re16*s4_3-re28*s4_3+re6*s78_3+re7*s2_3
d(s4_4)/dt=-re16*s4_4-re28*s4_4+re6*s78_4+re7*s2_4
d(s4_5)/dt=-re16*s4_5-re28*s4_5+re6*s78_5+re7*s2_5
d(s4_6)/dt=-re16*s4_6-re28*s4_6+re6*s78_6+re7*s2_6

s5 : Fructose6P (6 carbones)
0=-re9_b-re17_b-re18-re44-re55_b+re9_f+re17_f+re19+re55_f
d(s5_1)/dt=-re9_b*s5_1-re17_b*s5_1-re18*s5_1-re44*s5_1-re55_b*s5_1+re9_f*s2_1+re17_f*s15_1+re17_f+re19*s28_1+re55_f+re55_f*s19_1
d(s5_2)/dt=-re9_b*s5_2-re17_b*s5_2-re18*s5_2-re44*s5_2-re55_b*s5_2+re9_f*s2_2+re17_f*s15_2+re17_f+re19*s28_2+re55_f+re55_f*s19_2
d(s5_3)/dt=-re9_b*s5_3-re17_b*s5_3-re18*s5_3-re44*s5_3-re55_b*s5_3+re9_f*s2_3+re17_f+re17_f*s25_1+re19*s28_3+re55_f+re55_f*s19_3
d(s5_4)/dt=-re9_b*s5_4-re17_b*s5_4-re18*s5_4-re44*s5_4-re55_b*s5_4+re9_f*s2_4+re17_f+re17_f*s25_2+re19*s28_4+re55_f*s21_1+re55_f
d(s5_5)/dt=-re9_b*s5_5-re17_b*s5_5-re18*s5_5-re44*s5_5-re55_b*s5_5+re9_f*s2_5+re17_f+re17_f*s25_3+re19*s28_5+re55_f*s21_2+re55_f
d(s5_6)/dt=-re9_b*s5_6-re17_b*s5_6-re18*s5_6-re44*s5_6-re55_b*s5_6+re9_f*s2_6+re17_f+re17_f*s25_4+re19*s28_6+re55_f*s21_3+re55_f

s11 : Glucose1P (6 carbones)
0=-re11-re12+re10
d(s11_1)/dt=-re11*s11_1-re12*s11_1+re10*s2_1
d(s11_2)/dt=-re11*s11_2-re12*s11_2+re10*s2_2
d(s11_3)/dt=-re11*s11_3-re12*s11_3+re10*s2_3
d(s11_4)/dt=-re11*s11_4-re12*s11_4+re10*s2_4
d(s11_5)/dt=-re11*s11_5-re12*s11_5+re10*s2_5
d(s11_6)/dt=-re11*s11_6-re12*s11_6+re10*s2_6

s13 : b-glucanes (0 carbone) : métabolite sortant

s20 : EPS (0 carbone) : métabolite sortant

s14 : Ribulose5P (5 carbones)
0=-re13_f-re14_f+re13_b+re14_b+re16
d(s14_1)/dt=-re13_f*s14_1-re14_f*s14_1+re13_b*s15_1+re14_b*s16_1+re16*s4_2
d(s14_2)/dt=-re13_f*s14_2-re14_f*s14_2+re13_b*s15_2+re14_b*s16_2+re16*s4_3
d(s14_3)/dt=-re13_f*s14_3-re14_f*s14_3+re13_b*s15_3+re14_b*s16_3+re16*s4_4
d(s14_4)/dt=-re13_f*s14_4-re14_f*s14_4+re13_b*s15_4+re14_b*s16_4+re16*s4_5
d(s14_5)/dt=-re13_f*s14_5-re14_f*s14_5+re13_b*s15_5+re14_b*s16_5+re16*s4_6

s15 : Xylulose5P (5 carbones)
0=-re13_b-re15_f-re17_f+re13_f+re15_b+re17_b
d(s15_1)/dt=-re13_b*s15_1-re15_f*s15_1-re17_f*s15_1+re13_f*s14_1+re15_b*s19_1+re15_b+re17_b*s5_1+re17_b
d(s15_2)/dt=-re13_b*s15_2-re15_f*s15_2-re17_f*s15_2+re13_f*s14_2+re15_b*s19_2+re15_b+re17_b*s5_2+re17_b
d(s15_3)/dt=-re13_b*s15_3-re15_f*s15_3-re17_f*s15_3+re13_f*s14_3+re15_b+re15_b*s21_1+re17_b+re17_b*s21_1
d(s15_4)/dt=-re13_b*s15_4-re15_f*s15_4-re17_f*s15_4+re13_f*s14_4+re15_b+re15_b*s21_2+re17_b+re17_b*s21_2
d(s15_5)/dt=-re13_b*s15_5-re15_f*s15_5-re17_f*s15_5+re13_f*s14_5+re15_b+re15_b*s21_3+re17_b+re17_b*s21_3

s16 : Ribose5P (5 carbones)
0=-re14_b-re15_f-re46+re14_f+re15_b
d(s16_1)/dt=-re14_b*s16_1-re15_f*s16_1-re46*s16_1+re14_f*s14_1+re15_b*s19_3+re15_b
d(s16_2)/dt=-re14_b*s16_2-re15_f*s16_2-re46*s16_2+re14_f*s14_2+re15_b*s19_4+re15_b
d(s16_3)/dt=-re14_b*s16_3-re15_f*s16_3-re46*s16_3+re14_f*s14_3+re15_b*s19_5+re15_b
d(s16_4)/dt=-re14_b*s16_4-re15_f*s16_4-re46*s16_4+re14_f*s14_4+re15_b*s19_6+re15_b
d(s16_5)/dt=-re14_b*s16_5-re15_f*s16_5-re46*s16_5+re14_f*s14_5+re15_b*s19_7+re15_b

s19 : Sedohept7P (7 carbones)
0=-re15_b-re55_f+re15_f+re55_b
d(s19_1)/dt=-re15_b*s19_1-re55_f*s19_1+re15_f*s15_1+re15_f+re55_b+re55_b*s5_1
d(s19_2)/dt=-re15_b*s19_2-re55_f*s19_2+re15_f*s15_2+re15_f+re55_b+re55_b*s5_2
d(s19_3)/dt=-re15_b*s19_3-re55_f*s19_3+re15_f+re15_f*s16_1+re55_b+re55_b*s5_3
d(s19_4)/dt=-re15_b*s19_4-re55_f*s19_4+re15_f+re15_f*s16_2+re55_b*s25_1+re55_b
d(s19_5)/dt=-re15_b*s19_5-re55_f*s19_5+re15_f+re15_f*s16_3+re55_b*s25_2+re55_b
d(s19_6)/dt=-re15_b*s19_6-re55_f*s19_6+re15_f+re15_f*s16_4+re55_b*s25_3+re55_b
d(s19_7)/dt=-re15_b*s19_7-re55_f*s19_7+re15_f+re15_f*s16_5+re55_b*s25_4+re55_b

s21 : GA3P (3 carbones)
0=-re15_b-re17_b-re20_b-re21_b-re22_f-re29_b-re48-re55_f+re15_f+re17_f+re20_f+re21_f+re22_b+re29_f+re55_b
d(s21_1)/dt=-re15_b*s21_1-re17_b*s21_1-re20_b*s21_1-re21_b*s21_1-re22_f*s21_1-re29_b*s21_1-re48*s21_1-re55_f*s21_1+re15_f*s15_3+re15_f+re17_f*s15_3+re17_f+re20_f*s28_4+re21_f*s31_3+re22_b*s34_1+re29_f*s46_4+re55_b+re55_b*s5_4
d(s21_2)/dt=-re15_b*s21_2-re17_b*s21_2-re20_b*s21_2-re21_b*s21_2-re22_f*s21_2-re29_b*s21_2-re48*s21_2-re55_f*s21_2+re15_f*s15_4+re15_f+re17_f*s15_4+re17_f+re20_f*s28_5+re21_f*s31_2+re22_b*s34_2+re29_f*s46_5+re55_b+re55_b*s5_5
d(s21_3)/dt=-re15_b*s21_3-re17_b*s21_3-re20_b*s21_3-re21_b*s21_3-re22_f*s21_3-re29_b*s21_3-re48*s21_3-re55_f*s21_3+re15_f*s15_5+re15_f+re17_f*s15_5+re17_f+re20_f*s28_6+re21_f*s31_1+re22_b*s34_3+re29_f*s46_6+re55_b+re55_b*s5_6

s25 : Erythrose4P (4 carbones)
0=-re17_f-re47-re55_b+re17_b+re55_f
d(s25_1)/dt=-re17_f*s25_1-re47*s25_1-re55_b*s25_1+re17_b*s5_3+re17_b+re55_f+re55_f*s19_4
d(s25_2)/dt=-re17_f*s25_2-re47*s25_2-re55_b*s25_2+re17_b*s5_4+re17_b+re55_f+re55_f*s19_5
d(s25_3)/dt=-re17_f*s25_3-re47*s25_3-re55_b*s25_3+re17_b*s5_5+re17_b+re55_f+re55_f*s19_6
d(s25_4)/dt=-re17_f*s25_4-re47*s25_4-re55_b*s25_4+re17_b*s5_6+re17_b+re55_f+re55_f*s19_7

s28 : Fruc1,6biP (6 carbones)
0=-re19-re20_f+re18+re20_b
d(s28_1)/dt=-re19*s28_1-re20_f*s28_1+re18*s5_1+re20_b*s31_1+re20_b
d(s28_2)/dt=-re19*s28_2-re20_f*s28_2+re18*s5_2+re20_b*s31_2+re20_b
d(s28_3)/dt=-re19*s28_3-re20_f*s28_3+re18*s5_3+re20_b*s31_3+re20_b
d(s28_4)/dt=-re19*s28_4-re20_f*s28_4+re18*s5_4+re20_b+re20_b*s21_1
d(s28_5)/dt=-re19*s28_5-re20_f*s28_5+re18*s5_5+re20_b+re20_b*s21_2
d(s28_6)/dt=-re19*s28_6-re20_f*s28_6+re18*s5_6+re20_b+re20_b*s21_3

s31 : DHAP (3 carbones)
0=-re20_b-re21_f+re20_f+re21_b
d(s31_1)/dt=-re20_b*s31_1-re21_f*s31_1+re20_f*s28_1+re21_b*s21_3
d(s31_2)/dt=-re20_b*s31_2-re21_f*s31_2+re20_f*s28_2+re21_b*s21_2
d(s31_3)/dt=-re20_b*s31_3-re21_f*s31_3+re20_f*s28_3+re21_b*s21_1

s34 : 1,3biPglycérate (3 carbones)
0=-re22_b-re23_f+re22_f+re23_b
d(s34_1)/dt=-re22_b*s34_1-re23_f*s34_1+re22_f*s21_1+re23_b*s35_1
d(s34_2)/dt=-re22_b*s34_2-re23_f*s34_2+re22_f*s21_2+re23_b*s35_2
d(s34_3)/dt=-re22_b*s34_3-re23_f*s34_3+re22_f*s21_3+re23_b*s35_3

s35 : 3Pglycérate (3 carbones)
0=-re23_b-re24_f-re49+re23_f+re24_b
d(s35_1)/dt=-re23_b*s35_1-re24_f*s35_1-re49*s35_1+re23_f*s34_1+re24_b*s36_1
d(s35_2)/dt=-re23_b*s35_2-re24_f*s35_2-re49*s35_2+re23_f*s34_2+re24_b*s36_2
d(s35_3)/dt=-re23_b*s35_3-re24_f*s35_3-re49*s35_3+re23_f*s34_3+re24_b*s36_3

s36 : 2Pglycérate (3 carbones)
0=-re24_b-re25_f+re24_f+re25_b
d(s36_1)/dt=-re24_b*s36_1-re25_f*s36_1+re24_f*s35_1+re25_b*s37_1
d(s36_2)/dt=-re24_b*s36_2-re25_f*s36_2+re24_f*s35_2+re25_b*s37_2
d(s36_3)/dt=-re24_b*s36_3-re25_f*s36_3+re24_f*s35_3+re25_b*s37_3

s37 : PEP (3 carbones)
0=-re25_b-re26-re50+re25_f
d(s37_1)/dt=-re25_b*s37_1-re26*s37_1-re50*s37_1+re25_f*s36_1
d(s37_2)/dt=-re25_b*s37_2-re26*s37_2-re50*s37_2+re25_f*s36_2
d(s37_3)/dt=-re25_b*s37_3-re26*s37_3-re50*s37_3+re25_f*s36_3

s38 : Pyruvate (3 carbones)
0=-re29_b-re30-re40_f-re41_b-re42_b-re51+re26+re29_f+re40_b+re41_f+re42_f
d(s38_1)/dt=-re29_b*s38_1-re30*s38_1-re40_f*s38_1-re41_b*s38_1-re42_b*s38_1-re51*s38_1+re26*s37_1+re29_f*s46_1+re40_b*s57_1+re41_f*s56_1+re42_f*s56_1
d(s38_2)/dt=-re29_b*s38_2-re30*s38_2-re40_f*s38_2-re41_b*s38_2-re42_b*s38_2-re51*s38_2+re26*s37_2+re29_f*s46_2+re40_b*s57_2+re41_f*s56_2+re42_f*s56_2
d(s38_3)/dt=-re29_b*s38_3-re30*s38_3-re40_f*s38_3-re41_b*s38_3-re42_b*s38_3-re51*s38_3+re26*s37_3+re29_f*s46_3+re40_b*s57_3+re41_f*s56_3+re42_f*s56_3

s46 : KDPG (6 carbones)
0=-re29_f+re28+re29_b
d(s46_1)/dt=-re29_f*s46_1+re28*s4_1+re29_b+re29_b*s38_1
d(s46_2)/dt=-re29_f*s46_2+re28*s4_2+re29_b+re29_b*s38_2
d(s46_3)/dt=-re29_f*s46_3+re28*s4_3+re29_b+re29_b*s38_3
d(s46_4)/dt=-re29_f*s46_4+re28*s4_4+re29_b*s21_1+re29_b
d(s46_5)/dt=-re29_f*s46_5+re28*s4_5+re29_b*s21_2+re29_b
d(s46_6)/dt=-re29_f*s46_6+re28*s4_6+re29_b*s21_3+re29_b

s47 : Acétyl-CoA (2 carbones)
0=-0.5*re31-re39-re52+re30
d(s47_1)/dt=-re31*(s47_1)^2-re39*s47_1-re52*s47_1+re30*s38_2
d(s47_2)/dt=-re31*(s47_2)^2-re39*s47_2-re52*s47_2+re30*s38_3

s48 : PHB (4 carbones) : métabolite sortant

s50 : Citrate (6 carbones)
0=-re32_f+re32_b+re39
d(s50_1)/dt=-re32_f*s50_1+re32_b*s51_1+re39*s57_4+re39
d(s50_2)/dt=-re32_f*s50_2+re32_b*s51_2+re39*s57_3+re39
d(s50_3)/dt=-re32_f*s50_3+re32_b*s51_3+re39*s57_2+re39
d(s50_4)/dt=-re32_f*s50_4+re32_b*s51_4+re39*s57_1+re39
d(s50_5)/dt=-re32_f*s50_5+re32_b*s51_5+re39+re39*s47_2
d(s50_6)/dt=-re32_f*s50_6+re32_b*s51_6+re39+re39*s47_1

s51 : Isocitrate (6 carbones)
0=-re32_b-re33_f+re32_f+re33_b
d(s51_1)/dt=-re32_b*s51_1-re33_f*s51_1+re32_f*s50_1+re33_b*s52_1+re33_b
d(s51_2)/dt=-re32_b*s51_2-re33_f*s51_2+re32_f*s50_2+re33_b*s52_2+re33_b
d(s51_3)/dt=-re32_b*s51_3-re33_f*s51_3+re32_f*s50_3+re33_b*s52_3+re33_b
d(s51_4)/dt=-re32_b*s51_4-re33_f*s51_4+re32_f*s50_4+re33_b+re33_b*s79_1
d(s51_5)/dt=-re32_b*s51_5-re33_f*s51_5+re32_f*s50_5+re33_b*s52_4+re33_b
d(s51_6)/dt=-re32_b*s51_6-re33_f*s51_6+re32_f*s50_6+re33_b*s52_5+re33_b

s52 : aKG (5 carbones)
0=-re33_b-re34-re54+re33_f
d(s52_1)/dt=-re33_b*s52_1-re34*s52_1-re54*s52_1+re33_f*s51_1
d(s52_2)/dt=-re33_b*s52_2-re34*s52_2-re54*s52_2+re33_f*s51_2
d(s52_3)/dt=-re33_b*s52_3-re34*s52_3-re54*s52_3+re33_f*s51_3
d(s52_4)/dt=-re33_b*s52_4-re34*s52_4-re54*s52_4+re33_f*s51_5
d(s52_5)/dt=-re33_b*s52_5-re34*s52_5-re54*s52_5+re33_f*s51_6

s53 : Succinyl-CoA (4 carbones)
0=-re35_f+re34+re35_b
d(s53_1)/dt=-re35_f*s53_1+re34*s52_2+re35_b*s54_1
d(s53_2)/dt=-re35_f*s53_2+re34*s52_3+re35_b*s54_2
d(s53_3)/dt=-re35_f*s53_3+re34*s52_4+re35_b*s54_3
d(s53_4)/dt=-re35_f*s53_4+re34*s52_5+re35_b*s54_4

s54 : Succinate (4 carbones)
0=-re35_b-re36_f+re35_f+re36_b
d(s54_1)/dt=-re35_b*s54_1-re36_f*s54_1+re35_f*s53_1+re36_b*s55_1
d(s54_2)/dt=-re35_b*s54_2-re36_f*s54_2+re35_f*s53_2+re36_b*s55_2
d(s54_3)/dt=-re35_b*s54_3-re36_f*s54_3+re35_f*s53_3+re36_b*s55_3
d(s54_4)/dt=-re35_b*s54_4-re36_f*s54_4+re35_f*s53_4+re36_b*s55_4

s55 : Fumarate (4 carbones)
0=-re36_b-re37_f+re36_f+re37_b
d(s55_1)/dt=-re36_b*s55_1-re37_f*s55_1+re36_f*s54_1+re37_b*s56_1
d(s55_2)/dt=-re36_b*s55_2-re37_f*s55_2+re36_f*s54_2+re37_b*s56_2
d(s55_3)/dt=-re36_b*s55_3-re37_f*s55_3+re36_f*s54_3+re37_b*s56_3
d(s55_4)/dt=-re36_b*s55_4-re37_f*s55_4+re36_f*s54_4+re37_b*s56_4

s56 : Malate (4 carbones)
0=-re37_b-re38_f-re41_f-re42_f+re37_f+re38_b+re41_b+re42_b
d(s56_1)/dt=-re37_b*s56_1-re38_f*s56_1-re41_f*s56_1-re42_f*s56_1+re37_f*s55_1+re38_b*s57_1+re41_b*s38_1+re41_b+re42_b*s38_1+re42_b
d(s56_2)/dt=-re37_b*s56_2-re38_f*s56_2-re41_f*s56_2-re42_f*s56_2+re37_f*s55_2+re38_b*s57_2+re41_b*s38_2+re41_b+re42_b*s38_2+re42_b
d(s56_3)/dt=-re37_b*s56_3-re38_f*s56_3-re41_f*s56_3-re42_f*s56_3+re37_f*s55_3+re38_b*s57_3+re41_b*s38_3+re41_b+re42_b*s38_3+re42_b
d(s56_4)/dt=-re37_b*s56_4-re38_f*s56_4-re41_f*s56_4-re42_f*s56_4+re37_f*s55_4+re38_b*s57_4+re41_b+re41_b*s79_1+re42_b+re42_b*s79_1

s57 : OAA (4 carbones)
0=-re38_b-re39-re40_b-re53+re38_f+re40_f
d(s57_1)/dt=-re38_b*s57_1-re39*s57_1-re40_b*s57_1-re53*s57_1+re38_f*s56_1+re40_f*s38_1+re40_f
d(s57_2)/dt=-re38_b*s57_2-re39*s57_2-re40_b*s57_2-re53*s57_2+re38_f*s56_2+re40_f*s38_2+re40_f
d(s57_3)/dt=-re38_b*s57_3-re39*s57_3-re40_b*s57_3-re53*s57_3+re38_f*s56_3+re40_f*s38_3+re40_f
d(s57_4)/dt=-re38_b*s57_4-re39*s57_4-re40_b*s57_4-re53*s57_4+re38_f*s56_4+re40_f+re40_f*s79_1

s72 : Glucose (6 carbones)
0=-re2-re8+re1
d(s72_1)/dt=-re2*s72_1-re8*s72_1+V_s1_1
d(s72_2)/dt=-re2*s72_2-re8*s72_2+V_s1_2
d(s72_3)/dt=-re2*s72_3-re8*s72_3+V_s1_3
d(s72_4)/dt=-re2*s72_4-re8*s72_4+V_s1_4
d(s72_5)/dt=-re2*s72_5-re8*s72_5+V_s1_5
d(s72_6)/dt=-re2*s72_6-re8*s72_6+V_s1_6

s73 : Gluconate (6 carbones)
0=-re4-re5+re8
d(s73_1)/dt=-re4*s73_1-re5*s73_1+re8*s72_1
d(s73_2)/dt=-re4*s73_2-re5*s73_2+re8*s72_2
d(s73_3)/dt=-re4*s73_3-re5*s73_3+re8*s72_3
d(s73_4)/dt=-re4*s73_4-re5*s73_4+re8*s72_4
d(s73_5)/dt=-re4*s73_5-re5*s73_5+re8*s72_5
d(s73_6)/dt=-re4*s73_6-re5*s73_6+re8*s72_6

s77 : Glucose (6 carbones)
0=-re3+re2
d(s77_1)/dt=-re3*s77_1+re2*s72_1
d(s77_2)/dt=-re3*s77_2+re2*s72_2
d(s77_3)/dt=-re3*s77_3+re2*s72_3
d(s77_4)/dt=-re3*s77_4+re2*s72_4
d(s77_5)/dt=-re3*s77_5+re2*s72_5
d(s77_6)/dt=-re3*s77_6+re2*s72_6

s78 : Gluconate (6 carbones)
0=-re6+re4
d(s78_1)/dt=-re6*s78_1+re4*s73_1
d(s78_2)/dt=-re6*s78_2+re4*s73_2
d(s78_3)/dt=-re6*s78_3+re4*s73_3
d(s78_4)/dt=-re6*s78_4+re4*s73_4
d(s78_5)/dt=-re6*s78_5+re4*s73_5
d(s78_6)/dt=-re6*s78_6+re4*s73_6

s79 : CO2 (1 carbone)
0=-re33_b-re40_f-re41_b-re42_b+re16+re30+re33_f+re34+re40_b+re41_f+re42_f
d(s79_1)/dt=-re33_b*s79_1-re40_f*s79_1-re41_b*s79_1-re42_b*s79_1+re16*s4_1+re30*s38_1+re33_f*s51_4+re34*s52_1+re40_b*s57_4+re41_f*s56_4+re42_f*s56_4

s83 : Biomasse (0 carbone) : métabolite sortant
