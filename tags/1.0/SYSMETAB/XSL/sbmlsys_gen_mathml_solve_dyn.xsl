<?xml version="1.0" encoding="ISO-8859-1" ?> 

<!--
Auteur: Florent Tixier
Date: lun 17 aout 11:20 CEST 2008
Projet: SYSMETAB/Carnot
-->

<!--
Cette feuille de style a pour but de g�n�rer diff�rentes fonctions sp�cifiques
d'un mod�le donn� sous forme SBML, �dit� dans CellDesigner pour le cas non-sationnaire.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sbml="http://www.sbml.org/sbml/level2" xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:exslt="http://exslt.org/common" xmlns:str="http://exslt.org/strings" xmlns:math="http://exslt.org/math" version="1.0" xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner" xmlns:smtb="http://www.utc.fr/sysmetab" exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
  <xsl:include href="common_gen_mathml.xsl"/>
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1" /> 
  <xsl:param name="file" /> 
  <xsl:param name="verbose">no</xsl:param> 
  <xsl:param name="type">dynamique</xsl:param> 
  <xsl:param name="experiences">2</xsl:param>
  <xsl:param name="nb_t_obs">0</xsl:param>
  
  <xsl:key name="reaction" match="sbml:reaction" use="@id" /> 
  <xsl:key name="species" match="sbml:species" use="@id" /> 
  <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id" /> 
  <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id" /> 
  <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species" /> 
  <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species" /> 
  <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species" /> 
  <xsl:strip-space elements="*" /> 
 
  <!-- D�termination du poids maximum n�cessaire : on regarde le poids le plus grand dans les cumom�res contribuant aux observations --> 
  <xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)" /> 
  
  <xsl:template match="/">
        <carbon-labeling-system>
          <xsl:apply-templates/>
        </carbon-labeling-system>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    
    <xsl:template match="sbml:model">
    	<m:apply>
	<m:ci>stacksize(1e8);</m:ci>
	</m:apply>
    	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		
		<!-- Fonctions de calcul des bi et Mi -->
    		<xsl:variable name="current_weight" select="@weight"/>
		<smtb:comment><xsl:value-of select="concat('Fonction de calcul des matrices M',@weight,' et b',@weight,'.')"/></smtb:comment>
    		<smtb:function>
			<m:ci><xsl:value-of select="concat('init_p',@weight)"/></m:ci>
			<smtb:input>
				<m:list>
				    <m:ci> <xsl:value-of select="concat('x',@weight,'_input')"/></m:ci>
					<xsl:for-each select="preceding-sibling::*">
    				    <m:ci><xsl:value-of select="concat('x',@weight)"/></m:ci>
    				   </xsl:for-each>
				<m:ci>v</m:ci>
				</m:list>
			</smtb:input>
			<smtb:output>
                	<m:list>
				<xsl:call-template name="gen_names_dg_dv2"/>
        	        	<m:ci><xsl:value-of select="concat('M',@weight)"/> </m:ci> 
				<m:ci><xsl:value-of select="concat('x',@weight)"/> </m:ci>
				<m:ci>t</m:ci>      
	     	     	</m:list>
    			</smtb:output>
			<smtb:body>
				<m:apply>
					<m:eq/>
					<m:ci><xsl:value-of select="concat('n',@weight)"/></m:ci>
					<m:cn><xsl:value-of select="count(smtb:cumomer)"/></m:cn>
				</m:apply>
    				    <m:apply>
					<m:eq/>
					<m:list separator=",">
						<m:ci>d_t1</m:ci>
						<m:ci>d_t2</m:ci>
						<m:ci>d_t3</m:ci>
					</m:list>
						<m:cn><xsl:value-of select="concat('size(x',@weight,'_input)')"/></m:cn>
				      </m:apply>
				      <m:apply>
						<m:eq/>
						<m:ci><xsl:value-of select="concat('b',@weight)"/></m:ci>
						<m:cn><xsl:value-of select="concat('hypermat([n',@weight,',d_t2,d_t3])')"/></m:cn>
				      </m:apply>
				      <smtb:optimize>
						<smtb:matrix-open type="sparse" id="{concat('M',@weight)}" rows="{concat('n',@weight)}" cols="{concat('n',@weight)}"/>
						<smtb:hypermatrix-open type="s_full3" id="{concat('b',@weight)}" rows="{concat('n',@weight)}" cols="1" versions="{$experiences}" nb_step="d_t3"/>
						<smtb:hypermatrix-open type="hypersparse" id="{concat('dg',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../sbml:listOfReactions/sbml:reaction)}" exp="{$experiences}" nb_step="d_t3" transpose="1"/>
						<xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
			        			<smtb:hypermatrix-open type="hypersparse" id="{concat('db',$current_weight,'_dx',@weight)}" rows="{concat('n',$current_weight)}"  cols="{concat('size(x',@weight,',1)')}" exp="{$experiences}" nb_step="d_t3" transpose="1"/>
		       				</xsl:for-each>
						<xsl:for-each select="smtb:cumomer">
							<xsl:call-template name="iteration"/>                                   
            					</xsl:for-each>
						<smtb:matrix-close id="{concat('M',@weight)}"/>
						<smtb:hypermatrix-close id="{concat('b',@weight)}"/>
						<m:apply>
							<m:eq/>
							<m:list separator=",">
								<m:ci>t</m:ci>
								<m:ci><xsl:value-of select="concat('x',@weight)"/></m:ci>	
							</m:list>
							<m:cn><xsl:value-of select="concat('CN_HM(t0,t1,d_t3,M',@weight,',b',@weight,',K',@weight,')')"/></m:cn>
						</m:apply>
						<smtb:hypermatrix-close id="{concat('dg',@weight,'_dv')}"/>
						<xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
			        			<smtb:hypermatrix-close id="{concat('db',$current_weight,'_dx',@weight)}" />
		        			</xsl:for-each>
				       </smtb:optimize>			       		
				</smtb:body>
			</smtb:function>
			
			<!--calcul du membre de droite des �quations pour le calcul des adjoints-->
			<smtb:comment><xsl:value-of select="concat('Fonction de calcul du membre de droite des adjoints de poids ',@weight)"/></smtb:comment>
			<smtb:function>
				<m:ci><xsl:value-of select="concat('adj_init_p',@weight)"/></m:ci>
				<smtb:input>
					<m:list/>
				</smtb:input>
				<smtb:output>
                		<m:list>
					<m:ci>f</m:ci>
					<m:ci>o</m:ci>
	     	     		</m:list>
    				</smtb:output>
				<smtb:body>
					<xsl:choose>
						<xsl:when test="@weight=$weight">
							<m:apply>
							<m:eq/>
								<m:ci>f</m:ci>
								<m:cn><xsl:value-of select="concat('-Y',@weight,'(:,:,$)')"/></m:cn>
							</m:apply>
							<m:apply>
							<m:eq/>
								<m:ci>o</m:ci>
								<m:cn><xsl:value-of select="concat('hypermat(size(Y',@weight,'))-Y',@weight)"/></m:cn>
							</m:apply>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="adj_init"/>
						</xsl:otherwise>
					</xsl:choose>
				</smtb:body>
			</smtb:function>		
			
			</xsl:for-each>
			<!--fonction de calcul du gradient-->
			<smtb:comment>Calcul du gradient</smtb:comment>
			<smtb:function>
				<m:ci>gradient</m:ci>
				<smtb:input>
					<m:list>
					<m:ci>delta</m:ci>
					<m:ci>e_flux</m:ci>
					</m:list>
				</smtb:input>
				<smtb:output>
					<m:list><m:ci>grad</m:ci></m:list>
				</smtb:output>
				<smtb:body>
					<xsl:call-template name="gradient"/>
				</smtb:body>	
			</smtb:function>
			
			 <!-- Fonction de calcul de A et B pour les contraintes sur les fluxs Av=b (dans common_gen_mathml.xsl)-->
			<xsl:call-template name="fluxSubspace"/>
			<!-- Construction des matrices d'observation -->
			<xsl:call-template name="construct_matrix_C"/>	 
			<!--Construction de la matrice E"-->
			<xsl:call-template name="construct_matrix_E"/>
			<!-- Construction des matrices d'entr�e, permettant d'obtenir la contribution sur les cumom�res des isotopom�res en entr�e. -->	
			<xsl:call-template name="construct_matrix_D"/> 
			<!--Contruction des matrice de noms et ids des fluxs -->	
			<xsl:call-template name="names_and_ids_fluxes"/>
			<!--fonction pour effectuer le calcul des adjoints et du gradient-->
			<smtb:comment>Calcul des adjoints puis du gradient</smtb:comment>
			<smtb:function>
				<m:ci>calc_gradient</m:ci>
				<smtb:input>
					<m:list>
					<m:ci>delta</m:ci>
					<m:ci>e_flux</m:ci>
					</m:list>
				</smtb:input>
				<smtb:output>
					<m:list>
					<m:ci>grad</m:ci>
					</m:list>
				</smtb:output>
				<smtb:body>	
					<xsl:call-template name="adj_cascade"/>
					<m:apply>
						<m:eq/>
						<m:ci>grad</m:ci>
						<m:cn>gradient(delta,e_flux)</m:cn>
					</m:apply>
					<!--<xsl:text>disp("Gradient:");&#xA;</xsl:text>
					<xsl:text>disp(grad);&#xA;</xsl:text>-->
				</smtb:body>	
			</smtb:function>
			
			<smtb:comment>Initialisation de variables utiles pour la fonction costAndGrad</smtb:comment>
			<smtb:function>
				<m:ci>init_cost_var</m:ci>
				<smtb:input>
					<m:list>
						<m:ci>v</m:ci>
						<m:ci>vobs</m:ci>
						<xsl:call-template name="names_of_matrix_measurement"/>
					</m:list>
				</smtb:input>
				<smtb:output>
					<m:list>
					<m:ci>m_obs</m:ci>
					<m:ci>y_obs</m:ci>
					<m:ci>v_obs</m:ci>
					<m:ci>alpha</m:ci>
					<m:ci>delta</m:ci>
					<m:ci>vtm</m:ci>
					</m:list>	
				</smtb:output>
				<smtb:body>
					<m:apply>
						<m:eq/>
						<m:ci>m_obs</m:ci>
						<m:apply>
							<m:fn><m:ci>hypermat</m:ci></m:fn>
							<m:ci><xsl:value-of select="concat('[size(y,1),3*',$experiences,',',$nb_t_obs,']')"/></m:ci>
						</m:apply>
					</m:apply>
					<xsl:call-template name="collapse_measurement"/>
					<m:apply>
						<m:eq/>
						<m:ci>alpha</m:ci>
						<m:cn><xsl:value-of select="concat('m_obs(:,1:3:3*',$experiences,',:)')"/></m:cn>		
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:ci>y_obs</m:ci>
						<m:cn><xsl:value-of select="concat('m_obs(:,2:3:3*',$experiences,',:)')"/></m:cn>		
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:ci>v_obs</m:ci>
						<m:cn>vobs(:,2)</m:cn>		
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:ci>delta</m:ci>
						<m:cn>v_obs(:,1)</m:cn>		
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:ci>vtm</m:ci>
						<m:list separator=";">	
							<xsl:call-template name="vector_times_measurement"/>
						</m:list>
					</m:apply>
				</smtb:body>
			</smtb:function>
			<!-- Fonction qui calcul le gardient, le cout et l'erreur sur les marquages m�me liste d'entr�e et de sortie
			que dans le cas stationnaire pour pouvoir se servir de la fonction optimize sans aucune modification-->
			<smtb:comment>Fonction costAndGrad (meme I/O qu'en stationnaire pour compatibilite avec l'optimiseur)</smtb:comment>
			<smtb:function>
				<m:ci>costAndGrad</m:ci>
				<smtb:input>
					<m:list>
					<m:ci>v</m:ci>
					</m:list>
				</smtb:input>
				<smtb:output>
					<m:list>
					<m:ci>cost</m:ci>
					<m:ci>grad</m:ci>
					<m:ci>label_error</m:ci>
					</m:list>
				</smtb:output>
				<smtb:body>
					<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">	
						<m:apply>
							<m:eq/>
							<m:list separator=",">
								<xsl:call-template name="gen_names_dg_dv2"/>
								<m:ci><xsl:value-of select="concat('M',@weight)"/></m:ci>
								<m:ci><xsl:value-of select="concat('x',@weight)"/></m:ci>
								<m:ci>t</m:ci>
							</m:list>
							<m:cn>
								<xsl:value-of select="concat('init_p',@weight,'(x',@weight,'_input,')"/>
								<xsl:for-each select="preceding-sibling::*">
    									<xsl:value-of select="concat('x',@weight,',')"/>
    								</xsl:for-each>
								<xsl:text>v)</xsl:text>
							</m:cn>
						</m:apply>
					</xsl:for-each>
					<m:apply>
						<m:eq/>
						<m:ci>y</m:ci>
						<m:apply>
							<m:plus/>
							<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
								<m:apply>
									<m:times/>
									<m:ci><xsl:value-of select="concat('C',@weight)"/></m:ci>
									<m:ci><xsl:value-of select="concat('x',@weight,'(:,:)')"/></m:ci>
								</m:apply>
							</xsl:for-each>
						</m:apply>
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:ci>[d_c1,d_c2]</m:ci>
						<m:cn>size(C1)</m:cn>
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:ci>[d_hm1,d_hm2,d_hm3]</m:ci>
						<m:cn>size(x1)</m:cn>
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:ci>y</m:ci>
						<m:cn>matrix(y,[d_c1,d_hm2,d_hm3])</m:cn>
					</m:apply>
					<m:apply>
						<m:eq/>
						<m:apply>
						<m:list separator=",">
							<m:ci>m_obs</m:ci>
							<m:ci>y_obs</m:ci>
							<m:ci>v_obs</m:ci>
							<m:ci>alpha</m:ci>
							<m:ci>delta</m:ci>
							<m:ci>vtm</m:ci>
						</m:list>
						</m:apply>
						<m:apply>
							<m:fn><m:ci>init_cost_var</m:ci></m:fn>
							<m:ci>fluxes(:,1)</m:ci>
							<m:ci>flux_obs</m:ci>
							<xsl:call-template name="names_of_matrix_measurement"/>
						</m:apply>
					</m:apply>
					
				<m:apply>
					<m:eq/>
					<m:ci>h</m:ci>
					<m:cn>t(2)-t(1)</m:cn>
				</m:apply>
				
				
				<m:apply>
					<m:eq/>
					<m:ci>ident</m:ci>
					<m:cn>y(:,:,(vtm/h)+1)</m:cn>
				</m:apply>	
				<m:apply>
					<m:eq/>
					<m:ci>e_label</m:ci>
					<m:apply>
						<m:minus/>
						<m:ci>ident</m:ci>
						<m:ci>y_obs</m:ci>
					</m:apply>
				</m:apply>
				<m:apply>
					<m:eq/>
					<m:ci>e_flux</m:ci>
					<m:apply>
						<m:minus/>
						<m:apply>
							<m:times/>
							<m:ci>E</m:ci>
							<m:ci>v</m:ci>
						</m:apply>
						<m:ci>v_obs</m:ci>
					</m:apply>
				</m:apply>
				<m:apply>
			        <m:eq/>
			        <m:ci>flux_error</m:ci>
				    <m:apply>
					    <m:plus/>
					    <m:apply> <!-- contribution des observations des flux -->
						    <m:times/>
						    <m:apply>
							    <m:fn>
								    <m:ci>sum</m:ci>
							    </m:fn>
							    <m:apply>
								    <m:times type="array"/>
								    <m:ci>delta</m:ci>
								    <m:apply>
									    <m:power type="array"/>
									    <m:ci type="matrix">e_flux</m:ci>
									    <m:cn>2</m:cn>
								    </m:apply>
							    </m:apply>
						    </m:apply>
					    </m:apply>
					</m:apply>
				</m:apply>
				
				<m:apply>
			        	<m:eq/>
			        	<m:ci>label_error</m:ci>
					<m:apply>
						<m:fn><m:ci>sum</m:ci></m:fn>
						<m:apply>
							<m:times type="array"/>
							<m:ci>alpha</m:ci>
							<m:apply>
								<m:power type="array"/>
								<m:ci>e_label</m:ci>
								<m:cn>2</m:cn>
							</m:apply>
						</m:apply>
			        	</m:apply>
		        	</m:apply>
				
				<m:apply>
			        	<m:eq/>
			        	<m:ci>cost</m:ci>
			        	<m:apply>
				        	<m:times/>
				        	<m:cn>0.5</m:cn>
				        	<m:apply>
					        	<m:plus/>
							<m:ci>flux_error</m:ci>
							<m:ci>label_error</m:ci>
						</m:apply>
			        	</m:apply>
		        	</m:apply>
	
				<m:apply>
					<m:eq/>
					<m:ci>t_cost</m:ci>
					<m:apply>	
						<m:plus/>
						<m:ci>c_cost</m:ci>
						<m:apply>
							<m:times/>
							<m:ci>f_cost</m:ci>
							<m:apply>
								<m:fn><m:ci>ones</m:ci></m:fn>
								<m:ci><xsl:value-of select="$experiences"/></m:ci>
								<m:ci>1</m:ci>
							</m:apply>
						</m:apply>
					</m:apply>	
				</m:apply>	
				<m:apply>
					<m:eq/>
					<m:ci>grad</m:ci>
					<m:cn>calc_gradient()</m:cn>
				</m:apply>
				<!--<m:apply>
					<m:eq/>
					<m:ci>cost</m:ci>
					<m:cn>sum(t_cost,'r')</m:cn>
				</m:apply>-->
				</smtb:body>	
			</smtb:function>
			
			
		
			<!--Fonction pour sauvegarder les donn�es au format csv-->
			<smtb:comment>Sauvegarde des donn�es au format csv</smtb:comment>
			<smtb:function>
				<m:ci>Save</m:ci>
				<smtb:input><m:list/></smtb:input>
				<smtb:output><m:list/></smtb:output>
				<smtb:body>			
					<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
						<xsl:call-template name="loop-exp"/>
					</xsl:for-each>
					<xsl:variable name="type_float">'%1.7f, '</xsl:variable>
					<xsl:variable name="double_quote">"</xsl:variable>
					<m:apply>
					<m:ci>result=tk_savefile(file_mask="*.csv",title="Nom du fichier de r�sultats",path=oldpath);</m:ci>
					</m:apply>
					
					<m:apply>
						<m:eq/>
						<m:ci>desc</m:ci>
						<m:cn>mopen(result,"w")</m:cn>
					</m:apply>
					<xsl:call-template name="sauvegarde"/>
					<xsl:value-of select="concat('mfprintf(desc,',$double_quote,'\n\nMatrice t\n',$double_quote,');&#xA;')"/>
					<xsl:text>for i=1:d_t3&#xA;</xsl:text>
						<xsl:value-of select="concat('mfprintf(desc,',$type_float,',t(i));&#xA;')"/>
					<xsl:text>end;&#xA;</xsl:text>
					<xsl:text>mclose(desc);&#xA;</xsl:text>
				</smtb:body>
			</smtb:function>
		<!-- Script pour le calcul direct des cumom�res. Ce script est ex�cut� dans l'application XMLlab, d�s qu'un flux est chang� ou
	     	d�s que l'on sort de l'optimisation avec des nouvelles valeurs de fluxs. -->

		<smtb:script href="{$file}_direct.sce">
		 <!-- En fonction des valeurs des fluxs, les free fluxes peuvent changer, en g�n�ral c'est le cas quand
			     le statut de certaines contraintes change (satur�es ou inactives). La fonction adjustFluxesTab
			     (d�finie dans SYSMETAB/SCI/sysmetab4.sci) a pour but de rendre actives les cases du tableau des
			     fluxs correspondant � des free fluxes. On peut alors les faire varier avec <ctrl>+fl�che droite
			     ou gauche. -->
		
		<xsl:call-template name="input_changed"/>
		
	        <smtb:comment>Ajustement du controle des free fluxes</smtb:comment>

	        <m:apply>
		        <m:eq/>
		        <m:ci>nothing</m:ci>
		        <m:apply>
			        <m:fn>
				        <m:ci>adjustFluxesTab</m:ci>
			        </m:fn>
			        <m:ci>fluxes</m:ci>
		        </m:apply>
	        </m:apply>

	        <smtb:comment>Cumom�res en entr�e, calcul�s � partir des isotopom�res des m�tabolites d'entr�e.</smtb:comment>

	        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		        <m:apply>
			        <m:eq/>
			        <m:ci>
				        <xsl:value-of select="concat('x',@weight,'_input')"/>
			        </m:ci>
			        <m:apply>
				        <m:times/>
				        <m:ci>
					        <xsl:value-of select="concat('D',@weight)"/>
				        </m:ci>
				        <m:list separator=";">
					        <xsl:for-each select="../../sbml:listOfSpecies/sbml:species[smtb:input]">
						        <m:ci>
							        <xsl:value-of select="concat(@id,'_input')"/>
						        </m:ci>
					        </xsl:for-each>
				        </m:list>
			        </m:apply>
		        </m:apply>
	        </xsl:for-each>
			
			
			<!--Cumom�res en entr�e, calcul�s � partir des isotopom�res des m�tabolites d'entr�e.-->
			<xsl:call-template name="input_cumomeres"/>
			
			<!--Reformatage des xi_input-->
			<smtb:comment>Reformatage des xi_input</smtb:comment>
			<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
			<m:apply>
			<m:eq/>
			<m:ci><xsl:value-of select="concat('x',@weight,'_input')"/></m:ci>
			<m:cn><xsl:value-of select="concat('MtoHM(x',@weight,'_input, d_t3)')"/></m:cn>
			</m:apply>
			</xsl:for-each>
			
			<!-- Matrices de concentration des metabolites-->
			<smtb:comment>Matrices de concentration des metabolites</smtb:comment>
			<xsl:call-template name="concentration_matrix"/>							
			<!-- Cascade -->
			<xsl:text>if (t0_changed | t1_changed | d_t3_changed | fluxes_changed | concentrations_changed |input_changed) then&#xA;</xsl:text>
			<smtb:comment>Cascade.</smtb:comment>
			<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">	
				<m:apply>
				<m:eq/>
				<m:list separator=",">
					<xsl:call-template name="gen_names_dg_dv2"/>
					<m:ci><xsl:value-of select="concat('M',@weight)"/></m:ci>
					<m:ci><xsl:value-of select="concat('x',@weight)"/></m:ci>
					<m:ci>t</m:ci>
				</m:list>
				<m:cn><xsl:value-of select="concat('init_p',@weight,'(x',@weight,'_input,')"/>
					<xsl:for-each select="preceding-sibling::*">
    					<xsl:value-of select="concat('x',@weight,',')"/>
    			</xsl:for-each>
    				<xsl:text>fluxes(:,1))</xsl:text>
				</m:cn>
				</m:apply>
			</xsl:for-each>
			
			
			<smtb:comment>Calcul des observations en fonction des cumom�res</smtb:comment>
			<m:apply>
				<m:eq/>
				<m:ci>y</m:ci>
				<m:apply>
				<m:plus/>
				<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
					<m:apply>
						<m:times/>
						<m:ci><xsl:value-of select="concat('C',@weight)"/></m:ci>
						<m:ci><xsl:value-of select="concat('x',@weight,'(:,:)')"/></m:ci>
					</m:apply>
				</xsl:for-each>
				</m:apply>
			</m:apply>
			
			<m:apply>
				<m:eq/>
				<m:ci>[d_c1,d_c2]</m:ci>
				<m:cn>size(C1)</m:cn>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>[d_hm1,d_hm2,d_hm3]</m:ci>
				<m:cn>size(x1)</m:cn>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>y</m:ci>
				<m:cn>matrix(y,[d_c1,d_hm2,d_hm3])</m:cn>
			</m:apply>
			
			<!-- Creation d'une matrice par exp�riences -->
			 <smtb:comment>Creation d'une matrice par exp�riences</smtb:comment>
			<!--<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
				<xsl:call-template name="loop-exp"/>
			</xsl:for-each>-->
			<xsl:call-template name="loop-exit"/>
			
			<xsl:text>end;&#xA;</xsl:text>
			<!--Cout-->
			<smtb:comment>Calcul des couts</smtb:comment>
			<m:apply>
				<m:eq/>
				<m:ci>f_cost</m:ci>
				<m:apply>
					<m:fn><m:ci>sum</m:ci></m:fn>
					<m:apply>
						<m:times type="array"/>
						<m:ci>flux_obs(:,1)</m:ci>
						<m:apply>
							<m:power type="array"/>
							<m:apply>
								<m:minus/>
								<m:apply>
									<m:times type="matrix"/>	
									<m:ci>E</m:ci>
									<m:ci>fluxes(:,1)</m:ci>
								</m:apply>
								<m:ci>flux_obs(:,2)</m:ci>
							</m:apply>
							<m:cn>2</m:cn>
						</m:apply>
					</m:apply>	
				</m:apply>		
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>m_obs</m:ci>
				<m:apply>
					<m:fn><m:ci>hypermat</m:ci></m:fn>
					<m:ci><xsl:value-of select="concat('[size(y,1),3*',$experiences,',',$nb_t_obs,']')"/></m:ci>
				</m:apply>
			</m:apply>
			<xsl:call-template name="collapse_measurement"/>
			<!--Construction d'un vecteur avec les temps auquels les mesures ont ete effectue-->
			<smtb:comment>Vecteur des temps observes</smtb:comment>
			<m:apply>
				<m:eq/>
				<m:ci>vtm</m:ci>
				<m:list separator=";">	
				<xsl:call-template name="vector_times_measurement"/>
				</m:list>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>h</m:ci>
				<m:cn>t(2)-t(1)</m:cn>
			</m:apply>
			
			<m:apply>
				<m:eq/>
				<m:ci>ident</m:ci>
				<m:cn>y(:,:,(vtm/h)+1)</m:cn>
			</m:apply>	
			<m:apply>
				<m:eq/>
				<m:ci>c_cost_tmp</m:ci>
				<m:apply>
					<m:times/>
					<m:ci><xsl:value-of select="concat('m_obs(:,1:3:3*',$experiences,',:)')"/></m:ci>
					<m:apply>
						<m:times type="array"/>
						<m:ci>0.5</m:ci>
						<m:apply>
							<m:power type="array"/>
							<m:apply>
								<m:minus/>
								<m:ci>ident</m:ci>
								<m:ci><xsl:value-of select="concat('m_obs(:,2:3:3*',$experiences,',:)')"/></m:ci>
							</m:apply>
							<m:cn>2</m:cn>
						</m:apply>
					</m:apply>
				</m:apply>
			</m:apply>		
			<!--Effectue la somme sur les lignes (cumomers) de l'hypermermatrice c_cost_tmp et cree la
			matrice c_cost avec en colonnes les temps et en lignes les experiences-->
			<m:apply>
				<m:eq/>
				<m:ci>c_cost_tmp2</m:ci>
				<m:apply>
					<m:fn><m:ci>matrix</m:ci></m:fn>
					<m:apply>
						<m:fn><m:ci>sum</m:ci></m:fn>
						<m:apply>
							<m:selector/>
				        		<m:ci type="matrix">c_cost_tmp</m:ci>
				        		<m:cn>:</m:cn>
				        		<m:cn>:</m:cn>
						</m:apply>
						<m:ci>'r'</m:ci>
					</m:apply>
					<m:ci><xsl:value-of select="concat('[',$experiences,',',$nb_t_obs,']')"/></m:ci>
				</m:apply>	
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>c_cost</m:ci>
				<m:apply>
					<m:fn><m:ci>sum</m:ci></m:fn>
					<m:ci>c_cost_tmp2</m:ci>
					<m:ci>'c'</m:ci>
				</m:apply>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>label_error</m:ci>
				<m:apply>
					<m:fn><m:ci>sum</m:ci></m:fn>
					<m:ci>2*c_cost</m:ci>
					<m:ci>'r'</m:ci>
				</m:apply>
			</m:apply>	
			<m:apply>
				<m:eq/>
				<m:ci>t_cost</m:ci>
				<m:apply>	
					<m:plus/>
					<m:ci>c_cost</m:ci>
					<m:apply>
						<m:times/>
						<m:ci>f_cost</m:ci>
						<m:apply>
							<m:fn><m:ci>ones</m:ci></m:fn>
							<m:ci><xsl:value-of select="$experiences"/></m:ci>
							<m:ci>1</m:ci>
						</m:apply>
					</m:apply>
				</m:apply>	
			</m:apply>	
			<xsl:call-template name="break_ident"/>
			<xsl:call-template name="error_by_exp"/>
			<m:apply>
				<m:eq/>
				<m:ci>flux_obs(:,3)</m:ci>
				<m:apply>
					<m:times type="matrix"/>	
					<m:ci>E</m:ci>
					<m:ci>fluxes(:,1)</m:ci>
				</m:apply>
			</m:apply>
			
			
			<!-- Flux intervenant dans le calcul du gradient (utile que lors de l'appel de la fonction gradient sans passer par costAndGrad-->
			<m:apply>
				<m:eq/>
				<m:apply>
					<m:list separator=",">
						<m:ci>m_obs</m:ci>
						<m:ci>y_obs</m:ci>
						<m:ci>v_obs</m:ci>
						<m:ci>alpha</m:ci>
						<m:ci>delta</m:ci>
						<m:ci>vtm</m:ci>
					</m:list>
				</m:apply>
				<m:apply>
					<m:fn><m:ci>init_cost_var</m:ci></m:fn>
					<m:ci>fluxes(:,1)</m:ci>
					<m:ci>flux_obs</m:ci>
					<xsl:call-template name="names_of_matrix_measurement"/>
				</m:apply>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>e_flux</m:ci>
				<m:apply>
					<m:minus/>
					<m:apply>
						<m:times/>
						<m:ci>E</m:ci>
						<m:ci>v</m:ci>
					</m:apply>
					<m:ci>v_obs</m:ci>
				</m:apply>
			</m:apply>
		</smtb:script>		
    
    </xsl:template>
    
    <!--Matrice de concentration de m�tabolites-->
    <xsl:template name="concentration_matrix">
    	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		<xsl:variable name="weight" select="@weight"/>
		<m:apply>
			<m:eq/>
			<m:ci><xsl:value-of select="concat('Kv',@weight)"/></m:ci>
			<m:list separator="&#xA;">
				<xsl:for-each select="../../sbml:listOfSpecies/sbml:species[@type='intermediate']">
					<xsl:variable name="position" select="position()"/>
					<xsl:for-each select="smtb:cumomer[@weight=$weight]">
						<m:apply>
							<m:selector/>
							<m:ci>concentrations</m:ci>
							<m:cn><xsl:value-of select="concat('(',$position,')')"/></m:cn>
						</m:apply>
					</xsl:for-each>
				</xsl:for-each>		
			</m:list>
		</m:apply>
		<m:apply>
			<m:eq/>
			<m:ci><xsl:value-of select="concat('K',$weight)"/></m:ci>
			<m:cn><xsl:value-of select="concat('spdiag(Kv',$weight,')')"/></m:cn>
		</m:apply>
	</xsl:for-each>
    </xsl:template>
    
    <!--decomposition des hypermatrice x en plusieurs matrice, sert dans la fonction de sauvegarde des donnees-->
    <xsl:template name="loop-exp">
	 <xsl:param name="i">1</xsl:param>	
	 <m:apply>
		<m:eq/>
		<m:ci><xsl:value-of select="concat('x',@weight,'_',$i)"/></m:ci>
		<m:cn><xsl:value-of select="concat('flatten_exp(x',@weight,',',$i,')')"/></m:cn>
	</m:apply>
	<xsl:if test="$i&lt;$experiences">
		<xsl:call-template name="loop-exp">
			<xsl:with-param name="i" select="($i)+1"/>
		</xsl:call-template>
	</xsl:if>
   </xsl:template>
   
   
   <!-- Consctruction d'une matrice y_i pour chaque experiences � partir de l'hypermatrice y (pour affichage graphique)--> 
   <xsl:template name="loop-exit">
	 <xsl:param name="i">1</xsl:param>	
	 <m:apply>
		<m:eq/>
		<m:ci><xsl:value-of select="concat('y',@weight,'_',$i)"/></m:ci>
		<m:cn><xsl:value-of select="concat('flatten_exp(y',@weight,',',$i,')')"/></m:cn>
	</m:apply>
	<xsl:if test="$i&lt;$experiences">
		<xsl:call-template name="loop-exit">
			<xsl:with-param name="i" select="($i)+1"/>
		</xsl:call-template>
	</xsl:if>
   </xsl:template>


<xsl:template match="sbml:listOfReactions" mode="stoichiometric-matrix">
        <m:apply>
		<m:eq/>
		<m:ci>H</m:ci>
		<m:apply>
			<m:fn><m:ci>zeros</m:ci></m:fn>
			<m:cn><xsl:value-of select="count(sbml:reaction[@known='yes'])"/></m:cn>
			<m:cn>xsl:value-of select="count(sbml:reaction)"/></m:cn>
			</m:apply>
		</m:apply> 
        <xsl:apply-templates select="sbml:reaction[@known='yes']" mode="stoichiometric-matrix"/>    
</xsl:template>

<xsl:template match="sbml:reaction" mode="stoichiometric-matrix">
        <m:apply>
            <m:eq/>
            <m:apply>
                <m:selector/>
		<m:ci type="matrix">H</m:ci>
		<m:cn><xsl:value-of select="position()"/></m:cn>
		<m:cn><xsl:value-of select="@position"/></m:cn>
            </m:apply>
            <m:cn>1</m:cn>
        </m:apply>
</xsl:template>

<!--Construction d'un vecteur contenant les temps auxquels les mesures ont �t� effectu�es-->
<xsl:template name="vector_times_measurement">
	<xsl:param name="instant">1</xsl:param>
		<smtb:int>
			<xsl:value-of select="concat('t_obs_',$instant)"/>
            	</smtb:int>
	<xsl:if test="$instant&lt;$nb_t_obs">	
		<xsl:call-template name="vector_times_measurement">
			<xsl:with-param name="instant" select="($instant)+1"/>
		</xsl:call-template>		
	</xsl:if>
</xsl:template>

<!--Colle les matrices des cumomers measurement pour avoir une structure semblable a celle des matrices y-->
<xsl:template name="collapse_measurement">
	<xsl:param name="instant">1</xsl:param>
	<m:apply>		
		<m:eq/>	
		<m:ci><xsl:value-of select="concat('m_obs(:,:,',$instant,')')"/></m:ci>
		<m:apply>
			<m:list separator=";">
			<xsl:for-each select="/sbml:model/sbml:listOfSpecies/sbml:species[smtb:measurement]">
				<m:ci><xsl:value-of select="concat(@id,'_obs_',$instant)"/></m:ci>
			</xsl:for-each>
			</m:list>
		</m:apply>
	</m:apply>
	<xsl:if test="$instant&lt;$nb_t_obs">	
		<xsl:call-template name="collapse_measurement">
			<xsl:with-param name="instant" select="($instant)+1"/>
		</xsl:call-template>		
	</xsl:if>
</xsl:template>

<xsl:template name="names_of_matrix_measurement">
	<xsl:param name="instant">1</xsl:param>
	<xsl:for-each select="/sbml:model/sbml:listOfSpecies/sbml:species[smtb:measurement]">
		<m:ci><xsl:value-of select="concat(@id,'_obs_',$instant)"/></m:ci>
	</xsl:for-each>
	<xsl:if test="$instant&lt;$nb_t_obs">	
		<xsl:call-template name="names_of_matrix_measurement">
			<xsl:with-param name="instant" select="($instant)+1"/>
		</xsl:call-template>		
	</xsl:if>
</xsl:template>

<!-- template calculant les valeurs des colonnes ident. dans les matrices d'observations de cumom�res-->
<xsl:template name="break_ident">
	<xsl:param name="instant">1</xsl:param>
		<xsl:for-each select="/sbml:model/sbml:listOfSpecies/sbml:species[smtb:measurement]">
			<m:apply>
			<m:eq/>
				<m:ci><xsl:value-of select="concat(@id,'_obs_',$instant,'(:,3:3:$)')"/></m:ci>
				<m:cn><xsl:value-of select="concat('ident(',count(preceding-sibling::*/smtb:measurement)+1,':',count(preceding-sibling::*/smtb:measurement)+count(smtb:measurement),',:,',$instant,')')"/></m:cn>
			</m:apply>
		</xsl:for-each>
	<xsl:if test="$instant&lt;$nb_t_obs">	
		<xsl:call-template name="break_ident">
			<xsl:with-param name="instant" select="($instant)+1"/>
		</xsl:call-template>		
	</xsl:if>
</xsl:template>


<!--Ne sert pas???-->
<!--
<xsl:template name="e_fct_cost">
	<xsl:param name="instant">1</xsl:param>
		<xsl:for-each select="/sbml:model/sbml:listOfSpecies/sbml:species[smtb:measurement]">
			<m:ci><xsl:value-of select="concat(@id,'_obs_',$instant)"/></m:ci>
		</xsl:for-each>
	<xsl:if test="$instant&lt;$nb_t_obs">	
		<xsl:call-template name="e_fct_cost">
			<xsl:with-param name="instant" select="($instant)+1"/>
		</xsl:call-template>		
	</xsl:if>
</xsl:template>
-->

<!--calcul de l'erreur pour chaque experiences-->
<xsl:template name="error_by_exp">
	<xsl:param name="exp">1</xsl:param>
	<m:apply>
		<m:eq/>
		<m:ci><xsl:value-of select="concat('matrix_cost(',$exp,',1)')"/></m:ci>
		<m:cn><xsl:value-of select="concat('2*c_cost(',$exp,')')"/></m:cn>
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:ci><xsl:value-of select="concat('matrix_cost(',$exp,',2)')"/></m:ci>
		<m:cn><xsl:value-of select="concat('f_cost+2*c_cost(',$exp,')')"/></m:cn>
	</m:apply>
	<xsl:if test="$exp&lt;$experiences">
		<xsl:call-template name="error_by_exp">
			<xsl:with-param name="exp" select="($exp)+1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<!--Template pour creer la variable input_changed valant true si l'un des LABEL_INPUT a �t� chang�-->
<xsl:template name="input_changed">
	<m:apply>
		<m:eq/>
		<m:ci>input_changed</m:ci>
		<m:apply>
			<m:plus/>
			<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:input]">
				<m:ci><xsl:value-of select="concat(@id,'_input_changed')"/></m:ci>
			</xsl:for-each>
		</m:apply>
	</m:apply>
</xsl:template>

<!-- Template pour creer les parametres de sorties dg_dv des fonctions init_pi -->
<xsl:template name="gen_names_dg_dv">
	<xsl:param name="exp">1</xsl:param>
	<xsl:variable name="current_weight" select="@weight"/>
	<m:ci> <xsl:value-of select="concat('dg',@weight,'_dv_',$exp)"/> </m:ci>
	<xsl:for-each select="preceding-sibling::*">
					<m:ci> <xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_',$exp)"/></m:ci>
				</xsl:for-each>	
	<xsl:if test="$exp&lt;$experiences">
		<xsl:call-template name="gen_names_dg_dv">
			<xsl:with-param name="exp" select="($exp)+1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>


<xsl:template name="gen_names_dg_dv2">
	<xsl:variable name="current_weight" select="@weight"/>
	<m:ci> <xsl:value-of select="concat('dg',@weight,'_dv')"/> </m:ci>
	<xsl:for-each select="preceding-sibling::*">
		<m:ci> <xsl:value-of select="concat('db',$current_weight,'_dx',@weight)"/></m:ci>
	</xsl:for-each>	

</xsl:template>

<!-- sert pour la generation des fonctions adj_init_pi--> 
<xsl:template name="adj_init">
	<xsl:variable name="current_weight" select="@weight"/>
	<m:apply>
		<m:eq/>
		<m:ci>t</m:ci>
		<m:cn>linspace(t0,t1,d_t3)</m:cn>
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:ci>h</m:ci>
		<m:cn>t(2)-t(1)</m:cn>
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:ci>t_y</m:ci>
		<m:cn><xsl:value-of select="concat('size(Y',@weight,')')"/></m:cn>
	</m:apply>
	<!--initialisation de l'hypermatrices o-->
	<m:apply>
		<m:eq/>
		<m:ci>o</m:ci>
		<m:cn>hypermat([t_y])</m:cn>
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:ci>o</m:ci>
		<m:apply>
			<m:plus/>
			<xsl:for-each select="following-sibling::smtb:listOfCumomers[@weight&lt;=$weight]">
				<m:apply>
					<m:times/>
					<m:ci><xsl:value-of select="concat('db',@weight,'_dx',$current_weight)"/></m:ci>
					<m:ci><xsl:value-of select="concat('P',@weight)"/></m:ci>
				</m:apply>
			</xsl:for-each>
		</m:apply>			
	</m:apply>		
	<m:apply>
		<m:eq/>
		<m:ci>o</m:ci>
		<m:apply>
			<m:minus/>
			<m:apply>
				<m:times/>
				<m:ci>(h/2)</m:ci>
				<m:ci>o</m:ci>
			</m:apply>
			<m:ci><xsl:value-of select="concat('Y',@weight)"/></m:ci>	
		</m:apply>
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:ci>f</m:ci>
		<m:cn>o(:,:,$)</m:cn>
	</m:apply>
</xsl:template>

<!--appelle des fonctions adj_init_pi (resolution en cascade)-->
<xsl:template name="adj_cascade">
	<xsl:param name="current_weight" select="$weight"/>
	<xsl:variable name="double_quote">"</xsl:variable>
	<m:apply>
		<m:eq/>
		<m:cn><xsl:value-of select="concat('Y',$current_weight)"/></m:cn>
		<m:apply>
			<m:fn><m:ci>init_Y</m:ci></m:fn>
				<m:ci><xsl:value-of select="concat('C',$current_weight)"/></m:ci>
				<m:ci><xsl:value-of select="$experiences"/></m:ci>
		</m:apply>				
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:apply>
			<m:list separator=",">
				<m:ci><xsl:value-of select="concat('f',$current_weight)"/></m:ci>
				<m:ci><xsl:value-of select="concat('o',$current_weight)"/></m:ci>
			</m:list>
		</m:apply>
		<m:apply>
			<m:fn><m:ci><xsl:value-of select="concat('adj_init_p',$current_weight)"/></m:ci></m:fn>
		</m:apply>				
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:cn><xsl:value-of select="concat('p',$current_weight)"/></m:cn>
		<m:apply>
			<m:fn><m:ci>adjoint</m:ci></m:fn>
			<m:ci><xsl:value-of select="concat('M',$current_weight)"/></m:ci>
			<m:ci><xsl:value-of select="concat('K',$current_weight)"/></m:ci>
			<m:ci><xsl:value-of select="concat('f',$current_weight)"/></m:ci>
			<m:ci><xsl:value-of select="concat('Y',$current_weight)"/></m:ci>
			<m:ci><xsl:value-of select="concat('o',$current_weight)"/></m:ci>
		</m:apply>				
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:cn><xsl:value-of select="concat('P',$current_weight)"/></m:cn>
		<m:apply>
			<m:fn><m:ci>add_adjoint</m:ci></m:fn>
			<m:ci><xsl:value-of select="concat('p',$current_weight)"/></m:ci>
		</m:apply>				
	</m:apply>
	<xsl:if test="$current_weight&gt;1">
		<xsl:call-template name="adj_cascade">
			<xsl:with-param name="current_weight" select="($current_weight)-1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<!--calcul du gradient-->
<xsl:template name="gradient">
	<m:apply>
		<m:eq/>
		<m:ci>t</m:ci>
		<m:cn>linspace(t0,t1,d_t3)</m:cn>
	</m:apply>
	<m:apply>
		<m:eq/>
		<m:ci>h</m:ci>
		<m:cn>t(2)-t(1)</m:cn>
	</m:apply>
	<!--<m:apply>
		<m:eq/>
		<m:ci>t_dg_dv</m:ci>
		<m:cn>size(dg1_dv)</m:cn>
	</m:apply>-->
	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		<m:apply>
			<m:eq/>
			<m:ci><xsl:value-of select="concat('j_',@weight)"/></m:ci>
			<m:apply>
				<m:times/>
				<m:ci><xsl:value-of select="concat('dg',@weight,'_dv')"/></m:ci>
				<m:ci><xsl:value-of select="concat('P',@weight)"/></m:ci>
			</m:apply>	
		</m:apply>
	</xsl:for-each>
	<m:apply>
		<m:eq/>
		<m:ci>grad</m:ci>
		<m:apply>
			<m:plus/>
			<m:apply>
				<m:times/>
				<m:ci>-(h/2)</m:ci>
				<m:apply>
					<m:fn><m:ci>sum</m:ci></m:fn>
					<m:apply>
						<m:list separator=",">	
							<xsl:call-template name="gradient_aux_3"/>
						</m:list>
					</m:apply>
					<m:ci>'c'</m:ci>
				</m:apply>
			</m:apply>
			<m:ci>E'*(delta.*e_flux)</m:ci>
		</m:apply>	
	</m:apply>
</xsl:template>

<!-- template appell� par la template gradient-->
<xsl:template name="gradient_aux_3">
	<xsl:variable name="quote">'</xsl:variable>
	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		<m:apply>
			<m:fn><m:ci>sum</m:ci></m:fn>
			<m:ci><xsl:value-of select="concat('j_',@weight,'(:,:)')"/></m:ci>
			<m:ci>'c'</m:ci>		
		</m:apply>	
	</xsl:for-each>
</xsl:template>

<!--G�n�ration d'un partie du code de la fonction de sauvegarde des donnees-->
<xsl:template name="sauvegarde">
	<xsl:param name="n_exp">1</xsl:param>
	<xsl:variable name="type_float">'%1.7f, '</xsl:variable>
	<xsl:variable name="double_quote">"</xsl:variable>
	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		<xsl:variable name="nb_pos" select="math:max(smtb:cumomer/@position)"/>	
		<xsl:value-of select="concat('mfprintf(desc,',$double_quote,'//Matrice x',@weight,'_',$n_exp,'\n',$double_quote,');&#xA;')"/>
		<xsl:value-of select="concat('for j=1:',$nb_pos,'&#xA;')"/>
		<xsl:text>for i=1:d_t3&#xA;</xsl:text>
			<xsl:value-of select="concat('mfprintf(desc,',$type_float,',x',@weight,'_',$n_exp,'(j,i));&#xA;')"/>
		<xsl:text>end;&#xA;</xsl:text>
		<xsl:text>mfprintf(desc,"\n");&#xA;</xsl:text>
		<xsl:text>end;&#xA;</xsl:text>
		<xsl:text>mfprintf(desc,"\n\n");&#xA;</xsl:text>
	</xsl:for-each>
	<xsl:for-each select="sbml:listOfSpecies/sbml:species/smtb:measurement/smtb:cumomer-contribution[@id]">
		<xsl:variable name="n_graph" select="count(../preceding-sibling::smtb:measurement)+1"/>
		<xsl:value-of select="concat('mfprintf(desc,',$double_quote,'Exp�rience=',$n_exp,' Esp�ce=',../../@name,' Pattern=',@string,' ',$double_quote,')&#xA;')"/>
		<xsl:text>for i=1:d_t3&#xA;</xsl:text>
			<xsl:value-of select="concat('mfprintf(desc,',$type_float,',y_',$n_exp,'(',$n_graph,',i));&#xA;')"/>
		<xsl:text>end;&#xA;</xsl:text>
		<xsl:text>mfprintf(desc,"\n");&#xA;</xsl:text>
	</xsl:for-each>
	<xsl:if test="$n_exp&lt;$experiences">
		<xsl:call-template name="sauvegarde">
			<xsl:with-param name="n_exp" select="($n_exp)+1"/>
		</xsl:call-template>	
    	</xsl:if>
</xsl:template>
	
</xsl:stylesheet>
