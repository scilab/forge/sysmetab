<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Tue Mar 27 09:27:49 CEST 2007

    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but 

    1 - de mettre les informations de transition
    entre les atomes de carbones sous une forme exploitable par la suite, en les
    d�crivant � l'aide d'�l�ments <smtb:carbons> que l'on inclut � l'int�rieur
    de chaque �l�ment <speciesReference> dans les �lements <listOfReactants>
    et <listOfProducts>. Par exemple pour la r�action 
    
                              s1+s2->s3 
    
    avec des transitions des atomes de carbones �crites "� la Wiechert" 
    
                             AB + DC -> ADBC 
    cela donne :
    
    <reaction>
        <listOfReactants>
            <speciesReference species="s1">
                <smtb:carbon position="1" destination="1" species="s3"/>
                <smtb:carbon position="2" destination="4" species="s3"/>
            </speciesReference>
            <speciesReference species="s2">
                <smtb:carbon position="1" destination="2" species="s3"/>
                <smtb:carbon position="2" destination="8" species="s3"/>
            </speciesReference>
        </listOfReactants>
        <listOfProducts>
            <speciesReference species="s3">
                <smtb:carbon position="1" destination="1" species="s1"/>
                <smtb:carbon position="2" destination="1" species="s2"/>
                <smtb:carbon position="4" destination="2" species="s1"/>
                <smtb:carbon position="8" destination="2" species="s2"/>
            </speciesReference>
        </listOfProducts>
    </reaction>
    
	Les positions ont des num�ros en puissances de 2 successives, c'est � dire que
	les positions 1,2,3,4 sont num�rot�es 1,2,4,8. Cela permet, par la suite, 
	de g�n�rer les �quations du marquages beaucoup plus facilement.
	
    2 - pour chaque esp�ce, on ajoute des �l�ments qui donnent les identificateurs 
    des diff�rents cumom�res ainsi que les carbones marqu�s (meme convention que
    ci dessus, y compris pour les num�ros en puissances de 2) :
    
    <sbml:species id="s1">
        <smtb:cumomer id="s1_1" weight="1">
            <smtb:carbon position="1"/>
        </smtb:cumomer>
        <smtb:cumomer id="s1_2" weight="1">
            <smtb:carbon position="2"/>        
        </smtb:cumomer>
        <smtb:cumomer id="s1_3" weight="2">        
            <smtb:carbon position="1"/>
            <smtb:carbon position="2"/>        
        </smtb:cumomer>
    </sbml:species>
    
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"    
    xmlns:math="http://exslt.org/math"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml m math smtb">
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="sbml:species[key('reactionSpecies',@id)]">
        <sbml:species>
            <xsl:copy-of select="@*"/>

            <!-- Pour chacune de ces esp�ces on r�cup�re la premi�re occurence d'un �l�ment <speciesReference>
                 dans un �l�ment <reaction>, dont l'attribut @carbons donne le nombre de carbones de l'esp�ce. -->

            <xsl:variable name="pattern" select="translate(key('reactionSpecies',@id)[@carbons!='']/@carbons,
												 'ABCEDFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
												 '0000000000000000000000000000000000000000000000000000')"/>
            <xsl:variable name="nbC" select="string-length($pattern)"/>

			<xsl:choose>
            	<xsl:when test="key('products',@id) and key('reactants',@id)">        
					<xsl:attribute name="type">intermediate</xsl:attribute>
				</xsl:when>
				<xsl:when test="not (key('products',@id)) and key('reactants',@id)">
                    <xsl:attribute name="type">input</xsl:attribute>
                </xsl:when>

                <!-- Si l'esp�ce est un m�tabolite sortant -->

                <xsl:when test="not(key('reactants',@id)) and key('products',@id)">
					<xsl:attribute name="type">output</xsl:attribute>    
                </xsl:when>
			</xsl:choose>

            <xsl:choose>
                <xsl:when test="$nbC&gt;0">
                    <xsl:attribute name="carbons">
                         <xsl:value-of select="$nbC"/>
                    </xsl:attribute>
                    
                    <!-- G�n�ration des identificateurs des diff�rents cumom�res -->
                    
                    <xsl:call-template name="iteration-cumomers">
                        <xsl:with-param name="nbCumomer" select="(math:power(2,$nbC))-1"/>
                        <xsl:with-param name="i" select="'1'"/>
                        <xsl:with-param name="carbons">
                            <smtb:carbon position="1" index="0"/>
                        </xsl:with-param>
                        <xsl:with-param name="pattern" select="$pattern"/>
                    </xsl:call-template>
                    
                </xsl:when>   
            </xsl:choose> 

			<xsl:copy-of select="*"/>
        </sbml:species>
    </xsl:template>

    
    <xsl:template match="sbml:speciesReference">
        <xsl:element name="sbml:speciesReference">
            <xsl:apply-templates select="@*"/>
                <xsl:call-template name="iteration">
                    <xsl:with-param name="i" select="'1'"/>
                </xsl:call-template>
        </xsl:element>
    </xsl:template>
    
    <xsl:template name="iteration">
        <xsl:param name="i"/>
        
        <xsl:if test="$i&lt;=string-length(@carbons)">
            
            <xsl:variable name="c" select="substring(@carbons,$i,1)"/>
            
            <!-- Attention, on doit num�roter les carbones de droite � gauche si on veut avoir
                une correspondance directe entre les num�ros des esp�ces marqu�es et le nombre
                binaire repr�sentant le marquage. -->
            
            <smtb:carbon position="{math:power(2,string-length(@carbons)-($i))}">
            
                <!-- Ici on balaye les sbml:listOfProducts/sbml:speciesReference si 
                     le contexte appelant �tait un sbml:listOfReactants/sbml:speciesReference
                     et vice-versa. Cela se fait grace � l'expression XPath (un peu compliqu�e...)
                     ci dessous : -->
                     
                <xsl:for-each select="../../*[name()!=name(current()/..)]/sbml:speciesReference">
                    <xsl:if test="contains(@carbons,$c)">
                        <xsl:attribute name="destination">
                            <xsl:value-of select="math:power(2,string-length(substring-after(@carbons,$c)))"/>
                        </xsl:attribute>
                        
                        <!-- L'attribut "occurence" sert � diff�rencier les deux versions d'une esp�ce quand
                            la stoechiom�trie est sup�rieure � 1. -->
                        
                        <xsl:attribute name="occurence">
                            <xsl:value-of select="count(preceding-sibling::sbml:speciesReference)+1"/>
                        </xsl:attribute>
                        <xsl:copy-of select="@species"/>
                    </xsl:if>
                </xsl:for-each>
                
            </smtb:carbon>    
            
            <xsl:call-template name="iteration">
                <xsl:with-param name="i" select="($i)+1"/>
            </xsl:call-template>
            
        </xsl:if>
  
    </xsl:template>
    
    <xsl:template name="iteration-cumomers">
    
    <!-- Cette template r�cursive permet de g�n�rer les diff�rents identificateurs des cumom�res -->
            
        <xsl:param name="nbCumomer"/>
        <xsl:param name="i"/>
        <xsl:param name="carbons"/>       
        <xsl:param name="pattern"/>       
        
        <xsl:if test="$i&lt;=$nbCumomer">

           <smtb:cumomer 
                id="{concat(@id,'_',$i)}" species="{@id}"
                weight="{count(exslt:node-set($carbons)/smtb:carbon)}">
				<xsl:attribute name="pattern">
					<xsl:for-each select="str:tokenize($pattern,'')">
						<xsl:variable name="position" select="string-length($pattern)-position()"/>
						<xsl:choose>
							<xsl:when test="exslt:node-set($carbons)/smtb:carbon[@index=$position]">
								<xsl:value-of select="'1'"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'x'"/>								
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:attribute>
                <xsl:copy-of select="exslt:node-set($carbons)"/>
           </smtb:cumomer>

           <xsl:call-template name="iteration-cumomers">
                <xsl:with-param name="nbCumomer" select="$nbCumomer"/>
                <xsl:with-param name="i" select="($i)+1"/>
                <xsl:with-param name="carbons">
                    <xsl:call-template name="plusone">  
                        <xsl:with-param name="j" select="'1'"/>
                        <xsl:with-param name="index" select="'0'"/>
                        <xsl:with-param name="carbons" select="$carbons"/>
                    </xsl:call-template>
                </xsl:with-param>
				<xsl:with-param name="pattern" select="$pattern"/>
            </xsl:call-template>
        
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="plusone">

    <!-- Cette template r�cursive permet d'obtenir les carbones marqu�s d'un cumom�re
        en fonction des carbones marqu�s du cumom�re pr�c�dent. En pratique, il s'agit
        d'ajouter 1 � un nombre binaire et de propager la retenue si n�cessaire. 
        
        Par exemple si en entr�e (param�tre "carbons") on a le fragment
        
            <smtb:carbon position="1"/>
            <smtb:carbon position="2"/>
            
        qui correspond au nombre binaire 11, on obtient en sortie 100, soit
        le fragment
        
            <smtb:carbon position="4"/>
    -->

    <xsl:param name="j"/>
    <xsl:param name="index"/>
    <xsl:param name="carbons"/>
         
        <xsl:choose>
        
        	<xsl:when test="(exslt:node-set($carbons)/smtb:carbon[1]/@position)=$j">
            	<xsl:call-template name="plusone">
                	<xsl:with-param name="j" select="($j)*2"/>
                	<xsl:with-param name="index" select="($index)+1"/>
                	<xsl:with-param name="carbons">
                    	<xsl:copy-of select="exslt:node-set($carbons)/smtb:carbon[position()&gt;1]"/>
                	</xsl:with-param> 
            	</xsl:call-template>
        	</xsl:when>

        	<xsl:otherwise>
            	<smtb:carbon position="{$j}" index="{$index}"/>
            	<xsl:copy-of select="exslt:node-set($carbons)"/>
        	</xsl:otherwise>

    	</xsl:choose>
    </xsl:template>


</xsl:stylesheet>
