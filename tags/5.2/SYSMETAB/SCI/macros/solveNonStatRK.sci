function [residual,X,y,y_unscaled,MList,omega,Sypm2_e_label,Y,Mass,err]=solveNSRK(v,m,Xinp,M,b,cum,time_struct,pool_ind_weight,free)
    //
    // Solve the state equation cascade, SDRIK scheme + non uniform time grid
    // we suppose that a(i,i)==gam, i=1...s and A_s=b (b is equal to the last line of A)
    // c vector of Butcher tableau is not used as the ode is autonomous
    //
    gam=scheme.gamma
    alpha=scheme.alpha
    errc=scheme.err
    s=size(alpha,1) // number of RK stages
    N=length(cum)
    nX=size(Xinp,1)
    //
    MList=list()
    MX=list()
    Mass=zeros(nX,1)
    
    // initial stepsize, initial factorization
    
    stepsize=time_struct.t(2)-time_struct.t(1)
    for n=1:N    
      Mn_sp=sparse(M(n).ij,M(n).t*v(M(n).m1),M(n).size)
      mcn=m(pool_ind_weight(n))
      MList(n)=struct('Mn',Mn_sp,'MnImp_handle',umf_lufact(sparse(spdiag(mcn)-stepsize*gam*Mn_sp)),'Bnv',b(n).t*spdiag(v(b(n).m3)))
      Mass(cum(n))=mcn
    end

    G=zeros(nX,1)    
    Z=zeros(nX,s)

    kstart=[1;time_struct.ind(1:$-1,3)+1] // ranges of state integration followed by measurement interpolation‡
    kend=time_struct.ind(:,3)
    kmeas=time_struct.ind(:,1:2)
    shape=time_struct.shape
    
    nt=kend($)+1
    X=repmat(Xinp,1,nt)
    Xi=zeros(nX,size(shape,2))
    err=zeros(1,nt-1)    

    Y=list()
    for i=1:s
      Y(i)=zeros(nX,nt) // to store RK stage vectors for the adjoint state
    end
    for l=1:size(kend,1) // loop on k ranges

      for k=kstart(l):kend(l)  // loop on discrete time ranges

        Xk=X(:,k)
        Yi=Xk 
      
        newstepzize=time_struct.t(k+1)-time_struct.t(k)
        if clean(newstepzize-stepsize)
          stepsize=newstepzize
          for n=1:N    
            umf_ludel(MList(n).MnImp_handle)
            MList(n).MnImp_handle=umf_lufact(sparse(spdiag(m(pool_ind_weight(n)))-stepsize*gam*MList(n).Mn))
          end
        end
      
        // rK stage 1

        for n=1:N // loop on cumomer weight -> solving cascade for Z1
          cumn=cum(n)
          MX(n)=MList(n).Mn*Xk(cumn)
          Z(cumn,1)=umf_lusolve(MList(n).MnImp_handle,stepsize*gam*(MX(n)+spset(b(n).value,MList(n).Bnv*(Yi(b(n).m1).*Yi(b(n).m2)))))
          Yi(cumn)=Xk(cumn)+Z(cumn,1)
        end
      
        Y(1)(:,k)=Yi        
      
        for i=2:s // loop on subsequent RK stages

          G=Mass.*(Z(:,1:i-1)*alpha(i,1:i-1)')
          Yi=Xk        
          for n=1:N // loop on cumomer weight -> solving cascade for Zi
            cumn=cum(n)
            Z(cumn,i)=umf_lusolve(MList(n).MnImp_handle,G(cumn)+stepsize*gam*(MX(n)+spset(b(n).value,MList(n).Bnv*(Yi(b(n).m1).*Yi(b(n).m2)))))
            Yi(cumn)=Xk(cumn)+Z(cumn,i)
          end
        
          Y(i)(:,k)=Yi        

        end // loop on RK stage

        X(:,k+1)=Yi // special case d=b*A^-1 and A_s=b (b is equal to the last line of A) hence d_s=1 and d_i=0, i<s      
        err(k)=sqrt(mean((Z*errc).^2))

      end // loop on discrete time values

      // Compute the interpolated states
      k1=kmeas(l,1)
      k2=kmeas(l,2)
      Xi(:,k1:k2)=repmat(Xk,1,k2-k1+1)+Z*shape(:,k1:k2)
    
    end // loop on discrete time ranges

    omega=ones(nb_group,1)
    y_unscaled=Cx*Xi
    for i=auto_scales_ind // for groups with scale="auto"
      ind=meas_ind_group(i)
      yui=y_unscaled(ind,:)./Sy(ind,:)
      ymi=ymeas(ind,:)./Sy(ind,:)
      omega(i)=yui(:)'*ymi(:)/norm(yui(:))^2
    end
    y=spdiag(omega(omega_ind_meas))*y_unscaled
    //
    // Compute the residual
    //
    try
      e_label=y-ymeas
      Sypm2_e_label=Sypm2.*e_label
      residual=Sypm2_e_label.*e_label
    catch
      e_label=[]
      Sypm2_e_label=[]
      residual=[]
    end

    for i=1:length(cum)
      umf_ludel(MList(i).MnImp_handle)
    end

endfunction

function [residual,gradq,gradm,X,y]=solveNSRKGradAdj(v,m,Xinp,M,b,cum,time_struct,pool_ind_weight,dP_dpool,dP_dpool_t,dv_dw,W)
  //
  // Solve state equation and compute adjoint gradient, SDIRK Scheme
  //
  gam=scheme.a(1,1)
  a=scheme.a
  s=size(a,1) // number of RK stages
  //
  [residual,X,y,y_unscaled,MListDir,omega,Sypm2_e_label,Y,Mass,err]=solveNSRK(v,m,Xinp,M,b,cum,time_struct,pool_ind_weight,%t)  
  Mass=spdiag(Mass)
  CxtOmega=Cxt*spdiag(omega(omega_ind_meas))
  //
  gradv=zeros(v)
  gradm=zeros(m)
  N=length(cum)
  nX=size(Xinp,1)
  nt=length(time_struct.t)    
  p=zeros(nX,1)// no need to keep adjoint state values in the adjoint RK formulation
  
  // final stepsize (warning, backward in time !), final factorization
  
  MList=list()
  stepsize=time_struct.t(nt)-time_struct.t(nt-1)
  for n=1:N-1
    MList(n)=struct('Mn_adj',MListDir(n).Mn','MnImp_adj_handle',umf_lufact(sparse(spdiag(m(pool_ind_weight(n)))-stepsize*gam*MListDir(n).Mn')),'Dbdxtv',db_dxt(n).t*spdiag(v(db_dxt(n).m2)))
  end
  MList(N)=struct('Mn_adj',MListDir(N).Mn','MnImp_adj_handle',umf_lufact(sparse(spdiag(m(pool_ind_weight(N)))-stepsize*gam*MListDir(N).Mn'))) // the handle is not valid but only used for initial field type
  cumN=cum(N)
  H=zeros(nX,1)
  S=zeros(nX,1)
  phi=zeros(nX,s)

  for k=nt-1:-1:1 // warning the last value of p is not used (but the loop must end with k=1 for correct gradient computation)
    // RK stage s, weight N
    
    ind=time_struct.ind(find(time_struct.ind(:,3)==k),:)
    
    try
      ind_meas=ind(1):ind(2); // index of measurements
      dJdXk_t=CxtOmega*sum(Sypm2_e_label(:,ind_meas),2)
      dJdZk_t=CxtOmega*Sypm2_e_label(:,ind_meas)*time_struct.shape(:,ind_meas)';
    catch
      dJdXk_t=[]
      dJdZk_t=[]
    end

      newstepzize=time_struct.t(k+1)-time_struct.t(k)
      if clean(newstepzize-stepsize)
        stepsize=newstepzize
        for n=1:N    
          umf_ludel(MList(n).MnImp_adj_handle)
          MList(n).MnImp_adj_handle=umf_lufact(sparse(spdiag(m(pool_ind_weight(n)))-stepsize*gam*MList(n).Mn_adj))
        end
      end
    
    phi(cumN,s)=umf_lusolve(MList(N).MnImp_adj_handle,p(cumN)-dJdZk_t(cumN,s))
    S(cumN)=gam*phi(cumN,s)
    for n=N-1:-1:1 // RK stage s, weight < N   
      cumn=cum(n)
      phi(cumn,s)=umf_lusolve(MList(n).MnImp_adj_handle,spset(db_dxt(n).value,MList(n).Dbdxtv*Y(s)(db_dxt(n).m1,k))*S*stepsize+p(cumn)-dJdZk_t(cumn,s))
      S(cumn)=gam*phi(cumn,s)
    end
    gradv=gradv-spset(dgdvt.value,dgdvt.t*(X(dgdvt.m1,k+1).*X(dgdvt.m2,k+1)))*S*stepsize
    gradm=gradm+spset(dP_dpool_t.value,dP_dpool_t.t*(X(dP_dpool_t.m1,k+1)-X(dP_dpool_t.m1,k)))*phi(:,s); // Y_ks=X_{k+1} ! (see solveNSRK above)
    //
    for i=s-1:-1:1 
      // RK stage i<s, weight N
      H=phi(:,i+1:s)*a(i+1:s,i)
      phi(cumN,i)=umf_lusolve(MList(N).MnImp_adj_handle,stepsize*MList(N).Mn_adj*H(cumN)-dJdZk_t(cumN,i))
      S(cumN)=H(cumN)+gam*phi(cumN,i)
      for n=N-1:-1:1 // RK stage i<s, weight < N
         cumn=cum(n)
         phi(cumn,i)=umf_lusolve(MList(n).MnImp_adj_handle,stepsize*(MList(n).Mn_adj*H(cumn)+spset(db_dxt(n).value,MList(n).Dbdxtv*Y(i)(db_dxt(n).m1,k))*S)-dJdZk_t(cumn,i))
         S(cumn)=H(cumn)+gam*phi(cumn,i)
      end
      gradv=gradv-spset(dgdvt.value,dgdvt.t*(Y(i)(dgdvt.m1,k).*Y(i)(dgdvt.m2,k)))*S*stepsize
      gradm=gradm+spset(dP_dpool_t.value,dP_dpool_t.t*(Y(i)(dP_dpool_t.m1,k)-X(dP_dpool_t.m1,k)))*phi(:,i);  // Y_ki-X_k=Z_ki
    end
    p=sum(Mass*phi+dJdZk_t,2)-dJdXk_t
  end
  
  gradq=2*(dv_dw*W)'*gradv
  gradm=2*gradm
    
  for n=1:length(cum)
    umf_ludel(MList(n).MnImp_adj_handle)
  end
    
endfunction

function [residual,X,y,dy_dp,omega]=solveNSRKDer(v,m,Xinp,M,b,cum,time_struct,pool_ind_weight,dP_dpool,dv_dw,W)
 //
 // Solve state equation and compute derivatives, SDIRK Scheme
 // 
 gam=scheme.a(1,1)
 alpha=scheme.alpha
 s=size(alpha,1) // number of RK stages
 //
 [residual,X,y,y_unscaled,MList,omega,Sypm2_e_label,Y,Mass,err]=solveNSRK(v,m,Xinp,M,b,cum,time_struct,pool_ind_weight,%t)  
 Mass=spdiag(Mass)
 Omega=spdiag(omega(omega_ind_meas))
 //
 N=length(cum)
 nq=size(W,2)
 nbParam=nq+length(m);
 nX=size(Xinp,1)
 sizeMeas=size(Cx,1);
 nbMeas=length(time_struct.tmeas)

 dv_dwW=dv_dw*W

 dX_dpp1=zeros(nX,nbParam);
 dZ_dp=list() // hypermatrices are inefficient in this context
 for i=1:s
   dZ_dp(i)=zeros(nX,nbParam)
 end
 
 MdXdp=list()

 // initial stepsize, initial factorization

 stepsize=time_struct.t(2)-time_struct.t(1)
 for n=1:N    
   MList(n).MnImp_handle=umf_lufact(sparse(spdiag(m(pool_ind_weight(n)))-stepsize*gam*MList(n).Mn))
 end
 
 for n=2:N
   MList(n).Dbdxv=db_dx(n).t*spdiag(v(db_dx(n).m2))
 end
 
 dy_dp=zeros(sizeMeas,nbParam,nbMeas); // output derivative at each measurement time
 dy_unscaled_dp=zeros(sizeMeas,nbParam,nbMeas); // unscaled output derivative at each measurement time

 kstart=[1;time_struct.ind(1:$-1,3)+1] // ranges of state integration followed by measurement interpolation‡
 kend=time_struct.ind(:,3)
 kmeas=time_struct.ind(:,1:2)
 shape=time_struct.shape
 
 for l=1:size(kend,1) // loop on measurement sets (occuring between consecutive discrete times)
 
   for k=kstart(l):kend(l)  // loop on discrete time 
     dX_dp=dX_dpp1

     newstepzize=time_struct.t(k+1)-time_struct.t(k)
     if clean(newstepzize-stepsize)
       stepsize=newstepzize
       for n=1:N    
         umf_ludel(MList(n).MnImp_handle)
         MList(n).MnImp_handle=umf_lufact(sparse(spdiag(m(pool_ind_weight(n)))-stepsize*gam*MList(n).Mn))
       end
     end

     bcum=[stepsize*gam*spset(dgdv.value,dgdv.t*(Y(1)(dgdv.m1,k).*Y(1)(dgdv.m2,k)))*dv_dwW, -spset(dP_dpool.value,dP_dpool.t*(Y(1)(dP_dpool.m1,k)-X(dP_dpool.m1,k)))]

     cum1=cum(1) // rK stage 1, weight 1
     MdXdp(1)=MList(1).Mn*dX_dp(cum1,:)
     dZ_dp(1)(cum1,:)=umf_lusolve(MList(1).MnImp_handle,stepsize*gam*MdXdp(1)+bcum(cum1,:))

     for n=2:N // rK stage 1, weight > 1
       cumn=cum(n)
       MdXdp(n)=MList(n).Mn*dX_dp(cumn,:)
       dZ_dp(1)(cumn,:)=umf_lusolve(MList(n).MnImp_handle,stepsize*gam*(MdXdp(n)+spset(db_dx(n).value,MList(n).Dbdxv*Y(1)(db_dx(n).m1,k))*(dZ_dp(1)+dX_dp))+bcum(cumn,:))
     end

     for i=2:s // loop on subsequent RK stages
       dGi_dp=dZ_dp(1)*alpha(i,1)
       alphaijZj=(Y(1)(:,k)-X(:,k))*alpha(i,1)
       for j=2:i-1
         dGi_dp=dGi_dp+dZ_dp(j)*alpha(i,j)
         alphaijZj=alphaijZj+(Y(j)(:,k)-X(:,k))*alpha(i,j)
       end
       dGi_dp=Mass*dGi_dp
       dGi_dp(:,nq+1:$)=dGi_dp(:,nq+1:$)+spset(dP_dpool.value,dP_dpool.t*alphaijZj(dP_dpool.m1))

       bcum=[stepsize*gam*spset(dgdv.value,dgdv.t*(Y(i)(dgdv.m1,k).*Y(i)(dgdv.m2,k)))*dv_dwW, -spset(dP_dpool.value,dP_dpool.t*(Y(i)(dP_dpool.m1,k)-X(dP_dpool.m1,k)))]
       // rK stage i, weight 1
       dZ_dp(i)(cum1,:)=umf_lusolve(MList(1).MnImp_handle,stepsize*gam*MdXdp(1)+dGi_dp(cum1,:)+bcum(cum1,:))
       for n=2:N //  rK stage i, weight > 1
         cumn=cum(n)
         dZ_dp(i)(cumn,:)=umf_lusolve(MList(n).MnImp_handle,stepsize*gam*(MdXdp(n)+spset(db_dx(n).value,MList(n).Dbdxv*Y(i)(db_dx(n).m1,k))*(dZ_dp(i)+dX_dp))+dGi_dp(cumn,:)+bcum(cumn,:))
       end
     end // loop on RK stage
     dX_dpp1=dX_dp+dZ_dp(s)
   end   
      
   for j=kmeas(l,:)
     dyujdp=Cx*dX_dp
     for i=1:s
       dyujdp=dyujdp+shape(i,j)*Cx*dZ_dp(i)
     end
     dy_dp(:,:,j)=Omega*dyujdp
     dy_unscaled_dp(:,:,j)=dyujdp
   end  
 end

 dy_dp=NSDerScales(y,y_unscaled,dy_dp,dy_unscaled_dp,auto_scales_ind)

 for n=1:length(cum)
   umf_ludel(MList(n).MnImp_handle)
 end

endfunction
