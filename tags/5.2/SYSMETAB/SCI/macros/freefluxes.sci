function [W,w0,free,ftype,flux_adm,wadm]=freefluxes(C,d,me,ffnames,ffvalues)
    // Determination of "free fluxes" and of setting of affine subspace define 
    // by A*w=b, as w=W*q+w0, where q is a subset of w component, the "free fluxes"

    W=[]
    free=[]
    A=C(1:me,:)
		b=d(1:me)
    n=size(A,2)
    n1=rank(A)
    user_ff=%f
    
    if exists("ffnames","local")  
      user_ff=%t
      for i=1:size(ffnames,"*")
        free(i)=find(flux_ids==ffnames(i))
      end
      others=(1:n)'
      others(free)=[]
      p=[others;free]
      if rank(A(:,others))<>n1 | size(ffvalues,1)<>n-n1
        disp('The choice of free fluxes is incoherent, falling back to automatic freefluxes determination')
        user_ff=%f
        free=[]
      end  
    end
    
    if isempty(free)        
      // We use QR factorisation of A : A*P=QR with P of size (n,n), R of size (n1,n)
      // and Q of size (n1,n1). We use here the fact that the permutation P ensures
      // that the principal submatrix (n1,n1) of A*P has full rank if rank(A)=n1.
      [Q,R,P]=qr(A)
      [p,j]=find(P)
      free=p(n1+1:$)
      others=p(1:n1)
    end
    
    A1=A(:,others)
    A2=A(:,free)
    
    W=zeros(n,n-n1)
    w0=zeros(n-n1,1)
    W(p,:)=[-A1\A2;eye(n-n1,n-n1)]
    w0(p)=[A1\b;zeros(n-n1,1)]
    
    ftype=string(zeros(n,1))
    ftype(:)='d'
    ftype(free)='f'
    ftype(find(clean(sum(abs(W),'c'))==0))='c'
    
    // To verify that the user does not impose incoherent constraints, we try to
    // solve a quadratic program whose cost function is the squared norm 
    // of the residual, restricted to the flux measurements, plus a
    // regularization term, and a random perturbation on the linear term,
    // in order to randomize the solution.
    // The flux values that have been proposed in the fml file are
    // added as equality constraints and allow to test if
    // the admissible set {[C(1:me,:),Flux_M]*w=[b(1:me),b_Flux]} 
    // and {C(me+1:$,:)*w<=b(me+1,:)} are non-empty.
    
    try
      Q=full(E'*E)+1e-6*eye(n,n)
      p=-full(E'*wmeas)+1e-6*rand(n,1,'normal')
      [wadm,lagr]=qld(Q,p,C,d,[],[],me)
      if user_ff
        M=zeros(n-n1,n)
        M(:,free)=eye(n-n1,n-n1)
        [wadm,lagr]=qld(M'*M,-M'*ffvalues,C,d,[],[],me)
        residual=norm(M*wadm-ffvalues);
        if residual>1e-6
           printf("Residual=%f, user free fluxes have been projected on admissible domain\n",residual) 
        end
      end
    catch
      error('The constraints are inconsistent');
    end
    flux_adm=tlist(['fluxes' 'names' 'values' 'sigma'],flux_ids,wadm,zeros(n,1));
        
endfunction
