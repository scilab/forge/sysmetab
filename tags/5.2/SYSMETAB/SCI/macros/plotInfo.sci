function plotInfo(outputname,confname,resid)
  doc=xmlRead(outputname+'.params.xml');
	params=xmlXPath(doc,'//smtb:params',['smtb','http://www.utc.fr/sysmetab']);
  s=size(params(1).children,"*")
  clf
  for i=1:s
    name=params(1).children(i).attributes.name
    content=params(1).children(i).content
    xstring(0,1-i/s,sprintf("%s=%s",name,content))
  end
  title(sprintf("Network %s.fml, configuration=%s, residual=%f",outputname,confname,resid))
  set(get(gca(),'title'),'font_style',8)
  xmlDelete(doc)
endfunction  
  
