function plotNonStatMeasurements(flux_opt,time_struct,name,sysmetab)

  v=Phi(flux_opt.values,0)
  time_struct_plot=time_discr_from_t(scheme,linspace(0,time_struct.tmeas($),256),time_struct.t);
  auto_scales_ind=[]

  [residual,X,y,y_unscaled,MList,omega,Sypm2_e_label,Y,Mass,err]=solveNSRK(v,m,Xinp,M,b,cum,time_struct_plot,pool_ind_weight,%t);

  t=time_struct_plot.tmeas

  d=driver()
  driver pdf
  scf(0)
  plotInfo(name,sum(label_error)+sum(flux_error)+sum(pool_error))
  xs2pdf(0,sprintf("%s00.pdf",name))
  for i=1:nb_group
      clf
      plot(tmeas,ymeas(meas_ind_group(i),:)',"o")
      plot(t,y(meas_ind_group(i),:)')
      curves=gce()
      a=gca()
      xlabel("time")
      str=tokens(group_textual(i),["[","]"])
      if size(str,"*")==3 // MS measurements
        head=tokens(group_textual(i),"#")
        title(sprintf("%s, residual=%f, scale=%f",head(1),sum(label_error(meas_ind_group(i),:)),scale_opt.values(i)))
        str=tokens(str(3),"M")
        str=tokens(str(2),",")
        for j=1:size(str,"*")
          labels(j)="M+"+str(j)
        end
        legend(curves,labels,-2,%f)
      else // RMN or arbitraty labelling
        str=tokens(group_textual(i),[";"," ",","])
        head=tokens(str(1),"#")
        title(sprintf("%s, residual=%f, scale=%f",head(1),sum(label_error(meas_ind_group(i),:)),scale_opt.values(i)))
        for j=1:size(str,"*")
          label=tokens(str(j),"#")
          labels(j)=label(2) 
        end
        legend(curves,labels,-2,%f)
      end  
      a.data_bounds(3:4)=[0 1]
      a.sub_ticks=[1,1]
      xs2pdf(0,sprintf("%s%02d.pdf",name,i))
  end
  for j=1:size(wmeas,1)
    clf
    wsim=E*(W*q+w0)
    bar(1,[wmeas(j) wsim(j)])
    a=gca()
    a.sub_ticks=[0,1]
    a.x_ticks.labels(1)=fluxmeasurement_textual(j)
    title(sprintf("flux measurement %s, residual=%e",fluxmeasurement_id(j),flux_error(j)))
    legend("measurement","simulation",-2)
    xs2pdf(0,sprintf("%s%02d.pdf",name,i+j))
  end
  for k=1:size(mmeas,1)
    clf
    msim=Em*m
    bar(1,[mmeas(k) msim(k)])
    a=gca()
    a.sub_ticks=[0,1]
    a.x_ticks.labels(1)=poolmeasurement_textual(k)
    title(sprintf("pool measurement %s, residual=%e",poolmeasurement_id(k),pool_error(k)))
    legend("measurement","simulation",-2)
    xs2pdf(0,sprintf("%s%02d.pdf",name,i+j+k))
  end
  driver(d)
  pdftk="pdftk"
  if unix_g("echo $(uname)")=="Darwin"
    pdftk=sysmetab+"/TOOLS/pdftk/bin/pdftk"
  end    
  unix_g(pdftk+" "+name+"??.pdf cat output "+name+".pdf")
  unix_g("rm "+name+"??.pdf")

endfunction
