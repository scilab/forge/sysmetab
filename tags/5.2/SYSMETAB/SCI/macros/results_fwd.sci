function [out]=results_fwd(flux,CONF,RES,message,filename,outputname,SYSMETAB)

    // This script modify step3.xml file in order to save results obtain 
    // with scilab after optimization and transfer them to a fwd file wich
    // contains all details.
    
    // STILL TO BE UPDATED FOR NON STATIONARY CASE !!!!!!!!!!!!!!!!!!!!!!!!
    
    ftypetext.f='free';
    ftypetext.d='dependent';
    ftypetext.c='constraint';
    //
    // Read last step (enriched) fml file
    //
    params=xmlRead(filename+'.params.xml');
    doc=xmlRead(filename+'.step4.xml');
    listOfPools=xmlXPath(doc,'//f:pool',['f','http://www.13cflux.net/fluxml']);
    fluxml=xmlXPath(doc,'//f:fluxml',['f','http://www.13cflux.net/fluxml']);
    //
    // create stoichiometry paragraph which will contain the type and the value of
    // fluxes id.
    //
    nfluxes=size(flux.names,'*')/2;
    //
    // Common stoichiometry element
    //
    stoich = xmlReadStr("<stoichiometry/>");
    //
    for i=1:nfluxes
      f=xmlElement(stoich,"flux");
      f.attributes.id=part(flux.names(i),1:$-2);
      //
      flux_net= xmlElement(stoich,"net");
      xmlSetAttributes(flux_net,['type' ftypetext(ftype(i))
                                 'value' sprintfnumber(flux.values(i))
                                 'stddev' sprintfnumber(flux.sigma(i))] )
      if isfield(flux,"err") & flux.sigma(i) <> 0
        xmlSetAttributes(flux_net,['rerr' sprintfnumber(flux.err(i)/flux.sigma(i))])
      end
      if isfield(flux,"quantiles")
        xmlSetAttributes(flux_net,['min' sprintfnumber(flux.quantiles(i,1))
                                   'max' sprintfnumber(flux.quantiles(i,3))])
      end
      if ftype(i)=='d'
          vect=find(clean(W(i,:)));
          form='';
          if clean(w0(i))<>0
              form=string(w0(i));
          end
          for j=1:length(vect)
              coef=W(i,vect(j));
              coefs='';
              if coef>0 & form<>''
                  coefs='+'
              elseif coef<0
                  coefs='-'
              end
              if clean(abs(coef)-1)
                  coefs=coefs+string(abs(coef))+'*';
              end
              form=form+coefs+part(flux.names(ff(vect(j))),1:$-2)+'.n';
          end
      else
        form=part(flux.names(i),1:$-2)+'.n';;
      end
      flux_net.content=form;
      xmlAppend(f,flux_net);
      flux_xch= xmlElement(stoich,"xch");
      xmlSetAttributes(flux_xch,['type' ftypetext(ftype(nfluxes+i))
                                 'value' sprintfnumber(flux.values(nfluxes+i))
                                 'stddev' sprintfnumber(flux.sigma(nfluxes+i))] )
       if isfield(flux,"err") & flux.sigma(nfluxes+i) <> 0
         xmlSetAttributes(flux_xch,['rerr' sprintfnumber(flux.err(nfluxes+i)/flux.sigma(nfluxes+i))])
       end
       if isfield(flux,"quantiles")
         xmlSetAttributes(flux_xch,['min' sprintfnumber(flux.quantiles(nfluxes+i,1))
                                    'max' sprintfnumber(flux.quantiles(nfluxes+i,3))])
       end

       if ftype(nfluxes+i)=='d'
           vect=find(clean(W(nfluxes+i,:)));
           form='';
           if clean(w0(nfluxes+i))<>0
               form=string(w0(nfluxes+i));
           end
           for j=1:length(vect)
               coef=W(nfluxes+i,vect(j));
               coefs='';
               if coef>0 & form<>''
                   coefs='+'
               elseif coef<0
                   coefs='-'
               end
               if clean(abs(coef)-1)
                   coefs=coefs+string(abs(coef))+'*';
               end
               form=form+coefs+part(flux.names(ff(vect(j))),1:$-2)+'.x';
           end
       else
         form=part(flux.names(nfluxes+i),1:$-2)+'.x';;
       end
       flux_xch.content=form;
        xmlAppend(f,flux_xch);
        xmlAppend(stoich.root,f);
    end
      
    configurations=xmlXPath(doc,'//f:configuration',['f','http://www.13cflux.net/fluxml']);
    
    for iconf=1:length(CONF) // loop on configurations
    
      fwdsim = xmlReadStr("<fwdsim xmlns=""http://www.13cflux.net/fwdsim""></fwdsim>");
      xmlAppend(fwdsim.root,stoich.root);
      simulation=xmlElement(fwdsim,"simulation");
              
      value=xmlElement(fwdsim,"value")
      for i=1:length(listOfPools)
        pool=xmlElement(fwdsim,"pool");
        id=listOfPools(i).attributes.id;
        xmlSetAttributes(pool,["id",id]);
        cumomers=xmlXPath(doc,sprintf("//smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer[@pool=''%s'']",id),...
                                            ['smtb','http://www.utc.fr/sysmetab']);
        if CONF(iconf).stationary=="true" // dump cumomers values only for non-stationary case
          for j=1:length(cumomers)
            xmlSetAttributes(value,["cfg",cumomers(j).attributes.pattern]);
            if info>=0
              value.content=sprintfnumber(RES(iconf).X(evstr(cumomers(j).attributes.number)));
            end
            xmlAppend(pool,value);
          end
        else
          for j=1:length(cumomers)
            xmlSetAttributes(value,["cfg",cumomers(j).attributes.pattern]);
            xmlAppend(pool,value);
          end
        end     
        xmlAppend(simulation,pool);
      end
              
      residual=0
      measurements=xmlElement(fwdsim,"measurements");

      netflux=xmlXPath(doc,sprintf("//f:configuration[%d]//f:fluxmeasurement/f:netflux",iconf),['f','http://www.13cflux.net/fluxml']);

      spec=xmlElement(fwdsim,"spec");
      value=xmlElement(fwdsim,"value");
      for j=1:length(netflux)
        mgroup=xmlElement(fwdsim,"mgroup");
        
        xmlSetAttributes(mgroup,["id"   netflux(j).attributes.id
                                 "type" "NETFLUX"
                                 "norm"  sprintfnumber(RES(iconf).flux_error(j))]);
                                 
        value.content=sprintfnumber(RES(iconf).flux_meas(j))
        spec.content=netflux(j).children(1).content // <textual> element          
        xmlAppend(mgroup,spec)          
        xmlAppend(mgroup,value)          
        xmlAppend(measurements,mgroup)
        residual=residual+RES(iconf).flux_error(j)
      end
      
      LMgroups=xmlXPath(doc,sprintf("//f:configuration[%d]//f:labelingmeasurement/f:group",iconf),['f','http://www.13cflux.net/fluxml']);
      
      spec=xmlElement(fwdsim,"spec");
      for j=1:length(LMgroups)
        mgroup=xmlElement(fwdsim,"mgroup");
        meas_ind=CONF(iconf).meas_ind_group(j)
        xmlSetAttributes(mgroup,["id"    LMgroups(j).attributes.id
                                 "type"  LMgroups(j).attributes.type])
        if info>=0
          xmlSetAttributes(mgroup,["scale" sprintfnumber(RES(iconf).scale(j))
                                   "norm"  sprintfnumber(sum(RES(iconf).label_error(meas_ind,:)))])
        end
        spec.content=LMgroups(j).children(1).content // <textual> element          
        xmlAppend(mgroup,spec)                
        
        if info>=0
          if CONF(iconf).stationary=="true" // stationary measurements
            time=xmlElement(doc,"time")
            xmlSetAttributes(time,["value" sprintfnumber(%inf)])
            for k=1:length(meas_ind) // for each value in the group
                value=xmlElement(doc,"value")
                value.content=sprintfnumber(RES(iconf).y(meas_ind(k)));
                xmlAppend(time,value)
            end      
            xmlAppend(mgroup,time)
          else // non stationary measurements
          end  
          residual=residual+sum(RES(iconf).label_error(meas_ind,:))
        end
        
        xmlAppend(measurements,mgroup)
      end
      
      xmlSetAttributes(measurements,["residual" sprintfnumber(residual)])
      xmlSetAttributes(simulation,["configuration" CONF(iconf).name])
      
      xmlAppend(fwdsim.root,simulation);
      xmlAppend(fwdsim.root,measurements);
      
      optimization=xmlElement(fwdsim,"optimization")
      param=xmlElement(fwdsim,"param")
      for i=1:length(params.root.children)
        xmlSetAttributes(param,["name" params.root.children(i).attributes.name
                               "type" params.root.children(i).attributes.type])
        param.content=params.root.children(i).content
        xmlAppend(optimization,param)
      end

      xmlAppend(fwdsim.root,optimization);
      fwdname=outputname+"_"+configurations(iconf).attributes.name+".fwd";
      xmlWrite(fwdsim,fwdname);
      xmlDelete(fwdsim);
      
    end // loop on configurations

    // report generated from the last configuration (does not depend on measurements)
    unix(sprintf("xsltproc %s/XSL/fmlsys_gen/fwd_report.xsl %s",SYSMETAB,fwdname));

    xmlDelete(doc);
    out=1;
endfunction


function y=sprintfnumber(number)
    if floor(number)==number then
        y=sprintf('%g',number);
    else
        y=sprintf('%.16g',clean(number));
    end
endfunction
