function out=tracing(output,ncum)

  sources=s_sparse(output+".network/sources.txt",ncum,1);
  adj=s_sparse(output+".network/adj.txt",ncum,ncum);
  x=full(sources);
  for i=1:10000
    prevx=x;
    x(adj*x>0)=1;
    if norm(x-prevx)==0
     break
    end
  end
  del=find(x==0)

  doc=xmlRead(output+".step2.xml");
  cum=xmlXPath(doc,"//smtb:cumomer",["smtb","http://www.utc.fr/sysmetab"]);

  for k=1:length(del)
    xmlRemove(cum(del(k)));
  end

  xmlWrite(doc,output+".step2.xml");
  
  out=0
endfunction
