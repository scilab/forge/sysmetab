function [flux,RES,dYdp]=measurements_and_stats(CONF,q,m,eps_phi,info,task,conflevel)
    
	// This function return the standard deviation of w=W*q+w0
	// The actual number of "free" parameters depend on the saturation of constraints
	// at the optimum.
  
  nff=size(q,1)
  nfluxes=size(W,1)
  nsim=size(q,2)-1;
  w=W*q(:,1)+w0; 	// get the net/xch flux vector

  for i=1:length(CONF)
    RES(i).flux_meas=CONF(i).E*w;
    e_flux=RES(i).flux_meas-CONF(i).wmeas;
    RES(i).flux_error=CONF(i).Svpm2.*e_flux.^2;
  end

  alpha=(1-conflevel)/2

  if info>=0

  	// Computation of the fwd/rev flux vector

  	[v,dv_dw]=Phi(w,eps_phi);
    
    if isempty(m) // stationnary configuration

      // Solve the state equation, then compute the label observation residual and gradient

      dYdp=[];Syv=[];
      MList=factorizeStateMatrices(v,M,cum);
      for i=1:length(CONF)   
        [RES(i).label_error,RES(i).X,RES(i).y,dy_dq,RES(i).scale]=solveStatAndDerivative(v,MList,b,cum,dv_dw,W,CONF(i));
        // Here, we denote by Y the full observation (labeling and fluxes) [y(Phi(W*q+w0));E*(W*q+w0)]
        // The derivative below is made with respect to free parameter q
    
        dYdp=[dYdp;dy_dq;CONF(i).E*W];
        Syv=[Syv;CONF(i).Sypm2;CONF(i).Svpm2]
      end
      freeStateMatrices(MList)
      	  
      if task=="mc" // in case of Monte Carlo stats
        values=gsort(W*q(:,2:$)+w0(:,ones(1,nsim)),'c')

        if (nsim*alpha)>1 // compute quantiles
          flux=tlist(['fluxes','names','values','sigma','quantiles'],flux_ids,w,zeros(w),zeros(w));
          flux.quantiles=[values(:,nsim*(1-alpha)) median(values,'c') values(:,nsim*alpha)]
        else
          flux=tlist(['fluxes','names','values','sigma'],flux_ids,w,zeros(w));
        end
    		flux.sigma=stdev(W*q(:,2:$),2)       
    	else
        flux=tlist(['fluxes','names','values','sigma'],flux_ids,w,zeros(w));  
    	          
        G=spdiag(sqrt(Syv))*dYdp;

        [U,S,V]=svd(G) // smart way of computing (F+reg*I)^-1 where F=G'*G
        s2=diag(S'*S)        
        reg=%eps
        //C=V*diag(1../(s2+reg))*V' // computation is improved below by calculating diagonal terms directly
        //flux.sigma=sqrt(abs(diag(W*C*W')));            
        flux.sigma=sqrt(sum(((W*V).^2)*diag(sparse(1../(s2+reg))),2))
        flux.sigma(flux.sigma>1/sqrt(reg)/10)=%inf
      end 
    else // non-stationnary configuration
      npool=size(m,1) 
      pool_meas=Em*m(:,1)
      e_pool=pool_meas-mmeas;
      pool_error=Smpm2.*e_pool.^2;

      if task=="mc" // in case of Monte Carlo stats
              
        [label_error,X,y,y_unscaled,MList,omega]=scheme.solveNS(v,m,Xinp,M,b,cum,time_struct,pool_ind_weight,%t)
        
        if (nsim*alpha)>1 // compute quantiles
          flux=tlist(['fluxes','names','values','sigma','quantiles'],flux_ids,w,zeros(w),zeros(w));
          pool=tlist(['pools','names','values','sigma','quantiles'],[],m(:,1),zeros(npool,1),zeros(npool,1));
          values=gsort(W*q(:,2:$)+w0(:,ones(1,nsim)),'c')
          flux.quantiles=[values(:,nsim*(1-alpha)) median(values,'c') values(:,nsim*alpha)]
          values=gsort(m(:,2:$),'c')
          pool.quantiles=[values(:,nsim*(1-alpha)) median(values,'c') values(:,nsim*alpha)]
        else
          flux=tlist(['fluxes','names','values','sigma'],flux_ids,w,zeros(w));
          pool=tlist(['pools','names','values','sigma'],[],m(:,1),zeros(npool,1));
        end
        flux.sigma=stdev(W*q(:,2:$),'c')       
        pool.sigma=stdev(m(:,2:$),'c')
             
      else
        if task=="extra" // error estimation (Richardson)
        
          flux=tlist(['fluxes','names','values','sigma','err'],flux_ids,w,zeros(w),zeros(w));
          pool=tlist(['pools','names','values','sigma','err'],[],m(:,1),zeros(npool,1),zeros(npool,1));
          
          flux.err=abs(W*q*[1;-1]/(.5^scheme.order-1))
          pool.err=abs(m*[1;-1]/(.5^scheme.order-1))
        else
          flux=tlist(['fluxes','names','values','sigma'],flux_ids,w,zeros(w));
          pool=tlist(['pools','names','values','sigma'],[],m(:,1),zeros(npool,1));
        end

        [label_error,X,y,dy_dp,omega]=scheme.solveNSDer(v,m(:,1),Xinp,M,b,cum,time_struct,pool_ind_weight,dP_dpool,dv_dw,W);
        pool_meas=Em*m(:,1)
        e_pool=pool_meas-mmeas;
        pool_error=Smpm2.*e_pool.^2;

        dYdp=[dy_dp
              E*W zeros(size(wmeas,1),size(m,1))
              zeros(size(mmeas,1),size(q,1)) full(Em)]
                    
        G=spdiag(sqrt([Sypm2(:);Svpm2;Smpm2]))*dYdp; // Sypm2 is also "vectorized" (see yq() and ym() above)

        [U,S,V]=svd(G) // smart way of computing (F+reg*I)^-1 where F=G'*G
        s2=diag(S'*S)        
        reg=%eps

        //C=V*diag(1../(s2+reg))*V' // computation is improved below by calculating diagonal terms directly
        Wqm=[W                zeros(nfluxes,npool);
             zeros(npool,nff) eye(npool,npool)]
        sigma=sqrt(sum(((Wqm*V).^2)*diag(sparse(1../(s2+reg))),2))
        sigma(sigma>1/sqrt(reg)/10)=%inf

        flux.sigma=sigma(1:nfluxes)           
        pool.sigma=sigma(nfluxes+1:$)        
        pool.sigma(m_type=="c")=0        
      end 
    end         
  else
    flux=tlist(['fluxes','names','values','sigma'],flux_ids,w,zeros(w));
  end
  	
endfunction
