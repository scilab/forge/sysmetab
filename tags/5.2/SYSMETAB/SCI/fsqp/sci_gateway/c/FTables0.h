#include "machine.h"

#define OK 1
#define FAIL 0

typedef void (*voidf)();

typedef struct {
  char *name;
  voidf f;
} FTAB;

#define MAXNAME 32
static char buf[MAXNAME];

/***********************************
 * fsqp  (ffsqpobj)
 ***********************************/

#if defined(__STDC__)
#define ARGS_ffsqpobj int, int,double *,double *,void *
typedef void (*ffsqpobjf)(ARGS_ffsqpobj);
#else
#define ARGS_ffsqpobj /**/
typedef void (*ffsqpobjf)();
#endif 


/***********************************
 * fsqp  (ffsqpcntr)
 ***********************************/

#if defined(__STDC__)
#define ARGS_ffsqpcntr int, int,double *,double *,void *
typedef void (*ffsqpcntrf)(ARGS_ffsqpcntr);
#else
#define ARGS_ffsqpcntr /**/
typedef void (*ffsqpcntrf)();
#endif 


/***********************************
 * fsqp  (ffsqpgrob)
 ***********************************/

#if defined(__STDC__)
#define ARGS_ffsqpgrob int, int,double *,double *,void (* dummy)(),void *
typedef void (*ffsqpgrobf)(ARGS_ffsqpgrob);
#else
#define ARGS_ffsqpgrob /**/
typedef void (*ffsqpgrobf)();
#endif 


/***********************************
 * fsqp  (ffsqpgrcn)
 ***********************************/

#if defined(__STDC__)
#define ARGS_ffsqpgrcn int, int,double *,double *,void (* dummy)(),void *
typedef void (*ffsqpgrcnf)(ARGS_ffsqpgrcn);
#else
#define ARGS_ffsqpgrcn /**/
typedef void (*ffsqpgrcnf)();
#endif 

