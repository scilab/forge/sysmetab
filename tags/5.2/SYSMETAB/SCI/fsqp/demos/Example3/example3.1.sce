mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 3 of the fsqp documentation



//[1] fsqp with Scilab functions
//=======================================================
//the objective function
function fj=obj71(j,x)
  fj=x(1)*x(4)*sum(x(1:3))+x(3)
endfunction

function gradfj=grob71(j,x)
  s=sum(x(1:3));p=x(1)*x(4);
  gradfj=[x(4)*s+p,p,1+p,x(1)*s];
endfunction

//the constraints
function gj=cntr71(j,x)
  select j
   case 1
    gj=25-prod(x);
   case 2
    gj=norm(x)^2-40;
  end
endfunction

function gradgj=grcn71(j,x)
  select j
   case 1
    gradgj=-(prod(x)./x)';
   case 2
    gradgj=2*x';
  end
endfunction

modefsqp=100;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=7.e-6;
udelta=0.e0;
nf=1;
neqn=1;
nineqn=1;
nineq=1;
neq=1;

x0=[1;5;5;1];

bl=ones(x0);
bu=5*bl;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];



//Scilab functions obj71,cntr71,grob71,grcn71 are called by srfsqp:
srpar=[0,0,0];mesh_pts=[];
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj71,cntr71,grob71,grcn71)
