<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Fri Mar 14 20:00:00 CET 2014
    Projet  :   PIVERT/Metalippro-PL1
-->

<!--
    fml2ftbl
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:exslt="http://exslt.org/common"
  xmlns:str="http://exslt.org/strings"   
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl fwd str">

  <!--<xsl:variable name="tab" select="'&#09;'"/>-->
  <xsl:variable name="tab" select="';'"/>
  <xsl:variable name="cr" select="'&#10;'"/>
  <xsl:variable name="quot" select="'&quot;'"/>
  <xsl:variable name="configuration" select="fwd:fwdsim/fwd:simulation/@configuration"/>
  <xsl:variable name="data" select="document(concat(fwd:fwdsim/fwd:optimization/fwd:param[@name='output'],'.fml'),/)/f:fluxml/f:configuration[@name=$configuration]/f:measurement/f:data"/>
  <xsl:variable name="reactionnetwork" select="document(concat(fwd:fwdsim/fwd:optimization/fwd:param[@name='output'],'.fml'),/)/f:fluxml/f:reactionnetwork"/>

  <xsl:output method="text" indent="yes" encoding="ISO-8859-1"/>

  <func:function name="smtb:abs">
    <xsl:param name="x"/>
    <func:result>
      <xsl:value-of select="$x*(1-2*($x&lt;0))"/>
    </func:result>
  </func:function>

  
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="fwd:fwdsim">
    <xsl:apply-templates select="fwd:stoichiometry"/>
    <xsl:apply-templates select="fwd:simulation"/>
    <xsl:apply-templates select="fwd:measurements"/>
    <!--<xsl:apply-templates select="fwd:simulation"/>-->
  </xsl:template>

  <xsl:template match="fwd:simulation">
    <xsl:value-of select="concat('METABOLITE_POOLS',$cr)"/>
    <xsl:value-of select="concat($tab,'META_NET',$tab,'VALUE',$tab,'STDDEV',$tab,'RSTDDEV',$tab,'LB',$tab,'UB',$cr)"/>
    <xsl:for-each select="fwd:pool[@value]">
      <xsl:variable name="rsd">
        <xsl:if test="@value != 0">
          <xsl:value-of select="@stddev div (@value*(1-2*(@value &lt; 0)))"/>
        </xsl:if>
      </xsl:variable>
      <xsl:value-of select="concat($tab,@id,$tab,translate(@value,'.',','),$tab,translate(@stddev,'.',','),$tab,translate($rsd,'.',','),$tab,translate(@min,'.',','),$tab,translate(@max,'.',','),$cr)"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="fwd:stoichiometry">
    <xsl:value-of select="concat('FLUXES',$cr)"/>
    <xsl:value-of select="concat($tab,'NET',$tab,'REACTION',$tab,'ID',$tab,'FCD',$tab,'VALUE',$tab,'STDDEV',$tab,'RSTDDEV',$tab,'LB',$tab,'UB',$cr)"/>
    <xsl:for-each select="fwd:flux/fwd:net">
      <!--<xsl:sort select="@stddev div number(translate(@value,'-',''))" data-type="number" order="ascending"/>-->
      <xsl:variable name="rsd">
        <xsl:if test="@value != 0">
          <xsl:value-of select="@stddev div (@value*(1-2*(@value &lt; 0)))"/>
        </xsl:if>
      </xsl:variable>
      <xsl:variable name="reaction">
        <xsl:for-each select="exslt:node-set($reactionnetwork)/f:reaction[@id=current()/../@id]/f:reduct/@id">
          <xsl:value-of select="."/>
          <xsl:if test="position()&lt;last()">+</xsl:if>
        </xsl:for-each>
        <xsl:text> -&gt; </xsl:text>
        <xsl:for-each select="exslt:node-set($reactionnetwork)/f:reaction[@id=current()/../@id]/f:rproduct/@id">
          <xsl:value-of select="."/>
          <xsl:if test="position()&lt;last()">+</xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat($tab,$tab,$quot,$reaction,$quot,$tab,../@id,$tab,@type,$tab,translate(@value,'.',','),$tab,translate(@stddev,'.',','),$tab,translate($rsd,'.',','))"/>
      <xsl:value-of select="concat($tab,translate(@min,'.',','),$tab,translate(@max,'.',','))"/>
      <xsl:value-of select="concat($tab,.,$cr)"/>
    </xsl:for-each>
    <xsl:value-of select="concat($cr,$tab,'XCH',$tab,'REACTION',$tab,'ID',$tab,'FCD',$tab,'VALUE',$tab,'STDDEV',$tab,'RSTDDEV',$tab,'LB',$tab,'UB',$cr)"/>
    <xsl:for-each select="fwd:flux/fwd:xch">
      <!--<xsl:sort select="@stddev div number(translate(@value,'-',''))" data-type="number" order="ascending"/>-->
      <xsl:variable name="rsd">
        <xsl:if test="@value != 0">
          <xsl:value-of select="@stddev div (@value*(1-2*(@value &lt; 0)))"/>
        </xsl:if>
      </xsl:variable>
      <xsl:variable name="reaction">
        <xsl:for-each select="exslt:node-set($reactionnetwork)/f:reaction[@id=current()/../@id]/f:reduct/@id">
          <xsl:value-of select="."/>
          <xsl:if test="position()&lt;last()">+</xsl:if>
        </xsl:for-each>
        <xsl:text> -&gt; </xsl:text>
        <xsl:for-each select="exslt:node-set($reactionnetwork)/f:reaction[@id=current()/../@id]/f:rproduct/@id">
          <xsl:value-of select="."/>
          <xsl:if test="position()&lt;last()">+</xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat($tab,$tab,$quot,$reaction,$quot,$tab,../@id,$tab,@type,$tab,translate(@value,'.',','),$tab,translate(@stddev,'.',','),$tab,translate($rsd,'.',','))"/>
      <xsl:value-of select="concat($tab,translate(@min,'.',','),$tab,translate(@max,'.',','))"/>
      <xsl:value-of select="concat($tab,.,$cr)"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="fwd:measurements">
    
    <xsl:value-of select="concat($cr,'FLUX_MEASUREMENTS',$cr,$tab,'NAME',$tab,'FORMULA',$tab,'VALUE',$tab,'MEASUREMENT',$tab,'STDDEV',$tab,$tab,'RESIDUAL',$cr)"/>
    <xsl:for-each select="fwd:mgroup[not(contains(fwd:spec,'#'))]">
      <xsl:value-of select="concat($tab,@id,$tab,$quot,fwd:spec,$quot,$tab,translate(fwd:value,'.',','),$tab,translate(exslt:node-set($data)/f:datum[@id=current()/@id],'.',','),$tab,translate(exslt:node-set($data)/f:datum[@id=current()/@id]/@stddev,'.',','),$tab,$tab,translate(@norm,'.',','),$cr)"/>
    </xsl:for-each>
    
    
    <xsl:value-of select="concat($cr,'MASS_SPECTROMETRY',$cr,$tab,'META_NAME',$tab,'FRAGMENT',$tab,'TIME',$tab,'WEIGHT',$tab,'VALUE',$tab,'MEASUREMENT',$tab,'STDDEV',$tab,'RESIDUAL',$cr)"/>
    <xsl:for-each select="fwd:mgroup[contains(fwd:spec,'#M')]">
      <xsl:variable name="group" select="@id"/>
      <xsl:variable name="weights" select="str:split(substring-after(fwd:spec,'#M'),',')"/>
      <xsl:value-of select="concat($cr,$tab,substring-before(fwd:spec,'['),$tab,$quot,substring-before(substring-after(fwd:spec,'['),']'),$quot,$tab)"/>
      <xsl:for-each select="fwd:time">
        <xsl:value-of select="concat('time=',@value,$tab)"/> <!-- time -->
        <xsl:for-each select="fwd:value">
          <xsl:variable name="pos" select="position()"/>
          <xsl:variable name="weight" select="exslt:node-set($weights)[$pos]"/>        
          <xsl:value-of select="concat($weight,$tab,translate(.,'.',','),$tab,translate(exslt:node-set($data)/f:datum[(@id=$group) and (@weight=$weight)],'.',','),$tab,translate(exslt:node-set($data)/f:datum[(@id=$group) and (@weight=$weight)]/@stddev,'.',','),$cr)"/>
          <xsl:if test="position()&lt;last()">
            <xsl:value-of select="concat($tab,$tab,$tab,$tab)"/>
          </xsl:if>
        </xsl:for-each>
        <xsl:if test="position()&lt;last()">
          <xsl:value-of select="concat($tab,$tab,$tab)"/>
        </xsl:if>
      </xsl:for-each>
      <xsl:value-of select="concat($tab,$tab,$tab,$tab,$tab,$tab,$tab,$tab,translate(@norm,'.',','),$cr)"/>
    </xsl:for-each>    
      
    <xsl:value-of select="concat($cr,'LABEL_MEASUREMENTS',$cr,$tab,'META_NAME',$tab,'CUM_GROUP',$tab,'CUM_CONSTRAINTS',$tab,'VALUE',$cr)"/>
    <xsl:for-each select="fwd:mgroup[contains(fwd:spec,'#') and not(contains(fwd:spec,'#M'))]">
      <xsl:sort data-type="text" select="substring-before(fwd:spec,'#')"/>
      <xsl:variable name="group" select="@id"/>
      <xsl:value-of select="concat($cr,$tab,substring-before(fwd:spec,'#'),$tab,$quot,substring-before(substring-after(@id,'_'),'_'),$quot,$tab)"/>
      <xsl:for-each select="fwd:time/fwd:value">
        <xsl:variable name="pos" select="position()"/>
        <xsl:value-of select="concat($quot,substring-after(str:split(../../fwd:spec,';')[$pos],'#'),$quot,$tab,translate(.,'.',','),$tab,translate(exslt:node-set($data)/f:datum[(@id=$group) and (@row=$pos)],'.',','),$tab,translate(exslt:node-set($data)/f:datum[(@id=$group) and (@row=$pos)]/@stddev,'.',','),$cr)"/>
        <xsl:if test="position()&lt;last()">
          <xsl:value-of select="concat($tab,$tab,$tab)"/>
        </xsl:if>
      </xsl:for-each>
      <xsl:value-of select="concat($tab,$tab,$tab,$tab,$tab,$tab,$tab,translate(@norm,'.',','),$cr)"/>
    </xsl:for-each>


    <xsl:value-of select="concat($cr,$tab,$tab,$tab,$tab,$tab,'SUM(RESIDUAL)',$tab,$tab,@residual)"/>

  </xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>
