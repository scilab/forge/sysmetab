<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- Cette feuille de style a pour but de générer différentes fonctions spécifiques
  d'un modèle donné sous forme FML -->

<!-- THIS STYLESHEET HAS TO BE CUT IN PIECES !!!!!!!!! -->


<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- Output file name -->
  <xsl:param name="output"/>
	
  <xsl:param name="language">scilab</xsl:param>
  <xsl:param name="storage">1</xsl:param>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

	<xsl:variable name="npool" select="count(//f:reactionnetwork/f:metabolitepools/f:pool)"/>
	<xsl:variable name="nX" select="1+count(//smtb:listOfIntermediateCumomers//smtb:cumomer | //smtb:listOfInputCumomers//smtb:cumomer)"/>
	<xsl:variable name="maxweight" select="math:max(//smtb:listOfIntermediateCumomers/smtb:listOfCumomers/@weight)"/>
  <xsl:variable name="freefluxes" select="exslt:node-set($params)[@name='freefluxes']"/>
  <xsl:variable name="nv" select="count(//f:reactionnetwork/f:reaction)"/>
  <xsl:variable name="np" select="count(//f:reactionnetwork/f:metabolitepools/f:pool)"/>

  <!-- Mathml generation of the code, this templates is common between stationnary case or dynamical one -->
  
  <!-- find all label input -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>
  
  <!-- find all f:pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>
  
  <!-- find all f:rproduct from forward f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

  <!-- find all f:reduct from forward f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:reaction -->
  <xsl:key name="REACTION" match="f:reaction" use="@id"/>
  
  <!-- find all f:datum -->
  <xsl:key name="CONFIG_DATUM_ROW_WEIGHT" match="f:datum" use="concat(../../../@name,'_',@id,'_',@row,@weight)"/>
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <!-- find all Mass Spectrometry (MS) -->
  <xsl:key name="MS" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'[')"/>

  <!-- find all Label Measurement (LM) from f:configuration -->
  <xsl:key name="LM" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'#')"/>

  <!-- find all f:poolmeasurement/f:textual -->
  <xsl:key name="poolmeasurement" match="f:poolsize" use="f:textual"/>

  <!-- find all smtb:cumomer from smtb:listOfInputCumomers -->
  <xsl:key name="INPUT_CUMOMER" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
  
  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <struct name="model" xmlns="http://www.utc.fr/sysmetab">
      <field name="network">
        <xsl:apply-templates select="f:reactionnetwork"/>
      </field>
      <field name="constraints">
        <xsl:apply-templates select="f:constraints"/>
      </field>
      <field name="configuration">
        <xsl:apply-templates select="f:configuration"/>
      </field>
    </struct>
    <struct name="params" xmlns="http://www.utc.fr/sysmetab">
      <xsl:apply-templates select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>
    </struct>
  </xsl:template>

  <xsl:template match="smtb:param[@type='string']">    
    <field name="{@name}" xmlns="http://www.utc.fr/sysmetab">
      <string>
        <xsl:value-of select="."/>
      </string>
    </field>
  </xsl:template>    

  <xsl:template match="smtb:param[@type='number']">    
    <field name="{@name}" xmlns="http://www.utc.fr/sysmetab">
      <cn>
        <xsl:value-of select="."/>
      </cn>
    </field>
  </xsl:template>    
  
  <xsl:template match="f:configuration">    
    <struct  xmlns="http://www.utc.fr/sysmetab">      
      <xsl:for-each select="@*">
        <field name="{name()}">
          <string>
            <xsl:value-of select="."/>
          </string>
        </field>
      </xsl:for-each>
      <xsl:call-template name="construct_Xinp"/>    <!-- cannot use xsl:apply-templates here ... -->
      <xsl:apply-templates select="*[not(self::f:input)]"/>
    </struct>
  </xsl:template>
    
  <xsl:template match="f:reactionnetwork">

    <struct xmlns="http://www.utc.fr/sysmetab">

  	  <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>dP_dpool</ci>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>gather</ci></fn>
          <smtb:string>
            <xsl:value-of select="concat($output,'.network/dP_dpool_m.txt')"/>
          </smtb:string>
          <cn><xsl:value-of select="$nX"/></cn>
          <cn><xsl:value-of select="$npool"/></cn>
  				<cn><xsl:value-of select="$storage"/></cn>
  			</apply>
      </apply>
    
    	<comment xmlns="http://www.utc.fr/sysmetab">cum(i) contains the indices of weight i cumomers</comment>

  	  <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>cum</ci>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  			  <fn><ci>list</ci></fn>      
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
            <cn><xsl:value-of select="concat('$-',number(($nX)-@first),':$-',number(($nX)-@last))"/></cn>
          </xsl:for-each>
        </apply>
      </apply>
    
      <!-- Building matrices of fluxes names and ids -->
      <!-- Matrix of metabolite names -->

      <field name="metabolite_names">
        <list separator=";" xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:for-each select="f:metabolitepools/f:pool">
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select='@id'/>
            </string>
          </xsl:for-each>
        </list>
      </field>
    
      <!-- Matrix of fluxes names -->
      <field name="flux_names">
        <list separator=";"  xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:for-each select="f:reaction">
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="concat(@id,'.n')"/>
            </string>
          </xsl:for-each>
          <xsl:for-each select="f:reaction">
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="concat(@id,'.x')"/>
            </string>
          </xsl:for-each>
        </list>
      </field>

  	  <field name="cum" >
        <ci>cum</ci>
      </field>      
        
  	  <field name="dP_dpool" >
        <ci>dP_dpool</ci>
      </field>
    
      <!-- index matrix allowing to form (d(P(m)*Z)/dm)^t as spset(dP_dpool_t.value,Z(dP_dpool_t.m1)) -->

    	<comment xmlns="http://www.utc.fr/sysmetab"> index matrix allowing to form (d(P(m)*Z)/dm)^t as spset(dP_dpool_t.value,Z(dP_dpool_t.m1))</comment>
    
  	  <field name="dP_dpool_t" >
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>gather</ci></fn>
          <smtb:string>
            <xsl:value-of select="concat($output,'.network/dP_dpool_t_m.txt')"/>
          </smtb:string>
          <cn><xsl:value-of select="$npool"/></cn>
          <cn><xsl:value-of select="$nX"/></cn>
  				<cn><xsl:value-of select="$storage"/></cn>
  			</apply>
  		</field>

    	<comment xmlns="http://www.utc.fr/sysmetab">pool_ind_weight(i) contains the pool numbers of weight i cumomers</comment>

      <field name="pool_ind_weight" >
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>list</ci></fn>      
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
            <apply>
              <selector/>
    					<ci type="vector">dP_dpool.ij</ci>
              <apply>
                <selector/>
                <ci type="vector">cum</ci>
                <cn><xsl:value-of select="@weight"/></cn>
              </apply>
            </apply>
          </xsl:for-each>
        </apply>
      </field>

      <field name="M">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>list</ci></fn>      
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
            <xsl:variable name="nc" select="number(@last)-number(@first)+1"/>
      			<apply>
      				<fn><ci>gather</ci></fn>
              <smtb:string>
                <xsl:value-of select="concat($output,'.network/M',@weight,'_m.txt')"/>
              </smtb:string>
              <cn><xsl:value-of select="$nc"/></cn>
              <cn><xsl:value-of select="$nc"/></cn>
      				<cn><xsl:value-of select="$storage"/></cn>
      			</apply>
          </xsl:for-each>
        </apply>
      </field>

      <field name="Mt">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>list</ci></fn>      
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
            <xsl:variable name="nc" select="number(@last)-number(@first)+1"/>
      			<apply>
      				<fn><ci>gather</ci></fn>
              <smtb:string>
                <xsl:value-of select="concat($output,'.network/Mt',@weight,'_m.txt')"/>
              </smtb:string>
              <cn><xsl:value-of select="$nc"/></cn>
              <cn><xsl:value-of select="$nc"/></cn>
      				<cn><xsl:value-of select="$storage"/></cn>
      			</apply>
          </xsl:for-each>
        </apply>
      </field>

      <field name="b">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>list</ci></fn>      
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
            <xsl:variable name="nc" select="number(@last)-number(@first)+1"/>
            <apply>
    				<fn><ci>gather</ci></fn>
            <smtb:string>
              <xsl:value-of select="concat($output,'.network/b',@weight,'_m.txt')"/>
            </smtb:string>
            <cn><xsl:value-of select="$nc"/></cn>
            <cn>1</cn>
    				<cn><xsl:value-of select="$storage"/></cn>
    			</apply>
          </xsl:for-each>
        </apply>
      </field>


      <field name="db_dx">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>list</ci></fn>      
          <vector/>
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
            <xsl:if test="@weight&gt;1">
              <xsl:variable name="id" select="concat('db',@weight,'_dx')"/>
        			<apply>
        				<fn><ci>gather</ci></fn>
                <smtb:string>
                  <xsl:value-of select="concat($output,'.network/',$id,'_m.txt')"/>
                </smtb:string>
                <cn><xsl:value-of select="number(@last)-number(@first)+1"/></cn>
                <cn><xsl:value-of select="$nX"/></cn>
        				<cn><xsl:value-of select="$storage"/></cn>
        			</apply>
            </xsl:if>
          </xsl:for-each>
        </apply>
      </field>

      <field name="db_dx_t">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>list</ci></fn>      
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
            <xsl:if test="@weight&lt;$maxweight">
              <xsl:variable name="id" select="concat('db_dx',@weight,'t')"/>
        			<apply>
        				<fn><ci>gather</ci></fn>
                <smtb:string>
                  <xsl:value-of select="concat($output,'.network/',$id,'_m.txt')"/>
                </smtb:string>
                <cn><xsl:value-of select="number(@last)-number(@first)+1"/></cn>
                <cn><xsl:value-of select="$nX"/></cn>
        				<cn><xsl:value-of select="$storage"/></cn>
        			</apply>    
            </xsl:if>
          </xsl:for-each>
        </apply>
      </field>

  		<comment xmlns="http://www.utc.fr/sysmetab">Other derivatives</comment>

      <field name="dgdv">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>gather</ci></fn>
          <smtb:string>
            <xsl:value-of select="concat($output,'.network/dgdv_m.txt')"/>
          </smtb:string>
          <cn><xsl:value-of select="$nX"/></cn>
          <cn><xsl:value-of select="2*$nv"/></cn>
  				<cn><xsl:value-of select="$storage"/></cn>
  			</apply>
  		</field>
      
      <field name="dgdvt">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
  				<fn><ci>gather</ci></fn>
          <smtb:string>
            <xsl:value-of select="concat($output,'.network/dgdvt_m.txt')"/>
          </smtb:string>
          <cn><xsl:value-of select="2*$nv"/></cn>
          <cn><xsl:value-of select="$nX"/></cn>
  				<cn><xsl:value-of select="$storage"/></cn>
  			</apply>
  		</field>
      
      <xsl:apply-templates mode="stoichiometric-matrix"/>
      
      <field name="N">
        <ci>N</ci>
      </field>
  
    </struct> <!-- network -->

  </xsl:template>
  
  <!-- Building input matrices, allows to obtain the contribution on the cumomers of isotopomers in input. -->
  
  <xsl:template name="construct_Xinp">

    <comment xmlns="http://www.utc.fr/sysmetab">Input Matrices</comment>

    <optimize xmlns="http://www.utc.fr/sysmetab">
    <!-- We open an item "optimize" in which we will gradually build the matrices D1,D2,...,Dn where "n" is the
	maximum weight, determined in preliminary transformations based on selected observations. -->
      <matrix-open id="D" 
                   cols="{1+count(f:input/f:label)}" 
                   rows="{$nX}" 
                   assignments="unique"
                   type="sparse"/>
        <!-- For each cumomer of an input metabolite, we determine what are the contributions of severals input 
        isotopomers. -->
        <matrix-assignment id="D" row="1" col="1">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
        </matrix-assignment> <!-- for the 0 weight cumomer, always equal to 1-->
 
        <xsl:for-each select="f:input/f:label">
          <xsl:variable name="labelnum" select="1+count(preceding::f:label[../../@name=current()/../../@name])"/>
          <xsl:for-each select="smtb:cumomer-contribution">
            <xsl:if test="key('INPUT_CUMOMER',concat(../../@pool,'_',@subscript))">
              <matrix-assignment id="D" row="{key('INPUT_CUMOMER',concat(../../@pool,'_',@subscript))/@number}" col="{1+$labelnum}">
                <xsl:value-of select="@sign"/>
              </matrix-assignment>
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>

        <matrix-close id="D"/>
    </optimize>
    
    <comment xmlns="http://www.utc.fr/sysmetab">Input cumomers, computed from isotopomers of input metabolites.</comment>

    <xsl:for-each select="f:input">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
        <list separator=";">
          <xsl:for-each select="f:label">
            <cn>
              <xsl:value-of select="@value"/>
            </cn>
          </xsl:for-each>
        </list>
      </apply>    
    </xsl:for-each>

    <field name="Xinp" xmlns="http://www.utc.fr/sysmetab">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <times/>
        <ci>D</ci>
        <vector>
  				<ci>1</ci>
          <xsl:for-each select="f:input">
            <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
          </xsl:for-each>
        </vector>
      </apply>
    </field>
            
  </xsl:template>

  <xsl:template name="Cx">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>Cx</ci>
      <apply>
        <fn><ci>s_sparse</ci></fn>
        <smtb:string>
          <xsl:value-of select="concat($output,'.network/Cx_ijv_',../../../@name,'.txt')"/>
        </smtb:string>
        <cn><xsl:value-of select="count(f:group/smtb:index)"/></cn>
        <cn><xsl:value-of select="$nX"/></cn>
      </apply>
    </apply>
    
    <field name="Cx" xmlns="http://www.utc.fr/sysmetab">
      <ci>Cx</ci>
    </field>
    
    <field name="Cxt" xmlns="http://www.utc.fr/sysmetab">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <transpose/>
        <ci>Cx</ci>
      </apply>
    </field>
  </xsl:template>

  <xsl:template match="f:measurement">
    <field name="measurement" xmlns="http://www.utc.fr/sysmetab">
      <struct>
        <xsl:apply-templates/>
      </struct>
    </field>
  </xsl:template>

  <xsl:template match="f:model">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:labelingmeasurement[../../../@stationary='false']">
    <xsl:variable name="config" select="../../../@name"/>
    <field name="labeling" xmlns="http://www.utc.fr/sysmetab">
      <struct>   
        <!-- meas and std vectors (non stationary measurements) -->
        <xsl:call-template name="Cx"/>
        <field name="tmeas">
          <vector xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:for-each select="smtb:times/smtb:time">
              <cn>
                <xsl:value-of select="."/>
              </cn>
            </xsl:for-each>
          </vector>
        </field>
        <field name="meas" xmlns="http://www.utc.fr/sysmetab">
          <matrix xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:for-each select="f:group">
              <xsl:for-each select="smtb:index">
                <xsl:variable name="keyvalue">
                  <xsl:value-of select="concat($config,'_',../@id,'_',.)"/>
                </xsl:variable>
                <matrixrow>
                  <xsl:for-each select="../../smtb:times/smtb:time">
                    <xsl:choose>
                      <xsl:when test="key('CONFIG_DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]">
                        <cn>
                          <xsl:value-of select="key('CONFIG_DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]"/>
                        </cn>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:message>Warning, cannot find data id=<xsl:value-of select="$keyvalue"/> in configuration=<xsl:value-of select="$config"/></xsl:message>
                        <cn>0</cn>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </matrixrow>
              </xsl:for-each>
            </xsl:for-each>
          </matrix>
        </field>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>stddev</ci>
          <matrix>
            <xsl:for-each select="f:group">
              <xsl:for-each select="smtb:index">
                <xsl:variable name="keyvalue">
                  <xsl:value-of select="concat($config,'_',../@id,'_',.)"/>
                </xsl:variable>
                <matrixrow>
                  <xsl:for-each select="../../smtb:times/smtb:time">
                    <xsl:choose>
                      <xsl:when test="key('CONFIG_DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]">
                        <cn>
                          <xsl:value-of select="key('CONFIG_DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]//@stddev"/>
                        </cn>
                      </xsl:when>
                      <xsl:otherwise>
                        <cn><infinity/></cn>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </matrixrow>
              </xsl:for-each>
            </xsl:for-each>
          </matrix>
        </apply>      
        
        <field name="stddev" xmlns="http://www.utc.fr/sysmetab">
          <ci>stddev</ci>
        </field>       

        <field name="stddevpm2" xmlns="http://www.utc.fr/sysmetab">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <power type="array"/>
            <ci>stddev</ci>
            <cn>-2</cn>
          </apply>
        </field>                 
        
        <xsl:call-template name="groups"/>
        
      </struct>  
    </field>
        
  </xsl:template>

  <xsl:template match="f:labelingmeasurement[../../../@stationary='true']">
    <xsl:variable name="config" select="../../../@name"/>
    <xsl:apply-templates/>

    <field name="labeling" xmlns="http://www.utc.fr/sysmetab">
      <struct>   
        <xsl:call-template name="Cx"/>
        <!-- meas and std vectors (stationary measurements) -->
        <field name="meas" xmlns="http://www.utc.fr/sysmetab">
          <vector xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:for-each select="f:group">
              <xsl:for-each select="smtb:index">
                <xsl:choose>
                  <xsl:when test="key('CONFIG_DATUM_ROW_WEIGHT',concat($config,'_',../@id,'_',.))">
                    <cn>
                      <xsl:value-of select="key('CONFIG_DATUM_ROW_WEIGHT',concat($config,'_',../@id,'_',.))"/>
                    </cn>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:message>Warning, cannot find data id=<xsl:value-of select="../@id"/> in configuration=<xsl:value-of select="$config"/></xsl:message>
                    <cn>0</cn>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </xsl:for-each>
          </vector>
        </field>
        
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>stddev</ci>
          <vector>
            <xsl:for-each select="f:group">
              <xsl:for-each select="smtb:index">
                <xsl:choose>
                  <xsl:when test="key('CONFIG_DATUM_ROW_WEIGHT',concat($config,'_',../@id,'_',.))">
                    <cn>
                      <xsl:value-of select="key('CONFIG_DATUM_ROW_WEIGHT',concat($config,'_',../@id,'_',.))/@stddev"/>
                    </cn>
                  </xsl:when>
                  <xsl:otherwise>
                    <cn><infinity/></cn>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </xsl:for-each>
          </vector>
        </apply>       

        <field name="stddev" xmlns="http://www.utc.fr/sysmetab">
          <ci>stddev</ci>
        </field>       

        <field name="stddevpm2" xmlns="http://www.utc.fr/sysmetab">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <power type="array"/>
            <ci>stddev</ci>
            <cn>-2</cn>
          </apply>
        </field>       

        <xsl:call-template name="groups"/>
  
      </struct>
    </field>
     
  </xsl:template>
  
  <xsl:template match="f:simulation[f:variables[f:fluxvalue or f:poolvalue]]">
    <field name="simulation" xmlns="http://www.utc.fr/sysmetab">
      <xsl:apply-templates/>
    </field>
  </xsl:template>

  <xsl:template match="f:variables">
      <struct xmlns="http://www.utc.fr/sysmetab">
        <field name="variables">
          <xsl:if test="f:fluxvalue">
            <struct>
              <field name="flux">
                <struct>
                  <field name="index">
                    <vector xmlns="http://www.w3.org/1998/Math/MathML">
                      <xsl:for-each select="f:fluxvalue">
                        <xsl:choose>
                          <xsl:when test="@type='net'">
                            <cn><xsl:value-of select="key('REACTION',@flux)/@position"/></cn>
                          </xsl:when>
                          <xsl:when test="@type='xch'">
                            <cn><xsl:value-of select="$nv+(key('REACTION',@flux)/@position)"/></cn>
                          </xsl:when>
                        </xsl:choose>
                      </xsl:for-each>
                    </vector>
                  </field>
                  <field name="value">
                    <vector xmlns="http://www.w3.org/1998/Math/MathML">
                      <xsl:for-each select="f:fluxvalue">
                        <cn><xsl:value-of select="."/></cn>
                      </xsl:for-each>
                    </vector>              
                  </field>
                </struct>
              </field>
            </struct>
          </xsl:if> 
          <xsl:if test="f:poolvalue">
            <struct>
              <field name="pool">
                <struct>
                  <field name="index">
                    <vector xmlns="http://www.w3.org/1998/Math/MathML">
                      <xsl:for-each select="f:poolvalue">
                        <cn><xsl:value-of select="key('pool',@pool)/@number"/></cn>
                      </xsl:for-each>
                    </vector>
                  </field>
                  <field name="value">
                    <vector xmlns="http://www.w3.org/1998/Math/MathML">
                      <xsl:for-each select="f:poolvalue">
                        <cn><xsl:value-of select="."/></cn>
                      </xsl:for-each>
                    </vector>              
                  </field>
                </struct>
              </field>
            </struct>
          </xsl:if>
        </field>
      </struct>
  </xsl:template>


<xsl:template name="netflux_coeff">
  <xsl:param name="string"/>
  <xsl:choose>
    <xsl:when test="contains($string,'*')">
      <matrix-assignment id="E" row="{position()}" col="{key('REACTION',substring-after($string,'*'))/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:value-of select="substring-before($string,'*')"/>
        </cn>
      </matrix-assignment>
    </xsl:when>
    <xsl:otherwise>
      <matrix-assignment id="E" row="{position()}" col="{key('REACTION',$string)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="iterate_netflux">
  <xsl:param name="string"/>
  <xsl:choose>
    <xsl:when test="contains($string,'+')">
      <xsl:call-template name="netflux_coeff">
        <xsl:with-param name="string" select="substring-before($string,'+')"/>
      </xsl:call-template>
      <xsl:call-template name="iterate_netflux">
        <xsl:with-param name="string" select="substring-after($string,'+')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="netflux_coeff">
        <xsl:with-param name="string" select="$string"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="iterate_pool">
  <xsl:param name="string"/>
  <xsl:choose>
    <xsl:when test="contains($string,'+')">
      <matrix-assignment id="Em" row="{position()}" col="{key('POOL',substring-before($string,'+'))/@number}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
      <xsl:call-template name="iterate_pool">
        <xsl:with-param name="string" select="substring-after($string,'+')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <matrix-assignment id="Em" row="{position()}" col="{key('POOL',$string)/@number}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="f:fluxmeasurement"> 
  <field name="flux" xmlns="http://www.utc.fr/sysmetab">

    <struct>
      <optimize xmlns="http://www.utc.fr/sysmetab">
        <!-- We open an item "optimize" in which we will gradually build the matrix E -->
        <matrix-open id="E" 
                     cols="{2*$nv}" 
                     rows="{count(f:netflux)}"
                     type="sparse"/>
        <xsl:for-each select="f:netflux">
          <xsl:call-template name="iterate_netflux">
            <xsl:with-param name="string" select="f:textual"/>
          </xsl:call-template>
        </xsl:for-each>  
        <matrix-close id="E"/>
      </optimize>
      
      <field name="E">
        <ci>E</ci>
      </field>
      
      <field name="stddevpm2">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <power type="array"/>
          <vector>
            <xsl:for-each select="f:netflux">
    					<xsl:choose>
    						<xsl:when test="key('DATUM',@id)">
                  <cn>
                    <xsl:value-of select="key('DATUM',@id)/@stddev"/>
                  </cn>
    						</xsl:when>
    						<xsl:otherwise>
    							<cn>
    								<infinity/>
    							</cn>
    						</xsl:otherwise>
    					</xsl:choose>
            </xsl:for-each>
          </vector>
          <apply>
            <minus/>
            <cn>2</cn>
          </apply>
        </apply>
      </field>
      <field name="meas">
        <vector xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:for-each select="f:netflux">
    				<xsl:choose>
    					<xsl:when test="key('DATUM',@id)">
                <cn>
                  <xsl:value-of select="key('DATUM',@id)"/>
                </cn>
    					</xsl:when>
    					<xsl:otherwise>
    						<cn>0</cn>
    					</xsl:otherwise>
    				</xsl:choose>
          </xsl:for-each>
        </vector>
      </field>
      
      <field name="textual">
      <vector xmlns="http://www.w3.org/1998/Math/MathML">
        <xsl:for-each select="f:netflux">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="f:textual"/>
          </string>
        </xsl:for-each>
      </vector>
    </field>

      <field name="id">
      <vector xmlns="http://www.w3.org/1998/Math/MathML">
        <xsl:for-each select="f:netflux">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="@id"/>
          </string>
        </xsl:for-each>
      </vector>
    </field>
    
    </struct>
  </field>  
</xsl:template>

<xsl:template match="f:poolmeasurement"> 
  <field name="pool" xmlns="http://www.utc.fr/sysmetab">
    <struct>
      <optimize xmlns="http://www.utc.fr/sysmetab">  
        <matrix-open id="Em" 
                     cols="{$np}" 
                     rows="{count(f:poolsize)}"
                     type="sparse"/>
        <xsl:for-each select="f:poolsize">        
          <xsl:call-template name="iterate_pool">
            <xsl:with-param name="string" select="f:textual"/>
          </xsl:call-template>
        </xsl:for-each>  
        <matrix-close id="Em"/>
      </optimize>  
      <field name="Em">
        <ci>Em</ci>
      </field>
      <field name="textual">
        <vector xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:for-each select="f:poolsize">
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="f:textual"/>
            </string>
          </xsl:for-each>
        </vector>
      </field>
      <field name="id">
        <vector xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:for-each select="f:poolsize">
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="@id"/>
            </string>
          </xsl:for-each>
        </vector>
      </field>
      <field name="stddevpm2">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <power type="array"/>
          <vector>
            <xsl:for-each select="f:poolsize">
    					<xsl:choose>
    						<xsl:when test="key('DATUM',@id)">
                  <cn>
                    <xsl:value-of select="key('DATUM',@id)/@stddev"/>
                  </cn>
    						</xsl:when>
    						<xsl:otherwise>
    							<cn>
    								<infinity/>
    							</cn>
    						</xsl:otherwise>
    					</xsl:choose>
            </xsl:for-each>
          </vector>
          <apply>
            <minus/>
            <cn>2</cn>
          </apply>
        </apply>
      </field>
      <field name="meas">
        <vector xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:for-each select="f:poolsize">
    				<xsl:choose>
    					<xsl:when test="key('DATUM',@id)">
                <cn>
                  <xsl:value-of select="key('DATUM',@id)"/>
                </cn>
    					</xsl:when>
    					<xsl:otherwise>
    						<cn>0</cn>
    					</xsl:otherwise>
    				</xsl:choose>
          </xsl:for-each>
        </vector>
      </field>
    </struct>
  </field>

</xsl:template>

<xsl:template name="groups">
  
  <xsl:variable name="configuration" select="../../../@name"/>

  <field name="nb_group" xmlns="http://www.utc.fr/sysmetab">
    <cn xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:value-of select="count(f:group)"/>
    </cn>
  </field>
  
	<!-- auto_scales_ind contains the number of auto scaled label measurement groups -->
  
  <field name="auto_scales_ind" xmlns="http://www.utc.fr/sysmetab">
    <matrix xmlns="http://www.w3.org/1998/Math/MathML">
      <matrixrow>
        <xsl:for-each select="f:group[key('DATUM',@id)]">
          <xsl:choose>
            <xsl:when test="exslt:node-set($params)[@name='scaling']='yes'">
              <cn><xsl:value-of select="position()"/></cn>
            </xsl:when>
            <xsl:when test="(exslt:node-set($params)[@name='scaling']='user') and (@scale='auto')">
              <cn><xsl:value-of select="position()"/></cn>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </matrixrow>
    </matrix>
  </field>
  
	<!-- omega_ind_meas contains the group number for each measurement -->
  
  <field name="omega_ind_meas" xmlns="http://www.utc.fr/sysmetab">
		<vector xmlns="http://www.w3.org/1998/Math/MathML">
		  <xsl:for-each select="f:group">
        <xsl:variable name="groupnumber" select="position()"/>
        <xsl:for-each select="smtb:index">
          <cn>
					  <xsl:value-of select="$groupnumber"/>
				  </cn>
        </xsl:for-each>
      </xsl:for-each>
    </vector>
  </field>
  
	<!-- meas_in_group(i) contains measurement numers of group i -->
  
  <field name="meas_ind_group" xmlns="http://www.utc.fr/sysmetab">
    <xsl:for-each select="f:group">
  		<vector xmlns="http://www.w3.org/1998/Math/MathML">
  			<xsl:for-each select="smtb:index">
  			  <cn>
  				  <xsl:value-of select="1+count(preceding::smtb:index[../../../../../@name=$configuration])"/>
  				</cn>
        </xsl:for-each>
      </vector>
    </xsl:for-each>
  </field>

  <field name="group_ids" xmlns="http://www.utc.fr/sysmetab">
    <list separator=";" xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:for-each select="f:group">
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="@id"/>
        </string>
      </xsl:for-each>
    </list>
  </field>
  
  <field name="group_textual" xmlns="http://www.utc.fr/sysmetab">
    <list separator=";" xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:for-each select="f:group">
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="f:textual"/>
        </string>
      </xsl:for-each>
    </list>
  </field>
  
</xsl:template>


<xsl:template name="fluxSubspace">

  <!--<comment xmlns="http://www.utc.fr/sysmetab">Building matrices (also the stoichiometry matrix) allows to compute the linear change of variable of the fluxes.</comment>-->
  
  <!--<comment xmlns="http://www.utc.fr/sysmetab">Equality and inequality constraints</comment>-->

  <field name="C" xmlns="http://www.utc.fr/sysmetab">
    <matrix xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:if test="f:net/m:math/m:apply[m:eq]">
        <matrixrow>
        <ci>A_eq_net</ci>
        <apply>
          <fn><ci>zeros</ci></fn>
          <cn>
            <xsl:value-of select="count(f:net/m:math/m:apply[m:eq])"/>
          </cn>
          <cn>
            <xsl:value-of select="$nv"/>
          </cn>
        </apply>
       </matrixrow>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:eq]">
        <matrixrow>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:xch/m:math/m:apply[m:eq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$nv"/>
            </cn>
          </apply>
          <ci>A_eq_xch</ci>            
        </matrixrow>
      </xsl:if>
      <matrixrow>
        <ci>N</ci>
        <apply>
          <fn><ci>zeros</ci></fn>
          <cn>
            <xsl:value-of select="count(../f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
          </cn>
          <cn>
            <xsl:value-of select="$nv"/>
          </cn>
        </apply>
      </matrixrow>
      <xsl:if test="f:net/m:math/m:apply[m:leq]">
        <matrixrow>
          <apply>
            <minus/>
            <ci>A_leq_net</ci>
          </apply>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:net/m:math/m:apply[m:leq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$nv"/>
            </cn>
          </apply>
        </matrixrow>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:leq]">
        <matrixrow>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:xch/m:math/m:apply[m:leq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$nv"/>
            </cn>
          </apply>
          <apply>
            <minus/>
            <ci>A_leq_xch</ci>
          </apply>            
        </matrixrow>
      </xsl:if>
    </matrix>
  </field>
  <field name="d" xmlns="http://www.utc.fr/sysmetab">
    <vector xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:if test="f:net/m:math/m:apply[m:eq]">
        <ci>b_eq_net</ci>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:eq]">
        <ci>b_eq_xch</ci>            
      </xsl:if>
      <apply>
        <fn><ci>zeros</ci></fn>
        <cn>
          <xsl:value-of select="count(../f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
        </cn>
        <cn>1</cn>
      </apply>
      <xsl:if test="f:net/m:math/m:apply[m:leq]">
        <apply>
          <minus/>
          <ci>b_leq_net</ci>
        </apply>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:leq]">
        <apply>
          <minus/>
          <ci>b_leq_xch</ci>
        </apply>            
      </xsl:if>
    </vector>
  </field>
  <field name="me" xmlns="http://www.utc.fr/sysmetab">
    <cn>
      <xsl:value-of select="count(../f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']) + count(f:net/m:math/m:apply[m:eq]) + count(f:xch/m:math/m:apply[m:eq])"/>
    </cn>
  </field>

</xsl:template>

<xsl:template match="f:constraints">
  <struct xmlns="http://www.utc.fr/sysmetab">
    <field name="flux">
      <struct>
        <optimize>
          <xsl:if test="f:net/m:math/m:apply[m:leq]">
            <matrix-open type="full" id="A_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="{$nv}"/>
            <matrix-open type="full" id="b_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="1"/>
          </xsl:if>
          <xsl:if test="f:net/m:math/m:apply[m:eq]">
            <matrix-open type="full" id="A_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="{$nv}"/>
            <matrix-open type="full" id="b_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="1"/>
          </xsl:if>
          <xsl:if test="f:xch/m:math/m:apply[m:leq]">
            <matrix-open type="full" id="A_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="{$nv}"/>
            <matrix-open type="full" id="b_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="1"/>
          </xsl:if>  
          <xsl:if test="f:xch/m:math/m:apply[m:eq]">
            <matrix-open type="full" id="A_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="{$nv}"/>
            <matrix-open type="full" id="b_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="1"/>
          </xsl:if>
          <xsl:apply-templates select="f:xch/m:math/m:apply[m:eq]"/>
          <xsl:apply-templates select="f:xch/m:math/m:apply[m:leq]"/>
          <xsl:apply-templates select="f:net/m:math/m:apply[m:eq]"/>
          <xsl:apply-templates select="f:net/m:math/m:apply[m:leq]"/>
          <xsl:if test="f:net/m:math/m:apply[m:leq]">
            <matrix-close id="A_leq_net"/>
            <matrix-close id="b_leq_net"/>
          </xsl:if>
          <xsl:if test="f:net/m:math/m:apply[m:eq]">
            <matrix-close id="A_eq_net"/>
            <matrix-close id="b_eq_net"/>
          </xsl:if>
          <xsl:if test="f:xch/m:math/m:apply[m:leq]">
            <matrix-close id="A_leq_xch"/>
            <matrix-close id="b_leq_xch"/>
          </xsl:if>  
          <xsl:if test="f:xch/m:math/m:apply[m:eq]">
            <matrix-close id="b_eq_xch"/>
            <matrix-close id="A_eq_xch"/>
          </xsl:if>
        </optimize>
        <xsl:call-template name="fluxSubspace"/>
      </struct>
    </field>
  </struct>
</xsl:template>

<xsl:template match="f:constraints/*/m:math/m:apply">
  
  <xsl:if test="m:cn!='0'">
    <matrix-assignment id="{concat('b_',name(*[1]),'_',name(../..))}" row="{position()}" col="1" xmlns="http://www.utc.fr/sysmetab">
      <xsl:choose>
        <xsl:when test="name(*[1])='leq' and name(*[2])='ci' and name(*[3])='cn'">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">-(<xsl:value-of select="m:cn"/>)</cn>
        </xsl:when>
        <xsl:otherwise>
          <cn xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="m:cn"/></cn>
        </xsl:otherwise>
      </xsl:choose>
    </matrix-assignment>
  </xsl:if>
  <xsl:variable name="id" select="concat('A_',name(*[1]),'_',name(../..))"/>

  <xsl:choose>
    <xsl:when test="m:ci and not(m:apply)">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',m:ci)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="name(*[1])='leq' and name(*[2])='ci' and name(*[3])='cn'">
            <cn xmlns="http://www.w3.org/1998/Math/MathML">-1</cn>
          </xsl:when>
          <xsl:otherwise>
            <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
          </xsl:otherwise>
        </xsl:choose>
      </matrix-assignment>
    </xsl:when>
    <xsl:when test="m:apply">
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="m:apply"/>
        <xsl:with-param name="minus">
          <xsl:choose>
            <xsl:when test="m:apply[m:plus]">1</xsl:when>
            <xsl:when test="m:apply[m:minus]">-1</xsl:when>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="iteration_eq_leq">
  <xsl:param name="id"/>
  <xsl:param name="apply"/>
  <xsl:param name="minus" select="'1'"/>
  <xsl:choose>
    <xsl:when test="$apply[m:times]">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="$minus='-1'">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <minus/>
              <cn><xsl:value-of select="$apply/m:cn"/></cn>
            </apply>
          </xsl:when>
          <xsl:otherwise>
            <cn><xsl:value-of select="$apply/m:cn"/></cn>
          </xsl:otherwise>
        </xsl:choose>
      </matrix-assignment>
    </xsl:when>
    <xsl:when test="(count($apply/m:ci)='2') and not($apply/m:apply)">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci[1])/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci[2])/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:value-of select="$minus"/>
        </cn>
      </matrix-assignment>
    </xsl:when>
    <xsl:when test="count($apply/m:apply)='1'">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:choose>
            <xsl:when test="$apply[m:plus]">1</xsl:when>
            <xsl:when test="$apply[m:minus]">-1</xsl:when>
          </xsl:choose>
        </cn>  
      </matrix-assignment>
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="$apply/m:apply"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="count($apply/m:apply)='2'">
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="$apply/m:apply[1]"/>
      </xsl:call-template>
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="$apply/m:apply[2]"/>
        <xsl:with-param name="minus">
          <xsl:choose>
            <xsl:when test="$apply[m:plus]">1</xsl:when>
            <xsl:when test="$apply[m:minus]">-1</xsl:when>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
  </xsl:choose>
</xsl:template> 

<xsl:template match="f:metabolitepools" mode="stoichiometric-matrix">
  <optimize xmlns="http://www.utc.fr/sysmetab">
    <matrix-open type="full" id="N" rows="{count(f:pool[@type='intermediate'])}" cols="{count(../f:reaction)}"/>
    <xsl:apply-templates select="f:pool[@type='intermediate']" mode="stoichiometric-matrix"/>
    <matrix-close id="N"/>
  </optimize>
</xsl:template>

<xsl:template match="f:pool" mode="stoichiometric-matrix">
  <xsl:variable name="position" select="position()"/>
  <xsl:for-each select="key('REDUCT',@id)">
	<matrix-assignment id="N" row="{$position}" col="{../@position}" xmlns="http://www.utc.fr/sysmetab">
		<xsl:choose>
			<xsl:when test="@stoichiometry"> <!-- used for non labeled pools or to simulate dilution effects-->
				<cn xmlns="http://www.w3.org/1998/Math/MathML">
				  <xsl:value-of select="concat('-',@stoichiometry)"/>
				</cn>
			</xsl:when>
			<xsl:otherwise>
				<cn xmlns="http://www.w3.org/1998/Math/MathML">-1</cn>
			</xsl:otherwise>
		</xsl:choose>
	</matrix-assignment>
  </xsl:for-each>
  <xsl:for-each select="key('RPRODUCT',@id)">
    <matrix-assignment id="N" row="{$position}" col="{../@position}" xmlns="http://www.utc.fr/sysmetab">
    	<cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
    </matrix-assignment>
  </xsl:for-each>
</xsl:template>

<xsl:template match="node()"/>

</xsl:stylesheet>