<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Jul 08 15:00:00 CET 2014
    Project :   PIVERT/Metalippro-PL1
-->

<!--
  This style sheet tranform a fml file into fwd file which will contain all data
  used in the optimization problem.
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f fwd exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- Maximum weight of all cumomers inside the network -->
  <xsl:variable name="maxweight" select="math:max(//smtb:listOfCumomers/@weight)"/>

  <!-- File name -->
  <xsl:param name="output"/>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <!-- find all f:datum -->
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <!-- find all f:pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>

  <!-- find all Intermediate Cumomer -->
  <xsl:key name="INTERMEDIATECUMOMER" match="smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer" use="@id"/>

  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <xsl:for-each select="f:configuration">
      <xsl:document method="xml" indent="yes" href="{concat($output,'_',@name,'.fwd')}">
        <fwdsim xmlns="http://www.13cflux.net/fwdsim">
          <xsl:apply-templates select="../fwd:stoichiometry"/>
          <xsl:apply-templates select="../f:reactionnetwork"/>
          <xsl:apply-templates select="."/>
          <optimization xmlns="http://www.13cflux.net/fwdsim">
            <xsl:for-each select="exslt:node-set($params)">
              <param name="{@name}" type="{@type}">
                <xsl:value-of select="."/>
              </param>
            </xsl:for-each>
          </optimization>
        </fwdsim>
      </xsl:document>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="fwd:stoichiometry">
    <xsl:copy>
      <xsl:copy-of select="@*|*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:reactionnetwork">
    <simulation method="cumomer" xmlns="http://www.13cflux.net/fwdsim">
      <xsl:for-each select="f:metabolitepools/f:pool">
        <xsl:variable name="id" select="@id"/>
        <pool>
          <xsl:copy-of select="@id"/>
          <xsl:if test="@value">
            <xsl:copy-of select="@value|@stddev|@rerr"/>
          </xsl:if>
          <xsl:for-each select="../../smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer[@pool=$id]">
            <xsl:sort select="@weight"/>
            <value cfg="{@pattern}" xmlns="http://www.13cflux.net/fwdsim"><xsl:value-of select="text()"/></value> 
          </xsl:for-each>
        </pool>
      </xsl:for-each>
    </simulation>
  </xsl:template>
  
  <xsl:template match="f:configuration">
    <xsl:apply-templates select="f:measurement"/>
  </xsl:template>

  <xsl:template match="f:measurement">
    <xsl:variable name="residual">
      <xsl:choose>
        <xsl:when test="(../@stationary='false') and (f:model/f:poolmeasurement)">
          <xsl:value-of select="f:model/f:labelingmeasurement/@residual + f:model/f:fluxmeasurement/@residual + f:model/f:poolmeasurement/@residual"/>
        </xsl:when>
        <xsl:when test="f:model/f:fluxmeasurement/@residual">
          <xsl:value-of select="f:model/f:labelingmeasurement/@residual + f:model/f:fluxmeasurement/@residual"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="f:model/f:labelingmeasurement/@residual"/>
        </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <measurements residual="{$residual}" xmlns="http://www.13cflux.net/fwdsim">
      <xsl:for-each select="f:model/f:fluxmeasurement">
        <xsl:for-each select="f:netflux">
          <mgroup id="{@id}" norm="{@norm}" scale="1" type="NETFLUX">
            <spec><xsl:value-of select="f:textual"/></spec>
            <value><xsl:value-of select="@value"/></value>
          </mgroup>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select="f:model/f:poolmeasurement">
        <xsl:for-each select="f:poolsize">
          <mgroup id="{@id}" norm="{@norm}" scale="1" type="POOL">
            <spec><xsl:value-of select="f:textual"/></spec>
            <value><xsl:value-of select="@value"/></value>
          </mgroup>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:for-each select="f:model/f:labelingmeasurement">
        <xsl:for-each select="f:group">
          <xsl:variable name="id" select="substring-before(f:textual,'[')"/>
          <mgroup id="{@id}" norm="{@norm}" scale="{@scale}">
            <xsl:choose>
              <xsl:when test="substring-before(@id,'_')='ms'">
                <xsl:attribute name="type">MS</xsl:attribute>
              </xsl:when>
              <xsl:when test="substring-before(@id,'_')='lmcg'">
                <xsl:attribute name="type">LM</xsl:attribute>
              </xsl:when>
            </xsl:choose>
            <spec><xsl:value-of select="f:textual"/></spec>
            <xsl:apply-templates select="f:time" mode="copy"/>
          </mgroup>
        </xsl:for-each>
      </xsl:for-each>
    </measurements>
  </xsl:template>

  <xsl:template match="*" mode="copy">
    <xsl:element name="{local-name()}" xmlns="http://www.13cflux.net/fwdsim">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="*|text()" mode="copy"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="text()" mode="copy">
    <xsl:value-of select="."/>
  </xsl:template>


</xsl:stylesheet>