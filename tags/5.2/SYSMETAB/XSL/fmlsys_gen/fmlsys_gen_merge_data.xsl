<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  xmlns:str="http://exslt.org/strings"
  exclude-result-prefixes="xsl smtb math m f exslt str">
  
  <!-- Output file name -->
  <xsl:param name="output"/>

  <xsl:param name="verb_level">1</xsl:param>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="f:info"/>
      <xsl:copy-of select="f:reactionnetwork"/>
      <xsl:copy-of select="document(concat($output,'.step0.xml'),/)/f:fluxml/f:constraints"/>
      <xsl:apply-templates select="f:configuration"/>
    </fluxml>
  </xsl:template>
  
  <xsl:template match="f:configuration">
    <configuration xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="f:input"/>
      <xsl:copy-of select="document(concat($output,'.step0.xml'),/)/f:fluxml/f:configuration[@name=current()/@name]/f:measurement"/>
      <xsl:copy-of select="document(concat($output,'.step0.xml'),/)/f:fluxml/f:configuration[@name=current()/@name]/f:simulation"/>
    </configuration>
  </xsl:template>


</xsl:stylesheet>
