exdir=get_absolute_file_path('example5.sce');

modefsqp=100;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-4;
epsneq=0.e0;
udelta=0.e0;
nf=1;
neqn=0;
nineqn=1;nineq=1;
ncsrn=1;
ncsrl=0;
mesh_pts=101;
neq=0;nfsr=0;
   
bl=[-bigbnd;-bigbnd];
bu=-bl;

x0=[-1;-2];

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];
srpar=[nfsr,ncsrl,ncsrn];

//[1] fsqp with Scilab functions
//=======================================================

getf(exdir+'/example5.sci');  //Loading scilab functions obj,cntr,grob,grc
// All scilab functions
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)

// Using Scilab functions which evaluate all the constraints in one step
t=(0:0.01:1)';
t2=t.*t;

x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj2,cntr2,grob2,grcn2)
clear all_objectives all_constraints all_gradobjectives all_gradconstraints 


//[2] fsqp with  C functions 
//=======================================================
exec(exdir+'/example5-src/loader.sce') 
// All C functions
//=======================================================
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj','cntr','grob','grcn')

//With cntr a scilab function
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj',cntr,'grob','grcn')

//With finite diff. approx.
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj,cntr,'grobfd','grcnfd')

//[2] fsqp with  lists 
//=======================================================

getf(exdir+'/listutils.sci')

getf(exdir+'/example5_list.sci'); //loading scilab functions f_1 and G_1

//Problem parameters
t=(0:0.001:1)';t2=t.*t;

// One regular objective 0 SR objective
list_obj=list(list(f_1),list()); 

//One non linear SR inequality constraint R^101 valued (G_1(x))
list_cntr=list(list(),list(G_1),list(),list(),list(),list());

list_grobj=list(list(grf_1),list());

list_grcn=list(list(),list(grG_1),list(),list(),list(),list());

x0=[-1;-2];
// fsqp parameters obtained by findparam
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-4;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=[-bigbnd;-bigbnd];
bu=-bl;

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)



