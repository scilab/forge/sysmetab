function y=f_1(x)
  y=(x(1)+3*x(2)+x(3))^2+4*(x(1)-x(2))^2
endfunction

function y=g_1(x)
  y=x(1)^3-6*x(2)-4*x(3)+3
endfunction

function y=A_1(x)
  y=[-1,-1,-1]*x+1;
endfunction

function yd=grf_1(x)
  fa=2*(x(1)+3*x(2)+x(3));
  fb=8*(x(1)-x(2));
  yd=[fa+fb,3*fa-fb,fa];
endfunction

function yd=grg_1(x)
  yd=[3*x(1)^2,-6,-4]
endfunction

function yd=grA_1
  yd=[-1,-1,-1]
endfunction


