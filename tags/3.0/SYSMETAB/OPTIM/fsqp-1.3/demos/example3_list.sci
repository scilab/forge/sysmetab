function f=f_1(x)
  f=x(1)*x(4)*sum(x(1:3))+x(3)

function g=grf_1(x)
  s=sum(x(1:3));p=x(1)*x(4);
  g=[x(4)*s+p,p,1+p,x(1)*s];
endfunction

function g=g_1(x)
//nonlinear ineq
  g=25-prod(x);
endfunction

function h=h_1(x)
//nonlinear equal.
  h=norm(x)^2-40;
endfunction

function gr=grg_1(x)
  gr=-(prod(x)./x)'
endfunction

function gr=grh_1(x)
  gr=2*x';
endfunction



