function cj=cntr(j,x)
  if x_is_new() then
    //    Common part
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    //
    cj=all_constraints(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    cj=all_constraints(j);
  end
endfunction
