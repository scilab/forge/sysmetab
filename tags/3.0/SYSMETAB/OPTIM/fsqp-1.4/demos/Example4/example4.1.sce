mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 4 of the fsqp documentation

//[1] sr-fsqp with Scilab functions
//=======================================================
function fj=obj1(j,x)
  fj=x(10);
endfunction

function gradfj=grob1(j,x)
  gradfj=[0,0,0,0,0,0,0,0,0,1];
endfunction


function ztx=z(t,x)
  k=1:9;
  tk=t*k;
  ztx=(cos(tk)*x(k))^2+(sin(tk)*x(k))^2
endfunction
function gj=cntr1(j,x)
  if j<= r
    gj=(1-x(10))^2-z(t(j),x);
  elseif j<= 2*r 
    gj=z(t(j),x)-(1+x(10))^2;
  elseif j<= 3.5*r
    gj=z(t(j),x)-(x(10))^2;
  end
endfunction

function grad=grz(t,x)
  k=1:9;
  tk=t*k;
  ctk=cos(tk);
  stk=sin(t*k);
  grad=[diag(2*ctk*x(k))*ctk+diag(2*stk*x(k))*stk]
endfunction
function w=grcn1(j,x)
  if j<= r
    w=[-grz(t(j),x),-2*(1-x(10))]
  elseif j<= 2*r
    w=[grz(t(j),x),-2*(1+x(10))]
  elseif j<= 3.5*r
    w=[grz(t(j),x),-2*x(10)]
  end
endfunction


modefsqp=100;
iprint=0;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=0.e0;
nf=1;
neqn=0;
nineqn=3;nineq=3;ncsrn=3;
ncsrl=0;
r=100;
mesh_pts=[r,r,3*r/2];
neq=0;nfsr=0;

x0=[0.1*ones(9,1);1];
bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
bigbnd=1.e10; eps=1.e-8; epsneq=0.e0; udelta=0.e0;
 
udelta=0.00001;
rpar=[bigbnd,eps,epsneq,udelta];

srpar=[nfsr,ncsrl,ncsrn];


t1=%pi*0.025*((1:r)-1)';
t2=%pi*0.025*((1:r)-1)';
t3=%pi*0.25*(1.2+0.2*((1:1.5*r)-1))';
t=[t1;t2;t3];

C=[];for j=1:6*r/2,C=[C;cntr1(j,x0)];end
format(6)		  
mprintf('%s\n',['---------------------------SR problem with srfsqp(Scilab code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)
		'   Value of the objective function: '+sci2exp(obj1(1,x0),0)
		'   Value of the constraints :       '+sci2exp(C,0)])
timer();
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj1,cntr1,grob1,grcn1)
tim=timer();
C=[];for j=1:6*r/2,C=[C;cntr1(j,x)];end

mprintf('%s\n',['Final values'
		'   Value of the solution:           '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(obj1(1,x),0)
		'   Value of the constraints :       '+sci2exp(C,0)
		'   Time :                           '+sci2exp(tim,0)
	       ])
format(10)

