mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 3 of the fsqp documentation

//[2] fsqp with  C functions 
//=======================================================
exec(get_absolute_file_path('example3.3.sce')+'example3-src/loader.sce') 


modefsqp=100;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=7.e-6;
udelta=0.e0;
nf=1;
neqn=1;
nineqn=1;
nineq=1;
neq=1;

x0=[1;5;5;1];

bl=ones(x0);
bu=5*bl;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];

x=fsqp(x0,ipar,rpar,[bl,bu],'obj71','cntr71','grob71','grcn71');

