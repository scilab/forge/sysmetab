mode(-1)
curdir=pwd()
chdir(get_absolute_file_path('cleaner.sce'))
files=listfiles(['*','scilab_en_US_help/*'])';
tokeep=["build_help.sce";"cleaner.sce";"fsqp.xml";"srfsqp.xml"
	"x_is_new.xml";"findparam.xml"];
for k=tokeep',files(files==k)=[];end
for f=files,disp(f),mdelete(f),end
chdir(curdir)
clear files f mdelete listfiles
