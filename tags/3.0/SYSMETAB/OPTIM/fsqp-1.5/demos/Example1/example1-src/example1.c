#include "cfsqpusr.h"
void obj32(int nparam,int j,double *x,double *fj,void *cd)
{
  *fj=pow((x[0]+3.e0*x[1]+x[2]),2.e0)+4.e0*pow((x[0]-x[1]),2.e0);
  return;
}

void grob32(int nparam,int j,double *x,double *gradfj,void (* dummy()),void *cd)
{
  double fa,fb;
  fa=2.e0*(x[0]+3.e0*x[1]+x[2]);
  fb=8.e0*(x[0]-x[1]);
  gradfj[0]=fa+fb;
  gradfj[1]=fa*3.e0-fb;
  gradfj[2]=fa;
  return;
}

void cntr32(int nparam,int j,double *x,double *gj,void *cd)
{
  switch (j) {
  case 1:
    *gj=pow(x[0],3.e0)-6.e0*x[1]-4.e0*x[2]+3.e0;
    break;
  case 2:
    *gj=1.e0-x[0]-x[1]-x[2];
    break;
  }
  return;
}

void grcn32(int nparam,int j,double *x,double *gradgj,void (* dummy()),void *cd)
{
  switch (j) {
  case 1:
    gradgj[0]=3.e0*x[0]*x[0];
    gradgj[1]=-6.e0;
    gradgj[2]=-4.e0;
    break;
  case 2:
    gradgj[0]=gradgj[1]=gradgj[2]=-1.e0;
    break;
  }
  return;
}   

