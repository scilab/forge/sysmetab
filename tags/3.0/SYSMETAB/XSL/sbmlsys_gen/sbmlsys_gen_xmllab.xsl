<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer le fichier
    XMLlab d�crivant l'interface du logiciel de simulation/optimisation
    sp�cifique � un r�seau donn�.
-->

<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:math="http://exslt.org/math" version="1.0"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">

  <xsl:output method="xml" doctype-system="http://www.xmllab.org/dtd/1.6/fr/simulation.dtd" 
  doctype-public="-//UTC//DTD XMLlab V1.6//FR" indent="yes" encoding="ISO-8859-1"/>

  <xsl:param name="file"/>
  <xsl:param name="verbose">no</xsl:param>
  <xsl:param name="type">stationnaire</xsl:param>
  <xsl:param name="experiences">1</xsl:param>

  <xsl:include href="common_xmllab.xsl"/> <!-- Templates communes au cas stationnaire et non stationnaire -->

  <xsl:key name="reaction" match="sbml:reaction" use="@id"/>
  <xsl:key name="species" match="sbml:species" use="@id"/>
  <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
  <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
  <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
  <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
  <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

  <xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>

  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <simulation>
      <xsl:apply-templates/>
    </simulation>
  </xsl:template>

  <xsl:template match="sbml:model">
    <header>
      <title>R�seau m�tabolique   <xsl:value-of select="@id"/></title>
      <title lang="english">Metabolic network   <xsl:value-of select="@id"/></title>
      <author>St�phane Mottelet</author>
      <date>
        <xsl:value-of select="concat(date:date(),', at ',date:time())"/>
      </date>
      <script href="{$file}.sci"/>
      <script href="sysmetab4.sci"/>
    </header>
    <parameters>
      <section>
        <title>Flux (tous)</title>
        <title lang="english">Fluxes (all)</title>
        <matrix label="fluxes" rows="{count(sbml:listOfReactions/sbml:reaction)}" cols="2" save="yes" load="yes">
          <name/>
          <colheader>
            <col state="disabled">
              <name>Valeur en %</name>
              <name lang="english">Value in %</name>
            </col>
            <col>
              <name>Valeur impos�e</name>
              <name lang="english">Constrained value</name>
            </col>
            <col state="disabled"/>
          </colheader>
          <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
            <row>
              <name>
                <xsl:value-of select="concat(@id,', ',@name)"/>
              </name>
              <name lang="english">
                <xsl:value-of select="concat(@id,', ',@name)"/>
              </name>
              <value>0,-1</value>
            </row>
          </xsl:for-each>
        </matrix>
      </section>
      <section>
        <title>Flux (net,xch)</title>
        <title lang="english">Fluxes (net,xch)</title>
        <matrix label="fluxesnetxch" rows="{count(sbml:listOfReactions/smtb:listOfBidirectionalReactions/smtb:bidirectionalReaction)}" cols="2" save="yes" load="yes">
          <name/>
          <colheader>
            <col>
              <name>Flux net</name>
              <name lang="english">Net flux</name>
            </col>
            <col>
              <name>Flux d'�change</name>
              <name lang="english">Exchange flux</name>
            </col>
            <col state="disabled"/>
          </colheader>
          <xsl:for-each select="sbml:listOfReactions/smtb:listOfBidirectionalReactions/smtb:bidirectionalReaction">
            <row>
              <name>
                <xsl:value-of select="concat(@id,', ',@name)"/>
              </name>
              <name lang="english">
                <xsl:value-of select="concat(@id,', ',@name)"/>
              </name>
              <value/>
            </row>
          </xsl:for-each>
        </matrix>
      </section>
      <section>
        <title>Flux mesur�s</title>
        <title lang="english">Fluxes measurements</title>
        <xsl:apply-templates select="sbml:listOfReactions" mode="with_ident"/>
      </section>
      <section>
        <title>Cumom�res</title>
        <title lang="english">Cumomers</title>
        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
          <matrix label="cumomers_weight_{@weight}" cols="{$experiences}" rows="{count(smtb:cumomer)}" state="disabled" save="yes">
            <name>
              <xsl:value-of select="concat('Cumom�res de poids ',@weight)"/>
            </name>
            <name lang="english">
              <xsl:value-of select="concat('Weight ',@weight, ' cumomers')"/>
            </name>
            <colheader>
              <xsl:call-template name="iterate-input-names"/>
            </colheader>
            <xsl:for-each select="smtb:cumomer">
              <row>
                <name>
                  <xsl:value-of select="concat(key('species',@species)/@name,' #',@pattern)"/>
                </name>
                <name lang="english">
                  <xsl:value-of select="concat(key('species',@species)/@name,' #',@pattern)"/>
                </name>
              </row>
            </xsl:for-each>
          </matrix>
        </xsl:for-each>
      </section>
      <section>
        <title>Marquages mesur�s</title>
        <title lang="english">Label measurements</title>
        <xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:measurement]"/>
        <scalar label="label_error" state="disabled">
          <name>Erreur sur les marquages</name>
          <name lang="english">Label error contribution</name>
        </scalar>
      </section>
      <section>
        <title>Marquages connus</title>
        <title lang="english">Known label</title>
        <xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:input]"/>
      </section>
      <section>
        <title>Calcul</title>
        <title lang="english">Computations</title>
        <subsection>
          <title>Fonction cout</title>
          <title lang="english">Cost function</title>
          <scalar label="epsilon" min="0" max="%inf">
            <name>Param�tre de r�gularisation</name>
            <name lang="english">Regularization parameter</name>
            <value>1e-5</value>
          </scalar>
          <scalar label="vmin" min="0" max="%inf">
            <name>Borne inf�rieure des flux</name>
            <name lang="english">Fluxes lower bound</name>
            <value>1e-5</value>
          </scalar>
        </subsection>
        <subsection>
          <title>Optimisation (fsqp)</title>
          <title lang="english">Optimisation (fsqp)</title>
          <scalar label="epsgrad" min="1e-10" max="%inf">
            <name>Tol�rance pour la convergence (norme du gradient projet�)</name>
            <name lang="english">Tolerance for convergence (norm of projected gradient)</name>
            <value>1e-8</value>
          </scalar>
          <scalar label="miter" min="1" max="%inf">
            <name>Nombre maximum d'it�rations pour une optimisation</name>
            <name lang="english">Maximum number of iterations for a single optimization</name>
            <value>1000</value>
          </scalar>
          <scalar label="itmax" min="1" max="%inf">
            <name>Nombre de tirages al�atoires de conditions initiales</name>
            <name lang="english">Number of successive random initial conditions</name>
            <value>10</value>
          </scalar>
        </subsection>
        <subsection>
          <title>Identifiabilit�</title>
          <title lang="english">Identifiability</title>
          <scalar label="penalmax">
            <name>Exposant du param�tre de p�nalisation maximal</name>
            <name lang="english">Exponent of maximum penalization parameter</name>
            <value>0</value>
          </scalar>
          <scalar label="penalmin">
            <name>Exposant du param�tre de p�nalisation minimal</name>
            <name lang="english">Exponent of maximum penalization parameter</name>
            <value>-10</value>
          </scalar>
          <scalar label="nbpenal" min="1" max="%inf">
            <name>Nombre de valeurs interm�diaires</name>
            <name lang="english">Number intermediate values</name>
            <value>20</value>
          </scalar>
        </subsection>
      </section>
      <actions>
        <title>Simulation</title>
        <title lang="english">Simulation</title>
        <action>
          <xsl:attribute name="update">
            <xsl:text>fluxes</xsl:text>
          </xsl:attribute>
          <title>Optimisation</title>
          <title lang="english">Optimization</title>
          <script>fluxes(:,1)=optimize()</script>
        </action>
        <action>
          <xsl:attribute name="update">
            <xsl:text>fluxes</xsl:text>
          </xsl:attribute>
          <title>Optimisation (avec compacification)</title>
          <title lang="english">Optimization (with compacification)</title>
          <script>fluxes(:,1)=optimize_compact()</script>
        </action>
        <action>
          <xsl:attribute name="update">
            <xsl:text>fluxes</xsl:text>
          </xsl:attribute>
          <title>Optimisation (boucle de p�nalisation)</title>
          <title lang="english">Optimization (penalization loop)</title>
          <script>fluxes(:,1)=penalization_loop()</script>
        </action>
        <action>
          <title>Ligne de commande Scilab</title>
          <title lang="english">Scilab command line</title>
          <script>pause</script>
        </action>
      </actions>
    </parameters>
    <script href="{$file}_direct.sce">
      <xsl:attribute name="update">
        <xsl:text>flux_obs</xsl:text>
        <xsl:text> label_error</xsl:text>
        <xsl:text> fluxesnetxch</xsl:text>
        <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
          <xsl:value-of select="concat(' ',@id,'_obs')"/>
        </xsl:for-each>
        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
          <xsl:value-of select="concat(' ','cumomers_weight_',@weight)"/>             
        </xsl:for-each>
      </xsl:attribute>
    </script>
  </xsl:template>

</xsl:stylesheet>