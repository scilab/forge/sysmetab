<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:ftbl="http://ftbl.org"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func ftbl">
  
  <xsl:key name="group" match="group" use="@id"/>
  
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()">    
      <xsl:copy>
          <xsl:apply-templates select="*|@*|text()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="fml">
    <fml>
      <xsl:apply-templates/>
    </fml>
  </xsl:template>

  <xsl:template match="measurement">
    <data> 
      <xsl:apply-templates select="data"/>
    </data>
  </xsl:template>


  <xsl:template match="data">
    <xsl:for-each select="datum[@weight]">
      <xsl:variable name="weight" select="@weight"/>
      <xsl:variable name="id" select="@id"/>
      <datum id="{@id}" stddev="{@stddev}" weight="{@weight}">
        <xsl:value-of select="../../../optimize_data/optimize_datum[@id=substring-before(key('group',$id)/textual,'[') and @weight=$weight]/@values"/>
      </datum>
    </xsl:for-each>
        <xsl:message>HHHHH 
      <xsl:value-of select="../../optimize_data/optimize_datum[@id=substring-before(key('group','ms_group_1')/textual,'[') and @weight='3']/@values"/>

<!--      <xsl:value-of select="../../../optimize_data/optimize_datum[@id=substring-before(key('group',@id)/textual,'[') and @weight=$weight]/@values"/>
	      <xsl:choose>
        <xsl:when test="../../../optimize_data/optimize_datum[@id=substring-before(key('group',$id)/textual,'[') and @weight=$weight]">
          <xsl:message>KKKK </xsl:message>
          <datum id="{@id}" stddev="{@stddev}" weight="{@weight}">
            <xsl:value-of select="./@values"/>
          </datum>
        </xsl:when>
      </xsl:choose>

-->
    </xsl:message>

  </xsl:template>
  
  <xsl:template match="node()"/>

</xsl:stylesheet>  