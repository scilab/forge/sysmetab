<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   

    Projet  :  
-->

<!-- Cette feuille de style a pour but 

    
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings"   
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  exclude-result-prefixes="smtb sbml celldesigner m str exslt xhtml math xsl f">
    
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/>

  <!-- find all irreversible reaction from f:constraints/f:xch -->
  <!-- ><xsl:key name="IRREVERSIBLE" match="m:apply[(m:eq) and (m:cn=0)]" use="m:ci"/>-->
  
  <!-- find all label input from f:configuration -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>  

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="f:info"/>
      <xsl:apply-templates select="f:reactionnetwork"/>
      <xsl:apply-templates select="f:constraints"/>
      <xsl:apply-templates select="f:configuration"/>
    </fluxml>
  </xsl:template>

  <xsl:template match="f:reactionnetwork">
    <reactionnetwork xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="f:metabolitepools"/>
      <xsl:for-each select="f:reaction">
        <xsl:call-template name="reactionfb">
          <xsl:with-param name="id" select="concat(@id,'_f')"/>
          <xsl:with-param name="pos" select="position()"/>
        </xsl:call-template>
      </xsl:for-each>
      <xsl:for-each select="f:reaction">
        <xsl:call-template name="reactionfb">
          <xsl:with-param name="id" select="concat(@id,'_b')"/>
          <xsl:with-param name="pos" select="position()"/>
        </xsl:call-template>
      </xsl:for-each>
    </reactionnetwork>
  </xsl:template>
          
  <xsl:template name="reactionfb">
    <xsl:param name="id"/>
    <xsl:param name="pos"/>
    <xsl:choose>
      <xsl:when test="contains($id,'_f')">
        <reaction id="{$id}" pos="{$pos}" xmlns="http://www.13cflux.net/fluxml">
          <xsl:apply-templates/>
        </reaction>
      </xsl:when>
      <xsl:when test="contains($id,'_b')">
        <reaction id="{$id}" pos="{$pos}" xmlns="http://www.13cflux.net/fluxml">
          <xsl:for-each select="f:rproduct">
            <reduct>
              <xsl:copy-of select="@*|*"/>
            </reduct>
          </xsl:for-each>      
          <xsl:for-each select="f:reduct">
            <rproduct>
              <xsl:copy-of select="@*|*"/>
            </rproduct>
          </xsl:for-each>      
        </reaction>
      </xsl:when>      
    </xsl:choose>    
  </xsl:template>

<!--  <xsl:template match="f:reaction[not(key('IRREVERSIBLE',@id)) and f:rproduct and not(key('INPUT',f:reduct/@id)) and not(key('INPUT',f:rproduct/@id))]">-->
<!--
  <xsl:template match="f:reaction">
    <reaction id="{@id}_f" pos="{position()-1}" xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </reaction>
    <reaction id="{@id}_b" pos="{position()-1}" xmlns="http://www.13cflux.net/fluxml">
      <xsl:for-each select="f:rproduct">
        <reduct>
            <xsl:copy-of select="@*|*"/>
        </reduct>
      </xsl:for-each>      
      <xsl:for-each select="f:reduct">
        <rproduct>
            <xsl:copy-of select="@*|*"/>
        </rproduct>
      </xsl:for-each>      
    </reaction>
  </xsl:template>
  -->
  <!-- The following template writes, for each input pool, the contribution of input isotopomers to cumomers -->
  
  <xsl:template match="smtb:input">
        <input xmlns="http://www.utc.fr/sysmetab">
            <xsl:copy-of select="@*"/>
            <xsl:variable name="string" select="@string"/>
            <xsl:for-each select="../smtb:cumomer">
                <xsl:sort select="@weight"/>
                <xsl:if test="count(smtb:carbon[substring($string,string-length($string)-@index,1)='1'])=count(smtb:carbon)">
                    <cumomer-contribution subscript="{@subscript}" string="{@pattern}" weight="{@weight}" sign="1"/>
                </xsl:if>
            </xsl:for-each>
        </input>
  </xsl:template>

  <xsl:template match="f:constraints">
    <!-- This template adds the implicit constraints on input and output fluxes-->
    <constraints xmlns="http://www.13cflux.net/fluxml">
      <net xmlns="http://www.13cflux.net/fluxml">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:apply-templates select="f:net/m:math/*"/>
          <xsl:for-each select="../f:reactionnetwork/f:reaction[key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct)]">
            <!-- net fluxes from input metabolites or to output metabolites are necessarily positive -->
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <leq/>
              <cn>0</cn>
              <ci><xsl:value-of select="@id"/></ci>
            </apply>
          </xsl:for-each>
        </math>
      </net>
      <xch xmlns="http://www.13cflux.net/fluxml">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:apply-templates select="f:xch/m:math/*"/>
          <xsl:for-each select="../f:reactionnetwork/f:reaction">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <xsl:choose>
                <xsl:when test="key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct)">
                  <!-- xch fluxes from input metabolites or to output metabolites are necessarily zero -->
                  <eq/>
                </xsl:when>
                <xsl:otherwise>
                  <!-- other xch fluxes are positive -->
                  <leq/>
                </xsl:otherwise>
              </xsl:choose>
              <cn>0</cn>
              <ci><xsl:value-of select="@id"/></ci>
            </apply>
          </xsl:for-each>
        </math>
      </xch>
    </constraints>
  </xsl:template>

</xsl:stylesheet>
