<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   
    Projet  :   PIVERT/Metalippro-PL1
-->

<!--
    
 -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings"   
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  exclude-result-prefixes="smtb sbml celldesigner m str exslt xhtml math xsl f">
  
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
  <xsl:include href="gen_isotopomer_cumomer_list.xsl"/>

  <xsl:strip-space elements="*"/>
    
  <!-- find all label input from f:configuration -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>
  
  <!-- find all Label Measurement (Mass Stocheometry) from f:configuration -->
  <xsl:key name="MEASUREMENTMS" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'[')"/>  

  <!-- find all Label Measurement (LM) from f:configuration -->
  <xsl:key name="MEASUREMENTLM" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'#')"/>  

  <!-- find all reactionType in annotation -->
  <xsl:key name="ANNOTATIONRT" match="f:annotation/f:extension/f:reactionType" use="."/> 
  
  <!-- find all f:reduct from f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>


  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
    
  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>
 
  <xsl:template match="f:pool">

    <pool atoms="{@atoms}" id="{@id}" xmlns="http://www.13cflux.net/fluxml">     

      <xsl:choose>
        <xsl:when test="key('RPRODUCT',@id) and key('REDUCT',@id)">
          <xsl:attribute name="type">intermediate</xsl:attribute>
        </xsl:when>
        <xsl:when test="not (key('RPRODUCT',@id)) and key('REDUCT',@id)">
          <xsl:attribute name="type">input</xsl:attribute>
        </xsl:when>
        <xsl:when test="not(key('REDUCT',@id)) and key('RPRODUCT',@id)">
          <xsl:attribute name="type">output</xsl:attribute>
        </xsl:when>
      </xsl:choose>

        <xsl:choose>

          <xsl:when test="key('INPUT',@id)">
              <xsl:for-each select="key('INPUT',@id)/f:label/@cfg">
                <input string="{.}" xmlns="http://www.utc.fr/sysmetab"/>
              </xsl:for-each>
          </xsl:when>

          <xsl:otherwise>
          
              <!-- Each measurement (if there are some) is translated into a set of <cumomer-contribution> 
                   elements (the measurement is the sum of these contributions) -->

              <!-- For each measurement of "label measurement" type -->
              
              <xsl:for-each select="key('MEASUREMENTLM',@id)">
                <measurement string="{substring-after(.,'#')}" id="{../@id}" xmlns="http://www.utc.fr/sysmetab">
                  <xsl:for-each select="str:split(substring-after(.,'#'),'+')">
                    <xsl:call-template name="disassemble-cumomer">
                      <xsl:with-param name="string" select="."/>    
                    </xsl:call-template>
                  </xsl:for-each>
                </measurement>
              </xsl:for-each>      
                      
              <!-- For each set of measurement of "mass spectrometry" type, the corresponding cumomer-contribution
                   are computed. For each mass isotopomer, say [1,..,n]M#p, where n >= p >=0,
                   all cumomers of weight p,p+1,..,n have contributions. The coefficient are given by the binomial
                   coefficients, up to an alternating sign.
                   
                   Here is an example for [1,2,3]M#0,1,2,3 :
                   
                   M#0 = 000 = 1*xxx - 1*(xx1 + x1x + 1xx) +1*(x11+1x1+11x) -1*(111)
                   M#1 = 001+010+100 = 1*(xx1 + x1x + 1xx) -2*(x11+1x1+11x) +3*(111)
                   M#2 = 011+101+110 =                      1*(x11+1x1+11x) -3*(111)
                   M#3 = 111 =                                               1*(111) 
                  -
                   Another example for [1,2,3,4]M#0,1,2,3,4 (the sum of isotopomers is omitted for clarity)
                   
                   M#0 = 1*xxxx - 1*(xxx1+xx1x+x1xx+1xxx) +1*(xx11+x1x1+x11x+1xx1+1x1x+11xx) -1*(x111+1x11+11x1+111x) +1*(1111)             
                   M#1 =          1*(xxx1+xx1x+x1xx+1xxx) -2*(xx11+x1x1+x11x+1xx1+1x1x+11xx) +3*(x111+1x11+11x1+111x) -4*(1111)
                   M#2 =                                   1*(xx11+x1x1+x11x+1xx1+1x1x+11xx) -3*(x111+1x11+11x1+111x)  6*(1111)
                   M#3 =                                                                      1*(x111+1x11+11x1+111x) -4*(1111)
                   M#4 =                                                                                               1*(1111)
                  
                  i.e. for the mass isotopomer M#p the coefficient of cumomers of weight k=p,p+1,...,n is equal to (-1)^(k-p)*C(k,p)
                  
                  Remark  : this fact should be proved, moreover, the coefficients remain the same even for incomplete fragments.
                  
                  -->
 
              <xsl:variable name="atoms" select="@atoms"/>                
              <xsl:for-each select="key('MEASUREMENTMS',@id)">
                  <xsl:variable name="ID" select="../@id"/> <!-- this is the measurement group id -->
                  <xsl:variable name="fragment" select="substring-before(substring-after(.,'['),']')"/>
                  <xsl:variable name="splittedFragment" select="str:split($fragment,',')"/>
                  <!-- Warning : fragments are supposed to be composed of consecutive atoms -->
                  <xsl:variable name="before" select="str:padding($splittedFragment[1]-1,'x')"/>
                  <xsl:variable name="after" select="str:padding(($atoms)-$splittedFragment[last()],'x')"/>
                  <!-- for each mass isotopomer -->
                  <xsl:for-each select="str:split(substring-after(.,'#M'),',')"> 
                    <xsl:variable name="weight" select="."/>
                    <measurement string="{concat('[',$fragment,']','#M',.)}" id="{$ID}" 
                                 weight="{.}"  xmlns="http://www.utc.fr/sysmetab">
                       <!-- for each cumomer of n atoms (where n is the length of the fragment) 
                            and weight >=  weight of the mass isotopomer -->
                       <xsl:for-each select="exslt:node-set($list)/listOfCumomers[@atoms=count($splittedFragment)]/cumomer[@weight&gt;=current()]">
                           <xsl:sort select="@weight" data-type="number" direction="ascending"/>
                           <!-- dump the cumomer contribution. The binomial coefficients are in the "Cnp" global variable, which is
                                created in the stylesheet "gen_isotopomer_cumomer.xsl" -->
                           <cumomer-contribution subscript="{math:power(2,string-length($after))*@number}" string="{concat($before,@pattern,$after)}" weight="{@weight}" 
                           sign="{(math:power((-1),(@weight)-$weight))*(exslt:node-set($Cnp)/c[(@p=$weight) and (@n=current()/@weight)])}" 
                           xmlns="http://www.utc.fr/sysmetab"/> 
                      </xsl:for-each>
                    </measurement>
                  </xsl:for-each>
              </xsl:for-each>              
          </xsl:otherwise>
          
        </xsl:choose>
        
        <!-- Generation of the pool cumomer list -->
        
        <xsl:call-template name="iteration-cumomers">
        <xsl:with-param name="nbCumomer" select="(math:power(2,@atoms))-1"/>
        <xsl:with-param name="i" select="'1'"/>
        <xsl:with-param name="carbons">
          <carbon position="1" index="0" xmlns="http://www.utc.fr/sysmetab"/>
        </xsl:with-param>
      </xsl:call-template>
         
    </pool>
    
  </xsl:template>

  <!-- These two templates translate the carbon atoms mapping to xml elements in order to 
       allow a straightforward matching of educt cumomers when assembling the equations -->
  
  <xsl:template match="f:reduct|f:rproduct">
    <xsl:element name="{name()}" namespace="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="@*"/>
      <xsl:call-template name="iteration-atoms-map"/>
    </xsl:element>
  </xsl:template>
 
  <xsl:template name="iteration-atoms-map">
    <xsl:param name="i" select="'1'"/>
    <xsl:if test="$i&lt;=string-length(@cfg)">
      <xsl:variable name="c" select="substring(@cfg,$i,1)"/>
      <!-- Attention, on doit numéroter les carbones de droite à gauche si on veut avoir
           une correspondance directe entre les numéros des espèces marquées et le nombre 
           binaire représentant le marquage. -->
      <carbon position="{math:power(2,string-length(@cfg)-($i))}" xmlns="http://www.utc.fr/sysmetab">
      <!-- Ici on balaye les f:rproduct si le contexte appelant était un f:reduct et vice-versa. 
           Cela se fait grace à l'expression XPath (un peu compliquée...) ci dessous : -->
        <xsl:for-each select="../*[name()!=name(current())]">
          <xsl:if test="contains(@cfg,$c)">
            <xsl:attribute name="destination">
              <xsl:value-of select="math:power(2,string-length(substring-after(@cfg,$c)))"/>
            </xsl:attribute>
            <!-- L'attribut "occurence" sert à différencier les deux versions d'une espèce quand la 
            stoechiométrie est supérieure à 1. -->
            <xsl:attribute name="occurence">
              <xsl:value-of select="count(preceding-sibling::*[name()=name(current())])+1"/>
            </xsl:attribute>
            <xsl:copy-of select="@id"/>
          </xsl:if>
        </xsl:for-each>
      </carbon>
      <xsl:call-template name="iteration-atoms-map">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
    
  <xsl:template match="f:configuration/f:measurement/f:model/f:labelingmeasurement"/>
  <xsl:template match="f:annotation"/>
    
</xsl:stylesheet>
