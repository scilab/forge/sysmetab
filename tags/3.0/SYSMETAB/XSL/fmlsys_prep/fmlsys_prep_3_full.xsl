<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Stéphane Mottelet, LMAC
    Date    :   
    Projet  :   PIVERT/Metalippro-PL1
-->


<!-- Cette feuille de style a pour but :

       
-->

<xsl:stylesheet  version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:math="http://exslt.org/math"   
    xmlns:smtb="http://www.utc.fr/sysmetab"
    xmlns:f="http://www.13cflux.net/fluxml"
      exclude-result-prefixes="math f exslt smtb xhtml">
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:strip-space elements="*"/>
	
	<!-- Poids maximal des cumomères présents dans le réseau. -->
	
    <xsl:param name="nodes"/>

    <xsl:variable name="maxweight" select="math:max(//f:pool/@atoms)"/>

    <xsl:variable name="mincumomers" select="f:pool/smtb:cumomer/*"/>
    
    <xsl:key name="cumomers" match="smtb:cumomer" use="@id"/>

    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>        

     <xsl:template match="f:metabolitepools">
        <metabolitepools xmlns="http://www.13cflux.net/fluxml">
            <xsl:apply-templates/>
        </metabolitepools>
        <listOfIntermediateCumomers xmlns="http://www.utc.fr/sysmetab">
            <xsl:call-template name="make-cumomer-list">
                <xsl:with-param name="type" select="'intermediate'"/>
            </xsl:call-template>                  
        </listOfIntermediateCumomers>
        <listOfInputCumomers xmlns="http://www.utc.fr/sysmetab">
            <xsl:call-template name="make-cumomer-list">
                <xsl:with-param name="type" select="'input'"/>
            </xsl:call-template>                  
        </listOfInputCumomers>
    </xsl:template>

    <xsl:template name="make-cumomer-list">
        <xsl:param name="w">1</xsl:param>
        <xsl:param name="type"/>
        <xsl:if test="$w&lt;=$maxweight">
			<xsl:if test="f:pool[@type=$type]/smtb:cumomer[@weight=$w]">
		        <listOfCumomers xmlns="http://www.utc.fr/sysmetab" weight="{$w}">
		            <xsl:for-each select="f:pool[@type=$type]/smtb:cumomer[@weight=$w]">
		                <xsl:sort select="@pool" data-type="text"/>
		                <xsl:sort select="@subscript" data-type="number"/>
		                <cumomer id="{@id}" pool="{@pool}" weight="{$w}" number="{position()}" pattern="{@pattern}"/>
		            </xsl:for-each>
		        </listOfCumomers>
			</xsl:if>
	        <xsl:call-template name="make-cumomer-list">
		        <xsl:with-param name="w" select="($w)+1"/>
		        <xsl:with-param name="type" select="$type"/>                
            </xsl:call-template>
        </xsl:if>
    </xsl:template>      

    <xsl:template match="f:reaction">
		<xsl:variable name="pos" select="count(preceding-sibling::f:reaction)+1"/>
		<reaction id="{@id}" position="{$pos}" xmlns="http://www.13cflux.net/fluxml">
			<xsl:copy-of select="@*"/>
       		<xsl:copy-of select="*"/>
   		</reaction>				
	</xsl:template>
    
    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
