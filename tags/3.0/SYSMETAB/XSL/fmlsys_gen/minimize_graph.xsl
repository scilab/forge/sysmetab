<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:exslt="http://exslt.org/common"  exclude-result-prefixes="exslt">

<xsl:output  method="xml" indent="yes"/>
<xsl:key name="nodes" match="node" use="@id"/> 

<xsl:variable name="nnodes" select="count(/graph/node)"/>

<xsl:variable name="maxchild">
  <xsl:for-each select="/graph/node">
    <xsl:sort select="count(child)" data-type="number"/>
    <xsl:if test="position()=last()">
      <xsl:value-of select="count(child)"/>
    </xsl:if>
  </xsl:for-each>
</xsl:variable>

<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="graph">
<!--   <xsl:message>Graph statistics</xsl:message>
        <xsl:message>****************</xsl:message>
    <xsl:message>nodes : <xsl:value-of select="$nnodes"/></xsl:message>
    <xsl:call-template name="outdegree"/>-->
    <graph>
        <xsl:call-template name="search-alt">
            <xsl:with-param name="todo">
                <xsl:for-each select="node[@source='yes']">
                    <link node="{@id}"/>
                </xsl:for-each>
            </xsl:with-param>
        </xsl:call-template>
    </graph>
</xsl:template>

<xsl:template name="outdegree">
    <xsl:param name="i">0</xsl:param>
    <xsl:variable name="out" select="count(node[count(child)=$i])"/>
    <xsl:if test="$i&lt;=$maxchild">
        <xsl:message>nodes with outdegree=<xsl:value-of select="$i"/> : <xsl:value-of select="concat($out,' (',100*($out div $nnodes),'%)')"/></xsl:message>
        <xsl:call-template name="outdegree">
            <xsl:with-param name="i" select="($i)+1"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template name="search-alt">
    <xsl:param name="todo"/>
    <xsl:param name="visited" select="/.."/>
    <xsl:variable name="nodeid" select="exslt:node-set($todo)/*[1]/@node"/>
    <xsl:choose>
        <xsl:when test="exslt:node-set($visited)/*[@node=$nodeid]">
            <xsl:if test="count(exslt:node-set($todo)/*)&gt;1">
                <xsl:call-template name="search-alt">
                    <xsl:with-param name="todo">
                        <xsl:copy-of select="exslt:node-set($todo)/*[position()&gt;1]"/>
                    </xsl:with-param>
                    <xsl:with-param name="visited">
                        <xsl:copy-of select="$visited"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:if>
        </xsl:when>
        <xsl:when test="(key('nodes',$nodeid)/child) or (count(exslt:node-set($todo)/*)&gt;1)">
            <node>
                <xsl:copy-of select="key('nodes',$nodeid)/@*"/>
            </node>
            <xsl:call-template name="search-alt">
                <xsl:with-param name="todo">
                    <xsl:copy-of select="key('nodes',$nodeid)/child"/>
                    <xsl:copy-of select="exslt:node-set($todo)/*[position()&gt;1]"/>
                </xsl:with-param>
                <xsl:with-param name="visited">
                    <xsl:copy-of select="$visited"/>
                    <link node="{$nodeid}"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:when>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
