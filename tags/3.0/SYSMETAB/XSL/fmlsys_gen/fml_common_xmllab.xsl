<?xml version="1.0" encoding="UTF-8"?>

<!--
Auteur: Florent Tixier
Date: ven juin 20 11:00 CEST 2008
Projet: SYSMETAB/Carnot
-->

<!--
Templates communes à la génération de la simulation XMLlab pour les cas stationnaire et
non-stationnaire. Extraites telles quelles du fichier initial sbmlsys_gen_xmllab4.xsl
-->

  <xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:math="http://exslt.org/math" version="1.0"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb f">


  <xsl:template match="f:pool[smtb:measurement]">
    <matrix label="{@id}_obs" rows="{count(smtb:measurement)}" cols="{3*($experiences)}" save="yes" clear="yes" load="yes">
      <name>
        <xsl:value-of select="@id"/>
      </name>
      <name lang="english">
        <xsl:value-of select="@id"/>
      </name>
      <colheader>
        <xsl:call-template name="iterate-observation-names"/>
      </colheader>
      <xsl:for-each select="smtb:measurement">
        <row>
          <name>
            <xsl:value-of select="concat('#',@string)"/>
          </name>
          <name lang="english">
            <xsl:value-of select="concat('#',@string)"/>
          </name>
        </row>
      </xsl:for-each>
    </matrix>
  </xsl:template>

  <xsl:template match="f:pool[smtb:input]">
    <matrix label="{@id}_input" rows="{count(smtb:input)}" cols="{$experiences}" save="yes" clear="yes" load="yes">
      <name>
        <xsl:value-of select="@id"/>
      </name>
      <name lang="english">
        <xsl:value-of select="@id"/>
      </name>
      <colheader>
        <xsl:call-template name="iterate-input-names"/>
      </colheader>
      <xsl:for-each select="smtb:input">
        <row>
          <name>
            <xsl:value-of select="concat('#',@string)"/>
          </name>
          <name lang="english">
            <xsl:value-of select="concat('#',@string)"/>
          </name>
        </row>
      </xsl:for-each>
    </matrix>
  </xsl:template>

  <xsl:template name="iterate-observation-names">
    <xsl:param name="i">1</xsl:param>
    <col>
      <name>
        <xsl:value-of select="concat('1/sigma',$i)"/>
      </name>
      <name lang="english">
        <xsl:value-of select="concat('1/sigma',$i)"/>
      </name>
    </col>
    <col>
      <name>
        <xsl:value-of select="concat('Obs. ',$i)"/>
      </name>
      <name lang="english">
        <xsl:value-of select="concat('Obs. ',$i)"/>
      </name>
    </col>
    <col state="disabled">
      <name>
        <xsl:value-of select="concat('Ident. ',$i)"/>
      </name>
      <name lang="english">
        <xsl:value-of select="concat('Ident. ',$i)"/>
      </name>
    </col>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-observation-names">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="iterate-input-names">
    <xsl:param name="i">1</xsl:param>
    <col>
      <name>
        <xsl:value-of select="concat('Input ',$i)"/>
      </name>
      <name lang="english">
        <xsl:value-of select="concat('Input ',$i)"/>
      </name>
    </col>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-input-names">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="f:reaction">
    <scalar label="{@id}">
      <name>
        <xsl:value-of select="concat(@id,', ',@name)"/>
      </name>
      <name lang="english">
        <xsl:value-of select="concat(@id,', ',@name)"/>
      </name>
    </scalar>
  </xsl:template>
  
  <xsl:template match="f:reactionnetwork">
    <matrix label="flux_obs" rows="{count(../f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux)}" cols="2" save="yes" load="yes">
      <name/>
      <colheader>
        <col>
          <name>1/sigma</name>
          <name lang="english">1/sigma</name>
        </col>
        <col>
          <name>Valeur en %</name>
          <name lang="english">Value in %</name>
        </col>
      </colheader>
      <xsl:for-each select="../f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
        <row>
          <name>
            <xsl:value-of select="concat(f:textual,', ',f:textual)"/>
          </name>
          <name lang="english">
            <xsl:value-of select="concat(f:textual,', ',f:textual)"/>
          </name>
          <value>1</value>
        </row>
      </xsl:for-each>
    </matrix>
  </xsl:template>
  
  <!--onglet flux observes avec une colonne ident.-->

  <xsl:template match="f:reactionnetwork" mode="with_ident">
    <matrix label="flux_obs" rows="{count(../f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux)}" cols="3" save="yes" load="yes">
      <name/>
      <colheader>
        <col>
          <name>1/sigma</name>
          <name lang="english">1/sigma</name>
        </col>
        <col>
          <name>Valeur en %</name>
          <name lang="english">Value in %</name>
        </col>
        <col state="disabled">
          <name>Ident.</name>
          <name lang="english">Ident.</name>
        </col>
      </colheader>
      <xsl:for-each select="../f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
        <row>
          <name>
            <xsl:value-of select="concat(f:textual,', ',f:textual)"/>
          </name>
          <name lang="english">
            <xsl:value-of select="concat(f:textual,', ',f:textual)"/>
          </name>
          <value>1</value>
        </row>
      </xsl:for-each>
    </matrix>
  </xsl:template>
  
</xsl:stylesheet>