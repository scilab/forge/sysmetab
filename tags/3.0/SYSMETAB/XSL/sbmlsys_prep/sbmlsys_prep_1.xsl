<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Thu Mar 27 12:21:21 CET 2008
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de faire quelques op�rations de base, avant d'enchainer
    les autres transformations XSL :
    
    1 -  v�rification de la pr�sence de la chaine de caract�res "LABEL_INPUT" dans l'�l�ment "notes"
        d'un �l�ment <sbml:species>, et dans ce cas ajout d'un �l�ment smtb:input. Exemple pour
		
		LABEL_INPUT 0001,0010,0100,1000
		
		cela donne :
		
		<smtb:input>
			<smtb:token>0001</smtb:token>
			<smtb:token>0010</smtb:token>
			<smtb:token>0100</smtb:token>
			<smtb:token>1000</smtb:token>
		</smtb:input>

		Attention, dans LABEL_INPUT on pr�cise des isotopom�res.
		
	2 - v�rification de la pr�sence de la chaine de caract�res "LABEL_MEASUREMENT" dans l'�l�ment "notes"
        d'un �l�ment <sbml:species>, et dans ce cas ajout d'un �l�ment smtb:measurement. Exemple pour
		
		LABEL_MEASUREMENT 1xx,x1x,xx1
		
		cela donne :
		
		<smtb:measurement>
			<smtb:token>1xx</smtb:token>
			<smtb:token>x1x</smtb:token>
			<smtb:token>xx1</smtb:token>
		</smtb:measurement>
    
	
		Attention dans LABEL_MEASUREMENT on pr�cise des cumom�res avec la vieille notation de Wiechert,
		� savoir 1 ou 0 = exactement marqu� ou non marqu� et x = indiff�rent. Cela permet de g�rer des
		observations plus r�alistes.

	3-	Pour les r�actions connues ou mesur�es TRANSPORT ou KNOWN_TRANSITION_OMITTED (choisi � la souris
		dans CellDesigner) on ajoute un attribut "known" ou "observation" � l'�l�ment <sbml:reaction> correspondant 
	
    4 - Lorqu'une esp�ce intervient avec une stoechiom�trie sup�rieure � 1, dans un �l�ment
    <speciesReference> d'une r�action, on remplace cet �l�ment par autant d'occurences de lui-meme
    avec une stoechiom�trie de 1. Cela est parfaitement standard et permet de ne pas avoir �
    g�rer l'exception pour la g�n�ration des �quations de bilan sur les esp�ces marqu�es.
    
 -->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="smtb sbml celldesigner m str exslt xhtml">
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*[name()!='metaid']|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="sbml:speciesReference">
		
		<!-- Template de prise en compte de la stoechiom�trie (2 au maximum) -->

        <xsl:choose>
            <xsl:when test="not(@stoichiometry)">
                <speciesReference xmlns="http://www.sbml.org/sbml/level2/version4">
                    <xsl:copy-of select="@*[name()!='metaid']"/>
                </speciesReference>
            </xsl:when>

			<!-- Dans le cas o� la stoechiom�trie est de 2, on duplique la r�f�rence � l'esp�ce (produit ou substrat) -->

           <xsl:when test="@stoichiometry=2">
                <speciesReference xmlns="http://www.sbml.org/sbml/level2/version4">
                    <xsl:copy-of select="@*[not(self::stoichiometry)]"/>
                </speciesReference>
                <speciesReference xmlns="http://www.sbml.org/sbml/level2/version4">
                    <xsl:copy-of select="@*[not(self::stoichiometry)]"/>
                </speciesReference>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    
    
    <xsl:template match="sbml:species[sbml:notes]">
	
		<!-- "Tokenisation" des entr�es et des observations (voir explication plus haut) -->
	
        <species xmlns="http://www.sbml.org/sbml/level2/version4">
            <xsl:apply-templates select="@*[name()!='metaid']"/>
			<xsl:if test="normalize-space(sbml:notes/xhtml:html/xhtml:body)='SYMMETRIC'">
				<xsl:attribute name="symmetric">yes</xsl:attribute>				
			</xsl:if>
            <xsl:choose>
                <xsl:when test="substring-before(normalize-space(sbml:notes/xhtml:html/xhtml:body),' ')='LABEL_INPUT'">
					<xsl:for-each select="str:split(substring-after(normalize-space(sbml:notes/xhtml:html/xhtml:body),' '),',')">
						<input string="{.}" xmlns="http://www.utc.fr/sysmetab">
							<xsl:for-each select="str:split(.,'+')">
								<token>
									<xsl:value-of select="."/>
								</token>
							</xsl:for-each>
						</input>
					</xsl:for-each>
                </xsl:when>
                <xsl:when test="substring-before(normalize-space(sbml:notes/xhtml:html/xhtml:body),' ')='LABEL_MEASUREMENT'">
					<xsl:for-each select="str:split(substring-after(normalize-space(sbml:notes/xhtml:html/xhtml:body),' '),';')">
						<measurement string="{.}" xmlns="http://www.utc.fr/sysmetab">
							<xsl:for-each select="str:split(.,'+')">
								<token>
									<xsl:value-of select="."/>
								</token>
							</xsl:for-each>
						</measurement>
					</xsl:for-each>
                </xsl:when>
            </xsl:choose>            
            <xsl:apply-templates select="*[not(self::sbml:notes)]"/>
         </species>
    </xsl:template>
	
    <xsl:template match="sbml:reaction">

		<!-- Pour les r�actions connues ou mesur�es TRANSPORT ou KNOWN_TRANSITION_OMITTED 
			on ajoute un attribut "known" ou "observation" � l'�l�ment <sbml:reaction> correspondant -->
	
        <reaction xmlns="http://www.sbml.org/sbml/level2/version4">
            <xsl:apply-templates select="@*[name()!='metaid']"/>
			<xsl:choose>
            	<xsl:when test="sbml:annotation/celldesigner:extension/celldesigner:reactionType='TRANSPORT'">
                	<xsl:attribute name="known">yes</xsl:attribute>
            	</xsl:when>
				<xsl:when test="sbml:annotation/celldesigner:extension/celldesigner:reactionType='KNOWN_TRANSITION_OMITTED'">
                	<xsl:attribute name="observation">yes</xsl:attribute>
            	</xsl:when>
			</xsl:choose>
            <xsl:apply-templates select="*"/>
        </reaction>
    </xsl:template>

   <xsl:template match="sbml:listOfUnitDefinitions"/>

   <xsl:template match="sbml:listOfCompartments"/>

	<!-- Par d�faut on supprime les autres annotation sp�cifiques � CellDesigner -->

   <xsl:template match="sbml:annotation"/>
   
</xsl:stylesheet>
