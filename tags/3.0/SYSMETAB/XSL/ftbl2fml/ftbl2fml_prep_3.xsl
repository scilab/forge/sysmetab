<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:ftbl="http://ftbl.org"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func ftbl math">
  
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

<!--  <xsl:key name="cfg" match="contains(.,'#')[1]" use="."/>-->

  <xsl:key name="REDUCTPRODUCT" match="f:reduct|f:rproduct" use="@id"/>  

  <xsl:key name="REDUCT" match="f:reduct" use="@id"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="fluxml">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </fluxml>
  </xsl:template>

  <xsl:template match="f:metabolitepools">
    <metabolitepools xmlns="http://www.13cflux.net/fluxml">
      <xsl:for-each select="f:pool">
        <xsl:sort select="@id" order="ascending" data-type="text"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </metabolitepools>
  </xsl:template>

  <xsl:template match="m:math">
    <math xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:for-each select="m:apply[m:eq or m:leq or m:geq]"> 
        <xsl:sort select="m:ci" order="ascending" data-type="text"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>      
    </math>
  </xsl:template>

<!--  <xsl:template match="f:variables">
    <variables xmlns="http://www.13cflux.net/fluxml">
      <xsl:for-each select="f:fluxvalue[@type='net']"> 
        <xsl:sort select="@flux" order="ascending" data-type="text"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>      
      <xsl:for-each select="f:fluxvalue[@type='xch']"> 
        <xsl:sort select="@flux" order="ascending" data-type="text"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>      
    </variables>
  </xsl:template>
-->
  <xsl:template match="*|@*|text()">    
      <xsl:copy>
          <xsl:apply-templates select="*|@*|text()"/>
      </xsl:copy>
  </xsl:template>


</xsl:stylesheet>  
