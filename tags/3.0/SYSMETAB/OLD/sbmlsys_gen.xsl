<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"    version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml">
    
    
    <xsl:output method="text" encoding="ISO-8859-1"/>
    <xsl:param name="verbose">no</xsl:param>

    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:model">
        <xsl:apply-templates select="sbml:listOfReactions"/>
        <xsl:apply-templates select="sbml:listOfSpecies"/>
    </xsl:template>

    <xsl:template match="sbml:listOfReactions">
        <xsl:if test="$verbose='yes'">
            <xsl:for-each select="sbml:reaction">
                <xsl:value-of select="concat(@id,' : ',@name,'&#xA;')"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template match="sbml:listOfSpecies">
        
        <!-- Boucle sur les esp�ces qui sont soit des produits (products) soit des substrats (educts) : -->        
        
        <xsl:for-each select="sbml:species[key('reactionSpecies',@id)]"> 

            <!-- Pour chacune de ces esp�ces on r�cup�re la premi�re occurence d'un �l�ment <speciesReference>
                 dans un �l�ment <reaction>, dont l'attribut @carbons donne le nombre de carbones de l'esp�ce. -->

            <xsl:variable name="nbC" select="string-length(key('reactionSpecies',@id)[@carbons!='']/@carbons)"/>

            <!-- si on est en mode "bavard" on �crit l'id de l'esp�ce suivi de son nom complet et du
                 nombre de carbone. -->

            <xsl:if test="$verbose='yes'">
                <xsl:value-of select="concat('&#xA;',@id,' : ',@name)"/>
                <xsl:if test="$nbC&gt;0">
                    <xsl:value-of select="concat(' (',$nbC,' carbone')"/>
                    <xsl:if test="$nbC&gt;1">
                        <xsl:text>s</xsl:text>
                    </xsl:if>
                    <xsl:text>)</xsl:text>
                </xsl:if>
                <xsl:text>&#xA;</xsl:text>
            </xsl:if>
            
            <xsl:choose>
                
                <!-- Si l'esp�ce est un m�tabolite interm�diaire -->
                
                <xsl:when test="key('products',@id) and key('reactants',@id)">

                    <!-- On affiche l'�quation du bilan stoechiom�trique, -->

                    <xsl:call-template name="bilan"/>
                    
                    <!-- puis on g�n�re les �quations correspondant au bilan de chaque enrichissement positionnel de l'eps�ce. -->

                    <xsl:call-template name="iteration">
                        <xsl:with-param name="nbC" select="$nbC"/>
                        <xsl:with-param name="i" select="'1'"/>
                    </xsl:call-template>
                
                </xsl:when>
                
                <!-- Si l'esp�ce est un m�tabolite entrant (On suppose que le m�tabolite n'entre que via 
                     une seule r�action-->
                
                <xsl:when test="not (key('products',@id))">

                    <!-- Si on est en mode bavard, on affiche les informations de ce m�tabolite et on
                         donne les noms des fluxs  -->

                    <xsl:if test="$verbose='yes'">
                        <xsl:variable name="flux" select="key('reactants',@id)/../../@id"/>
                        <xsl:text>m�tabolite entrant&#xA;</xsl:text>
                        <xsl:text>Flux associ�s : &#xA;</xsl:text>
                        <xsl:value-of select="concat($flux,' : flux total&#xA;')"/>
                        <xsl:call-template name="iterationFluxEntrant">
                            <xsl:with-param name="flux" select="$flux"/>
                            <xsl:with-param name="nbC" select="$nbC"/>
                            <xsl:with-param name="i" select="'1'"/>
                        </xsl:call-template>
                        <xsl:text>&#xA;</xsl:text>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="not(key('reactants',@id))">
                    <xsl:if test="$verbose='yes'">
                        <xsl:text>m�tabolite sortant&#xA;</xsl:text>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="iterationFluxEntrant">
        <xsl:param name="flux"/>
        <xsl:param name="nbC"/>
        <xsl:param name="i"/>
        <xsl:variable name="name" select="@id"/>
        <xsl:value-of select="concat($flux,'_',$i,' : flux marqu� sur le carbone n�',$i,'&#xA;')"/>
        <xsl:if test="$i&lt;$nbC">
            <xsl:call-template name="iterationFluxEntrant">
                <xsl:with-param name="flux" select="$flux"/>
                <xsl:with-param name="nbC" select="$nbC"/>
                <xsl:with-param name="i" select="($i)+1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

<!-- Template de fabrication de l'�quation de bilan stoechiom�trique
     de l'esp�ce @id -->

<xsl:template name="bilan">
    <xsl:variable name="name" select="@id"/>

    <xsl:value-of select="'0='"/>

    <!-- outflux rule -->

    <xsl:for-each select="key('reactants',@id)">
        <xsl:choose>
            <xsl:when test="@stoichiometry=1">
                <xsl:value-of select="concat('-',../../@id)"/>
            </xsl:when>
            <xsl:when test="@stoichiometry=2">
                <xsl:value-of select="concat('-2*',../../@id)"/>
            </xsl:when>
        </xsl:choose>
    </xsl:for-each>

    <!-- influx rule -->

    <xsl:for-each select="key('products',@id)">
        <xsl:value-of select="concat('+',../../@id)"/>
    </xsl:for-each>
    
    <xsl:text>&#xA;</xsl:text>
    
</xsl:template>



<!-- Template de fabrication du second membre de l'�quation diff�rentielle
     correpondant � l'enrichissement positionnel num�ro i de l'esp�ce @id -->


<xsl:template name="iteration">
    <xsl:param name="nbC"/>
    <xsl:param name="i"/>
    <xsl:variable name="name" select="@id"/>
    <xsl:if test="$i&lt;=$nbC">
        <!--<xsl:value-of select="concat('d(',$name,'_',$i,')/dt=')"/>-->
        <xsl:text>0=</xsl:text>

        <!-- outflux rule -->

        <xsl:for-each select="key('reactants',@id)">
            <xsl:choose>
                <xsl:when test="@stoichiometry=1">
                    <xsl:value-of select="concat('-',../../@id,'*',$name,'_',$i)"/>
                </xsl:when>
                <xsl:when test="@stoichiometry=2">
                    <xsl:value-of select="concat('-2*',../../@id,'*',$name,'_',$i)"/>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>

        <!-- influx rule -->

        <xsl:for-each select="key('products',@id)"> 
            
            <xsl:variable name="Clabel" select="substring(@carbons,$i,1)"/>
            
            <xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[@stoichiometry=1]">
                <xsl:choose>
                    <xsl:when test="not(key('products',@species))">
                        <xsl:variable name="flux" select="key('reactants',@species)/../../@id"/>
                        <xsl:value-of select="concat('+',$flux,'_',$i)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="i_matching">
                            <xsl:for-each select="str:tokenize(@carbons,'')">
                                <xsl:if test=".=$Clabel">
                                    <xsl:value-of select="position()"/>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        <!-- on ajoute le terme que s'il y a correspondance des carbones -->
                        <xsl:if test="$i_matching&gt;0">
                            <xsl:value-of select="concat('+',../../@id)"/>
                            <xsl:value-of select="concat('*',@species,'_',$i_matching)"/>
                        </xsl:if>                        
                </xsl:otherwise>
                </xsl:choose>
           </xsl:for-each>
             
           <!-- Cas non rencontr� pour l'instant, mais on ne peut avoir de produit entre
               deux enrichissement positionnels, la correspondance entre le carbone de
               l'esp�ce courante et le carbone de la boucle <xsl:for-each select="str:tokenize(.,'')">
               ne peut se produire qu'une fois. -->
                          
           <xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[@stoichiometry=2]">
                <xsl:variable name="species" select="@species"/>
                <xsl:for-each select="str:split(@carbons,',')">
                    <xsl:variable name="i_matching">
                        <xsl:for-each select="str:tokenize(.,'')">
                            <xsl:if test=".=$Clabel">
                                <xsl:value-of select="position()"/>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:if test="$i_matching&gt;0">
                            <xsl:value-of select="concat('+',../../@id)"/>
                            <xsl:value-of select="concat('*',$species,'_',$i_matching)"/>
                    </xsl:if>
                </xsl:for-each>
            </xsl:for-each>

        </xsl:for-each>


        <xsl:text>&#xA;</xsl:text>
        <xsl:call-template name="iteration">
            <xsl:with-param name="nbC" select="$nbC"/>
            <xsl:with-param name="i" select="($i)+1"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>


</xsl:stylesheet>
