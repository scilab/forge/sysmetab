<?xml version="1.0" encoding="ISO-8859-1"?>


<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:math="http://exslt.org/math"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml">
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

	<xsl:template match="species">
		<species>
			<xsl:for-each select="str:split(@measurement,',')">
				<measurement>
					<xsl:for-each select="str:split(.,'+')">
						<xsl:call-template name="truc"/>
					</xsl:for-each>
				</measurement>
			</xsl:for-each>
		</species>
	</xsl:template>

	<xsl:template name="truc">
		<xsl:param name="i" select="string-length(.)"/>
		<xsl:param name="string" select="."/>
		<xsl:param name="id" select="'0'"/>
		
		<xsl:choose>
			<xsl:when test="$i=0">
				<isotopomer id="{$id}" string="{$string}"/>
			</xsl:when>
			<xsl:otherwise>		
				<xsl:choose>
					<xsl:when test="substring($string,$i,1)='x'">
						<xsl:call-template name="truc">
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'1',substring($string,($i)+1))"/>
							<xsl:with-param name="id" select="$id"/>
						</xsl:call-template>
						<xsl:call-template name="truc">
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'0',substring($string,($i)+1))"/>
							<xsl:with-param name="id" select="$id"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="substring($string,$i,1)='1'">
						<xsl:call-template name="truc">
							<xsl:with-param name="i" select="($i)-1"/>
							<xsl:with-param name="string" select="$string"/>
							<xsl:with-param name="id" select="$id + math:power(2,string-length(.)-$i)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="substring($string,$i,1)='0'">
						<xsl:call-template name="truc">
							<xsl:with-param name="i" select="($i)-1"/>
							<xsl:with-param name="string" select="$string"/>
							<xsl:with-param name="id" select="$id"/>
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
