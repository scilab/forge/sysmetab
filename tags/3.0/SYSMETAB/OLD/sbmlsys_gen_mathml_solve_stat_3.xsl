<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer diff�rentes fonctions sp�cifiques
	d'un mod�le donn� sous forme SBML, �dit� dans CellDesigner. On prend ici l'exemple
	du "Branching Network" de l'article de N. Isermann, W. Wiechert :
	
	Metabolic isotopomer labeling systems. Part II: structural flux identifiability
	analysis.

            A_out
	         |
			 |v6
			 |
			 v
	_________A _______
	|        |       |
	|        |v2     |
	|v3      |       |
	|        v       |
	|        D       |v1
	|        |       |
	|        |v4     |
	|        |       |
	|        v       |
	_______> F <______
	         |
			 |v5
			 |
			 v
			 G

    Ici on fait abstraction des identificateurs originaux des cumom�res et des flux. On a dans cet
	exemple (les cumom�res des m�tabolites d'entr�e et de sortie ne font pas partie de l'�tat) :
	
	x1=[A$01;A$10;D$1;F$01;F$10];
	x2=[A$11;F$11];
	
	x1_input=[A_out$01;A_out$10];
	x2_input=[A_out$11];
	
	v=[v1;v2;v3;v4;v5;v6];
    
    Ici on g�n�re en fait cette fonction d�crite en MathML, il faut enchainer avec
    une derni�re transformation pour avoir le code Scilab (sbml_sys_text.xsl)


*** 1 - une fonction Scilab permettant
    calculer les diff�rentes fractions des cumom�res en fonction des flux
    et des fractions des cumom�res des entr�es. 
	On g�n�re aussi le code des blocs non nuls de la jacobienne des �tats par rapport � v
	
	
	Exemple pour le "branching network"
    du papier "part II" de Wiechert :
	
	function [x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input)

		n1=5;

		// Cumom�res de poids 1

		M1_ijv=[1,1,-(v(1)+v(2)+v(3))
		2,2,-(v(1)+v(2)+v(3))
		3,2,v(2)
		3,1,v(2)
		3,3,-(v(4)+v(4))
		4,1,v(1)
		4,2,v(3)
		4,3,v(4)
		4,4,-v(5)
		5,2,v(1)
		5,1,v(3)
		5,3,v(4)
		5,5,-v(5)];
		M1=sparse(M1_ijv(:,1:2),M1_ijv(:,3),[n1,n1]);

		b1_ijv=[1,1,v(6).*x1_input(1,:)
		2,1,v(6).*x1_input(2,:)];
		b1_1=s_full(b1_ijv(:,1:2),b1_ijv(:,3),[n1,1]);

		[M1_handle,M1_rank]=lufact(M1);
		x1=lusolve(M1_handle,-[b1_1]);

		dg1_dv_ijv=[1,6,x1_input(1,:)
		1,1,-x1(1,:)
		1,2,-x1(1,:)
		1,3,-x1(1,:)
		2,6,x1_input(2,:)
		2,1,-x1(2,:)
		2,2,-x1(2,:)
		2,3,-x1(2,:)
		3,2,x1(2,:)
		3,2,x1(1,:)
		3,4,-x1(3,:)
		3,4,-x1(3,:)
		4,1,x1(1,:)
		4,3,x1(2,:)
		4,4,x1(3,:)
		4,5,-x1(4,:)
		5,1,x1(2,:)
		5,3,x1(1,:)
		5,4,x1(3,:)
		5,5,-x1(5,:)];
		dg1_dv_1=s_full(dg1_dv_ijv(:,1:2),dg1_dv_ijv(:,3),[n1,6]);
		dx1_dv=zeros(n1,6,1);
		dx1_dv_1=lusolve(M1_handle,-dg1_dv_1);

		ludel(M1_handle);
		dx1_dv(:,:,1)=dx1_dv_1;

		n2=2;

		// Cumom�res de poids 2

		M2_ijv=[1,1,-(v(1)+v(2)+v(3))
		2,1,v(1)
		2,1,v(3)
		2,2,-v(5)];
		M2=sparse(M2_ijv(:,1:2),M2_ijv(:,3),[n2,n2]);

		b2_ijv=[1,1,v(6).*x2_input(1,:)
		2,1,v(4).*x1(3,:).*x1(3,:)];
		b2_1=s_full(b2_ijv(:,1:2),b2_ijv(:,3),[n2,1]);

		[M2_handle,M2_rank]=lufact(M2);
		x2=lusolve(M2_handle,-[b2_1]);

		dg2_dv_ijv=[1,6,x2_input(1,:)
		1,1,-x2(1,:)
		1,2,-x2(1,:)
		1,3,-x2(1,:)
		2,1,x2(1,:)
		2,3,x2(1,:)
		2,4,x1(3,:).*x1(3,:)
		2,5,-x2(2,:)];
		dg2_dv_1=s_full(dg2_dv_ijv(:,1:2),dg2_dv_ijv(:,3),[n2,6]);
		db2_dx1_ijv=[2,3,x1(3,:).*v(4)
		2,3,x1(3,:).*v(4)];
		db2_dx1_1=sparse(db2_dx1_ijv(:,1:2),db2_dx1_ijv(:,3),[n2,n1]);
		dx2_dv=zeros(n2,6,1);
		dx2_dv_1=lusolve(M2_handle,-(dg2_dv_1+db2_dx1_1*dx1_dv_1));
		ludel(M2_handle);
		dx2_dv(:,:,1)=dx2_dv_1;
	endfunction

2 *** - Une fonction permettant de calculer la fonction cout du probl�me d'identification
	ainsi que son gradient (sans utiliser d'�tat adjoint; on utilise directement les jacobiennes
	des �tats par rapport � v)


	function [cost,grad]=costAndGrad(v)
		[x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input);
		e=(C1*x1+C2*x2)-yobs;
		cost=0.5*(e(:,1)'*alpha*e(:,1)+(E*v-vobs(:,1))'*sigma*(E*v-vobs(:,1)));
		grad=e(:,1)'*alpha*(C1*dx1_dv(:,:,1)+C2*dx2_dv(:,:,1))+(E*v-vobs(:,1))'*sigma*E;
	endfunction


3 *** - Les "matrices d'observation" correspondant aux cumom�res et aux fluxs observ�s

	1) isotopom�res en entr�e -> contributions aux cumom�res en entr�e, matrices D1,D2, etc.

	Exemple ici pour A_out (seul m�tabolite d'entr�e), on pr�cise dans les notes (dans CellDesigner)
	
	LABEL_INPUT 01,10,11

	ce qui signifie qu'on donne en entr�e les trois isotopom�res dans un vecteur A_out_input
	qui contient en fait [A_out#01;A_out#10;A_out#11]. Pour le calcul des cumom�res, on a besoin
	de connaitre les contributions aux vecteurs d'entr�es x1_input (cumom�res de poids 1) et
	x2_input (cumom�res de poids 2). On g�n�re donc les matrices D1 et D2 telles que
	
	x1_input=D1*A_out_input;
	x2_input=D2*A_out_input;


	// Matrices d'entr�e
	D1_ijv=[1,1,1
	1,3,1
	2,2,1
	2,3,1];
	D1=sparse(D1_ijv(:,1:2),D1_ijv(:,3),[2,3]);
	D2_ijv=[1,3,1];
	D2=sparse(D2_ijv(:,1:2),D2_ijv(:,3),[1,3]);
	
	2) cumom�res calcul�s -> isotopom�res observ�s

	Exemple ici pour F, dont on observe les cumom�res F$10,F$01,F$11, on a dans les notes

	LABEL_MEASUREMENT 1x,x1,11
	
	avec la convention du logiciel 13CFlux (1x=cumom�re 10, x1=cumom�re 01), on a besoin de
	pouvoir calculer ces observations en fonction des vecteurs des cumom�res calcul�s x1,x2,
	on g�n�re donc les matrices C1 et C2 telles que
	
	y=C1*x1+C2*x2

	et ici y contient finalement [F$10;F$01;F$11] car il n'y a pas d'autre observation dans 
	le mod�le.

	// Matrices d'observation (cumom�res)
	C1_ijv=[1,5,1
	2,4,1];
	C1=sparse(C1_ijv(:,1:2),C1_ijv(:,3),[3,5]);
	C2_ijv=[3,2,1];
	C2=sparse(C2_ijv(:,1:2),C2_ijv(:,3),[3,2]);


	3) flux -> flux observ�s
	
	Dans le Branching Network, l'exemple utilis�, on observe v5,v6. Dans le mod�le CellDesigner
	ces deux r�actions/flux sont identifi�s comme TRANSPORT ou KNOWN_TRANSITION_OMMITED


	// Matrice d'observation (flux)
	E_ijv=[1,5,1
	2,6,1];
	E=sparse(E_ijv(:,1:2),E_ijv(:,3),[2,6]);

    
    3 - une fonction Scilab [A,b]=fluxSubspace(w) calculant le param�trage du
    sous-espace des flux v v�riant
    
        Nv=0 
        Hv=w
        
    o� N est la matrice stoechiom�trique et Hv=w exprime des contraintes d'�galit�
    sur les flux, typiquement utilis�e lorsque des flux ont des valeurs connues.

	Ici A=[N;H] et b=[0;w]

	function [A,b]=fluxSubspace(w)
	// Construction des matrices (dont la matrice de stoechiom�trie) permettant
	// de calculer le changement de variable affine des flux.
		N=zeros(3,6);
		// A
		N(1,1)=N(1,1)-1;
		N(1,2)=N(1,2)-1;
		N(1,3)=N(1,3)-1;
		N(1,6)=N(1,6)+1;
		// D
		N(2,2)=N(2,2)+1;
		N(2,2)=N(2,2)+1;
		N(2,4)=N(2,4)-1;
		N(2,4)=N(2,4)-1;
		// F
		N(3,1)=N(3,1)+1;
		N(3,3)=N(3,3)+1;
		N(3,4)=N(3,4)+1;
		N(3,5)=N(3,5)-1;
		// Flux connus.
		H=zeros(1,6);
		H(1,6)=1;
		b=[zeros(3,1);w];
		A=[N;H];
	endfunction

    
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
        version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="file"/>
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="experiences">2</xsl:param>

	<xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>


	<xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>
    
    <xsl:template match="/">
        <carbon-labeling-system>
          <xsl:apply-templates/>
        </carbon-labeling-system>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:model">
		<smtb:function>
			<m:ci>solveCumomers</m:ci>
			<smtb:input>
				<m:list>
					<m:ci>v</m:ci>
	                <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				    <m:ci>
                            <xsl:value-of select="concat('x',@weight,'_input')"/>
                        </m:ci>
    	            </xsl:for-each>
				</m:list>
			</smtb:input>
			<smtb:output>
                <m:list>
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				    <m:ci>
                            <xsl:value-of select="concat('x',@weight)"/>
                        </m:ci>
                    </xsl:for-each>
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				    <m:ci>
                            <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                        </m:ci>
                    </xsl:for-each>
	            </m:list>
    		</smtb:output>
			<smtb:body>
	            <xsl:apply-templates select="smtb:listOfIntermediateCumomers"/>
			</smtb:body>
		</smtb:function>

		<smtb:function>
			<m:ci>costAndGrad</m:ci>
			<smtb:input>
				<m:list>
					<m:ci>v</m:ci>
				</m:list>
			</smtb:input>`
			<smtb:output>
				<m:list>
					<m:ci>cost</m:ci>
					<m:ci>grad</m:ci>
				</m:list>
			</smtb:output>
			<smtb:body>
				<m:apply>
					<m:eq/>
                	<m:list separator=",">
                    	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				    	<m:ci>
                            	<xsl:value-of select="concat('x',@weight)"/>
                        	</m:ci>
                    	</xsl:for-each>
                    	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				    	<m:ci>
                            	<xsl:value-of select="concat('dx',@weight,'_dv')"/>
                        	</m:ci>
                    	</xsl:for-each>
	            	</m:list>
					<m:apply>
						<m:fn>
							<m:ci>solveCumomers</m:ci>
						</m:fn>
						<m:ci>v</m:ci>
                    	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				    	<m:ci>
                            	<xsl:value-of select="concat('x',@weight,'_input')"/>
                        	</m:ci>
                    	</xsl:for-each>
					</m:apply>
				</m:apply>
				<m:apply>
					<m:eq/>
					<m:ci>e</m:ci>
					<m:apply>
						<m:minus/>
						<m:apply>
							<m:plus/>
							<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
								<m:apply>
									<m:times/>
 		   				    		<m:ci>
	                            		<xsl:value-of select="concat('C',@weight)"/>
	                        		</m:ci>
 		   				    		<m:ci>
	                            		<xsl:value-of select="concat('x',@weight)"/>
	                        		</m:ci>
								</m:apply>
                    		</xsl:for-each>						
						</m:apply>
						<m:ci>yobs</m:ci>
					</m:apply>
				</m:apply>
				<m:apply>
					<m:eq/>
					<m:ci>cost</m:ci>
					<m:apply>
						<m:times/>
						<m:cn>0.5</m:cn>
						<m:apply>
							<m:plus/>
							<m:apply> <!-- contribution des observations des flux -->
								<m:times/>
								<m:apply>
									<m:transpose/>
									<m:apply>
										<m:minus/>
										<m:apply>
											<m:times/>
											<m:ci>E</m:ci>
											<m:ci>v</m:ci>
										</m:apply>
										<m:ci type="matrix">vobs</m:ci>
									</m:apply>
								</m:apply>
								<m:ci>sigma</m:ci>
								<m:apply>
									<m:minus/>
									<m:apply>
										<m:times/>
										<m:ci>E</m:ci>
										<m:ci>v</m:ci>
									</m:apply>
									<m:ci type="matrix">vobs</m:ci>
								</m:apply>
							</m:apply>
							<xsl:call-template name="iterate-cost"/> <!-- contributions des observations  des marquages -->
						</m:apply>
					</m:apply>
				</m:apply>
				<m:apply>
					<m:eq/>
					<m:ci>grad</m:ci>
					<m:apply>
						<m:plus/>
						<m:apply> <!-- contribution des observations des flux -->
							<m:times/>
							<m:apply>
								<m:transpose/>
								<m:apply>
									<m:minus/>
									<m:apply>
										<m:times/>
										<m:ci>E</m:ci>
										<m:ci>v</m:ci>
									</m:apply>
									<m:ci type="matrix">vobs</m:ci>
								</m:apply>
							</m:apply>
							<m:ci>sigma</m:ci>
							<m:ci>E</m:ci>
						</m:apply>
						<xsl:call-template name="iterate-grad"/> <!-- contributions des observations  des marquages -->
					</m:apply>
				</m:apply>
			</smtb:body>
        </smtb:function>
        
        <smtb:function>
            <m:ci>fluxSubspace</m:ci>
			<smtb:input>
				<m:list>
					<m:ci>_fluxes</m:ci>
				</m:list>
			</smtb:input>
			<smtb:output>
				<m:list>
				    <m:ci>A</m:ci>
				    <m:ci>b</m:ci>
				</m:list>
			</smtb:output>
			<smtb:body>
                <smtb:comment>Construction des matrices (dont la matrice de stoechiom�trie) permettant</smtb:comment>
                <smtb:comment>de calculer le changement de variable affine des flux.</smtb:comment>
				
                <xsl:apply-templates select="sbml:listOfSpecies" mode="stoichiometric-matrix"/>
                
				<smtb:comment>Flux impos�s dans l'interface</smtb:comment>
				<m:apply>
					<m:eq/>
					<m:list separator=",">
						<m:ci>H</m:ci>
						<m:ci>w</m:ci>
					</m:list>
					<m:apply>
				        <m:fn><m:ci>fluxConstraints</m:ci></m:fn>
				        <m:ci>_fluxes</m:ci>
			        </m:apply>
				</m:apply>
                <m:apply>
                    <m:eq/>
                    <m:ci>b</m:ci>
                    <m:list>
                        <m:apply>
				            <m:fn><m:ci>zeros</m:ci></m:fn>
				            <m:cn>
					            <xsl:value-of select="count(sbml:listOfSpecies/sbml:species[@type='intermediate'])"/>
				            </m:cn>
				            <m:cn>1</m:cn>
			            </m:apply>
                        <m:ci>w</m:ci>
                    </m:list>
                </m:apply>
				<m:apply>
					<m:eq/>
					<m:ci>A</m:ci>
					<m:list separator=";">
						<m:ci>N</m:ci>
						<m:ci>H</m:ci>
					</m:list>
				</m:apply>
			</smtb:body>
        </smtb:function>  

		<!-- Construction des matrices d'observation -->
		
		<smtb:comment>Matrices d'observation (cumom�res)</smtb:comment>
		
		<smtb:optimize>
			
			<xsl:call-template name="observation-matrices-open">
				<xsl:with-param name="number-of-observations" select="count(sbml:listOfSpecies/sbml:species[@type='intermediate']/smtb:measurement)"/>
			</xsl:call-template>						

			<xsl:for-each select="sbml:listOfSpecies/sbml:species[@type='intermediate']/smtb:measurement">
				<xsl:variable name="position" select="position()"/>
				<xsl:for-each select="smtb:cumomer-contribution">
					<smtb:matrix-assignment id="C{@weight}" row="{$position}" col="{key('cumomer',@id)/@position}">
						<m:cn>
							<xsl:value-of select="@sign"/>
						</m:cn>
					</smtb:matrix-assignment>
				</xsl:for-each>
			</xsl:for-each>

			<xsl:call-template name="observation-matrices-close"/>
				 
		</smtb:optimize>

		<smtb:comment>Matrice d'observation (flux)</smtb:comment>

		<smtb:optimize>

			<smtb:matrix-open id="E" cols="{count(sbml:listOfReactions/sbml:reaction)}" rows="{count(sbml:listOfReactions/sbml:reaction[(@known='yes') or (@observation='yes')])}" type="sparse"/>

			<xsl:for-each select="sbml:listOfReactions/sbml:reaction[(@known='yes') or (@observation='yes')]">
				<smtb:matrix-assignment id="E" row="{position()}" col="{@position}">1</smtb:matrix-assignment>
			</xsl:for-each>			

			<smtb:matrix-close id="E"/>

		</smtb:optimize>

		<!-- Construction des matrices d'entr�e, permettant d'obtenir la contribution sur les cumom�res des isotopom�res en entr�e. -->
		
		<smtb:comment>Matrices d'entr�e</smtb:comment>

		<smtb:optimize>
			
			<xsl:variable name="number-of-inputs" select="count(sbml:listOfSpecies/sbml:species[smtb:input]/smtb:input)"/>

			<xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight) and smtb:cumomer]">
			
				<smtb:matrix-open id="D{@weight}" cols="{$number-of-inputs}" rows="{count(smtb:cumomer)}" type="sparse"/>
						
				<xsl:variable name="current-weight" select="@weight"/>
				<xsl:for-each select="smtb:cumomer">
					<xsl:variable name="position" select="@position"/>
					<xsl:variable name="carbons" select="smtb:carbon"/>
					<xsl:for-each select="../../../sbml:listOfSpecies/sbml:species[smtb:input]/smtb:input">
						<xsl:variable name="string" select="@string"/>
						<xsl:if test="count(exslt:node-set($carbons)[substring($string,string-length($string)-@index,1)='1'])=count(exslt:node-set($carbons))">
							<smtb:matrix-assignment id="D{$current-weight}" row="{$position}" col="{position()}">
								<m:cn>1</m:cn>
							</smtb:matrix-assignment> 
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>

				<smtb:matrix-close id="D{@weight}"/>

			</xsl:for-each>
				 
		</smtb:optimize>

	<!-- Matrice des noms des fluxs -->
	
	<m:apply>
		<m:eq/>
		<m:ci>flux_names</m:ci>
		<m:list separator=";">
			<xsl:for-each select="sbml:listOfReactions/sbml:reaction">
				<smtb:string>
					<xsl:value-of select='@name'/>
				</smtb:string>
			</xsl:for-each>
		</m:list>
	</m:apply>

	<m:apply>
		<m:eq/>
		<m:ci>flux_ids</m:ci>
		<m:list separator=";">
			<xsl:for-each select="sbml:listOfReactions/sbml:reaction">
				<smtb:string>
					<xsl:value-of select='@id'/>
				</smtb:string>
			</xsl:for-each>
		</m:list>
	</m:apply>


	<!-- Script pour le calcul direct des cumom�res -->

		<smtb:script href="{$file}_direct.sce">
			<smtb:comment>Cumom�res en entr�e</smtb:comment>
			<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
				<m:apply>
					<m:eq/>
					<m:ci>
						<xsl:value-of select="concat('x',@weight,'_input')"/>
					</m:ci>
					<m:apply>
						<m:times/>
						<m:ci>
							<xsl:value-of select="concat('D',@weight)"/>
						</m:ci>
						<m:list separator=";">
							<xsl:for-each select="../../sbml:listOfSpecies/sbml:species[smtb:input]">
								<m:ci>
									<xsl:value-of select="concat(@id,'_input')"/>
								</m:ci>
							</xsl:for-each>
						</m:list>
					</m:apply>
				</m:apply>
			</xsl:for-each>
			<smtb:comment>Calcul direct des cumom�res</smtb:comment>
			<m:apply>
				<m:eq/>
				<m:ci>v</m:ci>
				<m:apply>
					<m:selector/>
					<m:ci type="matrix">fluxes</m:ci>
					<m:cn>:</m:cn>
					<m:cn>1</m:cn>
				</m:apply>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:list separator=",">
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
                        <m:ci>
							<xsl:value-of select="concat('x',position())"/>
    					</m:ci>
                    </xsl:for-each>
				</m:list>
				<m:apply>
					<m:fn><m:ci>solveCumomers</m:ci></m:fn>
					<m:ci>v</m:ci>				
        			<xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight) and (smtb:cumomer)]">
						<m:ci>
							<xsl:value-of select="concat('x',@weight,'_input')"/>
						</m:ci>
					</xsl:for-each>										
				</m:apply>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>y</m:ci>
				<m:apply>
					<m:plus/>
					<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
						<m:apply>
							<m:times/>
							<m:ci>
								<xsl:value-of select="concat('C',@weight)"/>
							</m:ci>
							<m:ci>
								<xsl:value-of select="concat('x',@weight)"/>
							</m:ci>
						</m:apply>
					</xsl:for-each>
				</m:apply>
			</m:apply>
			<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
				<xsl:variable name="n" select="count(smtb:measurement)"/>
				<xsl:variable name="start" select="count(preceding::smtb:measurement)"/>
				<m:apply>
					<m:eq/>
					<m:apply>
						<m:selector/>
						<m:ci type="matrix">
                			<xsl:value-of select="concat(@id,'_obs')"/>
						</m:ci>
						<m:cn>
							<xsl:value-of select="concat('1:',$n)"/>
						</m:cn>
						<m:cn>
							<xsl:value-of select="concat(2+$experiences,':',1+2*$experiences)"/>
						</m:cn>
					</m:apply>
					<m:apply>
						<m:selector/>
						<m:ci type="matrix">y</m:ci>
						<m:cn>
							<xsl:value-of select="concat(1+$start,':',$n+$start)"/>
						</m:cn>
						<m:cn>
							<xsl:value-of select="concat('1:',$experiences)"/>
            			</m:cn>
					</m:apply>
				</m:apply>
			</xsl:for-each>
		</smtb:script>

	<!-- Script pour le lancement de l'optimisation -->

		<smtb:script href="{$file}_optim.sce">
			<smtb:comment>Cumom�res en entr�e</smtb:comment>
			<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
				<m:apply>
					<m:eq/>
					<m:ci>
						<xsl:value-of select="concat('x',@weight,'_input')"/>
					</m:ci>
					<m:apply>
						<m:times/>
						<m:ci>
							<xsl:value-of select="concat('D',@weight)"/>
						</m:ci>
						<m:list separator=";">
							<xsl:for-each select="../../sbml:listOfSpecies/sbml:species[smtb:input]">
								<m:ci>
									<xsl:value-of select="concat(@id,'_input')"/>
								</m:ci>
							</xsl:for-each>
						</m:list>
					</m:apply>
				</m:apply>
			</xsl:for-each>
			<smtb:comment>Flux observ�s</smtb:comment>
			<m:apply>	
				<m:eq/>
				<m:ci>vobs</m:ci>
				<m:apply>
					<m:selector/>
					<m:ci type="matrix">flux_obs</m:ci>
					<m:cn>:</m:cn>
					<m:cn>2</m:cn>
				</m:apply>						
			</m:apply>
			<smtb:comment>Matrice de pond�ration de l'erreur sur les flux observ�s</smtb:comment>
			<m:apply>	
				<m:eq/>
				<m:ci>sigma</m:ci>
				<m:apply>
					<m:fn><m:ci>sparse</m:ci></m:fn>
					<m:apply>
						<m:fn><m:ci>diag</m:ci></m:fn>					
						<m:apply>
							<m:selector/>
							<m:ci type="matrix">flux_obs</m:ci>
							<m:cn>:</m:cn>
							<m:cn>1</m:cn>
						</m:apply>
					</m:apply>
				</m:apply>
			</m:apply>
			<smtb:comment>Marquages observ�s</smtb:comment>
			<m:apply>	
				<m:eq/>
				<m:ci>yobs</m:ci>
				<m:list separator=";">
					<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
						<m:apply>
							<m:selector/>
							<m:ci type="matrix">
								<xsl:value-of select="concat(@id,'_obs')"/>
							</m:ci>
							<m:cn>:</m:cn>
							<m:cn>
								<xsl:value-of select="concat('2:',($experiences)+1)"/>
							</m:cn>	
						</m:apply>	
					</xsl:for-each>					
				</m:list>
			</m:apply>
			<smtb:comment>Matrice de pond�ration de l'erreur sur les marquages observ�s</smtb:comment>
			<m:apply>	
				<m:eq/>
				<m:ci>alpha</m:ci>
				<m:apply>
					<m:fn><m:ci>sparse</m:ci></m:fn>
					<m:apply>
						<m:fn><m:ci>diag</m:ci></m:fn>					
						<m:list separator=";">
							<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
								<m:apply>
									<m:selector/>
									<m:ci type="matrix">
										<xsl:value-of select="concat(@id,'_obs')"/>
									</m:ci>
									<m:cn>:</m:cn>
									<m:cn>1</m:cn>
								</m:apply>	
							</xsl:for-each>					
						</m:list>
					</m:apply>
				</m:apply>				
			</m:apply>
			<smtb:comment>Calcul du sous-espace "stoechiom�trique"</smtb:comment>
			<m:apply>	
				<m:eq/>
				<m:list separator=",">
				    <m:ci>A</m:ci>
				    <m:ci>b</m:ci>
				</m:list>
				<m:apply>
					<m:fn><m:ci>fluxSubspace</m:ci></m:fn>
					<m:ci>fluxes</m:ci>
				</m:apply>			
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:list separator=",">
					<m:ci>v</m:ci>
					<m:ci>label_error</m:ci>
				</m:list>
				<m:apply>
					<m:fn><m:ci>optimize</m:ci></m:fn>
				</m:apply>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:apply>
					<m:selector/>
					<m:ci type="matrix">fluxes</m:ci>
					<m:cn>:</m:cn>
					<m:cn>1</m:cn>
				</m:apply>
				<m:ci>v</m:ci>
			</m:apply>
			<smtb:comment>Calcul des cumom�res apr�s optimisation</smtb:comment>
			<m:apply>
				<m:eq/>
				<m:list separator=",">
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
                        <m:ci>
							<xsl:value-of select="concat('x',position())"/>
    					</m:ci>
                    </xsl:for-each>
				</m:list>
				<m:apply>
					<m:fn><m:ci>solveCumomers</m:ci></m:fn>
					<m:ci>v</m:ci>				
        			<xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight) and (smtb:cumomer)]">
						<m:ci>
							<xsl:value-of select="concat('x',@weight,'_input')"/>
						</m:ci>
					</xsl:for-each>										
				</m:apply>
			</m:apply>
			<m:apply>
				<m:eq/>
				<m:ci>y</m:ci>
				<m:apply>
					<m:plus/>
					<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
						<m:apply>
							<m:times/>
							<m:ci>
								<xsl:value-of select="concat('C',@weight)"/>
							</m:ci>
							<m:ci>
								<xsl:value-of select="concat('x',@weight)"/>
							</m:ci>
						</m:apply>
					</xsl:for-each>
				</m:apply>
			</m:apply>
			<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
				<xsl:variable name="n" select="count(smtb:measurement)"/>
				<xsl:variable name="start" select="count(preceding::smtb:measurement)"/>
				<m:apply>
					<m:eq/>
					<m:apply>
						<m:selector/>
						<m:ci type="matrix">
                			<xsl:value-of select="concat(@id,'_obs')"/>
						</m:ci>
						<m:cn>
							<xsl:value-of select="concat('1:',$n)"/>
						</m:cn>
						<m:cn>
							<xsl:value-of select="concat(2+$experiences,':',1+2*$experiences)"/>
						</m:cn>
					</m:apply>
					<m:apply>
						<m:selector/>
						<m:ci type="matrix">y</m:ci>
						<m:cn>
							<xsl:value-of select="concat(1+$start,':',$n+$start)"/>
						</m:cn>
						<m:cn>
							<xsl:value-of select="concat('1:',$experiences)"/>
            			</m:cn>
					</m:apply>
				</m:apply>
			</xsl:for-each>
			
		</smtb:script>

    </xsl:template>

	<xsl:template name="iterate-cost">
		<xsl:param name="i" select="'1'"/>
		<m:apply>
			<m:times/>
			<m:apply>
				<m:transpose/>
				<m:apply>
					<m:selector/>
					<m:ci type="matrix">e</m:ci>
					<m:cn>:</m:cn>
					<m:cn><xsl:value-of select="$i"/></m:cn>
				</m:apply>
			</m:apply>
			<m:ci>alpha</m:ci>
			<m:apply>
				<m:selector/>
				<m:ci type="matrix">e</m:ci>
				<m:cn>:</m:cn>
				<m:cn><xsl:value-of select="$i"/></m:cn>
			</m:apply>
		</m:apply>
		<xsl:if test="$i&lt;$experiences">
			<xsl:call-template name="iterate-cost">
				<xsl:with-param name="i" select="($i)+1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="iterate-grad">
		<xsl:param name="i" select="'1'"/>
		<m:apply>
			<m:times/>
			<m:apply>
				<m:transpose/>
				<m:apply>
					<m:selector/>
					<m:ci type="matrix">e</m:ci>
					<m:cn>:</m:cn>
					<m:cn><xsl:value-of select="$i"/></m:cn>
				</m:apply>
			</m:apply>
			<m:ci>alpha</m:ci>
			<m:apply>
				<m:plus/>
				<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
					<m:apply>
						<m:times/>
 		   				<m:ci>
	                    	<xsl:value-of select="concat('C',@weight)"/>
	                	</m:ci>
						<m:apply>
							<m:selector/>
 		   					<m:ci type="hypermatrix">
	                    		<xsl:value-of select="concat('dx',@weight,'_dv')"/>
	                		</m:ci>
							<m:cn>:</m:cn>
							<m:cn>:</m:cn>
							<m:cn><xsl:value-of select="$i"/></m:cn>
						</m:apply>
					</m:apply>
            	</xsl:for-each>
			</m:apply>			
		</m:apply>
		
		<xsl:if test="$i&lt;$experiences">
			<xsl:call-template name="iterate-grad">
				<xsl:with-param name="i" select="($i)+1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>


	
	<xsl:template name="observation-matrices-open">
		<xsl:param name="i" select="'1'"/>
		<xsl:param name="number-of-observations"/>
		<smtb:matrix-open id="C{$i}" rows="{$number-of-observations}" cols="{count(smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight=$i]/smtb:cumomer)}" type="sparse"/>
		<xsl:if test="$i&lt;$weight">
			<xsl:call-template name="observation-matrices-open">
				<xsl:with-param name="i" select="($i)+1"/>
				<xsl:with-param name="number-of-observations" select="$number-of-observations"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="observation-matrices-close">
		<xsl:param name="i" select="'1'"/>
		<smtb:matrix-close id="C{$i}"/>
		<xsl:if test="$i&lt;$weight">
			<xsl:call-template name="observation-matrices-close">
				<xsl:with-param name="i" select="($i)+1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

    <xsl:template match="sbml:listOfReactions" mode="stoichiometric-matrix">
        <m:apply>
			<m:eq/>
			<m:ci>H</m:ci>
			<m:apply>
				<m:fn><m:ci>zeros</m:ci></m:fn>
				<m:cn>
					<xsl:value-of select="count(sbml:reaction[@known='yes'])"/>
				</m:cn>
				<m:cn>
					<xsl:value-of select="count(sbml:reaction)"/>
				</m:cn>
			</m:apply>
		</m:apply> 
        <xsl:apply-templates select="sbml:reaction[@known='yes']" mode="stoichiometric-matrix"/>    
    </xsl:template>

    <xsl:template match="sbml:reaction" mode="stoichiometric-matrix">
<!--        <smtb:comment>
            <xsl:value-of select="@name"/>
        </smtb:comment>-->
        <m:apply>
            <m:eq/>
            <m:apply>
                <m:selector/>
			    <m:ci type="matrix">H</m:ci>
			    <m:cn>
                    <xsl:value-of select="position()"/>
                </m:cn>
			    <m:cn>
                    <xsl:value-of select="@position"/>
                </m:cn>
            </m:apply>
            <m:cn>1</m:cn>
        </m:apply>
    </xsl:template>


    <xsl:template match="sbml:listOfSpecies" mode="stoichiometric-matrix">
        <m:apply>
			<m:eq/>
			<m:ci>N</m:ci>
			<m:apply>
				<m:fn><m:ci>zeros</m:ci></m:fn>
				<m:cn>
					<xsl:value-of select="count(sbml:species[@type='intermediate'])"/>
				</m:cn>
				<m:cn>
					<xsl:value-of select="count(../sbml:listOfReactions/sbml:reaction)"/>
				</m:cn>
			</m:apply>
		</m:apply> 
        <xsl:apply-templates select="sbml:species[@type='intermediate']" mode="stoichiometric-matrix"/>
    </xsl:template>

    <xsl:template match="sbml:species" mode="stoichiometric-matrix">
        <xsl:variable name="position" select="position()"/>
		<smtb:comment><xsl:value-of select="@id"/></smtb:comment>
        <!-- influx rule -->
        <xsl:for-each select="key('products',@id) | key('reactants',@id)">
            <m:apply>
                <m:eq/>
                <m:apply>
                    <m:selector/>
			        <m:ci type="matrix">N</m:ci>
			        <m:cn>
                        <xsl:value-of select="$position"/>
                    </m:cn>
			        <m:cn>
                        <xsl:value-of select="../../@position"/>
                    </m:cn>
                </m:apply>
                <m:apply>
                    <xsl:choose>
                      <xsl:when test="parent::sbml:listOfProducts">
                            <m:plus/>
                        </xsl:when>
                        <xsl:when test="parent::sbml:listOfReactants">
                            <m:minus/>
                        </xsl:when>                   
                    </xsl:choose>
                     <m:apply>
                        <m:selector/>
			            <m:ci type="matrix">N</m:ci>
			            <m:cn>
                            <xsl:value-of select="$position"/>
                        </m:cn>
			            <m:cn>
                            <xsl:value-of select="../../@position"/>
                        </m:cn>
                    </m:apply>
                    <m:cn>1</m:cn>
                </m:apply>
            </m:apply>            
        </xsl:for-each>
    </xsl:template>
    

    <xsl:template match="smtb:listOfIntermediateCumomers">
        
        <xsl:for-each select="smtb:listOfCumomers[@weight&lt;=$weight]">    

			<!-- D�claration et initialisation matrice et vecteur -->
			
			<xsl:variable name="current_weight" select="@weight"/>
			

			<m:apply>
				<m:eq/>
				<m:ci>
                	<xsl:value-of select="concat('n',@weight)"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="count(smtb:cumomer)"/>
				</m:cn>
			</m:apply>

			<smtb:optimize>

				<smtb:matrix-open type="sparse" id="{concat('M',@weight)}" rows="{concat('n',@weight)}" cols="{concat('n',@weight)}"/>

				<smtb:matrix-open type="s_full" id="{concat('b',@weight)}" rows="{concat('n',@weight)}" cols="1" 
				             versions="{$experiences}"/>

				<smtb:matrix-open type="s_full" id="{concat('dg',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../sbml:listOfReactions/sbml:reaction)}" 
				             versions="{$experiences}" />
				
				<xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
					<smtb:matrix-open type="sparse" id="{concat('db',$current_weight,'_dx',@weight)}" rows="{concat('n',$current_weight)}" cols="{concat('n',@weight)}" 
					             versions="{$experiences}"/>
				</xsl:for-each>

            	<smtb:comment>Cumom�res de poids <xsl:value-of select="@weight"/></smtb:comment>

            	<xsl:for-each select="smtb:cumomer">
                    	<xsl:call-template name="iteration"/>                                    
            	</xsl:for-each>

				<smtb:matrix-close id="{concat('M',@weight)}"/>
				<smtb:matrix-close id="{concat('b',@weight)}"/>

				<!-- r�solution des syst�mes -->

				<!-- R�solution du syst�me pour x_weight -->

				<smtb:solve matrix="{concat('M',@weight)}">
					<smtb:lhs>
						<xsl:value-of select="concat('x',@weight)"/>
					</smtb:lhs>
					<smtb:rhs>
						<m:apply>	
							<m:minus/>
							<m:list separator=",">
								<xsl:call-template name="concatenate-rhs">	
									<xsl:with-param name="id" select="concat('b',@weight)"/>
								</xsl:call-template>
							</m:list>
						</m:apply>
					</smtb:rhs>
				</smtb:solve> 
	
				<smtb:matrix-close id="{concat('dg',@weight,'_dv')}"/>
			
				<xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
					<smtb:matrix-close id="{concat('db',$current_weight,'_dx',@weight)}" />
				</xsl:for-each>

				<!-- R�solution du syst�me pour dx_weight/dv -->
				
				<smtb:hypermatrix id="{concat('dx',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../sbml:listOfReactions/sbml:reaction)}" 
				             versions="{$experiences}"/>
				
				<xsl:call-template name="solve-dx-dv"/>
			
			</smtb:optimize>
			
        </xsl:for-each>
		
    </xsl:template>

	<xsl:template name="concatenate-rhs">
		<xsl:param name="i">1</xsl:param>
		<xsl:param name="id"/>

		<m:ci type="vector">
			<xsl:value-of select="concat($id,'_',$i)"/>
		</m:ci>		

		<xsl:if test="$i&lt;$experiences">
			<xsl:call-template name="concatenate-rhs">
				<xsl:with-param name="i" select="($i)+1"/>
				<xsl:with-param name="id" select="$id"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template> 

	<xsl:template name="solve-dx-dv">
		<xsl:param name="i">1</xsl:param>
		<xsl:variable name="current_weight" select="@weight"/>

		<smtb:solve matrix="{concat('M',@weight)}">
			<smtb:lhs>
				<m:apply>
					<m:selector/>
					<m:ci type="hypermatrix">
						<xsl:value-of select="concat('dx',@weight,'_dv')"/>
					</m:ci>
					<m:cn>:</m:cn>
					<m:cn>:</m:cn>
					<m:cn><xsl:value-of select="$i"/></m:cn>
				</m:apply>				
			</smtb:lhs>
			<smtb:rhs>
				<m:apply>
					<m:minus/>
					<m:apply>
						<m:plus/>
						<m:ci>
							<xsl:value-of select="concat('dg',@weight,'_dv_',$i)"/>
						</m:ci>
						<xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
							<m:apply>
								<m:times/>
								<m:ci>
									<xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_',$i)"/>
								</m:ci>
								<m:apply>
									<m:selector/>
									<m:ci type="hypermatrix">
										<xsl:value-of select="concat('dx',@weight,'_dv')"/>
									</m:ci>
									<m:cn>:</m:cn>
									<m:cn>:</m:cn>
									<m:cn><xsl:value-of select="$i"/></m:cn>
								</m:apply>
							</m:apply>
						</xsl:for-each>
					</m:apply>
				</m:apply>
			</smtb:rhs>
		</smtb:solve>
						
		<xsl:if test="$i&lt;$experiences">
			<xsl:call-template name="solve-dx-dv">
				<xsl:with-param name="i" select="($i)+1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template> 

   

<!-- Template de fabrication de l'�quation de bilan stoechiom�trique
     de l'esp�ce @id -->

<xsl:template name="bilan">

    <xsl:variable name="name" select="@id"/>

    <m:apply>
        <m:eq/>
        <m:cn>0</m:cn>
        <m:apply>
            <m:minus/>   
            
            <!-- influx rule -->

            <m:apply>
                <m:plus/>
                <xsl:for-each select="key('products',@id)">
                    <m:ci>
                        <xsl:value-of select="../../@id"/>
                    </m:ci>
                </xsl:for-each>
            </m:apply>
            
            <xsl:call-template name="bilan-outflux">
                <xsl:with-param name="id" select="@id"/>
            </xsl:call-template>
                 
        </m:apply>
    
    </m:apply>

</xsl:template>

<xsl:template name="iteration">
    
    <!-- Attention le noeud contextuel est un �lement <smtb:cumomer> -->

    <xsl:variable name="name" select="../@id"/>
    
	<!-- La variable "carbons" contient les carbones 13 de l'esp�ce courante -->
	
    <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>
	
    <xsl:variable name="weight" select="@weight"/>
    <xsl:variable name="position" select="@position"/>

	<!-- influx rule : le plus chiant, mais aussi le plus int�ressant. C'est l� que se cr�e v�ritablement
     	l'information suppl�mentaire par rapport � la stoechiom�trie simple. 

     On boucle sur tous les �l�ments <speciesReference> qui ont l'esp�ce courante comme produit,
     donc dans le for-each le noeud contextuel est de type reaction/listOfProducts/speciesReference. -->

    	<xsl:for-each select="key('products',@species)"> 

        	<!-- Maintenant, on essaye de trouver des occurences des carbones marqu�s
            	 du reactant de la r�action courante : c'est du boulot... -->

        	<xsl:variable name="species" select="@species"/>
        	<xsl:variable name="reaction" select="key('reaction',../../@id)/@position"/>
        	<xsl:variable name="occurence" select="count(preceding-sibling::sbml:speciesReference)+1"/>

        	<xsl:variable name="influx">

            	<!-- C'est ici que les choses s�rieuses commencent. On boucle sur tous les r�actants qui ont des atomes de carbones
                	 qui "pointent" sur l'esp�ce -->

            	<xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[smtb:carbon[(@species=$species) and (@occurence=$occurence)]]">
                	<xsl:variable name="somme">
						<!-- On boucle donc maintenant sur les carbones du reactant. Si un carbone du reactant pointe vers
					    	 vers un carbone 13 du cumom�re du produit, on note son num�ro dans un �l�ment <token/> -->
                    	<xsl:for-each select="smtb:carbon[(@species=$species) and (@occurence=$occurence)]">
                        	<xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                            	<token>
                                	<xsl:value-of select="@position"/>
                            	</token>
                        	</xsl:if>
                    	</xsl:for-each>
                	</xsl:variable>
                	<xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
                    	<token weight="{count(exslt:node-set($somme)/token)}" type="{key('species',@species)/@type}">
                        	<xsl:value-of select="concat(@species,'_',sum(exslt:node-set($somme)/token))"/>
                    	</token>
                	</xsl:if>
            	</xsl:for-each>

			</xsl:variable>

			<!-- Ici on g�n�re le code qui forme la matrice et le second membre du syst�me qui permet d'obtenir les
		    	 cumom�res de poids $weight -->

			<xsl:choose>
				<xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
					<!-- On n'a qu'un seul terme du poids courant, on assemble donc la matrice ainsi que sa d�riv�e -->
					<smtb:matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{key('cumomer',$influx)/@position}">
                        <m:apply>
                            <m:selector/>
                            <m:ci type="vector">v</m:ci>
						    <m:cn>
                            <!-- id de la r�action -->
	                            <xsl:value-of select="$reaction"/>
                            </m:cn>
                        </m:apply>
					</smtb:matrix-assignment>
					<smtb:matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}">
						<m:apply>
							<m:selector/>
							<m:ci type="matrix">
								<xsl:value-of select="concat('x',$weight)"/>
							</m:ci>
							<m:cn>
								<xsl:value-of select="key('cumomer',$influx)/@position"/>
							</m:cn>
							<m:cn>:</m:cn>
						</m:apply>
					</smtb:matrix-assignment>
				</xsl:when>
				<xsl:otherwise>

					<!-- On a un produit de plusieurs termes, donc de poids inf�rieur, on assemble donc le second membre
				    	 ainsi que la jacobienne du second membre par rapport aux cumom�res de poids inf�rieur  -->

					<xsl:variable name="influx-factors">
						<xsl:call-template name="iterate-influx">
							<xsl:with-param name="tokens">
								<xsl:copy-of select="$influx"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:variable>

					<smtb:matrix-assignment id="{concat('b',$weight)}" row="{$position}" col="1">
						<m:apply>
							<m:times type="array"/>
						    <m:apply>
                                <m:selector/>
                                <m:ci type="vector">v</m:ci>
						        <m:cn>
                                <!-- id de la r�action -->
	                                <xsl:value-of select="$reaction"/>
                                </m:cn>
                            </m:apply>
							<xsl:copy-of select="$influx-factors"/>
						</m:apply>						
					</smtb:matrix-assignment>
					
					<smtb:matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}">
						<m:apply>
							<m:times type="array"/>
							<xsl:copy-of select="$influx-factors"/>
						</m:apply>
					</smtb:matrix-assignment>					

					<xsl:call-template name="iterate-influx-jacobian-x">
						<xsl:with-param name="reaction" select="$reaction"/>
						<xsl:with-param name="weight" select="$weight"/>
						<xsl:with-param name="tokens">
							<xsl:copy-of select="$influx"/>
						</xsl:with-param>
						<xsl:with-param name="position" select="$position"/>
						<xsl:with-param name="i">1</xsl:with-param>
						<xsl:with-param name="n" select="count(exslt:node-set($influx)/token)"/>
					</xsl:call-template>

				</xsl:otherwise>
			</xsl:choose>

    	</xsl:for-each>

    	<!-- outflux rule : partie la plus simple � g�n�rer (voir papier Wiechert) -->

		<smtb:matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{$position}">
			<m:apply>
				<m:minus/>
	        	<xsl:call-template name="bilan-outflux">
    	        	<xsl:with-param name="id" select="@species"/>
        		</xsl:call-template>
			</m:apply>
    	</smtb:matrix-assignment>

        <xsl:for-each select="key('reactants',@species)">
			<smtb:matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{key('reaction',../../@id)/@position}">
				<m:apply>
					<m:minus/>
					<m:apply>
						<m:selector/>
						<m:ci type="matrix">
						<xsl:value-of select="concat('x',$weight)"/>
						</m:ci>
						<m:cn>
						<xsl:value-of select="$position"/>
						</m:cn>
						<m:cn>:</m:cn>
					</m:apply>
				</m:apply>
			</smtb:matrix-assignment>
        </xsl:for-each>

    
</xsl:template>


<xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

    <!-- outflux rule -->

    <m:apply>
        <m:plus/>
        <xsl:for-each select="key('reactants',$id)">
            <m:apply>
                <m:selector/>
                <m:ci type="vector">v</m:ci>
			    <m:cn>
                <!-- id de la r�action -->
	                <xsl:value-of select="key('reaction',../../@id)/@position"/>
                </m:cn>
            </m:apply>
        </xsl:for-each>
    </m:apply>

</xsl:template>


<xsl:template name="iterate-influx">
	<xsl:param name="tokens"/>
	<xsl:variable name="id" select="exslt:node-set($tokens)/token[1]"/>
	
	<xsl:choose>
		<xsl:when test="key('cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="matrix">
					<xsl:value-of select="concat('x',key('cumomer',$id)/@weight)"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="key('cumomer',$id)/@position"/>
				</m:cn>
				<m:cn>:</m:cn>
			</m:apply>
		</xsl:when>
		<xsl:when test="key('input-cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="matrix">
					<xsl:value-of select="concat('x',key('input-cumomer',$id)/@weight,'_input')"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="key('input-cumomer',$id)/@position"/>
				</m:cn>
				<m:cn>:</m:cn>
			</m:apply>
		</xsl:when>
	</xsl:choose>
	
	<xsl:if test="count(exslt:node-set($tokens)/token)&gt;1">
		<xsl:call-template name="iterate-influx">
			<xsl:with-param name="tokens">
				<xsl:copy-of select="exslt:node-set($tokens)/token[position()&gt;1]"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template name="iterate-influx-jacobian-x">

	<!-- Generation des jacobiennes (d�riv�es) dbi/dxj -->

	<xsl:param name="reaction"/>
	<xsl:param name="weight"/>
	<xsl:param name="tokens"/>
	<xsl:param name="position"/>
	<xsl:param name="i"/>
	<xsl:param name="n"/>

	<xsl:variable name="id" select="exslt:node-set($tokens)/token[position()=$i]"/>
	
	<xsl:if test="key('cumomer',$id)">
		<smtb:matrix-assignment id="{concat('db',$weight,'_dx',key('cumomer',$id)/@weight)}" row="{$position}" col="{key('cumomer',$id)/@position}">
			<m:apply>
				<m:times type="array"/>
				<xsl:call-template name="iterate-influx">
					<!-- La d�riv�e est �gale � tout ce qui est facteur de la variable -->
					<xsl:with-param name="tokens">
						<xsl:copy-of select="exslt:node-set($tokens)/token[position()!=$i]"/>
					</xsl:with-param>
				</xsl:call-template>
                <m:apply>
                    <m:selector/>
                    <m:ci type="vector">v</m:ci>
					<m:cn>
	                    <xsl:value-of select="$reaction"/>
                    </m:cn>
                </m:apply>				
			</m:apply>
		</smtb:matrix-assignment>
	</xsl:if>

	<xsl:if test="$i&lt;$n">
		<xsl:call-template name="iterate-influx-jacobian-x">
			<xsl:with-param name="reaction" select="$reaction"/>
			<xsl:with-param name="weight" select="$weight"/>
			<xsl:with-param name="tokens">
				<xsl:copy-of select="$tokens"/>
			</xsl:with-param>
			<xsl:with-param name="position" select="$position"/>
			<xsl:with-param name="i" select="($i)+1"/>
			<xsl:with-param name="n" select="$n"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>


</xsl:stylesheet>
