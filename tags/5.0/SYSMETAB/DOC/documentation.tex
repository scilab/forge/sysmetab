\documentclass[10pt]{article}
   \voffset=-1in
   \hoffset=-1in
   \topmargin=2cm
   \headheight=0cm
   \headsep=0cm
   \setlength{\paperheight}{29.7cm}%
   \setlength{\paperwidth}{21cm}%
   \setlength{\oddsidemargin}{2.5cm}%
   \setlength{\evensidemargin}{2.5cm}%
   \setlength{\marginparsep}{0cm}%
   \setlength{\marginparwidth}{0cm}%
   \setlength{\footskip}{1cm}% 
   \setlength{\textheight}{24cm}%
   \setlength{\textwidth}{16cm}%
   \usepackage{mathptmx,graphicx}
\newcounter{answer}
\newenvironment{answer}{\refstepcounter{answer}\par\noindent\rm\textbf{Answer \theanswer:}\bgroup\rm}{\egroup}

\title{13C metabolic flux analysis with Scilab }
\date{\texttt{version 5.0}}
\begin{document}
\maketitle


\section{Change log}
\subsection*{Version 5.0 (Thu Jan 14 07:04:09 CET 2016)}
5.0 is the second release of Sysmetab. Among other minor changes and speed improvements, Sysmetab is now able to solve MFA-CLE problems with non stationary data. Here is the whole story:
\begin{itemize}
\item Some parts of the generated code which were specifically generated have been factorized under the form of generic Scilab functions (new \texttt{SCI/macros} folder), which partly explains the gain. Moreover, the scaling factors are not optimization variables any more as they can be optimally determined on-the-fly (simple linear least-squares problem) for each forward simulation (this technique is also used in 13CFlux2, as explained in the PhD thesis manuscript of Michael Weitzel). For example, on our hardware (two ten-core Xeon E5-2660 v2 (2.20 GHz), but only one core is used for this timing), the optimization on the \texttt{e\_coli} network took only 0.35 seconds instead of 0.66 seconds with the previous version.
\item An new non-stationary example \texttt{e\_coli\_ns} has been added in the \texttt{EXAMPLES} folder (see the non-stationary test case).
\item The \texttt{sysmetab} command has new flags:
\begin{itemize}
\item[]\texttt{--tolx} : set upper bound for the norm of optimizer increment
\item[]\texttt{--toldir} : set upper bound for the norm of optimizer search direction 
\item[]\texttt{--maxnet} : maximum value for net fluxes 
\item[]\texttt{--mininout} : minimum value for input/output net flux 
\item[]\texttt{--minnr} : minimum value for non-reversible reactions net flux 
\item[]\texttt{--stepsize} : stepsize value for discrete time integration 
\item[]\texttt{--nc} : number of allocated processor cores 
\item[]\texttt{--seed} : initialize random generator with seed N 
\end{itemize}
\end{itemize}
\subsection*{Version 4.0 (Wed Jul 22 10:21:55 CEST 2015)}
4.0 is the first public release of Sysmetab

\section{Installation}

\subsection{Prerequisites}

\subsubsection{Operating system}

Sysmetab runs on Linux and Mac OSX, but should also run on computers
with other flavors of Unix. Two different packages are available since
some parts of Sysmetab are specifically compiled for each platform.

\subsubsection{Companion software}

\begin{itemize}
\item {\bf xsltproc} : during the code generation phase, Sysmetab uses the xsltproc XSLT
processor from the Libxml2 library (Libxml2 is the XML C parser and
toolkit developed for the Gnome project, see the project page)

\begin{itemize}
\item
  Mac OS X: the xsltproc command is already present
\item
  Linux: you may have to manually install xsltproc, for example under
  Ubuntu: 
  
\begin{verbatim}
sudo apt-get install xsltproc
\end{verbatim}
\end{itemize}

\item {\bf Java} : another step of the generation phase uses the Saxon XSLT processor
because of its terminal recursion handling (xsltproc cannot be used for
this step). Since Saxon is written in JAVA, a JRE (Java Runtime
Environment) should be present. Under Mac OS X nothing has to be
installed, but under Linux you may have to install the default JRE, For
example under Ubuntu: 

\begin{verbatim}
sudo apt-get install default-jre
\end{verbatim}

\item {\bf Scilab} : as Sysmetab generates Scilab code, you need to have Scilab to execute
the generated programs. Under MacOSX you just need to download the
latest version at the Scilab www site.

Under Linux the Scilab package now exists for all distributions. For
Unbuntu you just have to type: 

\begin{verbatim}
sudo apt-get install scilab
\end{verbatim}
\end{itemize}
\subsection{Installation of Sysmetab}

You just have to uncompress the package archive to a location where you
have write access.

\subsubsection{User environment}

In order that the command interpreter could find \texttt{sysmetab}
and/or \texttt{scilab} you have to modify your user environment profile:

\begin{itemize}
\item \textbf{Mac OS X} : open the Terminal application (in \texttt{Applications/Utilities}) and
add the following lines in your \texttt{.bash\_profile}:

\begin{verbatim}
export PATH=$PATH:$HOME/SYSMETAB-5.0/BIN:/Applications/scilab-5.5.2.app/Contents/MacOS/bin}
\end{verbatim}

here we have supposed that the SYSMETAB archive has been uncompressed in
your home directory and that you have Scilab version 5.5.2 in your
\texttt{Applications} folder

\item \textbf{Linux} : unlike Mac OS X, you just have to give the path to Sysmetab commands as
scilab is by default accessible from the command line. Hence, you just
have to add the following line to your \texttt{.bash\_profile}:

\begin{verbatim}
export PATH=$PATH:$HOME/SYSMETAB-5.0/BIN
\end{verbatim}
\end{itemize}
\section{Test cases}

\subsection{Stationary data}

As a test case we will use the file \texttt{e\_coli.ftbl} file coming
from the distribution of influx\_s. This file is in the folder \texttt{SYSMETAB-5.0/EXAMPLES/e\_coli}

\subsubsection{Conversion to the FluxML format}

In order to be processed by the \texttt{sysmetab} script, the file has
to be converted to the 13CFlux2 FluxML format. This is done as follows

\begin{verbatim}
ftbltofml e_coli.ftbl > e_coli.fml
\end{verbatim}

The \texttt{e\_coli.fml} file can be edited in any text editor but an
XML aware (at least for syntax highlighting) has to be preferred. If you
are just curious about the structure of the file you can open it in
Google Chrome and fold/unfold elements :

\begin{center}
\includegraphics[scale=0.7]{ecolifmlpng}
\end{center}

For example, inside the \texttt{<reactionnetwork>} element, here is the
\texttt{<reaction>} element associated to the Citrate synthase

\begin{center}
\includegraphics[scale=0.7]{ecolifmlreactionpng}
\end{center}

\subsubsection{Identification of fluxes}

The \texttt{e\_coli.fml} file is simply passed as first argument to the
\texttt{sysmetab} script as follows (the extension of the filename has
to be removed)

\begin{verbatim}
sysmetab e_coli
\end{verbatim}

Sysmetab then gives (by default) a lot of information about the successive steps of file processing (network analysis, code generation, \ldots{}). The last output lines should be the following:

\begin{verbatim}
Optimization terminated with the following message : Normal termination.

The following free fluxes are poorly determined (stddev>abs(value)): 

eno_xch, fum_a_xch.

You should compute Monte-Carlo statistics (--stats=mc:n) in order to confirm or infirm this.

Solvable free fluxes seem to be : 

Glucupt_1_net, ald_xch, pyk_net, zwf_net, gnd_net, ta_xch, tk1_xch, ppc_xch, out_Ac_net.
\end{verbatim}

Sysmetab here complains about the high values of standard deviation of some fluxes which are likely non-identifiable.

\subsubsection{Visualisation of results}

All the results, namely optimal flux values and associated standard
deviations, individual and group measurement residuals, values of
intermediate cumomers, \ldots{} are contained in the \texttt{e\_coli.fwd}
file. Like the \texttt{e\_coli.fml} file, the \texttt{fwd} file can be
opened in your navigator (here in Google Chrome, with folded elements)

\begin{center}
\includegraphics[scale=0.7]{ecolifwdpng}
\end{center}

For example, opening the \texttt{<measurement\textgreater>}
element (by clicking on the +) allows you to see the contribution of
Succinate measurements to overall residual in value of the \texttt{norm}
attribute, as well as the scaling factor.

\begin{center}
\includegraphics[scale=0.7]{ecolifwdmgrouppng}
\end{center}

\subsubsection{Conversion to spreadsheeet format}

The fwd file can be converted to the CSV spreadsheet format like this

\begin{verbatim}
fwdtocsv e_coli.fwd > e_coli.csv
\end{verbatim}

\subsection{Non-stationary data}

As a test case we will use the file \texttt{e\_coli\_ns.fml}, containing synthetic noisy data which has been generated by a forward non-stationary simulation using the flux estimated values from \texttt{e\_coli}. As you can see it below
\begin{center}
\includegraphics[scale=0.7]{ecolinsfmlgroup}
\end{center}
the measurements are made at times 1,\dots,10 seconds. The \texttt{<data>} section is 10 times bigger than the same section in \texttt{e\_coli.fml}. 

\subsubsection{Identification of fluxes}

The \texttt{e\_coli\_ns.fml} file is simply passed as first argument to the
\texttt{sysmetab} script as follows

\begin{verbatim}
sysmetab e_coli_ns
\end{verbatim}

Sysmetab then gives (by default) a lot of information about the successive steps of file processing (network analysis, code generation, \ldots{}). The last output lines should be the following:

\begin{verbatim}
Optimization terminated with the following message : Normal termination.

The following free pool sizes are poorly determined (stddev>abs(value)): 

AKG, Mal, OAA, Pyr.

The following free fluxes are poorly determined (stddev>abs(value)): 

eno_xch, tk2_xch, fum_a_xch.

You should compute Monte-Carlo statistics (--stats=mc:n) in order to confirm or infirm this.

Solvable free fluxes seem to be : 

Glucupt_1_net, ald_xch, pyk_net, zwf_net, gnd_net, ta_xch, tk1_xch, ppc_xch, out_Ac_net.

Solvable pool sizes seem to be : 

Ery4P, Fru6P, FruBP, GA3P, Glc6P, Gnt6P, ICit, PEP, PGA, Rib5P, Sed7P, Suc.
\end{verbatim}

Compared to the stationary case, some information on pool sizes is given

\subsubsection{Visualisation of results}

All the results, namely optimal flux values and associated standard
deviations, individual and group measurement residuals, values of
intermediate cumomers, \ldots{} are contained in the \texttt{ecoli\_ns.fwd}
file. Like the \texttt{e\_coli\_ns.fml} file, the \texttt{fwd} file can be
opened in your navigator (here in Google Chrome, with folded elements)

Opening the \texttt{<measurement\textgreater>}
element (by clicking on the +) allows you to see the contribution of
Succinate measurements to overall residual in value of the \texttt{norm}
attribute, as well as the scaling factor.

\begin{center}
\includegraphics[scale=0.7]{ecolinsfwdmgrouppng}
\end{center}

The time course of simulated measurement at intermediate times is available but only at the Scilab level. In order to be able to access it, you have to prevent \texttt{sysmetab} to launch Scilab . To this purpose, type the following command:

\begin{verbatim}
sysmetab --nocalc e_coli_ns
\end{verbatim}

then launch Scilab, open the file \texttt{e\_coli\_ns\_direct.sce}, and execute it. When the optimization terminates, open the file \texttt{e\_coli\_ns\_plot.sce} (this file will be automatically generated in the next release). The following figure should open:

\begin{center}
\includegraphics[scale=0.5]{ecolinsmeaspng}
\end{center}

These graphs show the reconstruction of mass isotopomer fractions of measured metabolites using the free fluxes and pool size values obtained after optimization. Comparing these simulated data with the different time measurements marked with circles demonstrates that synthetic data are well recovered. The legend is the following 

\begin{center}
\includegraphics[scale=0.7]{masspng}
\end{center}

\section{User guide (under construction)}

\begin{tabular}{ll}
    \texttt{--}output=filename                  &output filename (without extension)
\\    \texttt{--}full                             &no cumomer graph simplification
\\    \texttt{--}nocalc                           &generate Scilab code but do not execute it.
\\    \texttt{--}nooptimize                       &no optimization (forward simulation only)
\\    \texttt{--}stepsize                         &stepsize value for discrete time integration (non-stationary configurations)
\\    \texttt{--}gradient=adjoint$\vert$direct          &method for computing the gradient
\\    \texttt{--}freefluxes=user$\vert$auto[:n]$\vert$multi:n &freefluxes initialization
\\    \texttt{--}stats=lin$\vert$mc:n                   &linear or Monte Carlo statistics
\\    \texttt{--}nc=N                             &number of allocated processor cores (by default, all available cores are used)
\\    \texttt{--}seed=N                           &intialize random generator with seed N (default is number of seconds since the Epoch)
\\    \texttt{--}scaling=user$\vert$yes$\vert$no              &scaling of measurement groups
\\    \texttt{--}reg=REG                          &set regularization parameter of cost function (default=1e-6)
\\    \texttt{--}regphi=TOL                       &set smoothing parameter for the (net,xch)/(f,b) transformation (default=0)
\\    \texttt{--}toldir=TOL                       &set upper bound for the norm of optimizer search direction (default=1e-6)
\\    \texttt{--}tolx=TOL                         &set upper bound for the norm of optimizer increment (default=1e-5)
\\    \texttt{--}maxiter=N                        &set maximum number of iterations of optimizer (default=1000)
\\    \texttt{--}maxxch=MAXXCH                    &maximum value for xch flux (default=1e3)
\\    \texttt{--}maxnet=MAXNET                    &maximum value for net flux (default=1e3)
\\    \texttt{--}mininout=MININOUT                &minimum value for input/output net flux (default=0)
\\    \texttt{--}minnr=MINNR                      &minimum value for non-reversible reactions net flux (default=0, means no lower bound)
\\    \texttt{--}rebuild                          &rebuild Scilab code from scratch
\\    \texttt{--}verbose 1$\vert$0                      &verbosity level
\\    \texttt{-}h, \texttt{--}help                         &show this help message and exit
\end{tabular}

\end{document}


\end{document}
