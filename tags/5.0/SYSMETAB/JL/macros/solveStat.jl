function solveStat(v,Xinp,M,b,cum)
    #
    # Solve the state equation cascade
    #
    h=cell(length(cum))
    X=Xinp
    for i=1:length(cum)
        (X[cum[i]],h[i])=solveStatWeight(v,X,M[i],b[i])
    end
    #
    # Compute the simulated measurements
    #
    y_unscaled=Cx*X
    #
    # Compute optimal scaling factors (simple least squares problem)
    #
    omega=ones(nb_group)
    for i=auto_scales_ind # for groups with scale="auto"
      ind=meas_ind_group[i];
      yui=y_unscaled[ind]./Sy[ind];
      ymi=ymeas[ind]./Sy[ind];
      omega[i]=sum(yui.*ymi)/norm(yui)^2;
    end
    y=omega[omega_ind_meas][:].*y_unscaled;
    #
    # Compute the residual
    #
    e_label=y-ymeas;
    Sypm2_e_label=Sypm2.*e_label;
    residual=Sypm2_e_label.*e_label;
    #
    (residual,X,y,h,omega,Sypm2_e_label)
end

function solveStatAndGradDirect(v,Xinp,M,b,cum)
    #
    (residual,X,y,h,omega,Sypm2_e_label)=solveStat(v,Xinp,M,b,cum)
    #
    # Compute the state derivatives
    #
    (dgdv_nz,db_dx_nz)=forwardStatDerivatives(v,X,dgdv,db_dx)
    dX_dv=zeros(size(X,1),size(v,1))
    dgdv=full(spset(dgdv.value,dgdv_nz))
    #
    dX_dv[cum[1],:]=-(h[1]\dgdv(cum[1],:))
    for i=2:length(cum)
        dX_dv[cum[i],:]=-(h[i]\(dgdv[cum[i],:]+spset(db_dx[i].value,db_dx_nz[i])*dX_dv))
    end
    #
    # Compute the gradient (forward method)
    #
    Omega_Sypm2_e_label=omega[omega_ind_meas].*Sypm2_e_label
    gradv=2*(Omega_Sypm2_e_label'*Cx*dX_dv)'
    #
    (residual,gradv,X,y)
end

function solveStatAndDerivative(v,Xinp,M,b,cum)
  #
  (residual,X,y,h,omega,Sypm2_e_label)=solveStat(v,Xinp,M,b,cum)
  #
  # Compute the state derivatives
  #
  (dgdv_nz,db_dx_nz)=forwardStatDerivatives(v,X,dgdv,db_dx)
  dX_dv=zeros(size(X,1),size(v,1))
  dgdv=full(spset(dgdv.value,dgdv_nz))
  #
  dX_dv[cum[1],:]=-(h[1]\dgdv(cum[1],:))
  for i=2:length(cum)
      dX_dv[cum[i],:]=-(h[i]\(dgdv[cum[i],:]+spset(db_dx[i].value,db_dx_nz[i])*dX_dv))
  end
  #
  # Compute the derivative (forward method)
  #
  dy_dv=spdiag(omega[omega_ind_meas])*Cx*dX_dv
  #
  (residual,X,y,dy_dv,omega)
end


function solveStatAndGradAdjoint(v,Xinp,M,b,cum)
    #
    (residual,X,y,h,omega,Sypm2_e_label)=solveStat(v,Xinp,M,b,cum)
    #
    # Compute the state derivatives
    #
    (dgdvt_nz,db_dxt_nz)=adjointStatDerivatives(v,X,dgdvt,db_dxt)
    #
    Ct_Omega_Sypm2_e_label=Cxt*(omega[omega_ind_meas][:].*Sypm2_e_label)
    #
    p=zeros(X);
    ncum=length(cum);
    p[cum[ncum]]=h[ncum]'\Ct_Omega_Sypm2_e_label[cum[ncum]]
    for i=ncum-1:-1:1
        p[cum[i]]=h[i]'\(Ct_Omega_Sypm2_e_label[cum[i]]-spset(db_dxt[i].value,db_dxt_nz[i])*p);
    end
    #
    # Compute the gradient (adjoint method)
    #
    gradv=-2*spset(dgdvt.value,dgdvt_nz)*p;
    #
    (residual,gradv,X,y)
end


function forwardStatDerivatives(v,X,dgdv,db_dx)
    #
    # Derivatives needed for the forward computation of gradient
    #
    dgdv_nz=dgdv.t*(X[dgdv.m1].*X[dgdv.m2]);
    db_dx_nz=cell(1+length(db_dx))
    for i=2:length(db_dx)
        db_dx_nz[i]=db_dx[i].t*(v[db_dx[i].m2].*X[db_dx[i].m1]);
    end
    #
    (dgdv_nz,db_dx_nz)
end

function adjointStatDerivatives(v,X,dgdvt,db_dxt)
    #
    # Derivatives needed for the adjoint computation of gradient
    #
    dgdvt_nz=dgdvt.t*(X[dgdvt.m1].*X[dgdvt.m2]);
    db_dxt_nz=cell(length(db_dxt))
    for i=1:length(db_dxt)-1
        db_dxt_nz[i]=db_dxt[i].t*(v[db_dxt[i].m2].*X[db_dxt[i].m1]);
    end
    #
    (dgdvt_nz,db_dxt_nz)
end

function solveStatWeight(v,X,Mn,bn)
  #
  # Weight n cumomers
  #
  Mn_handle=lufact(sparse(Mn.ij[:,1],Mn.ij[:,2],Mn.t*v[Mn.m1],Mn.size[1],Mn.size[2]));
  Xn=-(Mn_handle\full(spset(bn.value,bn.t*(X[bn.m1].*X[bn.m2].*v[bn.m3]))));
  (Xn,Mn_handle)
end

function spset(A,v)
  A.nzval=v
  A
end

