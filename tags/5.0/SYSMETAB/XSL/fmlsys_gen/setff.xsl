<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Authors :   Stéphane Mottelet
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt fwd">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
    
  <xsl:strip-space elements="*"/>

	<xsl:param name="fwdfile"/>

  <xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

  <xsl:template match="f:variables">
  		<variables xmlns="http://www.13cflux.net/fluxml">
  			<xsl:for-each select="document($fwdfile,/)/fwd:fwdsim/fwd:stoichiometry/fwd:flux/*[@type='free']">
  				<fluxvalue flux="{../@id}" type="{name()}" xmlns="http://www.13cflux.net/fluxml">
  					<xsl:value-of select="@value"/>
  				</fluxvalue>
  			</xsl:for-each>
        <xsl:choose>
          <xsl:when test="//f:fluxml/f:configuration[@stationary='false'] and document($fwdfile,/)/fwd:fwdsim/fwd:simulation/fwd:pool[@value]">
  			      <xsl:for-each select="//f:fluxml/f:reactionnetwork/f:metabolitepools/f:pool">
  				      <poolvalue pool="{@id}" xmlns="http://www.13cflux.net/fluxml">
                  <xsl:if test="f:poolvalue[(@id=current()/@id) and @lo]">
                    <xsl:attribute name="lo">
                      <xsl:value-of select="f:poolvalue[(@id=current()/@id)]/@lo"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="f:poolvalue[(@id=current()/@id) and @hi]">
                    <xsl:attribute name="hi">
                      <xsl:value-of select="f:poolvalue[(@id=current()/@id)]/@hi"/>
                    </xsl:attribute>
                  </xsl:if>
  					      <xsl:value-of select="document($fwdfile,/)/fwd:fwdsim/fwd:simulation/fwd:pool[@id=current()/@id]/@value"/>
  				      </poolvalue>
  			      </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="f:poolvalue"/>
          </xsl:otherwise>
        </xsl:choose>
  		</variables>
	</xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>


</xsl:stylesheet>
