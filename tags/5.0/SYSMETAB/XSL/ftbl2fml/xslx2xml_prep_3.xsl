<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:ftbl="http://ftbl.org"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func ftbl">
  
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  
  <xsl:template match="*|@*|text()">    
      <xsl:copy>
          <xsl:apply-templates select="*|@*|text()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="ftbl">
    <ftbl>
      <xsl:apply-templates />
    </ftbl>
  </xsl:template>  

  <xsl:template match="section[@id='MASS_SPECTROMETRY']">
    <section id="{@id}">
      <xsl:for-each select="row[value[@position=3]][position()&gt;1]">
        <xsl:variable name="posnext" select="following-sibling::row[value[@position=3]][1]/@position"/>
        <xsl:choose>
          <xsl:when test="string-length($posnext)=0">
            <subsection>
              <xsl:choose>
                <xsl:when test="not(value[@position=2])">
                  <xsl:attribute name="id">
                    <xsl:value-of select="preceding-sibling::row[value[@position=2]][1]/value[@position=2]"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="id">
                    <xsl:value-of select="value[@position=2]"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="value[@position &gt; 2]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 2]"/>
                </row>
              </xsl:if>
              <xsl:copy-of select="following-sibling::row"/>
            </subsection>
          </xsl:when>
          <xsl:otherwise>
            <subsection>
              <xsl:choose>
                <xsl:when test="not(value[@position=2])">
                  <xsl:attribute name="id">
                    <xsl:value-of select="preceding-sibling::row[value[@position=2]][1]/value[@position=2]"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="id">
                    <xsl:value-of select="value[@position=2]"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="value[@position &gt; 2]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 2]"/>
                </row>
              </xsl:if>
              <xsl:for-each select="following-sibling::row[@position &lt;$posnext]">
                <xsl:copy-of select="."/>
              </xsl:for-each>
            </subsection>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </section>
  </xsl:template>

  <xsl:template match="section[@id='PEAK_MEASUREMENTS']">
    <section id="{@id}">
      <xsl:for-each select="row[value[@position=3]][position()&gt;1]">
        <xsl:variable name="peak_no" select="value[@position=3]"/>
        <xsl:variable name="posnext" select="following-sibling::row[value[@position=3]][1]/@position"/>
        <xsl:choose>
          <xsl:when test="string-length($posnext)=0">
            <subsection>
              <xsl:choose>
                <xsl:when test="not(value[@position=2])">
                  <xsl:attribute name="id">
                    <xsl:value-of select="preceding-sibling::row[value[@position=2]][1]/value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="peak_no">
                    <xsl:value-of select="$peak_no"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="id">
                    <xsl:value-of select="value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="peak_no">
                    <xsl:value-of select="$peak_no"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="value[@position &gt; 3]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 3]"/>
                </row>
              </xsl:if>
              <xsl:for-each select="following-sibling::row">
                <row position="{@position}">
                  <xsl:copy-of select="value"/>
                </row>
              </xsl:for-each>
            </subsection>
          </xsl:when>
          <xsl:otherwise>
            <subsection>
              <xsl:choose>
                <xsl:when test="not(value[@position=2])">
                  <xsl:attribute name="id">
                    <xsl:value-of select="preceding-sibling::row[value[@position=2]][1]/value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="peak_no">
                    <xsl:value-of select="$peak_no"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="id">
                    <xsl:value-of select="value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="peak_no">
                    <xsl:value-of select="$peak_no"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="value[@position &gt; 3]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 3]"/>
                </row>
              </xsl:if>
              <xsl:for-each select="following-sibling::row[@position &lt;$posnext]">
                <row position="{@position}">
                  <xsl:copy-of select="value"/>
                </row>
              </xsl:for-each>
            </subsection>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </section>
  </xsl:template>

  <xsl:template match="section[@id='LABEL_MEASUREMENTS']">
    <section id="{@id}">
      <xsl:for-each select="row[value[@position=3]][position()&gt;1]">
        <xsl:variable name="group" select="value[@position=3]"/>
        <xsl:variable name="posnext" select="following-sibling::row[value[@position=3]][1]/@position"/>
        <xsl:choose>
          <xsl:when test="string-length($posnext)=0">
            <subsection>
              <xsl:choose>
                <xsl:when test="not(value[@position=2])">
                  <xsl:attribute name="id">
                    <xsl:value-of select="preceding-sibling::row[value[@position=2]][1]/value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="group">
                    <xsl:value-of select="$group"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="id">
                    <xsl:value-of select="value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="group">
                    <xsl:value-of select="$group"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="value[@position &gt; 3]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 3]"/>
                </row>
              </xsl:if>
              <xsl:for-each select="following-sibling::row">
                <row position="{@position}">
                  <xsl:copy-of select="value"/>
                </row>
              </xsl:for-each>
            </subsection>
          </xsl:when>
          <xsl:otherwise>
            <subsection>
              <xsl:choose>
                <xsl:when test="not(value[@position=2])">
                  <xsl:attribute name="id">
                    <xsl:value-of select="preceding-sibling::row[value[@position=2]][1]/value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="group">
                    <xsl:value-of select="$group"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="id">
                    <xsl:value-of select="value[@position=2]"/>
                  </xsl:attribute>
                  <xsl:attribute name="group">
                    <xsl:value-of select="$group"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="value[@position &gt; 3]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 3]"/>
                </row>
              </xsl:if>
              <xsl:for-each select="following-sibling::row[@position &lt;$posnext]">
                <row position="{@position}">
                  <xsl:copy-of select="value"/>
                </row>
              </xsl:for-each>
            </subsection>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </section>
  </xsl:template>

  <xsl:template match="section[(@id='LABEL_INPUT') or (@id='FLUXES') or (@id='EQUALITIES') or (@id='INEQUALITIES')]">
    <xsl:variable name="start" select="number(not((@id='FLUXES') or (@id='EQUALITIES') or (@id='INEQUALITIES')))"/>    
    <section id="{@id}">
      <xsl:for-each select="row[value[@position=2]][position()&gt;$start]">
        <xsl:variable name="posnext" select="following-sibling::row[value[@position=2]][1]/@position"/>
        <xsl:choose>
          <xsl:when test="string-length($posnext)=0">
            <subsection id="{value[@position=2]}">
              <xsl:if test="value[@position &gt; 2]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 2]"/>
                </row>
              </xsl:if>
              <xsl:copy-of select="following-sibling::row"/>
            </subsection>
          </xsl:when>
          <xsl:otherwise>
            <subsection>
              <xsl:attribute name="id">
                <xsl:value-of select="value[@position=2]"/>
              </xsl:attribute>
              <xsl:if test="value[@position &gt; 2]">
                <row position="{@position}">
                  <xsl:copy-of select="value[@position &gt; 2]"/>
                </row>
              </xsl:if>
              <xsl:for-each select="following-sibling::row[@position &lt;$posnext]">
                <xsl:copy-of select="."/>
              </xsl:for-each>
            </subsection>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </section>
  </xsl:template>

</xsl:stylesheet>  