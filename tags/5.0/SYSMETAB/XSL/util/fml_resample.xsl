<?xml version="1.0" encoding="ISO-8859-1" ?>


<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  xmlns:str="http://exslt.org/strings"
  exclude-result-prefixes="xsl smtb math m f exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:param name="nsamples">100</xsl:param>
  <xsl:param name="output">out</xsl:param>
  <xsl:variable name="nd" select="floor(math:log($nsamples) div math:log(10))+1"/>

  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
      <xsl:call-template name="resample"/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:datum">
    <datum  xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <!-- Box Muller transform is used to generate a std centered normal sample -->
      <xsl:variable name="value" select="number(.)+@stddev*math:sqrt(-2*math:log(math:random()))*math:cos(8*math:atan(1)*math:random())"/>
      <xsl:choose>
        <xsl:when test="$value &gt;0">
          <xsl:value-of select="$value"/>
        </xsl:when>
        <xsl:otherwise>0</xsl:otherwise>    
      </xsl:choose>
    </datum>
  </xsl:template>

  <xsl:template name="resample">
    <xsl:param name="n" select="$nsamples"/>
    <xsl:if test="$n &gt; 0">
      <xsl:document href="{concat($output,'.',format-number($n,str:padding($nd,'0')),'.fml')}" method="xml" indent="yes">
        <xsl:apply-templates/>
      </xsl:document>
      <xsl:call-template name="resample">
        <xsl:with-param name="n" select="($n)-1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>


</xsl:stylesheet>