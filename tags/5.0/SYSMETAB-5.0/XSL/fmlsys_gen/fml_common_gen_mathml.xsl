<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
  
  <!-- Number of reactions divided by 2 -->
  <xsl:variable name="dimdiv2" select="count(f:fluxml/f:reactionnetwork/f:reaction) div 2"/>

  <!-- find all f:rproduct from forward f:reaction -->
  <xsl:key name="RPRODUCT_F" match="f:reaction[substring(@id, string-length(@id) - string-length('_f') + 1)='_f']/f:rproduct" use="@id"/>

  <!-- find all f:reduct from forward f:reaction -->
  <xsl:key name="REDUCT_F" match="f:reaction[substring(@id, string-length(@id) - string-length('_f') + 1)='_f']/f:reduct" use="@id"/>

  <!-- find all f:reaction -->
  <xsl:key name="REACTION" match="f:reaction" use="@id"/>

  <!-- find all forward f:reaction -->
  <xsl:key name="REACTION_F" match="f:reaction[substring(@id, string-length(@id) - string-length('_f') + 1)='_f']" use="@id"/>

  <!-- find all f:datum -->
  <xsl:key name="DATUM" match="f:datum" use="concat(@id,'_',@row,@weight)"/>

  <!-- find all f:group -->
  <xsl:key name="GROUP" match="f:group" use="@id"/>

  <!-- find all f:pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>

  <!-- find all smtb:cumomer-contribution -->
  <xsl:key name="cumomer-contribution" match="smtb:cumomer-contribution" use="@pool"/>

  <!-- find all f:poolmeasurement/f:textual -->
  <xsl:key name="poolmeasurement" match="f:poolsize" use="f:textual"/>


  
	<xsl:variable name="nX" select="1+count(//smtb:listOfIntermediateCumomers//smtb:cumomer | //smtb:listOfInputCumomers//smtb:cumomer)"/>
  <xsl:variable name="nY" select="count(//f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement)"/>
  <xsl:variable name="nv" select="count(//f:reactionnetwork/f:reaction)"/>

  <!--############################################################-->
  <!--## Template to determine Substring before last occurence  ##-->
  <!--## of a specific delemiter                                ##-->
  <!--############################################################-->
  <xsl:template name="substring-before-last">
    <!--passed template parameter -->
    <xsl:param name="list"/>
    <xsl:param name="delimiter"/>
    <xsl:choose>
      <xsl:when test="contains($list, $delimiter)">
        <!-- get everything in front of the first delimiter -->
        <xsl:value-of select="substring-before($list,$delimiter)"/>
        <xsl:choose>
          <xsl:when test="contains(substring-after($list,$delimiter),$delimiter)">
            <xsl:value-of select="$delimiter"/>
          </xsl:when>
        </xsl:choose>
        <xsl:call-template name="substring-before-last">
          <!-- store anything left in another variable -->
          <xsl:with-param name="list" select="substring-after($list,$delimiter)"/>
          <xsl:with-param name="delimiter" select="$delimiter"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Building of matrix E-->

  <xsl:template name="construct_matrix_E">

    <comment xmlns="http://www.utc.fr/sysmetab">Matrice d'observation (flux)</comment>

    <optimize xmlns="http://www.utc.fr/sysmetab">
      <!-- We open an item "optimize" in which we will gradually build the matrix E -->
      <matrix-open id="E" 
                   cols="{count(f:reactionnetwork/f:reaction)}" 
                   rows="{count(f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux)}"
                   assignments="unique"
                   type="sparse"/>
      <xsl:for-each select="f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
        <xsl:variable name="pool" select="@pool"/>
        <matrix-assignment id="E" row="{position()}" col="{key('REACTION_F',concat(f:textual,'_f'))/@pos}">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
        </matrix-assignment>
      </xsl:for-each>  
      <matrix-close id="E"/>

      <matrix-open id="Em" 
                   cols="{count(f:reactionnetwork/f:metabolitepools/f:pool)}" 
                   rows="{count(f:configuration/f:measurement/f:model/f:poolmeasurement/f:poolsize)}"
                   assignments="unique"
                   type="sparse"/>
      <xsl:for-each select="f:configuration/f:measurement/f:model/f:poolmeasurement/f:poolsize">        
        <matrix-assignment id="Em" row="{position()}" col="{key('POOL',f:textual)/@number}">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
        </matrix-assignment>
      </xsl:for-each>  
      <matrix-close id="Em"/>
      <!-- When we will found this element in the transformation which have a goal to produce the Scilab code, 
      we combine all assignments order to the matrix "E" and we build this matrix -->
    </optimize>
    
    <xsl:if test="f:configuration/@stationary='false'">
      <comment xmlns="http://www.utc.fr/sysmetab"> time vector</comment>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>tmeas</ci>
          <vector>
            <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/smtb:times/smtb:time">
              <cn>
                <xsl:value-of select="."/>
              </cn>
            </xsl:for-each>
          </vector>
        </apply>
    </xsl:if>

    <xsl:choose>  
      <xsl:when test="f:configuration/@stationary='true'">        
        <comment xmlns="http://www.utc.fr/sysmetab"> ymeas and Sy vectors (stationary measurements)</comment>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>Sy</ci>
          <vector>
            <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement">
							<xsl:choose>
								<xsl:when test="key('DATUM',concat(../@id,'_',@row,@weight))">
		              <cn>
		                <xsl:value-of select="key('DATUM',concat(../@id,'_',@row,@weight))/@stddev"/>
		              </cn>
								</xsl:when>
								<xsl:otherwise>
									<cn>
										<infinity/>
									</cn>
								</xsl:otherwise>
							</xsl:choose>
            </xsl:for-each>
          </vector>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>ymeas</ci>
          <vector>
            <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement">
							<xsl:choose>
								<xsl:when test="key('DATUM',concat(../@id,'_',@row,@weight))">
		              <cn>
		                <xsl:value-of select="key('DATUM',concat(../@id,'_',@row,@weight))"/>
		              </cn>
								</xsl:when>
								<xsl:otherwise>
									<cn>0</cn>
								</xsl:otherwise>
							</xsl:choose>
            </xsl:for-each>
          </vector>
        </apply>
      </xsl:when>

      <xsl:otherwise>
        <comment xmlns="http://www.utc.fr/sysmetab"> ymeas and Sy matrices (non-stationary measurements)</comment>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>Sy</ci>
          <matrix>
            <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement"> 
              <xsl:variable name="keyvalue">
                <xsl:value-of select="concat(../@id,'_',@row,@weight)"/>
              </xsl:variable>

              <matrixrow>
                <xsl:for-each select="../../smtb:times/smtb:time">
                  <xsl:choose>
                    <xsl:when test="key('DATUM',$keyvalue)[@time=current()/.]">
                      <cn>
                        <xsl:value-of select="key('DATUM',$keyvalue)[@time=current()/.]/@stddev"/>
                      </cn>
                    </xsl:when>
                    <xsl:otherwise>
                      <cn>
                        <infinity/>
                      </cn>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>  
              </matrixrow>

            </xsl:for-each>
          </matrix>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>ymeas</ci>
          <matrix>
            <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement"> 
              <xsl:variable name="keyvalue">
                <xsl:value-of select="concat(../@id,'_',@row,@weight)"/>
              </xsl:variable>

              <matrixrow>
                <xsl:for-each select="../../smtb:times/smtb:time">
                  <xsl:choose>
                    <xsl:when test="key('DATUM',$keyvalue)[@time=current()/.]">
                      <cn>
                        <xsl:value-of select="key('DATUM',$keyvalue)[@time=current()/.]"/>
                      </cn>
                    </xsl:when>
                    <xsl:otherwise>
                      <cn>0</cn>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>  
              </matrixrow>

            </xsl:for-each>
          </matrix>
        </apply>


      </xsl:otherwise>    
    </xsl:choose>    
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>Sypm2</ci>
      <apply>
        <power type="array"/>
        <ci>Sy</ci>
        <cn>-2</cn>
      </apply>
    </apply>

    <comment xmlns="http://www.utc.fr/sysmetab"> (Sv)^-2 array</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>Svpm2</ci>
      <apply>
        <power type="array"/>
        <vector>
          <xsl:for-each select="f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
						<xsl:choose>
							<xsl:when test="key('DATUM',@id)">
	              <cn>
	                <xsl:value-of select="key('DATUM',@id)/@stddev"/>
	              </cn>
							</xsl:when>
							<xsl:otherwise>
								<cn>
									<infinity/>
								</cn>
							</xsl:otherwise>
						</xsl:choose>
          </xsl:for-each>
        </vector>
        <apply>
          <minus/>
          <cn>2</cn>
        </apply>
      </apply>
    </apply>

    <comment  xmlns="http://www.utc.fr/sysmetab"> wmeas array</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>wmeas</ci>
      <vector>
        <xsl:for-each select="f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
					<xsl:choose>
						<xsl:when test="key('DATUM',@id)">
              <cn>
                <xsl:value-of select="key('DATUM',@id)"/>
              </cn>
						</xsl:when>
						<xsl:otherwise>
							<cn>0</cn>
						</xsl:otherwise>
					</xsl:choose>
        </xsl:for-each>
      </vector>
    </apply>

    <comment xmlns="http://www.utc.fr/sysmetab"> (Sm)^-2 array</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>Smpm2</ci>
      <apply>
        <power type="array"/>
        <vector>
          <xsl:for-each select="f:configuration/f:measurement/f:model/f:poolmeasurement/f:poolsize">
						<xsl:choose>
							<xsl:when test="key('DATUM',@id)">
	              <cn>
	                <xsl:value-of select="key('DATUM',@id)/@stddev"/>
	              </cn>
							</xsl:when>
							<xsl:otherwise>
								<cn>
									<infinity/>
								</cn>
							</xsl:otherwise>
						</xsl:choose>
          </xsl:for-each>
        </vector>
        <apply>
          <minus/>
          <cn>2</cn>
        </apply>
      </apply>
    </apply>

    <comment  xmlns="http://www.utc.fr/sysmetab"> mmeas array</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>mmeas</ci>
      <vector>
        <xsl:for-each select="f:configuration/f:measurement/f:model/f:poolmeasurement/f:poolsize">
					<xsl:choose>
						<xsl:when test="key('DATUM',@id)">
              <cn>
                <xsl:value-of select="key('DATUM',@id)"/>
              </cn>
						</xsl:when>
						<xsl:otherwise>
							<cn>0</cn>
						</xsl:otherwise>
					</xsl:choose>
        </xsl:for-each>
      </vector>
    </apply>

  </xsl:template>
  
  <!-- Building input matrices, allows to obtain the contribution on the cumomers of isotopomers in input. -->
  <xsl:template name="construct_matrix_D">

    <comment xmlns="http://www.utc.fr/sysmetab">Input Matrices</comment>

    <optimize xmlns="http://www.utc.fr/sysmetab">
    <!-- We open an item "optimize" in which we will gradually build the matrices D1,D2,...,Dn where "n" is the
	maximum weight, determined in preliminary transformations based on selected observations. -->
      <matrix-open id="D" 
                   cols="{1+count(f:reactionnetwork/f:metabolitepools/f:pool[smtb:input]/smtb:input)}" 
                   rows="{$nX}" 
                   assignments="unique"
                   type="sparse"/>
        <!-- For each cumomer of an input metabolite, we determine what are the contributions of severals input 
        isotopomers. -->
        <matrix-assignment id="D" row="1" col="1">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
        </matrix-assignment> <!-- for the 0 weight cumomer, always equal to 1-->
        <xsl:for-each select="f:reactionnetwork/smtb:listOfInputCumomers//smtb:cumomer">
        <!-- Warning, here the attribute "position" of the element <smtb:cumomer> represents only the number of 
        the cumomer in the vector of *all* cumomers . -->
          <xsl:variable name="position" select="@number"/>
          <xsl:variable name="cumomer_id" select="@id"/>
          <xsl:variable name="pool" select="@pool"/>
          <xsl:for-each select="../../../f:metabolitepools/f:pool[smtb:input]/smtb:input">
            <xsl:if test="../@id=$pool">
              <xsl:variable name="pos" select="position()"/>
              <xsl:for-each select="smtb:cumomer-contribution[concat($pool,'_',@subscript)=$cumomer_id]">
                <matrix-assignment id="D" row="{$position}" col="{1+number($pos)}">
                  <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
                </matrix-assignment>
              </xsl:for-each>
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>
        <matrix-close id="D"/>
    </optimize>
  </xsl:template>
  
  <!-- Building matrices of fluxes names and ids -->
  <xsl:template name="names_and_ids_fluxes">
    <!-- Matrix of metabolite names -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>metabolite_names</ci>
      <list separator=";">
        <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select='@id'/>
          </string>
        </xsl:for-each>
      </list>
    </apply>
    <!-- Matrix of fluxes names -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_names</ci>
      <list separator=";">
        <xsl:for-each select="f:reactionnetwork/f:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:choose>
              <xsl:when test="substring(@id, string-length(@id) - string-length('_f') + 1)='_f'">
                <xsl:variable name="id">
                  <xsl:call-template name="substring-before-last">
                    <xsl:with-param name="list" select="@id"/>
                    <xsl:with-param name="delimiter" select="'_f'"/>
                  </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="concat($id,'_n')"/>
              </xsl:when>
              <xsl:when test="substring(@id, string-length(@id) - string-length('_b') + 1)='_b'">
                <xsl:variable name="id">
                  <xsl:call-template name="substring-before-last">
                    <xsl:with-param name="list" select="@id"/>
                    <xsl:with-param name="delimiter" select="'_b'"/>
                  </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="concat($id,'_x')"/>
              </xsl:when>
            </xsl:choose>
          </string>
        </xsl:for-each>
      </list>
    </apply>
    <!-- Matrix of fluxes ids -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_ids</ci>
      <list separator=";">
        <xsl:for-each select="f:reactionnetwork/f:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:choose>
              <xsl:when test="substring(@id, string-length(@id) - string-length('_f') + 1)='_f'">
                <xsl:variable name="id">
                  <xsl:call-template name="substring-before-last">
                    <xsl:with-param name="list" select="@id"/>
                    <xsl:with-param name="delimiter" select="'_f'"/>
                  </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="concat($id,'_n')"/>
              </xsl:when>
              <xsl:when test="substring(@id, string-length(@id) - string-length('_b') + 1)='_b'">
                <xsl:variable name="id">
                  <xsl:call-template name="substring-before-last">
                    <xsl:with-param name="list" select="@id"/>
                    <xsl:with-param name="delimiter" select="'_b'"/>
                  </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="concat($id,'_x')"/>
              </xsl:when>
            </xsl:choose>
          </string>
        </xsl:for-each>
      </list>
    </apply>
    <!-- Matrix of scaled measurement ids -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>scaling_ids</ci>
      <list separator=";">
        <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="@id"/>
          </string>
        </xsl:for-each>
      </list>
    </apply>
    <!-- Matrix of measurement ids -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>measurement_ids</ci>
      <list separator=";">
        <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool/smtb:measurement">
          <xsl:sort select="@id" data-type="text"/>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(../@id,@string)"/>
          </string>
        </xsl:for-each>
      </list>
    </apply>
  </xsl:template>

  <!-- Cumomers in input, computed from isotopomers of input metabolites. -->
  <xsl:template name="input_cumomeres">

    <comment xmlns="http://www.utc.fr/sysmetab"><xsl:value-of select="name(.)"/>Cumomers in input, computed from isotopomers of input metabolites.</comment>

    <xsl:for-each select="f:configuration/f:input">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
        <list separator=";">
          <xsl:for-each select="f:label">
            <cn>
              <xsl:value-of select="."/>
            </cn>
          </xsl:for-each>
        </list>
      </apply>    
    </xsl:for-each>
		
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Xinp</ci>
        <apply>
          <times/>
          <ci>D</ci>
          <list separator=";">
            <xsl:for-each select="../f:metabolitepools/f:pool[smtb:input]">
              <ci><xsl:value-of select="concat(@id,'_input')"/></ci>
            </xsl:for-each>
          </list>
        </apply>
      </apply>
 
  </xsl:template>

  <xsl:template name="fluxSubspace">
  
    <comment xmlns="http://www.utc.fr/sysmetab">Building matrices (also the stoichiometry matrix) allows to compute the linear change of variable of the fluxes.</comment>
    
    <xsl:apply-templates select="f:constraints"/>
    <xsl:apply-templates select="f:reactionnetwork/f:metabolitepools" mode="stoichiometric-matrix"/>
    <xsl:apply-templates select="f:configuration/f:simulation"/>

    <comment xmlns="http://www.utc.fr/sysmetab">Equality and inequality constraints</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>C</ci>
      <matrix>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:eq]">
          <matrixrow>
          <ci>A_eq_net</ci>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:constraints/f:net/m:math/m:apply[m:eq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$dimdiv2"/>
            </cn>
          </apply>
         </matrixrow>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:eq]">
          <matrixrow>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(f:constraints/f:xch/m:math/m:apply[m:eq])"/>
              </cn>
              <cn>
                <xsl:value-of select="$dimdiv2"/>
              </cn>
            </apply>
            <ci>A_eq_xch</ci>            
          </matrixrow>
        </xsl:if>
        <matrixrow>
          <ci>N</ci>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
            </cn>
            <cn>
              <xsl:value-of select="$dimdiv2"/>
            </cn>
          </apply>
        </matrixrow>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:leq]">
          <matrixrow>
            <apply>
              <minus/>
              <ci>A_leq_net</ci>
            </apply>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(f:constraints/f:net/m:math/m:apply[m:leq])"/>
              </cn>
              <cn>
                <xsl:value-of select="$dimdiv2"/>
              </cn>
            </apply>
          </matrixrow>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:leq]">
          <matrixrow>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(f:constraints/f:xch/m:math/m:apply[m:leq])"/>
              </cn>
              <cn>
                <xsl:value-of select="$dimdiv2"/>
              </cn>
            </apply>
            <apply>
              <minus/>
              <ci>A_leq_xch</ci>
            </apply>            
          </matrixrow>
        </xsl:if>
      </matrix>
    </apply>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>d</ci>
      <vector>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:eq]">
          <ci>b_eq_net</ci>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:eq]">
          <ci>b_eq_xch</ci>            
        </xsl:if>
        <apply>
          <fn><ci>zeros</ci></fn>
          <cn>
            <xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
          </cn>
          <cn>1</cn>
        </apply>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:leq]">
          <apply>
            <minus/>
            <ci>b_leq_net</ci>
          </apply>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:leq]">
          <apply>
            <minus/>
            <ci>b_leq_xch</ci>
          </apply>            
        </xsl:if>
      </vector>
    </apply>

  </xsl:template>

  <xsl:template match="f:constraints">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>MAX_XCH</ci>
      <cn>
        <xsl:value-of select="exslt:node-set($params)[@name='max_xch']"/>
      </cn>
    </apply>  
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>MIN_INOUT</ci>
      <apply>
        <fn><ci>max</ci></fn>
        <cn>
          <xsl:value-of select="exslt:node-set($params)[@name='min_inout']"/>
        </cn>
        <cn>
         <xsl:value-of select="exslt:node-set($params)[@name='min_nr']"/>
       </cn>
     </apply>
    </apply>  
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>MIN_NR</ci>
      <cn>
        <xsl:value-of select="exslt:node-set($params)[@name='min_nr']"/>
      </cn>
    </apply>  
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>MAX_NET</ci>
      <cn>
        <xsl:value-of select="exslt:node-set($params)[@name='max_net']"/>
      </cn>
    </apply>  
    <optimize xmlns="http://www.utc.fr/sysmetab">
      <xsl:if test="f:net/m:math/m:apply[m:leq]">
        <matrix-open type="full" id="A_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="{$dimdiv2}"/>
        <matrix-open type="full" id="b_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="1"/>
      </xsl:if>
      <xsl:if test="f:net/m:math/m:apply[m:eq]">
        <matrix-open type="full" id="A_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="{$dimdiv2}"/>
        <matrix-open type="full" id="b_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="1"/>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:leq]">
        <matrix-open type="full" id="A_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="{$dimdiv2}"/>
        <matrix-open type="full" id="b_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="1"/>
      </xsl:if>  
      <xsl:if test="f:xch/m:math/m:apply[m:eq]">
        <matrix-open type="full" id="A_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="{$dimdiv2}"/>
        <matrix-open type="full" id="b_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="1"/>
      </xsl:if>
      <xsl:apply-templates select="f:xch/m:math/m:apply[m:eq]"/>
      <xsl:apply-templates select="f:xch/m:math/m:apply[m:leq]"/>
      <xsl:apply-templates select="f:net/m:math/m:apply[m:eq]"/>
      <xsl:apply-templates select="f:net/m:math/m:apply[m:leq]"/>
      <xsl:if test="f:net/m:math/m:apply[m:leq]">
        <matrix-close id="A_leq_net"/>
        <matrix-close id="b_leq_net"/>
      </xsl:if>
      <xsl:if test="f:net/m:math/m:apply[m:eq]">
        <matrix-close id="A_eq_net"/>
        <matrix-close id="b_eq_net"/>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:leq]">
        <matrix-close id="A_leq_xch"/>
        <matrix-close id="b_leq_xch"/>
      </xsl:if>  
      <xsl:if test="f:xch/m:math/m:apply[m:eq]">
        <matrix-close id="b_eq_xch"/>
        <matrix-close id="A_eq_xch"/>
      </xsl:if>
    </optimize>
  </xsl:template>

  <xsl:template match="f:constraints/*/m:math/m:apply">
    <xsl:if test="m:cn!='0'">
      <matrix-assignment id="{concat('b_',name(*[1]),'_',name(../..))}" row="{position()}" col="1" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="name(*[1])='leq' and name(*[2])='ci' and name(*[3])='cn'">
            <cn xmlns="http://www.w3.org/1998/Math/MathML">-(<xsl:value-of select="m:cn"/>)</cn>
          </xsl:when>
          <xsl:otherwise>
            <cn xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="m:cn"/></cn>
          </xsl:otherwise>
        </xsl:choose>
      </matrix-assignment>
    </xsl:if>
    <xsl:variable name="id" select="concat('A_',name(*[1]),'_',name(../..))"/>
    <xsl:choose>
      <xsl:when test="m:ci and not(m:apply)">
        <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION_F',concat(m:ci,'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <xsl:choose>
            <xsl:when test="name(*[1])='leq' and name(*[2])='ci' and name(*[3])='cn'">
              <cn xmlns="http://www.w3.org/1998/Math/MathML">-1</cn>
            </xsl:when>
            <xsl:otherwise>
              <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
            </xsl:otherwise>
          </xsl:choose>
        </matrix-assignment>
      </xsl:when>
      <xsl:when test="m:apply">
        <xsl:call-template name="iteration_eq_leq">
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="apply" select="m:apply"/>
          <xsl:with-param name="minus">
            <xsl:choose>
              <xsl:when test="m:apply[m:plus]">1</xsl:when>
              <xsl:when test="m:apply[m:minus]">-1</xsl:when>
            </xsl:choose>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="iteration_eq_leq">
    <xsl:param name="id"/>
    <xsl:param name="apply"/>
    <xsl:param name="minus" select="'1'"/>
    <xsl:choose>
      <xsl:when test="$apply[m:times]">
        <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',concat($apply/m:ci,'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <xsl:choose>
            <xsl:when test="$minus='-1'">
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <minus/>
                <cn><xsl:value-of select="$apply/m:cn"/></cn>
              </apply>
            </xsl:when>
            <xsl:otherwise>
              <cn><xsl:value-of select="$apply/m:cn"/></cn>
            </xsl:otherwise>
          </xsl:choose>
        </matrix-assignment>
      </xsl:when>
      <xsl:when test="(count($apply/m:ci)='2') and not($apply/m:apply)">
        <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',concat($apply/m:ci[1],'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
        </matrix-assignment>
        <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',concat($apply/m:ci[2],'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:value-of select="$minus"/>
          </cn>
        </matrix-assignment>
      </xsl:when>
      <xsl:when test="count($apply/m:apply)='1'">
        <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',concat($apply/m:ci,'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:choose>
              <xsl:when test="$apply[m:plus]">1</xsl:when>
              <xsl:when test="$apply[m:minus]">-1</xsl:when>
            </xsl:choose>
          </cn>  
        </matrix-assignment>
        <xsl:call-template name="iteration_eq_leq">
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="apply" select="$apply/m:apply"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="count($apply/m:apply)='2'">
        <xsl:call-template name="iteration_eq_leq">
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="apply" select="$apply/m:apply[1]"/>
        </xsl:call-template>
        <xsl:call-template name="iteration_eq_leq">
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="apply" select="$apply/m:apply[2]"/>
          <xsl:with-param name="minus">
            <xsl:choose>
              <xsl:when test="$apply[m:plus]">1</xsl:when>
              <xsl:when test="$apply[m:minus]">-1</xsl:when>
            </xsl:choose>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template> 

  <xsl:template match="f:metabolitepools" mode="stoichiometric-matrix">
    <optimize xmlns="http://www.utc.fr/sysmetab">
      <matrix-open type="full" id="N" rows="{count(f:pool[@type='intermediate'])}" cols="{count(../f:reaction) div 2}"/>
      <xsl:apply-templates select="f:pool[@type='intermediate']" mode="stoichiometric-matrix"/>
      <matrix-close id="N"/>
    </optimize>
  </xsl:template>

  <xsl:template match="f:pool" mode="stoichiometric-matrix">
    <xsl:variable name="position" select="position()"/>
    <xsl:for-each select="key('RPRODUCT_F',@id) | key('REDUCT_F',@id)">
      <matrix-assignment id="N" row="{$position}" col="{../@pos}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="self::f:rproduct">
            <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
          </xsl:when>
          <xsl:when test="self::f:reduct">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <minus/>
              <cn>1</cn>
            </apply>
          </xsl:when>
        </xsl:choose>
      </matrix-assignment>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:simulation">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>ffmat</ci>
      <matrix>
        <xsl:for-each select="f:variables/f:fluxvalue">
          <matrixrow>
            <cn>
              <xsl:choose>
                <xsl:when test="@type='net'">
                  <xsl:value-of select="key('REACTION',concat(@flux,'_f'))/@position"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="key('REACTION',concat(@flux,'_b'))/@position"/>
                </xsl:otherwise>
              </xsl:choose>
            </cn>
            <cn>
              <xsl:value-of select="text()"/>
            </cn>
          </matrixrow>
        </xsl:for-each>
      </matrix>
    </apply>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>mmat</ci>
      <matrix>
        <xsl:for-each select="f:variables/f:poolvalue">
          <matrixrow>
            <cn>
              <xsl:value-of select="key('POOL',@pool)/@number"/>                          
            </cn>
            <cn>            
              <xsl:value-of select="text()"/>
            </cn>
            <xsl:choose>
              <xsl:when test="@lo">
                <cn><xsl:value-of select="@lo"/></cn>
              </xsl:when>
              <xsl:otherwise>
                <cn>0</cn>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="@hi">
                <cn><xsl:value-of select="@hi"/></cn>
              </xsl:when>
              <xsl:otherwise>
                <cn><infinity/></cn>
              </xsl:otherwise>
            </xsl:choose>
          </matrixrow>
        </xsl:for-each>
        <xsl:for-each select="//f:reaction[not(f:rproduct)]"> <!-- output reaction -->
          <xsl:if test="not(key('cumomer-contribution',f:reduct/@id) or key('poolmeasurement',f:reduct/@id))"> <!-- if the output metabolite is not measured (via labeling or poolsize)-->
             <matrixrow>
               <cn><xsl:value-of select="key('POOL',f:reduct/@id)/@number"/></cn>
               <cn>1</cn> <!-- arbitrary value -->
               <cn>1</cn>
               <cn>1</cn>
             </matrixrow>
          </xsl:if>
        </xsl:for-each>
        <xsl:for-each select="//f:pool[@type='input']">
          <matrixrow>
            <cn><xsl:value-of select="@number"/></cn>
            <cn>1</cn> <!-- arbitrary value -->
            <cn>1</cn>
            <cn>1</cn>
          </matrixrow>          
        </xsl:for-each>
      </matrix>
    </apply>
  </xsl:template>

</xsl:stylesheet>
