<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Authors :   Stéphane Mottelet
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:exslt="http://exslt.org/common"
  xmlns:str="http://exslt.org/strings"
  exclude-result-prefixes="xsl smtb math m f exslt str fwd">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
    
  <xsl:strip-space elements="*"/>

	<xsl:param name="fwdfile"/>
	<xsl:param name="fmstddev">0.01</xsl:param>
	<xsl:param name="pmstddev">0.01</xsl:param>
	<xsl:param name="msstddev">0.01</xsl:param>

  <!-- find all f:datum -->
  <xsl:key name="DATUM" match="f:datum" use="concat(@id,'_',@row,@weight)"/>

  <xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

  <xsl:template match="f:measurement">
		<measurement xmlns="http://www.13cflux.net/fluxml">
	    <xsl:copy-of select="f:model"/>

			<data xmlns="http://www.13cflux.net/fluxml">

	      <xsl:for-each select="f:model/f:fluxmeasurement/f:netflux">
	        <datum id="{@id}">
						<xsl:attribute name="stddev">
							<xsl:choose>
								<xsl:when test="key('DATUM',concat(@id,'_'))">
									<xsl:value-of select="key('DATUM',concat(@id,'_'))/@stddev"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$fmstddev"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
	          <xsl:value-of select="document($fwdfile,/)/fwd:fwdsim/fwd:measurements/fwd:mgroup[@id=current()/@id]/fwd:value"/>
	        </datum>
	      </xsl:for-each>

	      <xsl:for-each select="f:model/f:poolmeasurement/f:poolsize">
	        <datum id="{@id}">
						<xsl:attribute name="stddev">
							<xsl:choose>
								<xsl:when test="key('DATUM',concat(@id,'_'))">
									<xsl:value-of select="key('DATUM',concat(@id,'_'))/@stddev"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$pmstddev"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
	          <xsl:value-of select="document($fwdfile,/)/fwd:fwdsim/fwd:measurements/fwd:mgroup[@id=current()/@id]/fwd:value"/>
	        </datum>
	      </xsl:for-each>

	      <xsl:for-each select="f:model/f:labelingmeasurement/f:group[contains(f:textual,'#M')]">
					<xsl:call-template name="times">
						<xsl:with-param name="spec" select="str:split(substring-after(f:textual,'#M'),',')"/>
						<xsl:with-param name="type" select="'MS'"/>
					</xsl:call-template>
	      </xsl:for-each>

	      <xsl:for-each select="f:model/f:labeling-measurement/f:group[not(contains(f:textual,'#M'))]">
					<xsl:call-template name="times">
						<xsl:with-param name="spec" select="str:split(f:textual,';')"/>
						<xsl:with-param name="type" select="'LM'"/>
					</xsl:call-template>
	      </xsl:for-each>

	    </data>  
		</measurement>
	</xsl:template>

	<xsl:template name="times">
		
		<xsl:param name="times">
			<xsl:choose>
				<xsl:when test="@times">
					<xsl:value-of select="@times"/>
				</xsl:when>
				<xsl:otherwise>Inf</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		<xsl:param name="i" select="'1'"/>
		<xsl:param name="last" select="count(str:split($times))"/>
		<xsl:param name="spec"/>
		<xsl:param name="type"/>
		
		<xsl:if test="$i&lt;=$last">
			<xsl:call-template name="spec">
				<xsl:with-param name="time" select="exslt:node-set(str:split($times))[$i]"/>
				<xsl:with-param name="spec" select="$spec"/>
				<xsl:with-param name="type" select="$type"/>
			</xsl:call-template>
			<xsl:call-template name="times">
				<xsl:with-param name="times" select="$times"/>
				<xsl:with-param name="i" select="1+$i"/>
				<xsl:with-param name="last" select="$last"/>
				<xsl:with-param name="spec" select="$spec"/>
				<xsl:with-param name="type" select="$type"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="spec">
		<xsl:param name="spec"/>
		<xsl:param name="i" select="'1'"/>
		<xsl:param name="last" select="count(exslt:node-set($spec))"/> 
		<xsl:param name="time"/>
		<xsl:param name="type"/>
		<xsl:if test="$i&lt;=$last">
			<datum id="{@id}" xmlns="http://www.13cflux.net/fluxml">
				<xsl:if test="$time!='Inf'">
					<xsl:attribute name="time">
						<xsl:value-of select="$time"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:variable name="weight" select="exslt:node-set($spec)[$i]"/>
				<xsl:choose>
					<xsl:when test="$type='MS'">
						<xsl:attribute name="weight">
							<xsl:value-of select="$weight"/>
						</xsl:attribute>
						<xsl:attribute name="stddev">
							<xsl:choose>
								<xsl:when test="key('DATUM',concat(@id,'_',$weight))">
									<xsl:value-of select="key('DATUM',concat(@id,'_',$weight))/@stddev"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$msstddev"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</xsl:when>
					<xsl:when test="$type='LM'">
						<xsl:attribute name="stddev">
							<xsl:choose>
								<xsl:when test="key('DATUM',concat(@id,'_',$i))">
									<xsl:value-of select="key('DATUM',concat(@id,'_',$i))/@stddev"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$lmstddev"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="document($fwdfile,/)/fwd:fwdsim/fwd:measurements/fwd:mgroup[@id=current()/@id]/fwd:time[@value=$time]/fwd:value[$i]"/>					
			</datum>
			<xsl:call-template name="spec">
				<xsl:with-param name="i" select="1+$i"/>
				<xsl:with-param name="last" select="$last"/>
				<xsl:with-param name="time" select="$time"/>
				<xsl:with-param name="spec" select="$spec"/>
				<xsl:with-param name="type" select="$type"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>	
	
  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>


</xsl:stylesheet>
