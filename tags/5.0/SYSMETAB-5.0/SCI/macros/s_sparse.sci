function a=s_sparse(ijv,n,m)
  if typeof(ijv)=='string'
    ijv=fscanfMat(ijv)
  end
  a=sparse(ijv(:,1:2),ijv(:,3),[n m])
endfunction
