function [d_min,d_max] = distToConstraints(A,b,x,r)
    d_min = -%inf;
    d_max = %inf;
    for i=1:size(A,1)
        den=A(i,:)*r;
        if abs(den)>1e-6
            rho=(b(i)-A(i,:)*x)/den;
            if den>0
                d_max=min(d_max,rho);
            else
                d_min=max(d_min,rho);
            end
        end
    end
endfunction
