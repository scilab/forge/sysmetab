function [residual,X,y,h,omega,Sypm2_e_label]=solveNonStat(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight)                            
    //
    // Solve the state equation cascade
    //
    h=list();
    X=repmat(Xinp,1,length(t));
    for i=1:length(cum)
        [X(cum(i),:),h(i)]=solveNonStatWeight(v,m,X,cum(i),M(i),b(i),t,pool_ind_weight(i));
    end
    //
    // Compute the simulated measurements
    //
    y_unscaled=Cx*X(:,ind_meas);
    //
    // Compute optimal scaling factors (simple least squares problem)
    //
    omega=ones(nb_group,1)
    for i=auto_scales_ind // for groups with scale="auto"
      ind=meas_ind_group(i);
      yui=y_unscaled(ind,:)./Sy(ind,:);
      ymi=ymeas(ind,:)./Sy(ind,:);
      omega(i)=sum(yui.*ymi)/norm(yui,'fro')^2;
    end
    y=spdiag(omega(omega_ind_meas))*y_unscaled;
    //
    // Compute the residual
    //
    e_label=y-ymeas;
    Sypm2_e_label=Sypm2.*e_label;
    residual=Sypm2_e_label.*e_label;
    //
endfunction
//
function [Xn,MnImp]=solveNonStatWeight(v,m,X,cum,Mn,bn,t,pool_ind_weightn)
  //
  // Weight n cumomers
  //
  Mn_sp=sparse(Mn.ij,Mn.t*v(Mn.m1),Mn.size);
  bmat_sp=bn.t*spdiag(v(bn.m3))*(X(bn.m1,:).*X(bn.m2,:));
  //
  // Implicit Euler scheme
  //
  stepsize=t(2)-t(1)
  n=length(t)
  MnImp=sparse(spdiag(m(pool_ind_weightn))-stepsize*Mn_sp);
  MnImp_handle=umf_lufact(MnImp);
  //
  bmat_sp=stepsize*bmat_sp;
  //
  Xn=zeros(length(cum),n);
  //
  for k=1:(n-1)
    Xn(:,k+1)=umf_lusolve(MnImp_handle,m(pool_ind_weightn).*Xn(:,k)+spset(bn.value,bmat_sp(:,k+1)));  
  end
  //
  umf_ludel(MnImp_handle)
  //
endfunction
//
function [residual,gradv,gradm,X,y]=solveNonStatAndGradAdjoint(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight,dP_dpool,dP_dpool_t)
    //
    [residual,X,y,h,omega,Sypm2_e_label]=solveNonStat(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight);
    //
    // Compute the state derivatives
    //
    db_dxt_nz=adjointNonStatDerivatives(v,X,dgdvt,db_dxt)
    //
    Ct_Omega_Sypm2_e_label=Cxt*spdiag(omega(omega_ind_meas))*Sypm2_e_label;
    //
    bmeas=zeros(X);
    bmeas(:,ind_meas)=Ct_Omega_Sypm2_e_label;
    //
    p=zeros(X);
    ncum=length(cum);    
    p(cum(ncum),:)=solveNonStatWeightAdjoint(p,bmeas,cum(ncum),h(ncum),t,m,pool_ind_weight(ncum));    
    for i=ncum-1:-1:1
        p(cum(i),:)=solveNonStatWeightAdjoint(p,bmeas,cum(i),h(i),t,m,pool_ind_weight(i),db_dxt(i).value,db_dxt_nz(i));        
    end
    //
    // Compute the gradient (adjoint method)
    //
    stepsize=t(2)-t(1)
    n=length(t)    
    gradv=zeros(length(v),1);
    for k=1:n-1        
        gradv=gradv+spset(dgdvt.value,dgdvt.t*(X(dgdvt.m1,k+1).*X(dgdvt.m2,k+1)))*p(:,k);
    end
    gradv=2*stepsize*gradv;
    //
    dP_dpool_t_nz=dP_dpool_t.t*(X(dP_dpool_t.m1,2:$)-X(dP_dpool_t.m1,1:$-1));
    gradm=zeros(nb_pool,1);
    for k=1:n-1        
        gradm=gradm+spset(dP_dpool_t.value,dP_dpool_t_nz(:,k))*p(:,k);
    end
    gradm=-2*gradm;      
    //
endfunction
//
function pn=solveNonStatWeightAdjoint(p,bmeas,cum,Mimp,t,m,pool_ind_weightn,db_dx_t,db_dx_t_mat)
  //
  // Solve weight n cumomer adjoint state
  //
  Mimp_adj_handle=umf_lufact(Mimp');
  stepsize=t(2)-t(1);
  n=length(t)
  pn=zeros(length(cum),n);
  //
  // Adjoint implicit Euler scheme
  //
  if exists("db_dx_t","local")
    for k=n:-1:2    
      pn(:,k-1)=umf_lusolve(Mimp_adj_handle,m(pool_ind_weightn).*pn(:,k)+bmeas(cum,k)+stepsize*(spset(db_dx_t,db_dx_t_mat(:,k))*p(:,k-1)));      
    end
  else
    for k=n:-1:2    
        pn(:,k-1)=umf_lusolve(Mimp_adj_handle,m(pool_ind_weightn).*pn(:,k)+bmeas(cum,k));      
    end
  end
  umf_ludel(Mimp_adj_handle)
endfunction
//
function [residual,gradv,gradm,X,y]=solveNonStatAndGradDirect(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight,dP_dpool,dP_dpool_t)
    //
    [residual,X,y,h,omega,Sypm2_e_label]=solveNonStat(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight);
    //
    // Compute the state derivatives
    //    
    [dgdv_nz,db_dx_nz]=forwardNonStatDerivatives(v,X,dgdv,db_dx);
    //
    stepsize=t(2)-t(1);
    h_handle=list();
    for i=1:length(cum)
        h_handle(i)=umf_lufact(h(i));
    end

    ind_meas0=[1;ind_meas];
    gradv=zeros(size(v,1),1);
    gradm=zeros(nb_pool,1);

    Ct_Omega_Sypm2_e_label=Cxt*spdiag(omega(omega_ind_meas))*Sypm2_e_label;
    dP_dpool_nz=dP_dpool.t*(X(dP_dpool.m1,2:$)-X(dP_dpool.m1,1:$-1));

    dX_dp=zeros(size(X,1),size(v,1)+nb_pool);

    for ind=1:length(ind_meas)
        for k=ind_meas0(ind):ind_meas0(ind+1)-1
            dpX1=dX_dp(cum(1),:);
            dgdv_kp1=spset(dgdv.value,dgdv_nz(:,k+1));
            dP_dpool_k=spset(dP_dpool.value,dP_dpool_nz(:,k)); 
            //           
            bcum1=[stepsize*dgdv_kp1(cum(1),:) -dP_dpool_k(cum(1),:)];
            dpX1=umf_lusolve(h_handle(1),spdiag(m(pool_ind_weight(1)))*dpX1+bcum1);
            //
            dX_dp(cum(1),:)=dpX1;
            for i=2:length(cum)
                dpXi=dX_dp(cum(i),:);
                //
                db_dx_mat=db_dx_nz(i);            
                bcumi=[stepsize*dgdv_kp1(cum(i),:) -dP_dpool_k(cum(i),:)];
                //
                dpXi=umf_lusolve(h_handle(i),spdiag(m(pool_ind_weight(i)))*dpXi+stepsize*(spset(db_dx(i).value,db_dx_mat(:,k+1))*dX_dp)+bcumi);
                //
                dX_dp(cum(i),:)=dpXi;
            end
        end               
        gradv=gradv+dX_dp(:,1:size(v,1))'*Ct_Omega_Sypm2_e_label(:,ind);
        gradm=gradm+dX_dp(:,1+size(v,1):$)'*Ct_Omega_Sypm2_e_label(:,ind);
    end
    for i=1:length(cum)
        umf_ludel(h_handle(i))
    end
    gradv=2*gradv;
    gradm=2*gradm;
//
endfunction
//
function [dgdv_nz,db_dx_nz]=forwardNonStatDerivatives(v,X,dgdv,db_dx)
    //
    // Derivatives needed for the forward computation of gradient
    //
    dgdv_nz=dgdv.t*(X(dgdv.m1,:).*X(dgdv.m2,:));
    db_dx_nz=list();
    for i=2:length(db_dx)
        db_dx_nz(i)=db_dx(i).t*spdiag(v(db_dx(i).m2))*X(db_dx(i).m1,:);
    end
    //
endfunction

function [db_dxt_nz]=adjointNonStatDerivatives(v,X,dgdvt,db_dxt)
    //
    // Derivatives needed for the adjoint computation of gradient
    //
    db_dxt_nz=list();
    for i=1:length(db_dxt)
        db_dxt_nz(i)=db_dxt(i).t*spdiag(v(db_dxt(i).m2))*X(db_dxt(i).m1,:);
    end
    //
endfunction
//
function [residual,X,y,dy_dv,dy_dm,omega]=solveNonStatAndDerivative(v,m,Xinp,M,b,cum,dP_dpool)
    //
    [residual,X,y,h,omega,Sypm2_e_label]=solveNonStat(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight);
    //
    // Compute the state derivatives
    //    
    [dgdv_nz,db_dx_nz]=forwardNonStatDerivatives(v,X,dgdv,db_dx);
    //
    stepsize=t(2)-t(1);
    h_handle=list();
    for i=1:length(cum)
        h_handle(i)=umf_lufact(h(i));
    end

    ind_meas0=[1;ind_meas];
    nmeas=size(Cx,1);    

    dP_dpool_nz=dP_dpool.t*(X(dP_dpool.m1,2:$)-X(dP_dpool.m1,1:$-1));
    
    dy_dv=zeros(size(Cx,1)*(length(ind_meas)),size(v,1));
    dy_dm=zeros(size(Cx,1)*(length(ind_meas)),nb_pool);        

    dX_dp=zeros(size(X,1),size(v,1)+nb_pool);

    for ind=1:length(ind_meas)
        for k=ind_meas0(ind):ind_meas0(ind+1)-1
            dpX1=dX_dp(cum(1),:);
            dgdv_kp1=spset(dgdv.value,dgdv_nz(:,k+1));
            dP_dpool_k=spset(dP_dpool.value,dP_dpool_nz(:,k)); 
            //           
            bcum1=[stepsize*dgdv_kp1(cum(1),:) -dP_dpool_k(cum(1),:)];
            dpX1=umf_lusolve(h_handle(1),spdiag(m(pool_ind_weight(1)))*dpX1+bcum1);
            //
            dX_dp(cum(1),:)=dpX1;
            for i=2:length(cum)
                dpXi=dX_dp(cum(i),:);
                //
                db_dx_mat=db_dx_nz(i);            
                bcumi=[stepsize*dgdv_kp1(cum(i),:) -dP_dpool_k(cum(i),:)];
                //
                dpXi=umf_lusolve(h_handle(i),spdiag(m(pool_ind_weight(i)))*dpXi+stepsize*(spset(db_dx(i).value,db_dx_mat(:,k+1))*dX_dp)+bcumi);
                //
                dX_dp(cum(i),:)=dpXi;
            end

        end                        
        dy_dv((ind-1)*nmeas+1:ind*nmeas,:)=spdiag(omega(omega_ind_meas))*Cx*dX_dp(:,1:size(v,1));
        dy_dm((ind-1)*nmeas+1:ind*nmeas,:)=spdiag(omega(omega_ind_meas))*Cx*dX_dp(:,size(v,1)+1:$);
    end
    for i=1:length(cum)
        umf_ludel(h_handle(i))
    end    
//
endfunction
