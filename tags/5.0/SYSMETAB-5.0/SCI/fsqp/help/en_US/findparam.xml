<?xml version="1.0" encoding="ISO-8859-1"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="findparam"><info><pubdate>June 1997  </pubdate></info><refnamediv><refname>findparam</refname><refpurpose> utility function for fsqp solver  </refpurpose></refnamediv>
  
  
  
  
  
  <refsynopsisdiv><title>Calling Sequence</title><synopsis>[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0)</synopsis></refsynopsisdiv>
  <refsection><title>Parameters</title><variablelist><varlistentry><term>list_objs  </term><listitem><para>list made of two lists.
  </para></listitem></varlistentry><varlistentry><term>list_cntr  </term><listitem><para>list made of six lists.
  </para></listitem></varlistentry><varlistentry><term>x0  </term><listitem><para>real column vector (e.g. initial guess)
  </para></listitem></varlistentry><varlistentry><term>nf, ..., ncsrn  </term><listitem><para>integers (see fsqp-doc).
  </para></listitem></varlistentry><varlistentry><term>mesh_pts  </term><listitem><para>real vector.
  </para></listitem></varlistentry></variablelist></refsection>
  <refsection><title>Description</title><para>
    Given a list of objective functions and a list of constraints
    functions, and a vector x0, findparam returns useful fsqp
    parameters.
    The list of objectives should be: (use empty list when necessary: list() )
  </para><programlisting><![CDATA[
list_obj=list(...
               list(f_1,...,f_nf0),...     //regular objectives
        list(F_1,...,F_nfsr)        //SR objectives
              )
   ]]></programlisting><para>
    The f_i's are functions: y=f_i(x) should return the value of the ith
    regular objective as a function of x. y can be a column vector if
    several regular objectives are stacked together.
    y=F_i(x) is the ith sequentially related objective. y is a column
    vector which contains the ith set of SR-objectives (mesh_pts(i) is
    set to size(F_i(x),1) by findparam).
  </para><para>
    The list of constraints functions should be as follows:
  </para><programlisting><![CDATA[
list_cntr=list(...
 list(g_1,...,g_ng0),...     //regular nonlinear inequality
        list(G_1,...,G_ncsrn),...   //SR      nonlinear inequality
        list(c_1,...,c_nc0),...     //regular linear    inequality
        list(C_1,...,C_ncsrl),...   //SR      linear    inequality
        list(h_1,...,h_nh0),...     //nonlinear         equality
        list(A_1,...,A_na0)         //linear            equality
             )
   ]]></programlisting><para>
    Functions g_i's, c_i's, h_1 can return column vectors y (e.g. y=g_1(x)) 
    if several constraints are stacked together.
    Functions G_i's, C_i's, A_i's should return in a column vector the
    set of appropriate SR constraints.
    Examples are given at the end of examplei.sce files. See
    listutils.sci: generic functions obj, cntr, grob, grcn are constructed
    from the lists list_obj, list_cntr, and similar lists list_grobj
    and list_cntr which contain the gradients of objectives and constraints
    (matrices whith nparam=dim(x) columns).
  </para></refsection>
  <refsection><title>See Also</title><simplelist type="inline">
    <member> <link linkend="srfsqp">srfsqp</link> </member>     <member> <link linkend="x_is_new">x_is_new</link> </member>     <member> <link linkend="set_x_is_new">set_x_is_new</link> </member>
  </simplelist></refsection>
</refentry>
