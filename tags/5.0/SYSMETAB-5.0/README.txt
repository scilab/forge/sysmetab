Sysmetab Version 5.0 (Fri Jan 15 14:58:38 CET 2016)

Sysmetab is an integrated software solving 13C metabolic flux identification problem, with automated Scilab code generation using XML/XSL technology.

Please see the project page at 

http://forge.scilab.org/index.php/p/sysmetab/

for more information.
