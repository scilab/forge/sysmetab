<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Fri Mar 14 20:00:00 CET 2014
    Projet  :   PIVERT/Metalippro-PL1
-->

<!--
    fml2ftbl
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func">
  
  <xsl:key name="datum" match="f:datum" use="@id"/>
  
  <xsl:key name="constraints_net_fluxes" match="f:net/m:math/m:apply[(m:eq) and not(m:apply)]" use="m:ci"/>

  <xsl:key name="constraints_xch_fluxes" match="f:xch/m:math/m:apply[(m:eq) and not(m:apply)]" use="m:ci"/>
  
  <xsl:key name="netfreefluxes" match="f:fluxvalue[@type='net']" use="@flux"/>

  <xsl:key name="xchfreefluxes" match="f:fluxvalue[@type='xch']" use="@flux"/>

  <xsl:output method="text" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:variable name="tab" select="'&#09;'"/>
  <xsl:variable name="rc" select="'&#10;'"/>
  
  <xsl:template match="f:fluxml">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="f:info">
    <xsl:value-of select="'PROJECT'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VERSION'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'FORMAT'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DATE'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'COMMENT'"/>
      <xsl:value-of select="$rc"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="f:name"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="f:version"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="f:format"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="f:date"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="f:comment"/>
        <xsl:value-of select="$rc"/>
        <xsl:value-of select="$rc"/>
  </xsl:template>
  
  <xsl:template match="f:reactionnetwork">
    <xsl:value-of select="'NETWORK'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'FLUX_NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'EDUCT_1'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'EDUCT_2'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'PRODUCT_1'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'PRODUCT_2'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$rc"/>
      <xsl:value-of select="$rc"/>
        <xsl:apply-templates select="f:reaction" mode="network"/>
        <xsl:apply-templates select="f:configuration"/>
  </xsl:template>
  
  <xsl:template match="f:reaction" mode="network">
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="@id"/>
    <xsl:variable name="nbrreduct" select="count(./f:reduct)"/>
    <xsl:for-each select="f:reduct">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="@id"/>
    </xsl:for-each>
    <xsl:if test="$nbrreduct='1'">
      <xsl:value-of select="$tab"/>
    </xsl:if>
    <xsl:choose>
      <xsl:when test="f:rproduct">
        <xsl:for-each select="f:rproduct">
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@id"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="concat(f:reduct/@id,'_Aux')"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:for-each select="f:reduct">
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="concat('#',@cfg)"/>
      </xsl:for-each>
      <xsl:if test="$nbrreduct='1'">
        <xsl:value-of select="$tab"/>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="f:rproduct">
          <xsl:for-each select="f:rproduct">
            <xsl:value-of select="$tab"/>
            <xsl:value-of select="concat('#',@cfg)"/>
          </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="concat('#',f:reduct/@cfg)"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$rc"/>
      <xsl:value-of select="$rc"/>
  </xsl:template>
  
  <xsl:template match="f:configuration">
    <xsl:value-of select="'FLUXES'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'NET'"/>
      <xsl:value-of select="$rc"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'NAME'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'FCD'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'VALUE(F/C)'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'ED_WEIGHT'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'LOW(F)'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'INC(F)'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'UP(F)'"/>
        <xsl:value-of select="$rc"/>
        <xsl:value-of select="$rc"/>
          <xsl:apply-templates select="f:simulation" mode="net"/>
          <xsl:apply-templates select="../f:constraints" mode="eq_net"/>
          <xsl:apply-templates select="../f:reactionnetwork" mode="dependent_net"/>
          <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'XCH'"/>
      <xsl:value-of select="$rc"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'NAME'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'FCD'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'VALUE(F/C)'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'ED_WEIGHT'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'LOW(F)'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'INC(F)'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'UP(F)'"/>
        <xsl:value-of select="$rc"/>
        <xsl:value-of select="$rc"/>
          <xsl:apply-templates select="f:simulation" mode="xch"/>
          <xsl:apply-templates select="../f:constraints" mode="eq_xch"/>
          <xsl:apply-templates select="../f:reactionnetwork" mode="dependent_xch"/>
          <xsl:value-of select="$rc"/>
    <xsl:value-of select="'EQUALITIES'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'NET'"/>
      <xsl:value-of select="$rc"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'VALUE'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'FORMULA'"/>
        <xsl:value-of select="$rc"/>
          <xsl:apply-templates select="../f:constraints/f:net/m:math/m:apply[(m:eq) and (m:apply)]" mode="equality_net"/>
          <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'XCH'"/>
      <xsl:value-of select="$rc"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'VALUE'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'FORMULA'"/>
        <xsl:value-of select="$rc"/>
          <xsl:apply-templates select="../f:constraints/f:xch/m:math/m:apply[(m:eq) and (m:apply)]" mode="equality_xch"/>
          <xsl:value-of select="$rc"/>
    <xsl:value-of select="'INEQUALITIES'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'NET'"/>
      <xsl:value-of select="$rc"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'VALUE'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'COMP'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'FORMULA'"/>
        <xsl:value-of select="$rc"/>
          <xsl:apply-templates select="../f:constraints" mode="leq_net"/>
          <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'XCH'"/>
      <xsl:value-of select="$rc"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'VALUE'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'COMP'"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'FORMULA'"/>
        <xsl:value-of select="$rc"/>
          <xsl:apply-templates select="../f:constraints" mode="leq_xch"/>
          <xsl:value-of select="$rc"/>
    <xsl:value-of select="'FLUX_MEASUREMENTS'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'FLUX_NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DEVIATION'"/>
      <xsl:value-of select="$rc"/>
      <xsl:value-of select="$rc"/>
        <xsl:apply-templates select="f:measurement" mode="FM"/>
        <xsl:value-of select="$rc"/>
    <xsl:value-of select="'LABEL_INPUT'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'META_NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'ISOTOPOMER'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE'"/>
      <xsl:value-of select="$rc"/>
      <xsl:value-of select="$rc"/>
        <xsl:apply-templates select="f:input"/>
        <xsl:value-of select="$rc"/>
    <xsl:value-of select="'LABEL_MEASUREMENTS'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'META_NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'CUM_GROUP'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DEVIATION'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'CUM_CONSTRAINTS'"/>
      <xsl:value-of select="$rc"/>
        <xsl:apply-templates select="f:measurement" mode="LM"/>
        <xsl:value-of select="$rc"/>
    <xsl:value-of select="'PEAK_MEASUREMENTS'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'META_NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'PEAK_NO'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE_S'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE_D-'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE_D+'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE_DD'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE_T'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DEVIATION_S'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DEVIATION_D-'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DEVIATION_D+'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DEVIATION_DD/T'"/>
      <xsl:value-of select="$rc"/>
        <xsl:apply-templates select="f:measurement/f:model/f:peakmeasurements"/>
        <xsl:value-of select="$rc"/>
    <xsl:value-of select="'MASS_SPECTROMETRY'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'META_NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'FRAGMENT'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'WEIGHT'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'VALUE'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'DEVIATION'"/>
      <xsl:value-of select="$rc"/>
      <xsl:value-of select="$rc"/>
        <xsl:apply-templates select="f:measurement" mode="MS"/>
        <xsl:value-of select="$rc"/>
    <xsl:value-of select="'OPTIONS'"/>
    <xsl:value-of select="$rc"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'OPT_NAME'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'OPT_VALUE'"/>
      <xsl:value-of select="$rc"/>
  </xsl:template>

  <xsl:template match="f:simulation" mode="net">
    <xsl:for-each select="f:variables/f:fluxvalue[@type='net']">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="@flux"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'F'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="text()"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:simulation" mode="xch">
    <xsl:for-each select="f:variables/f:fluxvalue[@type='xch']">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="@flux"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'F'"/>
      <xsl:value-of select="$tab"/>
      <xsl:choose>
        <xsl:when test="text()='0'">
          <xsl:value-of select="0"/>
        </xsl:when>
        <xsl:when test="text()='1.0e37'">
          <xsl:value-of select="'1'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="(text()) div (1 + text())"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="m:apply[(m:eq) and (m:apply)]" mode="equality_net">
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="m:cn"/>
    <xsl:value-of select="$tab"/>
    <xsl:call-template name="iteration_equation">
      <xsl:with-param name="apply" select="m:apply"/>
    </xsl:call-template>
    <xsl:value-of select="$rc"/>
  </xsl:template>

  <xsl:template match="m:apply[(m:eq) and (m:apply)]" mode="equality_xch">
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="m:cn"/>
    <xsl:value-of select="$tab"/>
    <xsl:call-template name="iteration_equation">
      <xsl:with-param name="apply" select="m:apply"/>
    </xsl:call-template>
    <xsl:value-of select="$rc"/>
  </xsl:template>

  <xsl:template name="iteration_equation">
    <xsl:param name="apply"/>
    <xsl:choose>
      <xsl:when test="$apply[(m:plus) and (count(m:apply)='1')]">
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply"/>
        </xsl:call-template>
        <xsl:text>+</xsl:text>
        <xsl:value-of select="$apply/m:ci"/>
      </xsl:when>
      <xsl:when test="$apply[(m:minus) and (count(m:apply)='1')]">
        <xsl:text>(</xsl:text>
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply"/>
        </xsl:call-template>
        <xsl:text>)</xsl:text>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$apply/m:ci"/>
      </xsl:when>
      <xsl:when test="$apply[(m:plus) and (count(m:apply)='2')]">
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply[1]"/>
        </xsl:call-template>
        <xsl:text>+</xsl:text>
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply[2]"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$apply[(m:minus) and (count(m:apply)='2')]">
        <xsl:text>(</xsl:text>
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply[1]"/>
        </xsl:call-template>
        <xsl:text>)</xsl:text>
        <xsl:text>-</xsl:text>
        <xsl:text>(</xsl:text>
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply[2]"/>
        </xsl:call-template>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:when test="$apply[(m:times) and (count(m:apply)='1')]">
        <xsl:choose>
          <xsl:when test="$apply[m:cn]">
            <xsl:value-of select="$apply/m:cn"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$apply/m:ci"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text>*</xsl:text>
        <xsl:text>(</xsl:text>
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply"/>
        </xsl:call-template>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:when test="$apply[(m:times) and (count(m:apply)='2')]">
        <xsl:text>(</xsl:text>
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply[1]"/>
        </xsl:call-template>
        <xsl:text>)</xsl:text>
        <xsl:text>*</xsl:text>
        <xsl:text>(</xsl:text>
        <xsl:call-template name="iteration_equation">
          <xsl:with-param name="apply" select="$apply/m:apply[2]"/>
        </xsl:call-template>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:when test="$apply[not(m:apply) and (not(m:times))]">
        <xsl:value-of select="$apply/m:ci[1]"/>
        <xsl:choose>
          <xsl:when test="$apply[m:plus]">
            <xsl:text>+</xsl:text>
          </xsl:when>
          <xsl:when test="$apply[m:minus]">
            <xsl:text>-</xsl:text>
          </xsl:when>
        </xsl:choose>
        <xsl:value-of select="$apply/m:ci[2]"/>
      </xsl:when>
      <xsl:when test="$apply[not(m:apply) and (m:times) and (m:cn) and (count(m:ci)='1')]">
        <xsl:value-of select="$apply/m:cn"/>
        <xsl:text>*</xsl:text>
        <xsl:value-of select="$apply/m:ci"/>
      </xsl:when>
      <xsl:when test="$apply[not(m:apply) and (m:times) and (count(m:ci)='2')]">
        <xsl:value-of select="$apply/m:ci[1]"/>
        <xsl:text>*</xsl:text>
        <xsl:value-of select="$apply/m:ci[2]"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="f:constraints" mode="leq_net">
    <xsl:for-each select="f:net/m:math/m:apply[(m:let) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
    <xsl:for-each select="f:net/m:math/m:apply[(m:leq) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&lt;=</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
    <xsl:for-each select="f:net/m:math/m:apply[(m:gt) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
    <xsl:for-each select="f:net/m:math/m:apply[(m:geq) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&gt;=</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:constraints" mode="leq_xch">
    <xsl:for-each select="f:xch/m:math/m:apply[(m:let) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
    <xsl:for-each select="f:xch/m:math/m:apply[(m:leq) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&lt;=</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
    <xsl:for-each select="f:xch/m:math/m:apply[(m:gt) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
    <xsl:for-each select="f:xch/m:math/m:apply[(m:geq) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:text disable-output-escaping="yes">&gt;=</xsl:text>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:constraints" mode="eq_net">
    <xsl:for-each select="f:net/m:math/m:apply[(m:eq) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'C'"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:cn"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:constraints" mode="eq_xch">
    <xsl:for-each select="f:xch/m:math/m:apply[(m:eq) and not(m:apply)]">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="m:ci"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="'C'"/>
      <xsl:value-of select="$tab"/>
      <xsl:choose>
        <xsl:when test="m:cn='0'">
          <xsl:value-of select="m:cn"/>
        </xsl:when>
        <xsl:when test="m:cn='1.0e37'">
          <xsl:value-of select="'1'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="(m:cn) div (1 + m:cn)"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:reactionnetwork" mode="dependent_net">
    <xsl:for-each select="f:reaction">
      <xsl:if test="not(key('constraints_net_fluxes',@id)) and not(key('netfreefluxes',@id))">
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="@id"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'D'"/>
        <xsl:value-of select="$rc"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:reactionnetwork" mode="dependent_xch">
    <xsl:for-each select="f:reaction">
      <xsl:if test="not(key('constraints_xch_fluxes',@id)) and not(key('xchfreefluxes',@id))">
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="@id"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="'D'"/>
        <xsl:value-of select="$rc"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:measurement" mode="FM">
    <xsl:for-each select="f:model/f:fluxmeasurement/f:netflux">
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="f:textual"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="key('datum',@id)/text()"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="key('datum',@id)/@stddev"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="f:input">
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="@pool"/>
    <xsl:for-each select="f:label">
      <xsl:choose>
        <xsl:when test="position()='1'">
          <xsl:value-of select="$tab"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$tab"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="concat('#',@cfg)"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="text()"/>
      <xsl:value-of select="$tab"/>
      <xsl:value-of select="$rc"/>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="f:measurement" mode="LM">
    <xsl:apply-templates select="f:model" mode="LM"/>
  </xsl:template>

  <xsl:template match="f:measurement" mode="MS">
    <xsl:apply-templates select="f:model" mode="MS"/>
  </xsl:template>

  <xsl:template match="f:peakmeasurements">
    <xsl:for-each select="f:peak">
      <xsl:variable name="id" select="substring-before(@id,'_')"/>
      <xsl:variable name="peak_no" select="substring-before(translate(substring-after(@id,'_'),'&#10;&#13;',''),'_')"/>
      <xsl:choose>
        <xsl:when test="@pos='1'">
          <xsl:value-of select="$rc"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$id"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$peak_no"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_s"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_dm"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_dp"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_dd"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_t"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_s"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_dm"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_dp"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_ddt"/>
          <xsl:value-of select="$rc"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$peak_no"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_s"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_dm"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_dp"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_dd"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@value_t"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_s"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_dm"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_dp"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="@deviation_ddt"/>
          <xsl:value-of select="$rc"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="f:model" mode="LM">
    <xsl:for-each select="f:labelingmeasurement/f:group[substring-before(@id,'_')='lmcg']">
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="cum_group" select="substring-before(translate(substring-after(@id,'_'),'&#10;&#13;',''),'_')"/>
      <xsl:choose>
        <xsl:when test="$cum_group='1'">
          <xsl:value-of select="$rc"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="translate(substring-before(f:textual,'#'),'&#10;&#13;','')"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$cum_group"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="key('datum',$id)/text()"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="key('datum',$id)/@stddev"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="concat('#',translate(substring-after(f:textual,'#'),'&#10;&#13;',''))"/>
          <xsl:value-of select="$rc"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="$cum_group"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="key('datum',$id)/text()"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="key('datum',$id)/@stddev"/>
          <xsl:value-of select="$tab"/>
          <xsl:value-of select="concat('#',translate(substring-after(f:textual,'#'),'&#10;&#13;',''))"/>
          <xsl:value-of select="$rc"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="iteration_lm">
    <xsl:value-of select="key('datum',$id)[@weight=$weight]/text()"/>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="key('datum',$id)[@weight=$weight]/@stddev"/>
    <xsl:value-of select="$rc"/>
  </xsl:template>

  <xsl:template match="f:model" mode="MS">
      <xsl:for-each select="f:labelingmeasurement/f:group[substring-before(@id,'_')='ms']">
        <xsl:variable name="id" select="@id"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="translate(substring-before(f:textual,'['),'&#10;&#13;','')"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="translate(substring-before(translate(substring-after(f:textual,'['),'&#10;&#13;',''),']'),'&#10;&#13;','')"/>
        <xsl:call-template name="iteration_ms">
          <xsl:with-param name="weight" select="substring-before(translate(substring-after(f:textual,'#M'),'&#10;&#13;',''),',')"/>
          <xsl:with-param name="weights" select="substring-after(translate(substring-after(f:textual,'#M'),'&#10;&#13;',''),',')"/>
          <xsl:with-param name="position" select="'1'"/>
          <xsl:with-param name="id" select="$id"/>
        </xsl:call-template>
        <xsl:value-of select="$rc"/>
      </xsl:for-each>
  </xsl:template>

  <xsl:template name="iteration_ms">
    <xsl:param name="weight"/>
    <xsl:param name="weights"/>
    <xsl:param name="position"/>
    <xsl:param name="id"/>
    <xsl:choose>
      <xsl:when test="$position='1'">
        <xsl:value-of select="$tab"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="$tab"/>          
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$weight"/>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="key('datum',$id)[@weight=$weight]/text()"/>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="key('datum',$id)[@weight=$weight]/@stddev"/>
    <xsl:value-of select="$rc"/>
<!--    <xsl:message>HHH=<xsl:value-of select="$weights"/>pos=<xsl:value-of select="$position"/>weight=<xsl:value-of select="$weight"/></xsl:message>  -->
    <xsl:choose>
      <xsl:when test="contains($weights,',')">
        <xsl:call-template name="iteration_ms">
          <xsl:with-param name="weight" select="substring-before($weights,',')"/>
          <xsl:with-param name="weights" select="substring-after($weights,',')"/>
          <xsl:with-param name="position" select="'2'"/>
          <xsl:with-param name="id" select="$id"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="count(str:split($weights,''))='1'">
        <xsl:call-template name="iteration_ms">
          <xsl:with-param name="weight" select="$weights"/>
          <xsl:with-param name="weights" select="''"/>
          <xsl:with-param name="position" select="'3'"/>
          <xsl:with-param name="id" select="$id"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>