<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet 
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
  
    Auxilliary stylesheet which transforms typical NMTOKEN data
    into element sets, adds (later) useful "position" attributes,
    reformulates constraints in standard form (=0 and <=0) and adds
    implicit constraints
    
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:str="http://exslt.org/strings"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt str">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
  
  <xsl:strip-space elements="*"/>
  
  <xsl:param name="output"/>
  
  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>  
    
  <!-- find all pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>
  
  <!-- find all f:rproduct from forward f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

  <!-- find all f:reduct from forward f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:reduct from forward f:reaction -->
  <xsl:key name="UNIDIR" match="f:reaction[@bidirectional='false']" use="@id"/>

  <!-- find all label input from f:configuration -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>
  
  <!-- find all reactions with zeros XCH flux -->
  <xsl:key name="NONREV" match="f:constraints/f:xch/m:math/m:apply[m:eq and m:ci and (m:cn='0')]" use="m:ci"/>
  
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:metabolitepools">      
    <metabolitepools xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="f:pool[key('REDUCT',@id)]"/>
    </metabolitepools>
  </xsl:template>

  <xsl:template match="f:pool">      
    <pool xmlns="http://www.13cflux.net/fluxml">
  	  <xsl:copy-of select="@*"/> <!-- preserve all original attributes -->
      <xsl:attribute name="number">
  	    <xsl:value-of select="position()"/> 
  	  </xsl:attribute>
  	  <!-- add an attribute "type" for each pool -->
  	  <xsl:choose>
        <xsl:when test="key('INPUT',@id)">
          <xsl:attribute name="type">input</xsl:attribute>
        </xsl:when>
        <xsl:when test="key('RPRODUCT',@id) or key('REDUCT',@id)[not(../@bidirectional)]">
          <xsl:attribute name="type">intermediate</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="type">input</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
    </pool>
  </xsl:template>
  
  <!-- Add position number to each reaction -->

  <xsl:template match="f:reaction">
    <reaction position="{count(preceding-sibling::f:reaction)+1}" xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="f:reduct"/>
      <xsl:apply-templates select="f:rproduct"/>
    </reaction>
  </xsl:template>  

  <xsl:template match="f:rproduct"> <!-- delete rproduct elements liking to an output metabolite (also deleted by a template above)-->
    <xsl:if test="key('REDUCT',@id)">
      <xsl:copy-of select="."/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="f:labelingmeasurement">
    <xsl:variable name="times">      
      <xsl:for-each select="f:group/@times">
        <xsl:copy-of select="str:split(.)"/>          
      </xsl:for-each>              
    </xsl:variable>
    <labelingmeasurement xmlns="http://www.13cflux.net/fluxml">
      <times xmlns="http://www.utc.fr/sysmetab">    
        <!-- Determine the union of all measurement times -->
        <xsl:for-each select="exslt:node-set($times)/*[not(.=preceding-sibling::*)]">
          <xsl:sort select="." data-type="number"/>
          <time>
            <xsl:value-of select="."/>          
          </time>
        </xsl:for-each>
      </times>
      <xsl:apply-templates/>      
    </labelingmeasurement>
  </xsl:template>

  <xsl:template match="f:group[f:textual[contains(.,'#M')]]">
    <group pos="{position()}" type="MS" xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="*"/>
      <xsl:for-each select="str:split(substring-after(.,'#M'),',')">
        <index xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="."/>
        </index>
      </xsl:for-each>
    </group>
  </xsl:template>

  <xsl:template match="f:group[f:textual[not(contains(.,'#M'))]]">
    <group pos="{position()}" type="LM" xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="*"/>
      <xsl:for-each select="str:split(.,';')">
        <index xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="position()"/>
        </index>
      </xsl:for-each>
    </group>
  </xsl:template>
  
  <!-- This template adds the implicit constraints on input and output fluxes -->
  <xsl:template match="f:constraints">
    <constraints xmlns="http://www.13cflux.net/fluxml">
      <net xmlns="http://www.13cflux.net/fluxml">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:apply-templates select="f:net/m:math/*"/>
          <xsl:for-each select="../f:reactionnetwork/f:reaction">
            <xsl:choose>
              <xsl:when test="@bidirectional='false'"> <!-- -->
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <leq/>
                  <xsl:variable name="minnr">
                    <token>0</token>
                    <token><xsl:value-of select="exslt:node-set($params)[@name='min_nr']"/></token>
                  </xsl:variable>
                  <cn>
                    <xsl:value-of select="math:max(exslt:node-set($minnr)/*)"/>
                  </cn>
                  <ci><xsl:value-of select="@id"/></ci>
                </apply>
              </xsl:when>
              <xsl:when test="key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct)">
                <!-- net fluxes from input metabolites or to output metabolites are necessarily positive -->
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <leq/>
                  <cn>
                    <xsl:value-of select="exslt:node-set($params)[@name='min_inout']"/>
                  </cn>
                  <ci><xsl:value-of select="@id"/></ci>
                </apply>
              </xsl:when>
              <xsl:when test="key('NONREV',@id)"> <!-- if XCH flux is constrained to 0 -->
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <leq/>
                  <cn>
                    <xsl:value-of select="exslt:node-set($params)[@name='min_nr']"/>
                  </cn>
                  <ci><xsl:value-of select="@id"/></ci>
                </apply>
              </xsl:when>
            </xsl:choose>
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <leq/>
              <ci><xsl:value-of select="@id"/></ci>
              <cn>
                <xsl:value-of select="exslt:node-set($params)[@name='max_net']"/>
              </cn>
            </apply>
          </xsl:for-each> 
        </math>
      </net>
      <xch xmlns="http://www.13cflux.net/fluxml">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:apply-templates select="f:xch/m:math/*"/>
          <xsl:for-each select="../f:reactionnetwork/f:reaction">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <xsl:choose>
                <xsl:when test="key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct) or @bidirectional='false'">
                  <!-- xch fluxes from input metabolites or to output metabolites are necessarily zero -->
                  <eq/>
                </xsl:when>
                <xsl:otherwise>
                  <!-- other xch fluxes are positive -->
                  <leq/>
                </xsl:otherwise>
              </xsl:choose>
              <cn>0</cn>
              <ci><xsl:value-of select="@id"/></ci>
            </apply>
            <xsl:choose>
              <xsl:when test="key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct)"/>
              <xsl:otherwise>
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <leq/>
                  <ci><xsl:value-of select="@id"/></ci>
                  <cn>
                    <xsl:value-of select="exslt:node-set($params)[@name='max_xch']"/>
                  </cn>
                </apply>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </math>
      </xch>
    </constraints>
  </xsl:template>

  <!-- transform geq to leq -->
  <xsl:template match="m:apply[m:geq and m:ci and m:cn]">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <leq/>
      <xsl:choose>
        <xsl:when test="name(*[2])='ci'">
          <cn><xsl:value-of select="m:cn"/></cn>
          <ci><xsl:value-of select="m:ci"/></ci>
        </xsl:when>
        <xsl:when test="name(*[2])='cn'">
          <ci><xsl:value-of select="m:ci"/></ci>
          <cn><xsl:value-of select="m:cn"/></cn>
        </xsl:when>
      </xsl:choose>
    </apply>
  </xsl:template>

  <!-- take to account a = b in net/xch -->
  <xsl:template match="m:apply[m:eq and count(m:ci)='2']">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <apply>
        <minus/>
        <ci><xsl:value-of select="m:ci[2]"/></ci>
        <ci><xsl:value-of select="m:ci[1]"/></ci>
      </apply>
      <cn>0</cn>
    </apply>
  </xsl:template>

  <!-- take to account a >= b in net/xch -->
  <xsl:template match="m:apply[m:geq and count(m:ci)='2']">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <leq/>
      <apply>
        <minus/>
        <ci><xsl:value-of select="m:ci[2]"/></ci>
        <ci><xsl:value-of select="m:ci[1]"/></ci>
      </apply>
      <cn>0</cn>
    </apply>
  </xsl:template>

  <!-- take to account a <= b in net/xch -->
  <xsl:template match="m:apply[m:leq and count(m:ci)='2']">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <leq/>
      <apply>
        <minus/>
        <ci><xsl:value-of select="m:ci[1]"/></ci>
        <ci><xsl:value-of select="m:ci[2]"/></ci>
      </apply>
      <cn>0</cn>
    </apply>
  </xsl:template>
  

</xsl:stylesheet>