<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   2013/2014
    Project :   PIVERT/Metalippro-PL1
-->
                                   
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:f="http://www.13cflux.net/fluxml"
  exclude-result-prefixes="xsl f">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <xsl:apply-templates select="f:reactionnetwork"/>
    <xsl:apply-templates select="f:configuration/f:measurement/f:model/f:labelingmeasurement"/>
  </xsl:template>
  
  <xsl:template match="f:reactionnetwork">
    <reactionnetwork xmlns="http://www.13cflux.net/fluxml">
      <metabolitepools>
        <xsl:for-each select="f:metabolitepools/f:pool">
          <xsl:sort select="@id" data-type="text"/>
          <pool atoms="{@atoms}" id="{@id}"/>
        </xsl:for-each>
      </metabolitepools>
      <xsl:for-each select="f:reaction">
        <xsl:sort select="@id" data-type="text"/>
        <reaction id="{@id}">
          <xsl:for-each select="f:reduct">
            <xsl:sort select="@id" data-type="text"/>
            <reduct cfg="{@cfg}" id="{@id}"/>
          </xsl:for-each>
          <xsl:for-each select="f:rproduct">
            <xsl:sort select="@id" data-type="text"/>
            <rproduct cfg="{@cfg}" id="{@id}"/>
          </xsl:for-each>
        </reaction>
      </xsl:for-each>
    </reactionnetwork>
  </xsl:template>

  <xsl:template match="f:labelingmeasurement">
    <labelingmeasurement xmlns="http://www.13cflux.net/fluxml">
      <xsl:for-each select="f:group">
        <xsl:sort select="@id" data-type="text"/>
        <group id="{@id}" scale="@scale">
          <textual>
            <xsl:value-of select="f:textual"/>
          </textual>
        </group>
      </xsl:for-each>
    </labelingmeasurement>
  </xsl:template>

</xsl:stylesheet>