mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 1 of the fsqp documentation

//fsqp with Scilab functions
//=======================================================


modefsqp=100;
iprint=0;
miter=500;
bigbnd=1.e10;
eps=1.e-4;
epsneq=0.e0;
udelta=0.e0;
nf=1;
neqn=0;
nineqn=1;nineq=1;
ncsrn=1;
ncsrl=0;
mesh_pts=101;
neq=0;nfsr=0;
   
bl=[-bigbnd;-bigbnd];
bu=-bl;

x0=[-1;-2];

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];
srpar=[nfsr,ncsrl,ncsrn];


t=(0:0.01:1)';
t2=t.*t;
function f=allobj(x), f=(x(1)^2)/3+x(2)^2+x(1)/2;endfunction
function gr=allgrob(x), gr=[2/3*x(1)+0.5,2*x(2)];endfunction
function c=allcntr(x), c=(1-(t*x(1))^2)^2-x(1)*t2+x(2)-x(2)^2;endfunction
function g=allgrcn(x), g=[-4*t2.*(1-(t*x(1))^2)^2*x(1)-t2,-2*x(2)+ones(t)];endfunction

function fj=obj2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    fj=all_objectives(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    fj=all_objectives(j);
  end
endfunction


function cj=cntr2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    cj=all_constraints(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    cj=all_constraints(j);
  end
endfunction

function gj=grob2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    gj=all_gradobjectives(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradobjectives(j,:);
  end
endfunction

function gj=grcn2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    gj=all_gradconstraints(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradconstraints(j,:);
  end
endfunction

C=[];for j=1:100,C=[C;-cntr2(j,x0)];end
format(6)		  
mprintf('%s\n',['---------------------------SIP problem with srfsqp(Scilab code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)
		'   Value of the objective function: '+sci2exp(obj2(1,x0),0)
		'   Value of the constraints :       '+sci2exp(C,0)])
timer();
[x,info,f]=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj2,cntr2,grob2,grcn2)
tim=timer();
C=[];for j=1:100,C=[C;-cntr2(j,x)];end

mprintf('%s\n',['Final values'
		'   Value of the solution:           '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Value of the constraints :       '+sci2exp(C,0)
		'   Time :                           '+sci2exp(tim,0)
	       ])
format(10)

clear all_objectives all_constraints all_gradobjectives all_gradconstraints 


