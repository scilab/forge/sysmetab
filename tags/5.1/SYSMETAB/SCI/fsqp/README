		Scilab-fsqp interface.
		======================

This contribution provides a Scilab interface to the FSQP code

What is FSQP
-------------
FSQP is an implementation of Feasible Sequential Quadratic
Programming (FSQP) algorithm, for directly tackling
optimization problems with: 

Multiple competing linear/nonlinear objective functions (minimax). 
linear/nonlinear inequality constraints. 
linear/nonlinear equality constraints. 

The fsqp code author is 
	Prof. Andre Tits
	Electrical Enginnering Dept.
	University of Maryland
	College Park, Md 20742
	USA
	E-mail: andre@eng.umd.edu

To install this contribution you have to get the source code for FSQP
 and copy the cfsqp.c file in the sci_gateway/c directory. This code can
be obtained  (free of charge for for academic usage) from "AEM Design":
http://aemdesign.com/how%20to%20order.htm


For convenience, a copy of the cfsqp documentation is put here in doc directory.

How to use the contribution
---------------------------
[1] To build the package: (you need a C compiler)
        de-tar the distribution file, start scilab and execute
	exec("<path>/builder.sce") 
	where <path> is the path where the distribution file has been de-tared
        The build process has to be done once.

[2] To load the package: 

	execute (exec) the scilab script loader.sce (from where you want) 
	the exec("<path>/loader.sce") instruction may be put into your
        SCIHOME/.scilab file

[3] To clean: 
	execute the scilab script cleaner.sce (from where you want) 

[4] To test the package 
        execute the demos included in the contribution (menu demos)

Remarks :
========

The examples given are the examples given in the documentation of fsqp.

In each example, the functions are either Scilab functions
or C functions (or mixture). 

Several Scilab functions are available:

- fsqp: standard fsqp solver.
- srfsqp: for sequentially related problems (e.g. semi-infinite programming).
- x_is_new(): to get the current value of the fsqp variable x_is_new.
- set_x_is_new: to assign a value to x_is_new.

For SR-problems with many constraints it may be useful to use
the x_is_new variable: examples are given (see e.g. example5.sci).

Authors: Francois.Delebecque@inria.fr, jpc@cermics.enpc.fr  (version 1.0)
         Serge.Steer@inria.fr  version 1.2 and 1.3 (adapdation to Scilab 4.x)
	 Serge.Steer@inria.fr  version 1.4  (adapdation to Scilab 5.x)
Enjoy!

June  2009
Serge Steer








