function [q,f,info,mess]=optimize(algorithm,qstart,qmin,qmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint,task,ntasks)

    // Monte-Carlo method 

    function [_q,_f,_info,_iter,_time]=optimize_mc(i)
        ymeas=ymeastab(:,i);
        wmeas=wmeastab(:,i);        
        [_q,_f,_info,_iter,_time]=optimize_single(algorithm,qstart,qmin,qmax,...
        C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%4d, info=%2d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)         
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    // multistart

    function [_q,_f,_info,_iter,_time]=optimize_multi(i)
        [_q,_f,_info,_iter,_time]=optimize_single(algorithm,qstarttab(:,i),qmin,qmax,...
            C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%4d, info=%2d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    time=0;
    eps_reg=max(1e-14,eps_reg) // safety reg (helps sometimes)

    if exists('task','local')
    
      params=init_param('dynamic_scheduling',1,'nb_workers',nb_cores)

      if task=='mc'
          [q,f,info,iter,time,mess]=optimize_single(algorithm,qstart,qmin,qmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint);
          if info>=0
            w=W*q+w0;
            [residual,X,y,h,omega,Sypm2_e_label]=solveStat(Phi(w,eps_phi),Xinp,M,b,cum)
            ymeastab=ymeas;
            wmeastab=wmeas;
            for i=1:ntasks
                ymeastab(:,i)=max(y+rand(ymeas,'normal')./sqrt(Sypm2),0)
                wmeastab(:,i)=max(E*w+rand(wmeas,'normal')./sqrt(Svpm2),0);
            end

            printf("\n%d tasks started, please wait.\n\n",ntasks)    

            qstart=q;

            tic
            [qmc,fmc,infomc,itermc,timemc]=parallel_run(1:ntasks,optimize_mc,[length(qstart) 1 1 1 1]',params);	
            time=toc()
            
            k=find(infomc==0 | infomc==3 | infomc==8); // keep semi-successfull optimization runs: 0=OK, 3=max. iter. reached, 8=should not be present (because of specific tolX handling)

            qmc=qmc(:,k);fmc=fmc(k);infomc=infomc(k);
            qt=quart(fmc);
            printf("\nResidual min=%f, q1=%f, median=%f, mean=%f, q3=%f, max=%f, std=%f\n",min(fmc),qt(1),qt(2),mean(fmc),qt(3),max(fmc),stdev(fmc))
          
            q=[q qmc]
            f=[f fmc]
            info=[info infomc]
            iter=[iter itermc]
          end
      elseif task=="multi"
          w=convexSampler(C,d,me,ntasks,wadm);
          qstarttab=w(ff,:);

          printf("\n%d tasks started, please wait\n\n",ntasks)

          [q,f,info,iter,time]=parallel_run(1:ntasks,optimize_multi,[length(qstart) 1 1 1 1]',params);
          
          k=find(info==0 | info==3 | info==8); // keep semi-successfull optimization runs: 0=OK, 3=max. iter. reached, 8=should not be present (because of specific tolX handling)

          if ~isempty(k)
            q=q(:,k);f=f(k);info=info(k);iter=iter(k);time=time(k);
            printf("\nResidual best=%f, mean=%f, median=%f, std=%f\n",min(f),mean(f),median(f),stdev(f))
            [bc,bk]=min(f);
            q=q(:,bk);
            f=f(bk);
            info=info(bk);
            iter=iter(bk);
            time=time(bk);
          else // in order to have a more detailed error message, since string output is complicated with parallel_run... 
            [q,f,info,iter,time,mess]=optimize_single(algorithm,qstart,qmin,qmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
          end
          // write best free fluxes set in original file ?
      end   
  else
    [q,f,info,iter,time,mess]=optimize_single(algorithm,qstart,qmin,qmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint);
  end

  mess.iter=iter(1);
  mess.time=time(1);
  infoStrings=['Normal termination'
  'Cannot find a feasible initial guess'
  'Cannot find a feasible initial guess'
  'Maximum number of iteration has been reached'
  'Step size smaller than machine precision'
  'Failure in constructing d0'
  'Failure in constructing d1'
  ''
  'New iterate is equal to previous iterate to machine precision'];
  if info($)==-1
    mess.info="Error during optimization"
  else
      mess.info=infoStrings(info($)+1);
  end
endfunction

function [q,f,info,iter,time,mess]=optimize_single(algorithm,qstart,qmin,qmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint)
    // w0=flux.values verify the equality constrains (CS) and the inequality
    // CS that why we reduce our problem by neglecting equality CS and working
    // with only inequality CS. In the other hand, we remove all evident 
    // inequality and zeros line of C(me+1:$,:)*W from inequality CS. 
    
    global lastobj iteration lastq message userstop
    
    mess=[];
    Cineq=C(me+1:$,:); dineq=d(me+1:$);
    Cq=Cineq*W;
    dq=dineq-Cineq*w0;
    ineq_constr=find(clean(sum(abs(Cq),'col'))~=0);
    Cq=clean(Cq(ineq_constr,:));
    dq=clean(dq(ineq_constr,:));

    // detection of pure bound constraints to treat as such

    bc=[];
    for i=1:size(Cq,1);
        j=find(Cq(i,:));
        if length(j)==1
            bc=[bc i];
            if Cq(i,j)<0 then
                qmin(j)=max(qmin(j),dq(i)/Cq(i,j));
            else
                qmax(j)=min(qmax(j),dq(i)/Cq(i,j));
            end
        end
    end
    
    Cq(bc,:)=[];
    dq(bc)=[];

    nineq=size(Cq,1); // is the number of (non-trivial) linear inequalities

    lower=[qmin]; // lower bounds
    upper=[qmax]; // upper bounds 

    if algorithm=='fsqp'

        nf=1; // is the number of cost function
        nineqn=0; // is the number of nonlinear inequalities
        neqn=0; // is the number of nonlinear equalities
        neq=0; // is the number of linear equalities
        modefsqp=110; // see documentation of fsqp (110 is max4 option)
        ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,4*miter,0];
        bigbnd=1.e10; // play the role of infinity
        epsneq=0.e0; // Maximum violation of nonlinear equality constraints allowed 
                     // by the user at an optimal point
        udelta=0.e0; // The perturbation size the user suggests to use in 
                     // approximating gradients by finite difference
        rpar=[bigbnd,max(eps_grad,1e-15),epsneq,udelta];
        // the input _obj,_cntr,_grobj and _grcntr must be defined as a function
        // inform is a parameter indicating the status of the execution of fsqp
        // f is a value of the cost function at q at the end of the execution
        // lambda are the values of the Lagrange multipliers at q 
        // at the end of execution
        // g are the values of all constraints at q at the end of execution
    
        f=0;
        lastobj=%inf
        iteration=0
        userstop=-1
        lastq=[]
        tic();
        [q,info,f,g]=fsqp([qstart],ipar,rpar,[lower,upper],_obj,_cntr,_grobj,_grcntr);
        if userstop>=0
          info=userstop
        end
        time=toc();
    end
    
    iter=iteration
    mess=message
    
    if ~isempty(message)
      info=-1
      f=%nan
    else
      f=f-eps_reg*norm(q)^2;
    end

endfunction

function [value,gradient]=all_stuff(_q,_algorithm)

    global lastobj iteration lastq message userstop
        
    // We build in the function all cost function and its gradient
    
    // Computation of a net/xch flux vector w=W*q+w0 using free fluxes.

    w=W*_q+w0;

		// Computation of flux observation residual

    e_flux=E*w-wmeas;
    flux_error=Svpm2.*e_flux.^2;

		// Computation of a fwd/rev flux vector

    [v,dv_dw]=Phi(w,eps_phi);

		// Solve the state equation and compute the label observation residual and gradient


    try

      [label_error,gradq]=solveStatAndGrad(v,Xinp,M,b,cum,dv_dw,W);

  		// Overall cost and gradient final computation

      value=sum(label_error)+sum(flux_error)+eps_reg*norm(_q)^2
      gradient=gradq+2*(W'*E'*(Svpm2.*e_flux))+2*eps_reg*_q

  		// Iteration report and stopping criterion

      if value<=lastobj
        lastobj=value;
        if iprint>0 & _algorithm=='fsqp' & lastq<>[]
            printf("iter=%5d, residual=%14.8e, errx=%14.8e\n",iteration,value-eps_reg*norm(_q)^2,norm(_q-lastq));
        end
        if norm(_q-lastq)<eps_x
          gradient=%nan+zeros(_q)
          userstop=0  
        elseif iteration>=miter
          gradient=%nan+zeros(_q)
          userstop=3          
        end
        iteration=iteration+1;
      end
      lastq=_q
      
    
    catch // only way of properly stopping fsqp...
      value=%nan;
      gradient=%nan+zeros(_q)
      [str,n,line,func]=lasterror();
      errclear(0);
      message=struct('str',str,'n',n,'line',line,'func',func);
    end
      
endfunction
