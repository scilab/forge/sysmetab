function [residual,X,y,MList,omega,Sypm2_e_label]=solveNSMP(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight,free)
    //
    // Solve the state equation cascade, all ode schemes. solveNS is redefined in calling context as
    // solveNSIE (Implicit Euler), solveNSMP (Implicit Midpoint), solveNSIT (Implicit Trapezoidal)
    // solveNSWeight is redefined accordingly as solveNSIEWeight, solveNSMPWeight, solveNSITWeight, respectively.
    //
    MList=list();
    X=repmat(Xinp,1,length(t));
    for i=1:length(cum)
        [X(cum(i),:),MList(i)]=solveNSMPWeight(v,m,X,cum(i),M(i),b(i),t,pool_ind_weight(i));
    end
    //
    // Compute the simulated measurements
    //
    y_unscaled=Cx*X(:,ind_meas);
    //
    // Compute optimal scaling factors (simple least squares problem)
    //
    omega=ones(nb_group,1)
    for i=auto_scales_ind // for groups with scale="auto"
      ind=meas_ind_group(i);
      yui=y_unscaled(ind,:)./Sy(ind,:);
      ymi=ymeas(ind,:)./Sy(ind,:);
      omega(i)=yui(:)'*ymi(:)/norm(yui(:))^2;
    end
    y=spdiag(omega(omega_ind_meas))*y_unscaled;
    //
    // Compute the residual
    //
    e_label=y-ymeas;
    Sypm2_e_label=Sypm2.*e_label;
    residual=Sypm2_e_label.*e_label;
    //
    if exists("free","local")
      if free
        for i=1:length(cum)
          umf_ludel(MList(i).MnImp_handle)
        end
      end
    end
endfunction

function [Xn,MnList]=solveNSMPWeight(v,m,X,cum,Mn,bn,t,pool_ind_weightn)
  //
  // Weight n cumomers,  Implicit Midpoint Rule
  //
  Mn_sp=sparse(Mn.ij,Mn.t*v(Mn.m1),Mn.size);
  bmat_sp=bn.t*spdiag(v(bn.m3))*((X(bn.m1,1:$-1)+X(bn.m1,2:$)).*(X(bn.m2,1:$-1)+X(bn.m2,2:$)))/4;
  //
  stepsize=t(2)-t(1)
  nt=length(t)
  MnImp=sparse(spdiag(m(pool_ind_weightn))-0.5*stepsize*Mn_sp);
  MnImp_handle=umf_lufact(MnImp);
  Mnx=sparse(spdiag(m(pool_ind_weightn))+0.5*stepsize*Mn_sp);
  //
  bmat_sp=stepsize*bmat_sp;
  //
  Xn=zeros(length(pool_ind_weightn),nt);
  //
  for k=1:(nt-1)
    Xn(:,k+1)=umf_lusolve(MnImp_handle,Mnx*Xn(:,k)+spset(bn.value,bmat_sp(:,k)));  
  end
  //
  MnList=struct("MnImp",MnImp,"Mnx",Mnx,"MnImp_handle",MnImp_handle)
  //
endfunction

function [residual,gradq,gradm,X,y]=solveNSMPGradAdj(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight,dP_dpool,dP_dpool_t,dv_dw,W)
    //
    // Solve state equation and compute adjoint gradient, implicit Midpoint Rule
    //
    [residual,X,y,MList,omega,Sypm2_e_label]=solveNSMP(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight);
    //
    Xm=.5*X(:,1:$-1)+.5*X(:,2:$);
    db_dxt_nz=adjointNSDerivatives(v,Xm,dgdvt,db_dxt);
    //
    Ct_Omega_Sypm2_e_label=Cxt*spdiag(omega(omega_ind_meas))*Sypm2_e_label;
    //
    nt=length(t)    
    p=zeros(size(X,1),nt-1);
    bmeas=zeros(X);
    bmeas(:,ind_meas)=Ct_Omega_Sypm2_e_label;
    //
    ncum=length(cum);            
    p(cum(ncum),:)=solveNSMPAdj(p,bmeas,cum(ncum),MList(ncum),t,m,pool_ind_weight(ncum));    
    for i=ncum-1:-1:1 
        p(cum(i),:)=solveNSMPAdj(p,bmeas,cum(i),MList(i),t,m,pool_ind_weight(i),db_dxt(i).value,db_dxt_nz(i));        
    end
    //
    // Compute the gradient (adjoint method)
    //
    stepsize=t(2)-t(1)
    Wtdv_dwt=(dv_dw*W)';
    gradq=zeros(size(W,2),1);
    for k=1:nt-1        
        gradq=gradq+Wtdv_dwt*spset(dgdvt.value,dgdvt.t*(Xm(dgdvt.m1,k).*Xm(dgdvt.m2,k)))*p(:,k);
    end
    gradq=2*stepsize*gradq;
    //
    dP_dpool_t_nz=dP_dpool_t.t*(X(dP_dpool_t.m1,2:$)-X(dP_dpool_t.m1,1:$-1));
    gradm=zeros(nb_pool,1);
    for k=1:nt-1        
        gradm=gradm+spset(dP_dpool_t.value,dP_dpool_t_nz(:,k))*p(:,k);
    end
    gradm=-2*gradm;      
    //
endfunction
//
function pn=solveNSMPAdj(p,bmeas,cum,MnStruct,t,m,pool_ind_weightn,db_dx_t,db_dx_t_mat)
  //
  // Solve weight n cumomer adjoint state,  Midpoint rule or Implicit Trapezoidal rule
  //
  Mnx_adj=MnStruct.Mnx'
  Mimp_adj_handle=umf_lufact(MnStruct.MnImp');
  step2=(t(2)-t(1))/2;
  nt=length(t)
  pn=zeros(length(pool_ind_weightn),nt-1);
  //
  if exists("db_dx_t","local")
    dbdxpk=spset(db_dx_t,db_dx_t_mat(:,nt-1))*p(:,nt-1);
    for k=nt-1:-1:2
      dbdxpkm1=spset(db_dx_t,db_dx_t_mat(:,k-1))*p(:,k-1);
      pn(:,k-1)=umf_lusolve(Mimp_adj_handle,Mnx_adj*pn(:,k)+bmeas(cum,k)+step2*(dbdxpk+dbdxpkm1));
      dbdxpk=dbdxpkm1;    
    end
  else    
    for k=nt-1:-1:2    
        pn(:,k-1)=umf_lusolve(Mimp_adj_handle,Mnx_adj*pn(:,k)+bmeas(cum,k));      
    end
  end
  umf_ludel(MnStruct.MnImp_handle)
  umf_ludel(Mimp_adj_handle)
endfunction
//
function [residual,gradq,gradm,X,y]=solveNSMPGradDir(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight,dP_dpool,dP_dpool_t,dv_dw,W)
    //
    // Solve state equation and compute direct gradient, implicit Midpoint Rule
    //
    [residual,X,y,MList,omega,Sypm2_e_label]=solveNSMP(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight);
    // MList(n).MnImp is MnImp   
    // MList(n).Mnx is Mnx   
    // MList(n).MnImp_handle is the handle to LU factors of MnImp   
    //
    [dgdv_nz,db_dx_nz]=forwardNSDerivatives(v,.5*X(:,1:$-1)+.5*X(:,2:$),dgdv,db_dx); // Compute derivatives at midpoint
    //
    stepsize=t(2)-t(1);
    nbParam=size(W,2)+length(m);
    ind_meas0=[1;ind_meas];
    nbCum=length(cum);
    nbMeas=length(ind_meas);
    gradp=zeros(nbParam,1);

    Ct_Omega_Sypm2_e_label=Cxt*spdiag(omega(omega_ind_meas))*Sypm2_e_label;
    dP_dpool_nz=dP_dpool.t*(X(dP_dpool.m1,2:$)-X(dP_dpool.m1,1:$-1));

    dX_dp=zeros(size(X,1),size(v,1)+nb_pool);

    for ind=1:nbMeas
        for k=ind_meas0(ind):ind_meas0(ind+1)-1
            dX_dpk=dX_dp // at end of loop dX_dp will be "dX_dpkp1"
            dgdq_kp1=spset(dgdv.value,dgdv_nz(:,k))*dv_dw*W;
            dP_dpool_k=spset(dP_dpool.value,dP_dpool_nz(:,k)); 
            //           
            bcum1=[stepsize*dgdq_kp1(cum(1),:) -dP_dpool_k(cum(1),:)];
            dX_dp(cum(1),:)=umf_lusolve(MList(1).MnImp_handle,MList(1).Mnx*dX_dp(cum(1),:)+bcum1);
            //
            for i=2:nbCum
                bcumi=[stepsize*dgdq_kp1(cum(i),:) -dP_dpool_k(cum(i),:)];
                dX_dp(cum(i),:)=umf_lusolve(MList(i).MnImp_handle,MList(i).Mnx*dX_dp(cum(i),:)+stepsize*(spset(db_dx(i).value,db_dx_nz(i)(:,k))*(dX_dp+dX_dpk)/2)+bcumi);
            end
        end               
        gradp=gradp+dX_dp'*Ct_Omega_Sypm2_e_label(:,ind);
    end
    
    for i=1:length(cum)
        umf_ludel(MList(i).MnImp_handle)
    end
    gradq=2*gradp(1:size(W,2));
    gradm=2*gradp(size(W,2)+1:$);
endfunction
//
function [residual,X,y,dy_dp,omega]=solveNSMPDer(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight,dP_dpool,dv_dw,W)
    //
    // Solve state equation and compute direct gradient, implicit Midpoint Rule
    //
    [residual,X,y,MList,omega,Sypm2_e_label]=solveNSMP(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight);
    // MList(n).MnImp is MnImp   
    // MList(n).Mnx is Mnx   
    // MList(n).MnImp_handle is the handle to LU factors of MnImp
    //
    [dgdv_nz,db_dx_nz]=forwardNSDerivatives(v,.5*X(:,1:$-1)+.5*X(:,2:$),dgdv,db_dx); // Compute derivatives at midpoint
    //
    stepsize=t(2)-t(1);
    nbParam=size(W,2)+length(m);
    ind_meas0=[1;ind_meas];
    sizeMeas=size(Cx,1);    
    nbMeas=length(ind_meas);
    nbCum=length(cum);
    
    dP_dpool_nz=dP_dpool.t*(X(dP_dpool.m1,2:$)-X(dP_dpool.m1,1:$-1));
    
    dX_dp=zeros(size(X,1),nbParam);
    dy_dp=zeros(sizeMeas,nbParam,nbMeas); // output derivative at each measurement time
    dy_unscaled_dp=zeros(sizeMeas,nbParam,nbMeas); // unscaled output derivative at each measurement time

    for j=1:nbMeas
        for k=ind_meas0(j):ind_meas0(j+1)-1
            dX_dpk=dX_dp // at end of loop dX_dp will be "dX_dpkp1"
            dgdq_kp1=spset(dgdv.value,dgdv_nz(:,k))*dv_dw*W;
            dP_dpool_k=spset(dP_dpool.value,dP_dpool_nz(:,k)); 
            //           
            bcum1=[stepsize*dgdq_kp1(cum(1),:) -dP_dpool_k(cum(1),:)];
            dX_dp(cum(1),:)=umf_lusolve(MList(1).MnImp_handle,MList(1).Mnx*dX_dp(cum(1),:)+bcum1);
            //
            for i=2:nbCum
                bcumi=[stepsize*dgdq_kp1(cum(i),:) -dP_dpool_k(cum(i),:)];
                dX_dp(cum(i),:)=umf_lusolve(MList(i).MnImp_handle,MList(i).Mnx*dX_dp(cum(i),:)+stepsize*(spset(db_dx(i).value,db_dx_nz(i)(:,k))*(dX_dp+dX_dpk)/2)+bcumi);
            end
        end               
        dy_dp(:,:,j)=spdiag(omega(omega_ind_meas))*Cx*dX_dp; 
        dy_unscaled_dp(:,:,j)=Cx*dX_dp;
    end
    
    dy_dp=NSDerScales(y,Cx*X(:,ind_meas),dy_dp,dy_unscaled_dp,auto_scales_ind,ind_meas)
    
    for i=1:length(cum)
        umf_ludel(MList(i).MnImp_handle)
    end    
endfunction

midpoint=struct("name","midpoint","order",2,"solveNS",solveNSMP,"solveNSGrad",solveNSMPGradAdj,"solveNSDer",solveNSMPDer);
