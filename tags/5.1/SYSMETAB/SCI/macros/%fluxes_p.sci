function %fluxes_p(f)
    // User friendly display of a fluxes set
    nfluxes=size(f.names,'*')/2;
    maxlength=max(length(f.names));
    printf("%"+string(maxlength)+"s : %23s, %23s\n\n",'flux','net, [sigma]     ','xch, [sigma]     ');
    for i=1:nfluxes
        fname=strsubst(f.names(i),'.n','');
        fdnet=ftype(i)
        fdxch=ftype(i+nfluxes);
        printf("%"+string(maxlength)+"s : %+10.5f [%6.3f] (%s), %10.5f [%6.3f] (%s)\n",...
        fname,f.values(i),f.sigma(i),fdnet,f.values(i+nfluxes),f.sigma(i+nfluxes),fdxch)
    end
endfunction
