function [m,minf,msup,mtype]=poolsizes(mnames,mvalues,nb_pool)
	val=0.8;
	m=rand(nb_pool,1);	
  minf=zeros(nb_pool,1)
  msup=1e5*ones(nb_pool,1)
  mtype=string(zeros(nb_pool,1))
  mtype(:)="f"

  for i=1:size(mnames,"*")
    k=find(mnames(i)==metabolite_names)
    m(k)=mvalues(i,1);
    minf(k)=mvalues(i,2)
    msup(k)=mvalues(i,3)
    mtype(find(minf==msup))="c"
  end
endfunction