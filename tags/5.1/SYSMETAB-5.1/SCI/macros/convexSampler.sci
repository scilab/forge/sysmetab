function R = convexSampler(A,b,me,m,x)
    // Sampling of uniformly distributed vectors x satisfying the linear system
    // of inequalities/equalities A.x <= b. It is assumed that the convex region described
    // by A.x <= b is not unbounded.
    // Derived from M. Weitzel Matlab code in Mathworks Central. 
    // The code has been adapted for mixed equality/inequality constraints
    // and a (fatal) bug in distToConstraints has been fixed... 
       
    R = rand(size(x,1),m);

endfunction
