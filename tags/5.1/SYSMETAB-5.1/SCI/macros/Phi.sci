function [v,Phiprime]=Phi(w,eps)
    // "mollification" of transformation (net,xch)->(forward,backward)
    n=length(w)/2;
    v_net=w(1:n,:);
    v_xch=w(n+1:$);
    I=speye(n,n);
    if (eps==0) then
        v=[v_xch+max(0,v_net)
        v_xch-min(0,v_net)];
        s=sign(v_net);
        Phiprime=[spdiag((1+s)/2),I
                 -spdiag((1-s)/2),I];
    else
        v=[v_xch+phi( v_net,eps)
        v_xch+phi(-v_net,eps)];
        Phiprime=[spdiag(phiprime( v_net,eps)),I
                 -spdiag(phiprime(-v_net,eps)),I];
    end
endfunction

function y=phi(x,e)
    // A smooth function near (0,0)
    y=((x>-e & x<e).*(x+e).^2)/(4*e)+(x>=e).*x;
endfunction

function y=phiprime(x,e)
    // The derivative of the smooth function near (0,0)
    y=(x>-e & x<e).*(x+e)/(2*e)+(x>=e);
endfunction

