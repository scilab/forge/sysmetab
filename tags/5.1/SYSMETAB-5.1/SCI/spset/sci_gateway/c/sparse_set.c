/* ==================================================================== */
/* Template toolbox_skeleton */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ==================================================================== */
#include "api_scilab.h"
#include <localization.h>
#include <string.h>
#include "Scierror.h"

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#ifdef __SCILAB6__
int sparse_set(char *fname, void *pvApiCtx)
#else
int sparse_set(char *fname)
#endif
{
    SciErr sciErr;
    int i,j,k;
    int* piAddr			= NULL;
    int* vAddr          = NULL;
    int iRows			= 0;
    int iCols			= 0;
    int viRows			= 0;
    int viCols			= 0;
    int iNbItem			= 0;
    int* piNbItemRow	= NULL;
    int* piColPos		= NULL;
    double* pdblReal	= NULL;	
    double* vpdblReal	= NULL;

    CheckInputArgument(pvApiCtx, 2, 2);
    CheckOutputArgument(pvApiCtx, 0, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    sciErr = getSparseMatrix(pvApiCtx, piAddr, &iRows, &iCols, &iNbItem, &piNbItemRow, &piColPos, &pdblReal);
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &vAddr);
    sciErr = getMatrixOfDouble(pvApiCtx, vAddr, &viRows, &viCols, &vpdblReal);

    memcpy(pdblReal, vpdblReal,min(iNbItem,viRows*viCols)*sizeof(double));
    sciErr = createSparseMatrix(pvApiCtx, nbInputArgument(pvApiCtx) + 1, iRows, iCols, iNbItem, piNbItemRow, piColPos,pdblReal);

    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
    ReturnArguments(pvApiCtx);
    
    return 0;
}
