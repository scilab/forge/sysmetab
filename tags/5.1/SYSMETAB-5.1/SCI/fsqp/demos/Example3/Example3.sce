mode(-1)
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("Example3.sce");
  methods=['Scilab Code fsqp','Scilab Code srfsqp','C code',..
	   'Using lists']
sz=[207 106];
fsqp_demo_gui(demopath,'example3',sz,methods)
