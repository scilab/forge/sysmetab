<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   St�phane Mottelet and Georges Sadaka
    Date    :   2013/2014
    Project :   PIVERT/Metalippro-PL1
-->

<!-- This style sheet aims to generate a graph representing the dependance of cumomers
     with respect to each other. Example for the branching network with measurements
     de F#x1, F#1x, F#11 :
    
    <graph>
      <node id="A_1" pool="A" subscript="1" weight="1" pattern="x1" type="intermediate">
        <child node="A_out_1"/>
        <child node="F_1"/>
        <child node="D_1"/>
        <child node="F_2"/>
      </node>
      <node id="A_2" pool="A" subscript="2" weight="1" pattern="1x" type="intermediate">
        <child node="A_out_2"/>
        <child node="F_2"/>
        <child node="D_1"/>
        <child node="F_1"/>
      </node>
      <node id="A_3" pool="A" subscript="3" weight="2" pattern="11" type="intermediate">
        <child node="A_out_3"/>
        <child node="F_3"/>
        <child node="D_1"/>
        <child node="D_1"/>
        <child node="F_3"/>
      </node>
      <node id="D_1" pool="D" subscript="1" weight="1" pattern="1" type="intermediate">
        <child node="A_2"/>
        <child node="A_1"/>
        <child node="F_2"/>
        <child node="F_1"/>
      </node>
      <node id="F_1" pool="F" subscript="1" weight="1" pattern="x1" type="intermediate" source="yes">
        <child node="A_1"/>
        <child node="A_2"/>
        <child node="D_1"/>
      </node>
      <node id="F_2" pool="F" subscript="2" weight="1" pattern="1x" type="intermediate" source="yes">
        <child node="A_2"/>
        <child node="A_1"/>
        <child node="D_1"/>
      </node>
      <node id="F_3" pool="F" subscript="3" weight="2" pattern="11" type="intermediate" source="yes">
        <child node="A_3"/>
        <child node="A_3"/>
        <child node="D_1"/>
        <child node="D_1"/>
      </node>
      <node id="A_out_1" pool="A_out" subscript="1" weight="1" pattern="x1" type="input"/>
      <node id="A_out_2" pool="A_out" subscript="2" weight="1" pattern="1x" type="input"/>
      <node id="A_out_3" pool="A_out" subscript="3" weight="2" pattern="11" type="input"/>
    </graph>
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb f exslt">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:param name="verb_level">1</xsl:param>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:rproduct" use="@id"/>

  <xsl:key name="SOURCES" match="f:group/smtb:measurement/smtb:cumomer-contribution" use="concat(@pool,'_',@subscript)"/>

  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>generation with network simplification</xsl:message>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:reactionnetwork">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:metabolitepools">
    <graph>
      <xsl:apply-templates/>
    </graph>
  </xsl:template>

  <xsl:template match="f:pool">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:cumomer">
    <xsl:element name="node">
      <xsl:copy-of select="@id"/>
      <xsl:copy-of select="@pool"/>
      <xsl:copy-of select="@subscript"/>
      <xsl:copy-of select="@weight"/>
      <xsl:copy-of select="@pattern"/>
      <xsl:copy-of select="../@type"/>
      <xsl:if test="key('SOURCES',concat(@pool,'_',@subscript))">
        <xsl:attribute name="source">yes</xsl:attribute>
      </xsl:if>
      <xsl:if test="../@type='intermediate'">
        <xsl:variable name="name" select="../@id"/>
        <xsl:variable name="carbons">
          <xsl:copy-of select="smtb:carbon"/>
        </xsl:variable>
        <!-- Find cumomers (other than itself) in the right hand side of balance equation by looping on all reactions
             where the cumomer pool is a product -->
        <xsl:for-each select="key('RPRODUCT',../@id)[../@follow='true']"> <!-- context node is a <reaction> -->
          <xsl:variable name="id" select="@id"/>
          <xsl:variable name="occurrence" select="count(preceding-sibling::f:rproduct)+1"/>
          <!-- Find all occurrences (in educts) of product marked carbons by making a loop on all educts 
               of the current reaction : -->
          <xsl:for-each select="../f:reduct[smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]]">
            <xsl:variable name="matchedCarbons">
              <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]">
                <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                  <token>
                    <xsl:value-of select="@position"/>
                  </token>
                </xsl:if>
              </xsl:for-each>
            </xsl:variable>
            <xsl:if test="string-length($matchedCarbons)&gt;0">
              <child node="{concat(@id,'_',sum(exslt:node-set($matchedCarbons)/token))}"/>
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:if>
    </xsl:element>
  </xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>