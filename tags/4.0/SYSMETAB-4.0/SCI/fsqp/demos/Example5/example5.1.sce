mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 1 of the fsqp documentation

//fsqp with Scilab functions
//=======================================================

function fj=obj(j,x), fj=(x(1)^2)/3+x(2)^2+x(1)/2,endfunction

function gr=grob(j,x), gr=[2/3*x(1)+0.5,2*x(2)];endfunction

function ct=cntr(j,x)
  y=(j-1)/100;
  ct=(1-(x(1)*y)^2)^2-x(2)^2+x(2)-x(1)*y^2;
endfunction

function gj=grcn(j,x)
  y=(j-1)/100;y2=y*y;
  gj=[-4*y2*(1-(y*x(1))^2)^2*x(1)-y2, -2*x(2)+1];
endfunction

modefsqp=100;
iprint=0;
miter=500;
bigbnd=1.e10;
eps=1.e-4;
epsneq=0.e0;
udelta=0.e0;
nf=1;
neqn=0;
nineqn=1;nineq=1;
ncsrn=1;
ncsrl=0;
mesh_pts=101;
neq=0;nfsr=0;
   
bl=[-bigbnd;-bigbnd];
bu=-bl;

x0=[-1;-2];

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];
srpar=[nfsr,ncsrl,ncsrn];


// All scilab functions
C=[];for j=1:100,C=[C;-cntr(j,x0)];end
format(6)		  
mprintf('%s\n',['---------------------------SIP problem with srfsqp(Scilab code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)
		'   Value of the objective function: '+sci2exp(obj(1,x0),0)
		'   Value of the constraints :       '+sci2exp(C,0)])
timer();

x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)
tim=timer();
C=[];for j=1:100,C=[C;-cntr(j,x)];end

mprintf('%s\n',['Final values'
		'   Value of the solution:           '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(obj(1,x),0)
		'   Value of the constraints :       '+sci2exp(C,0)
		'   Time :                           '+sci2exp(tim,0)
	       ])
format(10)

clear all_objectives all_constraints all_gradobjectives all_gradconstraints 


