void obj71(nparam,j,x,fj,cd)
int nparam,j;
double *x,*fj;
void *cd;
{
   *fj=x[0]*x[3]*(x[0]+x[1]+x[2])+x[2];
   return;
}

void grob71(nparam,j,x,gradfj,dummy,cd)
int nparam,j;
double *x,*gradfj;
void (* dummy)(),*cd;
{
   gradfj[0]=x[3]*(x[0]+x[1]+x[2])+x[0]*x[3];
   gradfj[1]=x[0]*x[3];
   gradfj[2]=x[0]*x[3]+1.e0;
   gradfj[3]=x[0]*(x[0]+x[1]+x[2]);
   return;
}

void cntr71(nparam,j,x,gj,cd)
int nparam,j;
double *x,*gj;
void *cd;
{
   switch (j) {
      case 1:
	 *gj=25.e0-x[0]*x[1]*x[2]*x[3];
	 break;
      case 2:
	 *gj=x[0]*x[0]+x[1]*x[1]+x[2]*x[2]+x[3]*x[3]-40.e0;
	 break;
   }
   return;
}

void grcn71(nparam,j,x,gradgj,dummy,cd)
int nparam,j;
double *x,*gradgj;
void (* dummy)(),*cd;
{
   switch (j) {
      case 1:
	 gradgj[0]=-x[1]*x[2]*x[3];
	 gradgj[1]=-x[0]*x[2]*x[3];
	 gradgj[2]=-x[0]*x[1]*x[3];
	 gradgj[3]=-x[0]*x[1]*x[2];
	 break;
      case 2:
	 gradgj[0]=2.e0*x[0];
	 gradgj[1]=2.e0*x[1];
	 gradgj[2]=2.e0*x[2];
	 gradgj[3]=2.e0*x[3];
	 break;
   }
   return;
}
