<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Project :   PIVERT/Metalippro-PL1
-->

<!-- This style sheet have a goal to generate the system equations as a text readable by a human. The output is 
     not designed to a particular software, but we can inspire from the code below in order to write a 
     style sheet designed for example to Maple or other.
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:exslt="http://exslt.org/common"
  xmlns:str="http://exslt.org/strings"
  exclude-result-prefixes="xsl smtb m exslt">

  <xsl:output method="text"  encoding="UTF-8"/>

  <xsl:param name="verb_level">1</xsl:param>

  <!-- Maximum weight -->
  <xsl:param name="weight">10000</xsl:param>

  <!-- Output language  -->
  <xsl:param name="lang">scilab</xsl:param>

  <xsl:strip-space elements="*"/>

  <!-- Find all <smtb:matrix-assignment>-->
  <xsl:key name="MATRIX-ASSIGNMENTS-BY-ID-ROW-COL" match="smtb:matrix-assignment" use="concat(@id,' ',@row,' ',@col)"/>

  <!-- Quote ' character -->
  <xsl:variable name="quote">'</xsl:variable>

  <!-- Double quote " character -->
  <xsl:variable name="double_quote">"</xsl:variable>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="carbon-labeling-system">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>Generating <xsl:value-of select="$lang"/>code, please wait...</xsl:message>
    </xsl:if>
    <xsl:apply-templates/>
		<xsl:apply-templates select="//smtb:matrix-close[@prepost='yes']" mode="post"/>
  </xsl:template>

  <xsl:template match="listOfReactions"/>

  <xsl:template match="listOfSpecies | metabolitepools">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="species | pool">
    <xsl:value-of select="concat('// ',@id,' , ',@type,'&#xA;')"/>
    <!-- <xsl:value-of select="concat('// ',@id,' : ',@name,', ',@type,'&#xA;')"/>-->
    <xsl:apply-templates select="equations"/>
  </xsl:template>

  <xsl:template match="equations">
    <xsl:apply-templates select="equation[@weight&lt;=$weight]"/>
  </xsl:template>

  <xsl:template match="equation/m:apply">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>=</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:plus]">
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:choose>
        <xsl:when test="following-sibling::m:apply[m:minus and (count(*)=2)]">
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="position()&lt;last()">
            <xsl:text>+</xsl:text>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="m:apply[count(m:apply)=count(*)]">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:plus and ((preceding-sibling::m:power) or (preceding-sibling::m:times) or (preceding-sibling::m:minus) or (preceding-sibling::m:backslash)) and (count(*)&gt;2)]">
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>+</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:minus and (count(*)=3)]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>-</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:minus and (count(*)=2)]">
    <xsl:text>-</xsl:text>
    <xsl:apply-templates select="*[2]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:minus and (count(*)=3) and ((preceding-sibling::m:power) or 
                       (preceding-sibling::m:times) or (preceding-sibling::m:transpose))]">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>-</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>)</xsl:text>
  </xsl:template>
   
  <xsl:template match="m:apply[m:times]">
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../m:times/@type='array'">
            <xsl:text>.*</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>*</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="m:apply[m:divide]">
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../m:divide/@type='array'">
            <xsl:text>./</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>/</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="m:apply[m:times and ((preceding-sibling::m:power) or (preceding-sibling::m:transpose))]">
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../m:times/@type='array'">
            <xsl:text>.*</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>*</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:diff]">
    <xsl:text>d(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>)/dt</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:transpose]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>'</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:transpose and m:apply]">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>)</xsl:text>
    <xsl:text>'</xsl:text>
  </xsl:template>


  <xsl:template match="m:apply[m:power]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>^</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:power[@type='array']]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>.^</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:eq]">
    <!-- <xsl:if test="ancestor::smtb:body">
    <xsl:text>  </xsl:text>
    </xsl:if>-->
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>=</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:eqplus]">
    <xsl:if test="ancestor::smtb:body">
      <xsl:text></xsl:text>
    </xsl:if>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>+=</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:selector and m:ci[@type='vector']]">
    <xsl:apply-templates select="m:ci[1]"/>
    <xsl:choose>
      <xsl:when test="$lang='scilab'">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[3]"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:when test="$lang='C'">
        <xsl:text>[</xsl:text>
        <xsl:choose>
          <xsl:when test="*[3][self::m:cn]">
            <xsl:value-of select="(*[3])-1"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="*[3]"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text>]</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="m:apply[m:selector and m:ci[@type='matrix']]">
    <xsl:apply-templates select="m:ci[1]"/>
    <xsl:choose>
      <xsl:when test="$lang='scilab'">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[3]"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="*[4]"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:when test="$lang='C'">
        <xsl:text>[</xsl:text>
        <xsl:apply-templates select="m:cn[1]"/>
        <xsl:text>][</xsl:text>
        <xsl:apply-templates select="m:cn[2]"/>
        <xsl:text>]</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="m:apply[m:backslash]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>\</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:min]">
    <xsl:text>min(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>,</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:max]">
    <xsl:text>max(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>,</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:fn]">
    <xsl:apply-templates select="m:fn/m:ci"/>
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*[not(self::m:fn)]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
		<xsl:if test="not(ancestor::m:apply)">
	    <xsl:text>&#xA;</xsl:text>
		</xsl:if>
  </xsl:template>

  <xsl:template match="smtb:input/m:list">
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="smtb:output/m:list">
    <xsl:text>[</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:list">
    <xsl:text>[</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../@separator">
            <xsl:value-of select="../@separator"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>;</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:vector">
    <xsl:text>[</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../@separator">
            <xsl:value-of select="../@separator"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>;</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:matrix">
    <xsl:text>[</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:matrixrow">
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:if test="position()&lt;last()">
      <xsl:text>;</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="smtb:function">
    <xsl:text>//&#xA;</xsl:text>
    <xsl:text>function </xsl:text>
    <xsl:apply-templates select="smtb:output"/>
    <xsl:text>=</xsl:text>
    <xsl:apply-templates select="m:ci"/>
    <xsl:apply-templates select="smtb:input"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="smtb:body"/>
    <xsl:text>//&#xA;</xsl:text>
    <xsl:text>endfunction&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="smtb:output">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:input">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:body">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:comment">
    <!-- <xsl:if test="ancestor::smtb:body">
    <xsl:text>  </xsl:text>
    </xsl:if>-->
    <xsl:choose>
      <xsl:when test="$lang='scilab'">
        <xsl:text>//&#xA;</xsl:text>
        <xsl:value-of select="concat('// ',normalize-space(.),'&#xA;//')"/>
      </xsl:when>
      <xsl:when test="$lang='C'">
        <xsl:text>/* </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>*/</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="smtb:optimize">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:matrix-open"/>
  <xsl:template match="smtb:single-matrix-open"/>
 
  <xsl:template match="smtb:matrix-assignment"/>
  <xsl:template match="smtb:single-matrix-row"/>

  <xsl:template match="smtb:single-matrix-close">
  	<xsl:value-of select="concat(@id,'=[')"/>		
    <xsl:for-each select="preceding-sibling::smtb:single-matrix-row[@id=current()/@id]">
      <xsl:sort select="smtb:ci[1]" data-type="number" direction="ascending"/>
      <xsl:sort select="smtb:ci[2]" data-type="number" direction="ascending"/>
	    <xsl:for-each select="*">
	      <xsl:apply-templates select="."/>
	      <xsl:if test="position()&lt;last()">
	        <xsl:text>,</xsl:text>
	      </xsl:if>
	    </xsl:for-each>
	    <xsl:if test="position()&lt;last()">
	      <xsl:text>&#xA;</xsl:text>
	    </xsl:if>    	
		</xsl:for-each>		
    <xsl:value-of select="concat(']','&#xA;')"/>

  	<xsl:value-of select="concat(@id,'ij=',@id,'(:,1:2)&#xA;')"/>		
		<xsl:variable name="id" select="@id"/>
		<xsl:for-each select="str:split(str:padding(number(preceding-sibling::smtb:single-matrix-open[@id=current()/@id]/@cols)-2),'')">
			<xsl:value-of select="concat($id,position(),'=',$id,'(:,',2+position(),')&#xA;')"/>		
		</xsl:for-each>
		
  </xsl:template>

  <xsl:template match="smtb:matrix-close">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message><xsl:value-of select="concat('Assembling matrix ',@id)"/></xsl:message>
    </xsl:if>
    <!-- On collecte les affectations aux éléments d'une matrice donnée, en regroupant
       les affectations multiples à un même terme (i,j), qui sont par convention 
       interprétées comme une affectation d'une somme. Pour cela il faut identifier les
       affectations "uniques" et boucler sur les éventuels multiples. Cela se fait avec
       un système de clés (méthode Muenchienne), qui est beaucoup plus rapide (mais
       gourmand en mémoire) que la méthode "naïve" commentée ci-dessous. -->
    <!-- 
    <xsl:variable name="assignments">
      <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[@id=current()/@id]">
        <xsl:if test="not(following-sibling::smtb:matrix-assignment[(@id=current()/@id) and (@row=current()/@row) and (@col=current()/@col)])">
          <smtb:matrix-assignment id="{@id}" row="{@row}" col="{@col}">
            <m:apply>
              <m:plus/>
              <xsl:copy-of select="*"/>
              <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[(@id=current()/@id) and (@row=current()/@row) and (@col=current()/@col)]">
                <xsl:copy-of select="*"/>
              </xsl:for-each>
            </m:apply>
          </smtb:matrix-assignment>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:for-each select="exslt:node-set($assignments)/smtb:matrix-assignment">
      <xsl:value-of select="concat(@row,',',@col,',')"/>
      <xsl:apply-templates/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>&#xA;</xsl:text>
      </xsl:if>
    </xsl:for-each>
    -->
    <!-- Muenchienne method http://www.jenitennison.com/xslt/grouping/muenchian.html -->
    <xsl:variable name="optimize_id" select="generate-id(ancestor::smtb:optimize)"/>
    <xsl:value-of select="concat(@id,'_ijv=[')"/>
    <xsl:choose>
      <xsl:when test="preceding-sibling::smtb:matrix-open[@id=current()/@id][(@assignments='unique') or (@type='sparse')]">
        <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[@id=current()/@id]">
          <xsl:value-of select="concat(@row,',',@col,',')"/>
          <xsl:apply-templates/>
          <xsl:if test="position()&lt;last()">
            <xsl:text>&#xA;</xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="preceding::smtb:matrix-assignment[@id=current()/@id][count(. | key('MATRIX-ASSIGNMENTS-BY-ID-ROW-COL',concat(@id,' ',@row,' ',@col))[1])=1]">
          <xsl:variable name="assignments">
            <m:apply>
              <m:plus/>
              <xsl:for-each select="key('MATRIX-ASSIGNMENTS-BY-ID-ROW-COL',concat(@id,' ',@row,' ',@col))">
                <xsl:copy-of select="*"/>
              </xsl:for-each>
            </m:apply>
          </xsl:variable>
          <xsl:value-of select="concat(@row,',',@col,',')"/>
          <xsl:apply-templates select="exslt:node-set($assignments)/*"/>
          <xsl:if test="position()&lt;last()">
            <xsl:text>&#xA;</xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>];&#xA;</xsl:text>
    <xsl:for-each select="preceding-sibling::smtb:matrix-open[@id=current()/@id][1]">
    	<xsl:value-of select="concat(@id,'=s_',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,3),',@rows,',',@cols,');&#xA;')"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="smtb:rhs">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:lhs">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="m:ci">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="m:cn">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="smtb:script">
    <xsl:document href="{@href}" method="text" encoding="ISO-8859-15">
      <xsl:apply-templates/>
    </xsl:document>
  </xsl:template>

  <xsl:template match="smtb:string">
    <xsl:value-of select="concat('&quot;',normalize-space(.),'&quot;')"/>
  </xsl:template>

  <xsl:template match="smtb:for">
		<xsl:value-of select="concat('for ',@var,'=',@from,':',@to,'&#xA;')"/>
		<xsl:apply-templates/>
		<xsl:text>end&#xA;</xsl:text>			
  </xsl:template>


</xsl:stylesheet>