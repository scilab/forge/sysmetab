<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   2013/2014
    Project :   PIVERT/Metalippro-PL1
-->

<!-- This style sheet is intended to add the list of intermediate cumomers and the list of input cumomers as:

    <listOfIntermediateCumomers xmlns="http://www.utc.fr/sysmetab">
      <listOfCumomers weight="1">
        <cumomer id="A_1" pool="A" weight="1" number="1" pattern="x1"/>
        <cumomer id="A_2" pool="A" weight="1" number="2" pattern="1x"/>
        <cumomer id="D_1" pool="D" weight="1" number="3" pattern="1"/>
        <cumomer id="F_1" pool="F" weight="1" number="4" pattern="x1"/>
        <cumomer id="F_2" pool="F" weight="1" number="5" pattern="1x"/>
      </listOfCumomers>
      <listOfCumomers weight="2">
        <cumomer id="A_3" pool="A" weight="2" number="1" pattern="11"/>
        <cumomer id="F_3" pool="F" weight="2" number="2" pattern="11"/>
      </listOfCumomers>
    </listOfIntermediateCumomers>
    <listOfInputCumomers xmlns="http://www.utc.fr/sysmetab">
      <listOfCumomers weight="1">
        <cumomer id="A_out_1" pool="A_out" weight="1" number="1" pattern="x1"/>
        <cumomer id="A_out_2" pool="A_out" weight="1" number="2" pattern="1x"/>
      </listOfCumomers>
      <listOfCumomers weight="2">
        <cumomer id="A_out_3" pool="A_out" weight="2" number="1" pattern="11"/>
      </listOfCumomers>
    </listOfInputCumomers>

     We also display the comparison between the original number of cumomers and the newest one after graph
     simplification for the all network and for each cumomer weight :
    
    Cumomers : original=7, new=7
    Weight 1 : original=5, new=5
    Weight 2 : original=2, new=2
    
     At the end, we add a position number to each reaction.
-->

<xsl:stylesheet  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math f exslt">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/>

  <!-- Output file name -->

  <xsl:param name="output"/>

  <xsl:param name="verb_level">1</xsl:param>

  <!-- New graph of nodes inside the network, after simplification -->

  <xsl:param name="nodes"/>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>  

  <!-- Maximum weight of all cumomers inside the network -->
  
  <xsl:variable name="maxweight" select="math:max(//f:pool/@atoms)"/>
  
  <!-- sorted cumomers, original set or after graph simplification -->
  
  <xsl:variable name="allcumomers">
    <cumomer id="" weight="0" number="1" xmlns="http://www.utc.fr/sysmetab"/>
    <xsl:choose>
      <xsl:when test="exslt:node-set($params)[@name='minimize']='yes'">
        <xsl:for-each select="document($nodes,/)/graph/*">
			    <xsl:sort select="@weight" data-type="number"/>
				  <xsl:sort select="@type"/>
          <xsl:sort select="@pool" data-type="text"/>
          <xsl:sort select="@subscript" data-type="number"/>
			    <cumomer id="{@id}" pool="{@pool}" type="{@type}" weight="{@weight}" number="{position()+1}" pattern="{@pattern}" xmlns="http://www.utc.fr/sysmetab"/>
			  </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="//f:pool/smtb:cumomer">
  				<xsl:sort select="@weight" data-type="number"/>
  				<xsl:sort select="../@type"/>
          <xsl:sort select="@pool" data-type="text"/>
          <xsl:sort select="@subscript" data-type="number"/>
  				<cumomer id="{@id}" pool="{@pool}" type="{../@type}" weight="{@weight}" number="{position()+1}" pattern="{@pattern}" xmlns="http://www.utc.fr/sysmetab"/>
  			</xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
	</xsl:variable>


  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <!-- The following two templates will add the list of intermediate cumomers and the list of input cumomers -->
  <xsl:template match="f:metabolitepools">
    <metabolitepools xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </metabolitepools>
		<xsl:if test="$verb_level&gt;0">
      <xsl:message>
        <xsl:text>Cumomers : </xsl:text>
        <xsl:value-of select="concat('original=',count(f:pool[@type='intermediate']/smtb:cumomer))"/>
        <xsl:value-of select="concat(', new=',count(exslt:node-set($allcumomers)/smtb:cumomer[@type='intermediate']))" />
      </xsl:message>
    </xsl:if>
					
    <listOfIntermediateCumomers xmlns="http://www.utc.fr/sysmetab">
      <xsl:call-template name="make-cumomer-list">
        <xsl:with-param name="type" select="'intermediate'"/>
      </xsl:call-template>   
    </listOfIntermediateCumomers>
    <listOfInputCumomers xmlns="http://www.utc.fr/sysmetab">
      <xsl:call-template name="make-cumomer-list">
        <xsl:with-param name="type" select="'input'"/>
      </xsl:call-template>   
    </listOfInputCumomers>
  </xsl:template>

  <xsl:template name="make-cumomer-list">
    <xsl:param name="w">1</xsl:param>
    <xsl:param name="type"/>
    <xsl:if test="$w&lt;=$maxweight">
      <xsl:if test="$type='intermediate'">
    		<xsl:if test="$verb_level&gt;0">
          <xsl:message>
            <xsl:value-of select="concat('Weight ',$w,' :')"/>
            <xsl:text> original=</xsl:text>
            <xsl:value-of select="count(f:pool[@type=$type]/smtb:cumomer[(@weight=$w)])"/>
            <xsl:text>, new=</xsl:text>
            <xsl:value-of select="count(exslt:node-set($allcumomers)/smtb:cumomer[(@weight=$w) and (@type=$type)])"/>
          </xsl:message>
        </xsl:if>
      </xsl:if>
      <xsl:if test="exslt:node-set($allcumomers)/smtb:cumomer[(@weight=$w) and (@type=$type)]">
        <listOfCumomers xmlns="http://www.utc.fr/sysmetab" weight="{$w}"
					first="{math:min(exslt:node-set($allcumomers)/smtb:cumomer[(@weight=$w) and (@type=$type)]/@number)}" 
					last="{math:max(exslt:node-set($allcumomers)/smtb:cumomer[(@weight=$w) and (@type=$type)]/@number)}">
					<xsl:for-each select="exslt:node-set($allcumomers)/smtb:cumomer[(@weight=$w) and (@type=$type)]">
						<cumomer>
							<xsl:copy-of select="@*"/>
							<xsl:attribute name="local-number">
								<xsl:value-of select="position()"/>
							</xsl:attribute>
						</cumomer>
					</xsl:for-each>
				</listOfCumomers>
      </xsl:if>
      <xsl:call-template name="make-cumomer-list">
        <xsl:with-param name="w" select="($w)+1"/>
        <xsl:with-param name="type" select="$type"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Add position number to each reaction -->
  <xsl:template match="f:reaction">
    <xsl:variable name="pos" select="count(preceding-sibling::f:reaction)+1"/>
    <reaction id="{@id}" position="{$pos}" xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="*"/>
    </reaction>
  </xsl:template>

</xsl:stylesheet>