<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   2013/2014
    Project :   PIVERT/Metalippro-PL1
-->

<!-- In this style sheet, each reaction is written in it's bidirectional form (the forward and the backward one),
     for example, for the reaction "v1" :
     
     <reaction id="v1">
       <reduct cfg="IJ" id="A">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="F"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="F"/>
       </reduct>
       <rproduct cfg="IJ" id="F">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="A"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="A"/>
       </rproduct>
     </reaction>
     
     we obtain :
     
     <reaction id="v1_f" pos="1">
       <reduct cfg="IJ" id="A">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="F"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="F"/>
       </reduct>
       <rproduct cfg="IJ" id="F">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="A"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="A"/>
       </rproduct>
     </reaction>
     
     and
     
     <reaction id="v1_b" pos="1">
       <reduct cfg="IJ" id="F">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="A"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="A"/>
       </reduct>
       <rproduct cfg="IJ" id="A">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="F"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="F"/>
       </rproduct>
     </reaction>

     In other hand, we write for each input pool, the contribution of input isotopomers to cumomers, example : 
     for the input pool "A_out" we obtain :
     
     <pool atoms="2" id="A_out" type="input">
       <input xmlns="http://www.utc.fr/sysmetab" string="01">
         <cumomer-contribution subscript="1" string="x1" weight="1" sign="1"/>
       </input>
       <input xmlns="http://www.utc.fr/sysmetab" string="10">
         <cumomer-contribution subscript="2" string="1x" weight="1" sign="1"/>
       </input>
       <input xmlns="http://www.utc.fr/sysmetab" string="11">
         <cumomer-contribution subscript="1" string="x1" weight="1" sign="1"/>
         <cumomer-contribution subscript="2" string="1x" weight="1" sign="1"/>
         <cumomer-contribution subscript="3" string="11" weight="2" sign="1"/>
       </input>
     </pool>

     At the end, we add the implicit constraints on input and output fluxes such that the net fluxes from input 
     metabolites or to output metabolites are necessarily positive and the xch fluxes from input metabolites or 
     to output metabolites are necessarily zero and other xch fluxes are positive.
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  exclude-result-prefixes="xsl smtb math m f">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/>

  <!-- find all label input from f:configuration -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="f:info"/>
      <xsl:apply-templates select="f:reactionnetwork"/>
      <xsl:apply-templates select="f:constraints"/>
      <xsl:apply-templates select="f:configuration"/>
    </fluxml>
  </xsl:template>

  <!-- The following two templates write each reaction (for example "v1") in its bidirectional form 
       ("v_f", "v_b"). -->
  <xsl:template match="f:reactionnetwork">
    <reactionnetwork xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="f:metabolitepools"/>
      <xsl:for-each select="f:reaction">
        <xsl:call-template name="reactionfb">
          <xsl:with-param name="id" select="concat(@id,'_f')"/>
          <xsl:with-param name="pos" select="position()"/>
        </xsl:call-template>
      </xsl:for-each>
      <xsl:for-each select="f:reaction">
        <xsl:call-template name="reactionfb">
          <xsl:with-param name="id" select="concat(@id,'_b')"/>
          <xsl:with-param name="pos" select="position()"/>
        </xsl:call-template>
      </xsl:for-each>
    </reactionnetwork>
  </xsl:template>

  <xsl:template name="reactionfb">
    <xsl:param name="id"/>
    <xsl:param name="pos"/>
    <xsl:choose>
      <xsl:when test="substring($id, string-length($id) - string-length('_f') + 1)='_f'">
        <reaction id="{$id}" pos="{$pos}" xmlns="http://www.13cflux.net/fluxml">
          <xsl:apply-templates/>
        </reaction>
      </xsl:when>
      <xsl:when test="substring($id, string-length($id) - string-length('_b') + 1)='_b'">
        <reaction id="{$id}" pos="{$pos}" xmlns="http://www.13cflux.net/fluxml">
          <xsl:for-each select="f:rproduct">
            <reduct>
              <xsl:copy-of select="@*|*"/>
            </reduct>
          </xsl:for-each>
          <xsl:for-each select="f:reduct">
            <rproduct>
              <xsl:copy-of select="@*|*"/>
            </rproduct>
          </xsl:for-each>
        </reaction>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- The following template writes, for each input pool, the contribution of input isotopomers to cumomers -->
  <xsl:template match="smtb:input">
    <input xmlns="http://www.utc.fr/sysmetab">
      <xsl:copy-of select="@*"/>
      <xsl:variable name="string" select="@string"/>
      <xsl:for-each select="../smtb:cumomer">
        <xsl:sort select="@weight"/>
        <xsl:if test="count(smtb:carbon[substring($string,string-length($string)-@index,1)='1'])=count(smtb:carbon)">
          <cumomer-contribution subscript="{@subscript}" string="{@pattern}" weight="{@weight}" sign="1"/>
        </xsl:if>
      </xsl:for-each>
    </input>
  </xsl:template>

  <!-- This template adds the implicit constraints on input and output fluxes -->
  <xsl:template match="f:constraints">
    <constraints xmlns="http://www.13cflux.net/fluxml">
      <net xmlns="http://www.13cflux.net/fluxml">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:apply-templates select="f:net/m:math/*"/>
          <xsl:for-each select="../f:reactionnetwork/f:reaction[key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct)]">
            <!-- net fluxes from input metabolites or to output metabolites are necessarily positive -->
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <leq/>
              <cn>MIN_INOUT</cn>
              <ci><xsl:value-of select="@id"/></ci>
            </apply>
          </xsl:for-each>
        </math>
      </net>
      <xch xmlns="http://www.13cflux.net/fluxml">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:apply-templates select="f:xch/m:math/*"/>
          <xsl:for-each select="../f:reactionnetwork/f:reaction">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <xsl:choose>
                <xsl:when test="key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct)">
                  <!-- xch fluxes from input metabolites or to output metabolites are necessarily zero -->
                  <eq/>
                </xsl:when>
                <xsl:otherwise>
                  <!-- other xch fluxes are positive -->
                  <leq/>
                </xsl:otherwise>
              </xsl:choose>
              <cn>0</cn>
              <ci><xsl:value-of select="@id"/></ci>
            </apply>
            <xsl:choose>
              <xsl:when test="key('INPUT',f:reduct/@id) or key('INPUT',f:rproduct/@id) or not(f:rproduct)"/>
              <xsl:otherwise>
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <leq/>
                  <ci><xsl:value-of select="@id"/></ci>
                  <cn><xsl:value-of select="'MAX_XCH'"/></cn>
                </apply>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </math>
      </xch>
    </constraints>
  </xsl:template>

</xsl:stylesheet>