<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- Cette feuille de style a pour but de générer différentes fonctions spécifiques
  d'un modèle donné sous forme FML -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- Output file name -->
  <xsl:param name="output"/>
	
  <xsl:param name="language">scilab</xsl:param>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <!-- Mathml generation of the code, this templates is common between stationnary case or dynamical one -->
  
	<xsl:include href="fml_common_gen_mathml.xsl"/>

  <!-- Definition of all templates using in this style sheet -->
	
  <xsl:include href="fmlsys_gen_mathml_solve_stat_templates.xsl"/>

  <!-- find all f:datum -->
  
	<xsl:key name="DATUM" match="f:datum" use="@id"/>

  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">

    <!-- Solve cumomers, cost function and gradient of the cost function -->

    <xsl:call-template name="solveCumomersAndGrad">
      <xsl:with-param name="method">Direct</xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="solveCumomersAndGrad">
      <xsl:with-param name="method">derivative</xsl:with-param>
    </xsl:call-template>
		
    <xsl:call-template name="solveCumomersAndGrad">
      <xsl:with-param name="method">Adjoint</xsl:with-param>
    </xsl:call-template>
    
		<xsl:call-template name="solveCumomersAndGrad">
      <xsl:with-param name="method">none</xsl:with-param>
    </xsl:call-template>

    <!-- Building input matrices, allowing to obtain the contribution over the input cumomers and isotopomers 
         (in fml_common_gen_mathml.xsl) -->
		
    <xsl:call-template name="construct_matrix_D"/>
    
		<!-- Computation function of C and b for the contraints on the fluxes Cv=b (in fml_common_gen_mathml.xsl) -->
    
		<xsl:call-template name="fluxSubspace"/>
    
		<xsl:call-template name="construct_matrix_E"/>
    
		<!-- Fluxes name matrices, Fluxes ids matrices (in fml_common_gen_mathml.xsl) -->
    
		<xsl:call-template name="names_and_ids_fluxes"/>
    
		<!-- Script for Direct computation of cumomers. This script is running in XMLlab application, once a flux 
         is changed or once we exit the optimization with new values of fluxes.
         path=get_absolute_file_path("exslt:node-set($params)[@name='file']_direct.sce"); 
         err=chdir(path);
         err=exec("exslt:node-set($params)[@name='file'].sci");
    -->
    
		<script href="{exslt:node-set($params)[@name='output']}_direct.sce" xmlns="http://www.utc.fr/sysmetab">

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>err</ci>
        <apply>
          <fn>
            <ci>exec</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/sysmetab_lib.sci')"/>
          </string>
          <cn>-1</cn>
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>err</ci>
        <apply>
          <fn>
            <ci>exec</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/fsqp.sci')"/>
          </string>
          <cn>-1</cn>
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>err</ci>
        <apply>
          <fn>
            <ci>exec</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/spset/loader.sce')"/>
          </string>
          <cn>-1</cn>
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>err</ci>
        <apply>
          <fn>
            <ci>exec</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/fsqp/loader.sce')"/>
          </string>
          <cn>-1</cn>
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>path</ci>
        <apply>
          <fn>
            <ci>get_absolute_file_path</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='output'],'_direct.sce')"/>
          </string>
        </apply>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>err</ci>
        <apply>
          <fn>
            <ci>chdir</ci>
          </fn>
          <ci xmlns="http://www.utc.fr/sysmetab">path</ci>
        </apply>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>err</ci>
        <apply>
          <fn>
            <ci>exec</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='file'],'_solve_state_equation.sci')"/>
          </string>
          <cn>-1</cn>
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>err</ci>
        <apply>
          <fn>
            <ci>exec</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='output'],'.sci')"/>
          </string>
          <cn>-1</cn>
        </apply>
      </apply>
  
      <comment> Input cumomers, computed from isotopomers of input metabolites.</comment>

	    <xsl:for-each select="f:configuration/f:input">
	      <apply xmlns="http://www.w3.org/1998/Math/MathML">
	        <eq/>
	        <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
	        <list separator=";">
	          <xsl:for-each select="f:label">
	            <cn>
	              <xsl:value-of select="."/>
	            </cn>
	          </xsl:for-each>
	        </list>
	      </apply>    
	    </xsl:for-each>
		
	      <apply xmlns="http://www.w3.org/1998/Math/MathML">
	        <eq/>
	        <ci>Xinp</ci>
	        <apply>
	          <times/>
	          <ci>D</ci>
	          <vector>
							<ci>1</ci>
	            <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[smtb:input]">
	              <ci><xsl:value-of select="concat(@id,'_input')"/></ci>
	            </xsl:for-each>
	          </vector>
	        </apply>
	      </apply>
      
      <comment>Computation of an eligible vector of fluxes wadm (net,xch)</comment>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>me</ci>
        <cn>
          <xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']) + count(f:constraints/f:net/m:math/m:apply[m:eq]) + count(f:constraints/f:xch/m:math/m:apply[m:eq])"/>
        </cn>
      </apply>

      <!-- [W,ff,ftype]=freefluxes(C,b,me); -->
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>W</ci>
          <ci>ff</ci>
          <ci>ftype</ci>
        </list>
        <apply>
          <fn><ci>freefluxes</ci></fn>
          <ci type="matrix">C</ci>
          <ci type="vector">b</ci>
          <ci>me</ci>
        </apply>
      </apply>

      <!-- [flux]=qld_test(C,b,me,Flux_M,b_Flux); -->
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>flux</ci>
        </list>
        <xsl:choose>
          <xsl:when test="(f:configuration/f:simulation/f:variables/f:fluxvalue) and exslt:node-set($params)[@name='freefluxes']='user'">
            <apply>
              <fn><ci>qld_test</ci></fn>
              <ci>C</ci>
              <ci>b</ci>
              <ci>me</ci>
              <ci>Flux_M</ci>
              <ci>b_Flux</ci>
            </apply>
          </xsl:when>
          <xsl:otherwise>
            <apply>
              <fn><ci>qld_test</ci></fn>
              <ci>C</ci>
              <ci>b</ci>
              <ci>me</ci>
            </apply>
          </xsl:otherwise>
        </xsl:choose>
      </apply>
      <!-- Admissible w : wadm=flux.values; -->
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>wadm</ci>
        <ci>flux.values</ci>
      </apply>
          <xsl:choose>
            <xsl:when test="exslt:node-set($params)[@name='zc']='yes'">
              <comment>Regularization term for the grad computation</comment>
              <!-- eps_phi=1e-3; -->
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>eps_phi</ci>
                <cn>1e-6</cn>
              </apply>
              <comment>Zero Crossing method</comment>
              <!-- method='zc'; -->
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>method</ci>
                <ci>'zc'</ci>
              </apply>
              <!-- [C,b]=Add_constraints(flux.values,method); -->
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <list separator=",">
                  <ci>C</ci>
                  <ci>b</ci>
                </list>
                <apply>
                  <fn><ci>Add_constraints</ci></fn>
                  <ci>flux.values</ci>
                  <ci>method</ci>
                </apply>
              </apply>
            </xsl:when>
          </xsl:choose>
          <comment>Initial guess to start the optimization</comment>


          <!-- the freefluxes qstart=wadm(ff); -->
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>q_start</ci>
            <apply>
              <fn><ci>wadm</ci></fn>
              <ci>ff</ci>
            </apply>
          </apply>
          <!-- w0=wadm-W*q; -->
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>w0</ci>
            <apply>
              <minus/>
              <ci>wadm</ci>
              <apply>
                <times/>
                <ci>W</ci>
                <ci>q_start</ci>
              </apply>
            </apply>
          </apply>
          <!-- omega=[...]; -->
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>omega_start</ci>
            <apply>
              <vector>
                <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group">
                  		<cn>1</cn>
                </xsl:for-each>
              </vector>
            </apply>
          </apply>

          <comment>Lower and upper bound for scaling parameter omega</comment>

          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>omega_min</ci>
            <apply>
              <vector>
                <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group">
                  <xsl:choose>
                    <xsl:when test="exslt:node-set($params)[@name='scaling']='yes'">
                      <cn>0</cn>
                    </xsl:when>
                    <xsl:when test="exslt:node-set($params)[@name='scaling']='no'">
                      <cn>1</cn>
                    </xsl:when>
                    <xsl:when test="not(@scale)">
                      <cn>1</cn>
                    </xsl:when>
                    <xsl:otherwise>
                      <cn>0</cn>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </vector>
            </apply>
          </apply>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>omega_max</ci>
            <apply>
              <vector>
                <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group">
                  <xsl:choose>
                    <xsl:when test="exslt:node-set($params)[@name='scaling']='yes'">
                      <cn>1e10</cn>
                    </xsl:when>
                    <xsl:when test="exslt:node-set($params)[@name='scaling']='no'">
                      <cn>1</cn>
                    </xsl:when>
                    <xsl:when test="not(@scale)">
                      <cn>1</cn>
                    </xsl:when>
                    <xsl:otherwise>
                      <cn>1e10</cn>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </vector>
            </apply>
          </apply>

      <xsl:choose>
        <xsl:when test="exslt:node-set($params)[@name='timer']='yes'">	
					<for var="i" from="1" to="1000" xmlns="http://www.utc.fr/sysmetab">
						<apply xmlns="http://www.w3.org/1998/Math/MathML">
							<eq/>
							<apply>
								<selector/>
								<ci type='matrix'>
									<xsl:value-of select="concat('tAdjoint',$maxweight)"/>
								</ci>
								<cn>:</cn>
								<ci>i</ci>
							</apply>
							<apply>
								<fn><ci>solveCumomersAndGradAdjoint</ci></fn>
								<ci>wadm</ci>
								<ci>omega_start</ci>
								<ci>Xinp</ci>
							</apply>
						</apply>
						<apply xmlns="http://www.w3.org/1998/Math/MathML">
							<eq/>
							<apply>
								<selector/>
								<ci type='matrix'>
									<xsl:value-of select="concat('tDirect',$maxweight)"/>
								</ci>
								<cn>:</cn>
								<ci>i</ci>
							</apply>
							<apply>
								<fn><ci>solveCumomersAndGradDirect</ci></fn>
								<ci>wadm</ci>
								<ci>omega_start</ci>
								<ci>Xinp</ci>
							</apply>
						</apply>
					</for>
				</xsl:when>			
        <xsl:when test="exslt:node-set($params)[@name='optimize']='yes'">
					
          <comment>Lower and upper bound for free fluxes (other constraints can be taken into account via C and b matrices)</comment>

          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>q_max</ci>
            <apply>
              <times/>
              <cn>1e10</cn>
              <apply>
                <fn><ci>ones</ci></fn>
                <ci>q_start</ci>
              </apply>
            </apply>
          </apply>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>q_min</ci>
            <apply>
              <minus/>
                <ci>q_max</ci>
            </apply>
          </apply>
          
          <comment>Verbosity level (iprint)</comment>
          
          <xsl:choose>
            <xsl:when test="exslt:node-set($params)[@name='optimize_method']='fsqp'">
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>iprint</ci>
                <cn>1</cn>
              </apply>
            </xsl:when>
            <xsl:when test="exslt:node-set($params)[@name='optimize_method']='ipopt'">
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>iprint</ci>
                <cn>5</cn>
              </apply>
            </xsl:when>
          </xsl:choose>
					
					<comment>Choice of the method for gradient computation</comment>  

          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>solveCumomersAndGrad</ci>
              <xsl:choose>
                <xsl:when test="exslt:node-set($params)[@name='gradient']='adjoint'">
                  <ci>solveCumomersAndGradAdjoint</ci>
                </xsl:when>
                <xsl:otherwise>
                  <ci>solveCumomersAndGradDirect</ci>
                </xsl:otherwise>
              </xsl:choose>
          </apply>
					                   
         <comment>Call the optimization function (fsqp)</comment>

          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <list separator=",">
              <ci>q</ci>
              <ci>omega</ci>
              <ci>f</ci>
              <ci>info</ci>
              <ci>message</ci>
            </list>
            <apply>
                  <fn><ci>optimize</ci></fn>
              <smtb:string>
                <xsl:value-of select="exslt:node-set($params)[@name='optimize_method']"/>
              </smtb:string>
              <ci>q_start</ci>
              <ci>q_min</ci>
              <ci>q_max</ci>
              <ci>omega_start</ci>
              <ci>omega_min</ci>
              <ci>omega_max</ci>
              <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_grad']"/></cn>
              <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_reg']"/></cn>
              <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_phi']"/></cn>
              <cn><xsl:value-of select="exslt:node-set($params)[@name='max_iter']"/></cn>
              <ci>iprint</ci>
              <xsl:choose>
                <xsl:when test="(starts-with(exslt:node-set($params)[@name='stats'],'mc')) and (number(substring-after(exslt:node-set($params)[@name='stats'],':')) &gt; 0)">
                  <smtb:string>mc</smtb:string>
                  <ci>
                    <xsl:value-of select="round(number(substring-after(exslt:node-set($params)[@name='stats'],':')))"/>
                  </ci>
                </xsl:when>
                <xsl:when test="(starts-with(exslt:node-set($params)[@name='freefluxes'],'multi')) and (number(substring-after(exslt:node-set($params)[@name='freefluxes'],':')) &gt; 0)">
                  <smtb:string>multi</smtb:string>
                  <ci>
                    <xsl:value-of select="round(number(substring-after(exslt:node-set($params)[@name='freefluxes'],':')))"/>
                  </ci>                  
                </xsl:when>
              </xsl:choose>
            </apply>
          </apply>

        </xsl:when>
        <xsl:otherwise>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>q</ci>
            <ci>q_start</ci>
          </apply>  
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>omega</ci>
            <ci>omega_start</ci>
          </apply>  
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>info</ci>
            <ci>0</ci>
          </apply>  
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>message</ci>
            <vector/>
          </apply>  
        </xsl:otherwise>
      </xsl:choose>

			<xsl:if test="exslt:node-set($params)[@name='timer']='no'">
      
	      <!-- [flux_opt,scale_opt,meas,norm_residu,norm_fm,X]=measurements_and_stats(q,omega); -->

	      <comment>Compute measurements, states and statistics</comment>
  
	      <apply xmlns="http://www.w3.org/1998/Math/MathML">
	        <eq/>
	        <list separator=",">
	          <ci>flux_opt</ci>
	          <ci>scale_opt</ci>
	          <ci>meas</ci>
	          <ci>label_error</ci>
	          <ci>norm_fm</ci>
	          <ci>X</ci>
	        </list>
	        <apply>
	          <fn><ci>measurements_and_stats</ci></fn>
	          <ci>q</ci>
	          <ci>omega</ci>
	          <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_phi']"/></cn>
	          <ci>info</ci>
	        </apply>
	      </apply>
  
	      <xsl:if test="exslt:node-set($params)[@name='optimize']='yes'">
	      <apply xmlns="http://www.w3.org/1998/Math/MathML">
	        <eq/>
	        <ci>err</ci>
	        <apply>
	          <fn><ci>writeMessage</ci></fn>
	          <ci>message</ci>
	          <string xmlns="http://www.utc.fr/sysmetab">
	            <xsl:value-of select="exslt:node-set($params)[@name='output']"/>                
	          </string>
	        </apply>
	      </apply>
	    </xsl:if>
	      <!-- results_fwd(flux_no_opt,"step3.xml","exslt:node-set($params)[@name='file']"); -->


	      <comment>update step3.xml file with new results and save them in a fwd file</comment>

	      <apply xmlns="http://www.w3.org/1998/Math/MathML">
	        <eq/>
	        <ci>err</ci>
	        <apply>
	          <fn><ci>results_fwd</ci></fn>
	          <ci>flux_opt</ci>
	          <ci>scale_opt</ci>
	          <ci>meas</ci>
	          <ci>X</ci>
	          <ci>label_error</ci>
	          <ci>message</ci>
	          <string xmlns="http://www.utc.fr/sysmetab">
	            <xsl:value-of select="exslt:node-set($params)[@name='file']"/>                
	          </string>
	          <string xmlns="http://www.utc.fr/sysmetab">
	            <xsl:value-of select="exslt:node-set($params)[@name='output']"/>                
	          </string>
	          <string xmlns="http://www.utc.fr/sysmetab">
	            <xsl:value-of select="exslt:node-set($params)[@name='sysmetab']"/>                
	          </string>
	        </apply>
	      </apply>
			      
	      <xsl:if test="exslt:node-set($params)[@name='computation']='yes'">
	        <comment>Quit Scilab</comment>
	        <apply xmlns="http://www.w3.org/1998/Math/MathML">
	          <eq/>
	          <ci>err</ci>
	          <apply>
	            <fn><ci>execstr</ci></fn>
	            <string xmlns="http://www.utc.fr/sysmetab">quit</string>
	            <string xmlns="http://www.utc.fr/sysmetab">errcatch</string>
	          </apply>
	        </apply>

	        <apply xmlns="http://www.w3.org/1998/Math/MathML">
	          <eq/>
	          <ci>err</ci>
	          <apply>
	            <fn><ci>execstr</ci></fn>
	            <string xmlns="http://www.utc.fr/sysmetab">quit</string>
	            <string xmlns="http://www.utc.fr/sysmetab">errcatch</string>
	          </apply>
	        </apply>
	      </xsl:if>
      
			</xsl:if>
			
    </script>
  </xsl:template>

</xsl:stylesheet>