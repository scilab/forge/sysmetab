<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:ftbl="http://ftbl.org"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func ftbl math">
  
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>
  
  <xsl:param name="date"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->
  
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()">    
      <xsl:copy>
          <xsl:apply-templates select="*|@*|text()"/>
      </xsl:copy>
  </xsl:template>

  <xsl:template match="ftbl">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="section[@id='PROJECT']"/>    
      <xsl:apply-templates select="section[@id='NETWORK']"/>
      <constraints xmlns="http://www.13cflux.net/fluxml">
        <net>
          <math xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:apply-templates select="section[@id='EQUALITIES']/subsection[@id='NET']"/>
            <xsl:apply-templates select="section[@id='INEQUALITIES']/subsection[@id='NET']"/>
            <xsl:apply-templates select="section[@id='FLUXES']/subsection[@id='NET']" mode="constraints"/>
          </math>
        </net>
        <xch>
          <math xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:apply-templates select="section[@id='EQUALITIES']/subsection[@id='XCH']"/>
            <xsl:apply-templates select="section[@id='INEQUALITIES']/subsection[@id='XCH']"/>
            <xsl:apply-templates select="section[@id='FLUXES']/subsection[@id='XCH']" mode="constraints"/>             
          </math>
        </xch>
      </constraints>
      <configuration name="default" stationary="true" xmlns="http://www.13cflux.net/fluxml">
        <xsl:apply-templates select="section[@id='LABEL_INPUT']"/>
        <measurement>
          <model>
            <labelingmeasurement>
              <xsl:apply-templates select="section[@id='LABEL_MEASUREMENTS']" mode="group"/>
              <xsl:apply-templates select="section[@id='MASS_SPECTROMETRY']" mode="group"/>
            </labelingmeasurement>
            <fluxmeasurement>
              <xsl:apply-templates select="section[@id='FLUX_MEASUREMENTS']" mode="netflux"/>
            </fluxmeasurement>
            <!--><peakmeasurements>
              <xsl:apply-templates select="section[@id='PEAK_MEASUREMENTS']"/>
            </peakmeasurements>-->
          </model>
          <data>
            <xsl:apply-templates select="section[@id='FLUX_MEASUREMENTS']" mode="data"/>
            <xsl:apply-templates select="section[@id='LABEL_MEASUREMENTS']" mode="data"/>
            <xsl:apply-templates select="section[@id='MASS_SPECTROMETRY']" mode="data"/>
          </data>
        </measurement>
        <xsl:apply-templates select="section[@id='FLUXES']" mode="configuration"/>
      </configuration>
    </fluxml>
  </xsl:template>

  <xsl:template match="section[@id='PROJECT']">
    <info xmlns="http://www.13cflux.net/fluxml">
      <name>
        <xsl:value-of select="row[2]/value[@position=2]"/>
      </name>
      <version>
        <xsl:value-of select="row[2]/value[@position=3]"/>
      </version>
      <date>
        <xsl:value-of select="$date"/>
      </date>
      <comment>
        <xsl:value-of select="row[2]/value[@position=6]"/>
      </comment>
    </info>
  </xsl:template>
   
  <xsl:template match="section[@id='NETWORK']">
    <reactionnetwork xmlns="http://www.13cflux.net/fluxml">  
      <metabolitepools>
        <xsl:for-each select="row[position() mod 2 = 0]/value[(@position &lt;= 6) and (@position &gt;=3)]">
          <xsl:if test="not(.=../preceding-sibling::row[position() mod 2 = 0]/value[(@position &lt;= 6) and (@position &gt;=3)]) and not(.=preceding-sibling::value[(@position &lt;= 6) and (@position &gt;=3)])">
            <pool id="{.}"/>
          </xsl:if>
        </xsl:for-each>
      </metabolitepools>
      <xsl:for-each select="row[position() mod 2 = 0]"> 
        <reaction id="{value[1]}" xmlns="http://www.13cflux.net/fluxml">
          <xsl:for-each select="value[(@position=3) or (@position=4)]">
            <reduct cfg="{substring-after(../following-sibling::row[1]/value[@position=current()/@position],'#')}" id="{.}"/>
          </xsl:for-each>
          <xsl:for-each select="value[(@position=5) or (@position=6)]">
            <rproduct cfg="{substring-after(../following-sibling::row[1]/value[@position=current()/@position],'#')}" id="{.}"/>
          </xsl:for-each>
        </reaction>
      </xsl:for-each>
    </reactionnetwork>
  </xsl:template>


  <xsl:template match="section[@id='LABEL_INPUT']">
    <xsl:for-each select="subsection">
      <input xmlns="http://www.13cflux.net/fluxml" pool="{@id}" type="isotopomer">
        <xsl:for-each select="row">
          <label cfg="{substring-after(value[@position=3],'#')}">
            <xsl:value-of select="value[@position=4]"/>
          </label>
        </xsl:for-each>
      </input>
    </xsl:for-each>
  </xsl:template>

<!--  <xsl:template match="section[@id='PEAK_MEASUREMENTS']">
    <xsl:for-each select="subsection">
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="peak_no" select="@peak_no"/>
      <xsl:variable name="pos">
        <xsl:number value="position()" format="00001"/>
      </xsl:variable>
      <peak id="{concat($id,'_',@peak_no,'_',$pos)}" pos="{position()}" value_s="{row/value[@position=4]}" value_dm="{row/value[@position=5]}" 
      value_dp="{row/value[@position=6]}" value_dd="{row/value[@position=7]}" value_t="{row/value[@position=8]}" deviation_s="{row/value[@position=9]}" 
      deviation_dm="{row/value[@position=10]}" deviation_dp="{row/value[@position=11]}" 
      deviation_ddt="{row/value[@position=12]}" xmlns="http://www.13cflux.net/fluxml"/>
    </xsl:for-each>
  </xsl:template>-->

  <xsl:template match="section[@id='LABEL_MEASUREMENTS']" mode="group">
    <xsl:for-each select="subsection">
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="group" select="@group"/>
      <xsl:variable name="pos">
        <xsl:number value="position()" format="00001"/>
      </xsl:variable>
      <group id="{concat('lmcg_',$group,'_',$pos)}" scale="auto" xmlns="http://www.13cflux.net/fluxml">
        <textual>
          <xsl:for-each select="row">
            <xsl:choose>
              <xsl:when test="contains(value[@position=6],'+')">
                <xsl:for-each select="str:split(normalize-space(translate(value[@position=6],'&#10;&#13;','')),'+')">
                  <xsl:value-of select="concat($id,.)"/>
                  <xsl:if test="position() &lt; last()">
                    <xsl:text>+ </xsl:text>
                  </xsl:if>
                </xsl:for-each>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat($id,value[@position=6])"/>
                <xsl:if test="position() &lt; last()">
                  <xsl:text>; </xsl:text>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
<!--          <xsl:choose>
            <xsl:when test="(contains(value[@position=6],'0')) and (contains(value[@position=6],'1'))">
              <xsl:value-of select="concat(../@id,value[@position=6])"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="../@id"/>[<xsl:for-each select="str:split(substring-after(value[@position=6],'#'),'')">
               <xsl:choose>
                <xsl:when test=".='1'">
                  <xsl:value-of select="position()"/>
                  <xsl:if test="position()!=last()">
                    <xsl:text>,</xsl:text>
                  </xsl:if>
                </xsl:when>
                <xsl:when test=".='0'">
                  <xsl:value-of select="position()"/>
                  <xsl:if test="position()!=last()">
                    <xsl:text>,</xsl:text>
                  </xsl:if>
                </xsl:when>
               </xsl:choose>
              </xsl:for-each>]#M<xsl:value-of select="string-length(value[@position=6])-string-length(translate(value[@position=6],'1',''))"/>
            </xsl:otherwise>
          </xsl:choose>
          -->
        </textual>
      </group>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="section[@id='MASS_SPECTROMETRY']" mode="group">
    <xsl:for-each select="subsection">
      <group id="{concat('ms_group_',position())}" scale="auto" xmlns="http://www.13cflux.net/fluxml">
        <textual>
          <xsl:value-of select="concat(@id,'[',row[1]/value[@position=3],']#M')"/>
          <xsl:for-each select="row">
            <xsl:value-of select="value[@position=4]"/>
            <xsl:if test="position()!=last()">
              <xsl:text>,</xsl:text>
            </xsl:if>
          </xsl:for-each>
        </textual>
      </group>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="section[@id='FLUX_MEASUREMENTS']" mode="netflux">
    <xsl:for-each select="row[position()&gt;1]">
      <xsl:variable name="pos" select="position()"/>
      <netflux id="fm_{$pos}" xmlns="http://www.13cflux.net/fluxml">
        <textual><xsl:value-of select="value[@position=2]"/></textual>
      </netflux>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="section[@id='FLUX_MEASUREMENTS']" mode="data">
    <xsl:for-each select="row[position()&gt;1]">
      <xsl:variable name="pos" select="position()"/>
      <datum id="fm_{$pos}" stddev="{value[@position=4]}" xmlns="http://www.13cflux.net/fluxml"><xsl:value-of select="value[@position=3]"/></datum>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="section[@id='LABEL_MEASUREMENTS']" mode="data">
    <xsl:for-each select="subsection">
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="group" select="@group"/>
      <xsl:variable name="pos">
        <xsl:number value="position()" format="00001"/>
      </xsl:variable>
      <xsl:for-each select="row">
        <datum id="{concat('lmcg_',$group,'_',$pos)}" row="{position()}"
               stddev="{value[@position=5]}" xmlns="http://www.13cflux.net/fluxml">
               <xsl:value-of select="value[@position=4]"/></datum>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="section[@id='MASS_SPECTROMETRY']" mode="data">
    <xsl:for-each select="subsection">
      <xsl:variable name="pos" select="position()"/>
      <xsl:for-each select="row">
        <datum id="ms_group_{$pos}" stddev="{value[@position=6]}" weight="{value[@position=4]}" xmlns="http://www.13cflux.net/fluxml"><xsl:value-of select="value[@position=5]"/></datum>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="section[@id='FLUXES']" mode="configuration">  
    <simulation method="cumomer" type="auto" xmlns="http://www.13cflux.net/fluxml">
      <variables>
        <xsl:for-each select="subsection[@id='NET']/row[value[@position=5]][position()&gt;1]">
          <xsl:if test="value[@position=4]='F'">
            <fluxvalue flux="{value[@position=3]}" type="net">
              <xsl:value-of select="value[@position=5]"/>
            </fluxvalue>
          </xsl:if>
        </xsl:for-each>
        <xsl:for-each select="subsection[@id='XCH']/row[value[@position=5]][position()&gt;1]">
          <xsl:if test="value[@position=4]='F'">
            <fluxvalue flux="{value[@position=3]}" type="xch">
              <xsl:choose>
                <xsl:when test="value[@position=5]='0'">
                  <xsl:value-of select="0"/>
                </xsl:when>
                <xsl:when test="value[@position=5]='1'">
                  <xsl:value-of select="1.0e37"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="(value[@position=5]) div (1 - value[@position=5])"/>
                </xsl:otherwise>
              </xsl:choose>
            </fluxvalue>
          </xsl:if>
        </xsl:for-each>
      </variables>
    </simulation>
  </xsl:template>

  <xsl:template match="section[@id='FLUXES']/subsection" mode="constraints">
    <xsl:for-each select="row[value[@position=5]][position()&gt;1]">
      <xsl:if test="value[@position=4]='C'">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci><xsl:value-of select="value[@position=3]"/></ci>
          <xsl:if test="../@id='XCH'">
            <xsl:choose>
              <xsl:when test="value[@position=5]='0'">
                <cn>0</cn>
              </xsl:when>
              <xsl:when test="value[@position=5]='1'">
                <cn>1.0e37</cn>
              </xsl:when>
              <xsl:otherwise>
                <cn><xsl:value-of select="(value[@position=5]) div (1 - value[@position=5])"/></cn>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <xsl:if test="../@id='NET'">
            <cn><xsl:value-of select="value[@position=5]"/></cn>
          </xsl:if>
        </apply>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="section[@id='INEQUALITIES']/subsection">
    <xsl:for-each select="row[value[@position=5]][position()&gt;1]">
      <xsl:choose>
        <xsl:when test="value[@position=4]='&lt;'">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <let/>
            <cn><xsl:value-of select="value[@position=3]"/></cn>
            <ci><xsl:value-of select="value[@position=5]"/></ci>
          </apply>
        </xsl:when>
        <xsl:when test="value[@position=4]='&lt;='">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <leq/>
            <cn><xsl:value-of select="value[@position=3]"/></cn>
            <ci><xsl:value-of select="value[@position=5]"/></ci>
          </apply>
        </xsl:when>
        <xsl:when test="value[@position=4]='&gt;'">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <gt/>
            <cn><xsl:value-of select="value[@position=3]"/></cn>
            <ci><xsl:value-of select="value[@position=5]"/></ci>
          </apply>
        </xsl:when>
        <xsl:when test="value[@position=4]='&gt;='">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <geq/>
            <cn><xsl:value-of select="value[@position=3]"/></cn>
            <ci><xsl:value-of select="value[@position=5]"/></ci>
          </apply>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="section[@id='EQUALITIES']/subsection">
    <xsl:for-each select="row[value[@position=4]][position()&gt;1]">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <apply>
          <xsl:call-template name="iteration">
            <xsl:with-param name="expression" select="value[@position=4]"/>
          </xsl:call-template>
        </apply>
        <cn><xsl:value-of select="value[@position=3]"/></cn>
      </apply>
    </xsl:for-each>
  </xsl:template>

<!--
  This template transforme an algebric expression like 2*a+b-c+2*d-3*e=3
  to a math constraints
  <apply>
    <eq/>
    <apply>
      <minus/>
      <apply>
        <plus/>
        <apply>
          <minus/>
          <apply>
            <plus/>
            <apply>
              <times/>
              <cn>2</cn>
              <ci>a</ci>
            </apply>
            <ci>b</ci>
          </apply>
          <ci>c</ci>
        </apply>
        <apply>
          <times/>
          <cn>2</cn>
          <ci>d</ci>
        </apply>
      </apply>
      <apply>
        <times/>
        <cn>3</cn>
        <ci>e</ci>
      </apply>  
    </apply>
    <cn>3</cn>
  </apply>
    
  In facts, first we split the expression "2*a+b-c+2*d-3*e" before and after each sign + or - and then we make a test to see what is the last sign;
  if it's a plus, we write : <plus/> and we call the template iteration-sign 
  with two parameters the before and the after last plus; we do the same for the <minus/> case.
  -->
    <xsl:template name="iteration">
    <xsl:param name="expression"/>
    <xsl:choose>
      <xsl:when test="not(contains(str:split($expression,'+')[last()],'-'))">
        <plus xmlns="http://www.w3.org/1998/Math/MathML"/>
        <xsl:call-template name="iteration-sign">
          <xsl:with-param name="beforelastsign" select="substring-before($expression,concat('+',str:split($expression,'+')[last()]))"/>
          <xsl:with-param name="afterlastsign" select="str:split($expression,'+')[last()]"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="not(contains(str:split($expression,'-')[last()],'+'))">
        <minus xmlns="http://www.w3.org/1998/Math/MathML"/>
        <xsl:call-template name="iteration-sign">
          <xsl:with-param name="beforelastsign" select="substring-before($expression,concat('-',str:split($expression,'-')[last()]))"/>
          <xsl:with-param name="afterlastsign" select="str:split($expression,'-')[last()]"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!--
  we test in this template if the expression before the last sign contains any
  other sign, if yes we call again the template iteration, and if no we put the
  last value in the <ci></ci> if it's not a number or in the <cn></cn> if it's 
  a number, taking into account the existence of the product * in the expression.
  In other hand, for the expression after the last sign we put it in the <ci></ci>
  also we take into account if it's a number or not and if the product * exists inside.
  -->
  
  <xsl:template name="iteration-sign">
    <xsl:param name="beforelastsign"/>
    <xsl:param name="afterlastsign"/>
    <xsl:choose>
      <xsl:when test="(contains($beforelastsign,'+')) or (contains($beforelastsign,'-'))">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:call-template name="iteration">
            <xsl:with-param name="expression" select="$beforelastsign"/>
          </xsl:call-template>
        </apply>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="contains($beforelastsign,'*')">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <times/>
              <xsl:for-each select="str:split($beforelastsign,'*')">
                <xsl:choose>
                  <xsl:when test="number(.)">
                    <cn xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="."/></cn>
                  </xsl:when>
                  <xsl:otherwise>
                    <ci xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="."/></ci>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </apply>
          </xsl:when>
          <xsl:otherwise>
            <ci xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="$beforelastsign"/></ci>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="contains($afterlastsign,'*')">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <times/>
          <xsl:for-each select="str:split($afterlastsign,'*')">
            <xsl:choose>
              <xsl:when test="number(.)">
                <cn xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="."/></cn>
              </xsl:when>
              <xsl:otherwise>
                <ci xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="."/></ci>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </apply>
      </xsl:when>
      <xsl:otherwise>
        <ci xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="$afterlastsign"/></ci>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

    
  <xsl:template match="node()"/>
</xsl:stylesheet>  