<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet 
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- File name -->
  <xsl:param name="file"/>

  <!-- File name -->
  <xsl:param name="date"/>

  <!-- SYSMETAB PATH -->
  <xsl:param name="sysmetab"/>
  
  <!-- Output file name -->
  <xsl:param name="output"/>

  <!-- minimize network or not -->
  <xsl:param name="minimize"/>

  <!-- Scaling parameter for all the fluxes -->
  <xsl:param name="scaling"/>

  <!-- Optimizing parameter -->
  <xsl:param name="optimize"/>
	
  <!-- Computation parameter -->
  <xsl:param name="computation"/>
	
  <!-- Maximum value for xch flux -->
  <xsl:param name="max_xch"/>

  <!-- Minimum value for input and output flux -->
  <xsl:param name="min_inout"/>

  <!-- Optimizing method -->
  <xsl:param name="optimize_method"/>

  <!-- Zero crossing strategy -->
  <xsl:param name="zc"/>

  <!-- stop iteration when norm of projected gradient is less that $eps_grad -->
  <xsl:param name="eps_grad"/>

  <!-- regularization parameter for the cost function-->
  <xsl:param name="eps_reg"/>

  <!-- regularization parameter for (net,xch)->(f,b) transformation -->
  <xsl:param name="eps_phi"/>

  <!-- maximum number of iterations -->
  <xsl:param name="max_iter"/>

  <!-- st deviation estimation mode (linear statistics or Monte-Carlo) -->
  <xsl:param name="stats"/>

  <!-- freefluxes choice : user, auto or random -->
  <xsl:param name="freefluxes"/>

  <!-- gradient choice : adjoint, direct -->
  <xsl:param name="gradient"/>

  <!-- maxweight -->
  <xsl:param name="maxweight"/>

  <!-- timer -->
  <xsl:param name="timer"/>

  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <params xmlns="http://www.utc.fr/sysmetab">
      <param name="date" type="string"><xsl:value-of select="$date"/></param>
      <param name="program" type="string">sysmetab-2.0</param>
      <param name="sysmetab" type="string"><xsl:value-of select="$sysmetab"/></param>
      <param name="file" type="string"><xsl:value-of select="$file"/></param>
      <param name="output" type="string"><xsl:value-of select="$output"/></param>
      <param name="minimize" type="string"><xsl:value-of select="$minimize"/></param>
      <param name="maxweight" type="number"><xsl:value-of select="$maxweight"/></param>
      <param name="timer" type="string"><xsl:value-of select="$timer"/></param>
      <param name="scaling" type="string"><xsl:value-of select="$scaling"/></param>
      <param name="optimize" type="string"><xsl:value-of select="$optimize"/></param>
      <param name="gradient" type="string"><xsl:value-of select="$gradient"/></param>
      <param name="computation" type="string"><xsl:value-of select="$computation"/></param>
      <param name="max_xch" type="number"><xsl:value-of select="$max_xch"/></param>
      <param name="min_inout" type="number"><xsl:value-of select="$min_inout"/></param>
      <param name="optimize_method" type="string"><xsl:value-of select="$optimize_method"/></param>
      <param name="zc" type="string"><xsl:value-of select="$zc"/></param>
      <param name="eps_grad" type="number"><xsl:value-of select="$eps_grad"/></param>
      <param name="eps_reg" type="number"><xsl:value-of select="$eps_reg"/></param>
      <param name="eps_phi" type="number"><xsl:value-of select="$eps_phi"/></param>
      <param name="max_iter" type="number"><xsl:value-of select="$max_iter"/></param>
      <param name="stats" type="string"><xsl:value-of select="$stats"/></param>
      <param name="freefluxes" type="string"><xsl:value-of select="$freefluxes"/></param>
    </params>
  </xsl:template>

</xsl:stylesheet>