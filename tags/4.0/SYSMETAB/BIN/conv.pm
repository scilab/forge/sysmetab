#!/usr/bin/perl

print "<xml>";

while (<>) {
    s!<!&lt;!;
    s!>!&gt;!;
    s!^!<row><value>!;
    s!\t!</value><value>!g;
    s!$!</value></row>!;
    print;
};
print "</xml>";
