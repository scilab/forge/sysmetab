\documentclass[11pt]{article}
   \voffset=-1in
   \hoffset=-1in
   \topmargin=2cm
   \headheight=0cm
   \headsep=0cm
   \setlength{\paperheight}{29.7cm}%
   \setlength{\paperwidth}{21cm}%
   \setlength{\oddsidemargin}{2.5cm}%
   \setlength{\evensidemargin}{2.5cm}%
   \setlength{\marginparsep}{0cm}%
   \setlength{\marginparwidth}{0cm}%
   \setlength{\footskip}{1cm}%
   \setlength{\textheight}{24cm}%
   \setlength{\textwidth}{16cm}%
   \usepackage{mathptmx}
\newcounter{answer}
\newenvironment{answer}{\refstepcounter{answer}\par\noindent\rm\textbf{Answer \theanswer:}\bgroup\rm}{\egroup}
\title{Metabolic Flux Analysis in Isotope Labeling Experiments using the adjoint approach}
\date{manuscript \# TCBB-2015-07-0323}
\begin{document}
\maketitle

\section{Summary of changes}
%
We hope that the new version will be more explicit about the main message of the paper, which is that applying an old method (the adjoint state), especially for nonstationary experiences, can probably help to improve \underline{all} existing MFA software. The fact that \verb1sysmetab1 could actually compete with them is important but somewhat secondary to this message, as well-established MFA software may have other advantages.

Hence, the main changes in the manuscript (see the detailed answers below) concern the Introduction and the Conclusion, which have been almost completely rewritten, the mathematical details resulting from multiple measurement times in the nonstationary case (Section 4), and last but not least, Section 6 "Numerical results", which has been completely restructured since the actual full implementation of the nonstationary case in Sysmetab made it necessary. The new 5.0 release is online at the following URL
\begin{center}
https://forge.scilab.org/index.php/p/sysmetab/downloads/
\end{center}

In the previous version of the manuscript, a lot of information was inadequately distributed through this section and we hope that the improvements will be valuable. In the beginning we give the details about the test case data and the actual hardware setup which were used.  In addition, we separated the comparison between the adjoint and direct approaches for sysmetab (Subsection 6.1) and the comparison of results with other softwares (stationary case), which were mixed in the previous version. The clarity of the arguments therein should benefit from this new structure (see e.g. Answer 9 below).

The new subsection 6.2 "Assessment of parameter estimates"  focuses firstly (Subsection 6.2.1) on the compared results and timings for stationary data. The timings of sysmetab have been improved because of new developments (see the details in Answer 3 below), and the comparison with influx\_s has been clarified (see Answers 4,5) after a bug fix (Monte Carlo method) and by providing a supplementary test case (a poorly defined network). Subsection 6.2.2 contains new material about a test case where fluxes and pool sizes are estimated from nonstationary data (see e.g. Figure 1 and Table 4 and the discussion therein) and fills the gap which was pointed out by the second reviewer.

Finally, the language and the style have been improved thanks to a native English speaker and for sake of homogeneity, every occurrence of "adjoint method" (including in the title) has been replaced by "adjoint approach" (as the latter was used almost everywhere in the manuscript)

\section{Answers to reviewer \#1}
{\em
Comments:
(...)
Major criticisms:
- Comparing the speed of different software is tedious and complex task. (...)

{\rm \medskip\noindent{\bf Answer 1:}~we have completely restructured the Section 6 (Numerical results) in order to present the comparison between the adjoint and the direct computation of the gradient for stationary and nonstationary data (Section 6.1) \underline{before} the assessment of parameters estimates (Section 6.2) of overall results when a full identification is considered. Moreover, as the absolute timings of a single run for e\_coli.ftbl are not the primary topic of the section, they have be removed from Table 3 and mentioned in the text (page 6, column 2, lines 6-8).
}\smallskip

As illustration of this assertion, let me cite few timing results. Time measuring was done on a Linux with Xeon bi-proc CPU E5-2609 v2 @ 2.50GHz, 8 cores in total. On our hardware, influx\_s showed 1.07s for optimization of e\_coli example while sysmetab needed 0.66s. These results are pretty close to those cited in the manuscript, 1.03s and 0.66s respectively. But the total time for sysmetab was only 2.03s instead of 12.9s cited in the paper (6.5s and 7.4s for influx\_s respectively). This more than 6 fold acceleration for sysmetab (and only modest acceleration for influx\_s) could result from better optimized system libraries or from better cache memory or from what ever of many other uncontrolled factors. This illustrates how timing can vary for one soft and not for another depending on the computational environment.

{\rm \medskip\noindent{\bf Answer 2:}~in order to force sysmetab to include all the steps (code generation, optimization and linear statistics), the flag \texttt{--rebuild} has to added, otherwise subsequent runs of the command may bypass the main part of the generation step and thus complete faster, which is the case when the modifications of the FML file do not change the resulting cumomer graph.
}\smallskip


Another remark on comparing of software is about criterion and options used in numerical experiments. To achieve the cost value of 61.5141593 in e\_coli example, influx\_s requires less iterations than were used by the authors in their experiments (5 instead of 6). So the timers for both software should be stopped at the same moment, when a given condition is achieved. Moreover, when two software are compared where so many aspects are different (languages, approaches, methods, scientific libraries), it would be convenient to compare the best results of one software with the best results of the other. For influx\_s, better optimization time can be achieved in EMU framework (with --emu option) though en equivalent option is not currently available in sysmetab. Combining new stopping criterion (optctrl\_errx: 1.e-4 instead of 1.e-5) with --emu option, we've got the result of 0.63s for influx\_s vs 0.66s for sysmetab (median of 10 runs with SD=0.03s).


{\rm \medskip\noindent{\bf Answer 3:}~even if the median optimization time (0.74 seconds on our Linux server) of influx\_s is smaller than reported in the original manuscript (by adding the \texttt{--emu} flag and \texttt{optctrl\_errx 1.e-4} in the ftbl file), the median optimization time of sysmetab has dropped to 0.35 seconds in the new 5.0 release of the software (available at https://forge.scilab.org/index.php/p/sysmetab/downloads/)

The explanation for this  lower timing is that the structure of the generated code has changed dramatically. Some parts which were specifically generated have been factorized under the form of generic Scilab functions, which partly explains the gain (these generic functions are more efficient than the replaced generated code). Moreover, the scaling factors are not optimization variables any more as they can be optimally determined on-the-fly (simple linear least-squares problem) for each forward simulation (this technique is also used in 13CFlux2, as explained in the PhD thesis manuscript of Michael Weitzel). Their values have been removed from Table 3 (page 6 of the revised manuscript).
}\smallskip

Comparing sub-second results is not very impressive, so the authors timed 1000 Monte-Carlo (MC) iterations to highlight better performance of sysmetab. It turned out that even with the new stopping criterion and --emu option enabled, influx\_s needed as much as 192s while sysmetab run during only 90s (120s vs 46s in the manuscript), more than two fold faster! But it appeared that the convergence was not the same for two programs. The summary of 1000 cost values for sysmetab is

   Min. 1st Qu.  Median    Mean 3rd Qu.    Max.

  46.47   84.51   96.83   97.76  108.70  161.20

while for influx\_s (an option --seed=7 was used for reproducibility of MC iterations) the same statistics are

   Min. 1st Qu.  Median    Mean 3rd Qu.    Max.

  14.64   28.44   33.68   34.19   39.19   67.51

i.e. MC iterations went to much lower cost level with influx\_s (median 33.68) than with sysmetab (96.83) . This returns us to the question of applying the same stopping criterion for the timer. Why the cost distribution is so different in two programs? Is it a convergence problem? A programming problem? On what side the problem is, on influx\_s or sysmetab? Anyhow, these numbers in hand, we cannot confirm that sysmetab is "much faster" as equivalent results were not obtained in MC experiment.

{\rm \medskip\noindent{\bf Answer 4:}~the remark on this point led us to realize that the way sysmetab was doing the resampling was incorrect: instead of adding it to the simulated measurement for the identified fluxes, the noise  was added to the original measurement, i.e. the noise amount was twice what it should have been. This has been fixed in the new version of sysmetab, which now gives, with the following command
\begin{center}
\texttt{sysmetab --rebuild --seed=7 --reg=0 --stats=mc:1000  e\_coli}
\end{center}
 the following distribution of final RSS values
\begin{center}
min=13.60, q1=28.01, median=33.07, mean=33.74, q3=38.82, max=69.58, std=8.09
\end{center}
which is similar to the one reported by influx\_s. To obtain these values, the new version of sysmetab took only 24 seconds, although a similar computation with \texttt{influx\_s}, with the command
\begin{center}
\texttt{influx\_s.py -seed=7 --emu --sens=mc=1000 e\_coli.ftbl}
\end{center}
took 105 seconds. These timings have been obtained by using all the available the cores of the processors (40)}.\smallskip


Last but not least, the timing made for a scolar, well defined and quickly converging example such as e\_coli, may be not relevant for real life cases where the networks are badly defined and the corresponding optimization  problems are very ill-posed. Generally speaking, gradient based methods slow down considerably their convergence speed near the optimum of such kind of problems due to loss of numerical stability. That leaves the following questions open: will sysmetab be able to reach the same cost level than influx\_s on badly defined networks? And if yes, will it be at price of comparable CPU time? In conclusion, authors should revise the conclusion of sysmetab being "much faster" than influx\_s for MC iterations. In hypothesis that they do prove by more convincing experiments that it is indeed faster, the conclusion must indicate to what extent this conclusion is justified -- is it true only for e\_coli example or for a larger class of problems?

{\rm \medskip\noindent{\bf Answer 5:}~to answer this question we have considered (page 6, column 2, lines 16-23) the badly defined network \texttt{Ecoli.1.ftbl} available in the supplementary material of the influx\_s paper (reference [10] in our manuscript), and its converted version Ecoli.1.fml available in the distribution of sysmetab.

We are aware that influx\_s shows its superiority in terms of numerical stability for this example, and the possibility of determining the least norm solution is very valuable. However, the approach used by sysmetab is different as the regularization is not done at the inner level of the optimizer (determination of the search direction) but at the top level by adding a Tikhonov regularization term $\epsilon \Vert q \Vert^2$ to the RSS, where $q$ is the vector of free fluxes. As a result, we don't obtain the true minimum norm solution achieving the minimum of the RSS, but even a small value of $\epsilon$ ($10^{-6}$ by default) allows to improve the convergence of the optimization algorithm while providing a solution which is very close to it.


On the Ecoli.1 network,  with the following command 
\begin{center}
\texttt{influx\_s.py --emu --clownr=1e-4 --ln Ecoli.1.ftbl}
\end{center}
influx\_s obtained a final RSS of 126.5625 whereas sysmetab, with the following command
\begin{center}
\texttt{sysmetab --seed=7 --minnr=1e-4 --freefluxes=multi:100 Ecoli.1}
\end{center}
obtained a final RSS of 126.85 (median of 100 runs with random initial conditions, with a standard deviation of 0.032) for the default value $\epsilon=10^{-6}$. With the default initial conditions of the Ecoli.1.ftbl file, and $\epsilon=0$ (\texttt{--reg=0} sysmetab option), sysmetab converges in barely more iterations (compared to the default value of $10^{-6}$) and gives a final RSS of 126.5599, a value which we were not able to reach with influx\_s, even by decreasing \texttt{optctrl\_errx} and increasing \texttt{optctrl\_maxit} in the ftbl file, as NLSIC iterations systematically stop at the 34th.

The Monte Carlo study on Ecoli.1 performed by sysmetab
\begin{center}
\texttt{sysmetab --maxiter=2000 --seed=7 --rebuild --minnr=1e-4 --stats=mc:1000 Ecoli.1}
\end{center}
completed in 110 seconds and gave the following distribution in the final RSS:
\begin{center}
min=89.28, q1=121.35, median=133.08, mean=133.76, q3=145.53, max=184.91, std=17.15
\end{center}
The same study by influx\_s
\begin{center}
\texttt{influx\_s.py --seed=7 --emu --clownr=1e-4 --ln --sens=mc=1000 Ecoli.1.ftbl}
\end{center}
after adding \texttt{optctrl\_errx 1.e-2} and \texttt{optctrl\_maxi 100}, completed in 480 seconds, but the distribution of the cost
\begin{center}
min=96.97, q1=139.69, median=150.99, mean=151.04, q3=161.71, max=201.85, std=17.15
\end{center}
significantly differs from the one obtained by sysmetab. The maximum number of NLSIC iterations (100) is reached 169 times, but it does not seem to impact the distribution as removing these "outliers" does not really change it. Morever, taking smaller values of the convergence parameter \texttt{optctrl\_errx} increased the number of iterations without improving the distribution.
}\smallskip

- considering non-stationary MFA, authors make a strong assumption that label measurements are done only at terminal time T. It remains unclear how real situations where measurements are done at many intermediate time points would impact the manuscript's considerations. Will we have to solve as many adjoint problems as there are time points where measurements are done ? Or will these measurements be treated as intermediate steps of one problem? Without complexifying their explanations, authors should give some indications on this aspect.

{\rm \medskip\noindent{\bf Answer 6:}~as the nonstationary case has been implemented in the new version of sysmetab, we found it better to update the equations of Section 4 in order to take into account the measurements made at intermediate times denoted by $0<\tau_j<T$, $j=1\dots M$ (having $\tau_M\neq T$ simplifies the final condition of the adjoint state). Generally speaking, there is still only one adjoint equation but jump conditions (continuous time) or a discrete Dirac non-homogeneous term (discrete time) have to be introduced.

In continuous time, the modified equations on page 3 are namely, the definition of the cost function (10), the expression of the gradient obtained by the direct method (12), the definition of the Lagrangian (column 2, lines 8-9) and the new jump conditions (14), which replace the final condition at time $T$ in the previous version of the manuscript. In discrete time, the last line of column 2, page 3 gives the new cost function in discrete time, where $n(j)$ denote the index of measurement time $\tau_j$, and the modified equations on page 4 are   the expression of the gradient obtained by the direct method (18), the discrete Lagrangian (19) and the non-homogeneous term $\delta_n$ in the adjoint equation (20).
}\smallskip

Minor criticism:
- the phrase "the default upper bound his reached by sysmetab and influx\_s." seems grammatically incorrect;

}

{\rm \medskip\noindent{\bf Answer 7:}~an English native has helped us to improve the manuscript, so the overall quality of the grammar and of the language has been improved in the new version.
}\smallskip

\section{Answers to reviewer \#2}
\em
 Comments:
The authors propose to use the adjoint model from the literature for the solution of MFA problem, which is inherently an inverse problem involving a state equation.

1. Only one set of experiments are provided. I suggest showing results for the case where time requirement is in the order of days and adjoint method really helps, instead of focusing on cases that requires only secs.

{\rm \medskip\noindent{\bf Answer 8:}~generally speaking, the reliability of flux estimations in very large networks (e.g. genome scaled) requires a huge number of measurements which are most of the time unavailable. But the estimation of fluxes and pool sizes of a mid-scale network from nonstationary data is already an intensive computational task. Since we have actually implemented the nonstationary MFA (see answer to question \#4), we have been able to compare the direct and the adjoint methods to achieve the full estimation of fluxes in the central metabolism of {\em E. Coli}. Since the direct methods leads to an overall time of almost 8 minutes (468 seconds) and the adjoint methods only 36 seconds, it seems possible to claim that the adjoint method really helps on a case which requires more than a few seconds.
}\smallskip

2. I do not understand why influx\_s takes less time than sysmetab. The whole point of the paper is that this method takes less time. Addressing (1) might help on this issue.

{\rm \medskip\noindent{\bf Answer 9:}~we have tried to be more clear in our explanations in Section 6 (page 6, lines 1-24). In order to obtain the optimal parameters, the overall computation time includes the following steps: analysis of the network and generation of the program (in R and Scilab language, for influx\_s and sysmetab, respectively), optimization, and linear statistics. Sysmetab is always slower than influx\_s in the first step because the choice of using only XSLT transformations, though very convenient on the software maintenance point of view, is clearly penalizing it. But there are two situations where the weight of this first step becomes negligible with respect to the total elapsed time, namely,
\begin{itemize}
\item {\bf computation of confidence intervals}: fluxes values are never published without realistic confidence intervals, and Monte Carlo resampling is the most frequently used method to calculate them. In this case multiple optimizations have to be done with perturbed data but since the network remains the same, the first step (program generation) is done only once. For example, even in the case where the computer configuration is the most favorable to influx\_s (40 available processor cores, which is not available for the average user), sysmetab is more than 4 times faster for a Monte-Carlo estimation with 1000 data samples.
\item {\bf estimation of parameters from nonstationary data}: the elapsed time in the optimization phase is larger (because the forward problem is time-dependent), and since the first step does not depend on the kind of data (stationary or nonstationary), it is once again negligible, even for a single run (a few seconds compared to 8 minutes when the direct method is used). But since influx\_s does not handle nonstationary problems, comparing it with sysmetab is not an issue in this case.
\end{itemize}
}\smallskip

3. Contributions of the paper needs to be clearly identified in the introduction, point by point. It is spread around the paper currently. For instance, authors state their implementation contribution in the conclusion section.

{\rm \medskip\noindent{\bf Answer 10:}~the introduction has been almost completely revised and now all the contributions are stated inside.
}\smallskip

4. Authors show that for stationary-MFA with the same cost. However, even though substantial amount of effort is spent on describing the nonstationary MFA problem, only a simulation is provided as the software is still under construction. I think the authors should provide results with an actual implementation.

{\rm \medskip\noindent{\bf Answer 11:}~we have worked hard to finish the developments in time and Section 6.2.2 (pages 6-7) presents a case study where the fluxes and pool sizes of the central metabolism of E. Coli are determined on the basis of nonstationary data. Associated performances are stated and show very interesting absolute and relative timings: the identification of fluxes and pool sizes takes 21 seconds when using the adjoint method and is more than 20 times faster than the same identification with the direct method.
}\smallskip


5. Table 2 should be updated with the results from influx\_s and 13CFlux2.

{\rm \medskip\noindent{\bf Answer 12:}~neither influx\_s nor 13CFlux2 implement the adjoint method (this is clearly stated in the new Introduction, column 2, lines 20-21), so the comparison cannot be made for them.
}\smallskip


6. Format Issues:
Reference format ([X],[Y]) should be changed to [X,Y]
Rename equations ST, ADJ1 etc to (1), (2) etc
Overall English and grammar of the paper must be improved. I list only the errors I notice in the first page. There are many more.

{\rm \medskip\noindent{\bf Answer 13:}~an English native has helped us to improve the manuscript. The references format has been changed (parenthesis have been removed) but the multiple citation format could not been updated as it is hard-coded in the IEEEtran document class.
}\smallskip


\end{document}
