// ====================================================================
// Copyright INRIA 2008
// Serge Steer 
// Allan CORNET (toolbox)
// ====================================================================
mode(-1);
lines(0);
try
 getversion('scilab');
catch
 error(gettext('Scilab 5.0 or more is required.'));  
end;
// ====================================================================
if ~with_module('development_tools') then
  error(msprintf(gettext("%s module not installed."),'development_tools'));
end
// ====================================================================
TOOLBOX_NAME = 'fsqp_toolbox';
TOOLBOX_TITLE = 'fsqp_toolbox';
// ====================================================================
toolbox_dir = get_absolute_file_path('builder.sce');

tbx_builder_macros(toolbox_dir);
//tbx_builder_src(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
exec(toolbox_dir+"demos/builder.sce",-1);
clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE 
clear tbx_builder_macros tbx_builder_src tbx_builder_gateway tbx_builder_help tbx_build_loader
    
// ====================================================================
