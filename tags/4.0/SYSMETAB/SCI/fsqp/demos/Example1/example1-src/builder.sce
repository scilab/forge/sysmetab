// builder for a shared library for example1.c functions 
//=======================================================
ex_path=get_absolute_file_path('builder.sce')
// functions to be added to the call table 
link_name =['obj32','cntr32','grob32','grcn32'] ;  

// the next call generates files (Makelib,loader.sce) used
// for compiling and loading ext1c and performs the compilation
incl_path=ex_path+"../../../sci_gateway/c"
//Next instruction to go around a bug under Windows with CFLAGS handling
if getos()=="Windows" then incl_path=getshortpathname(incl_path),end 
tbx_build_src(link_name,'example1.c','c',ex_path,[],"","-I"+incl_path,"","","example1")
clear ex_path get_absolute_file_path link_name tbx_build_src
