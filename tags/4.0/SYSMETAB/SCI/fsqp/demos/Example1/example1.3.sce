mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 1 of the fsqp documentation

//[2] fsqp with  lists 

//list of objectives fcts: 1 reg 0 SR, 
function y=f_1(x)
  y=(x(1)+3*x(2)+x(3))^2+4*(x(1)-x(2))^2
endfunction
//list_obj is a reserved name
list_obj=list(list(f_1),list());  

//list of constraints fcts: 1 non-lin-reg-ineq (g_1) one lin-equal (A_1) 
function y=g_1(x), y=x(1)^3-6*x(2)-4*x(3)+3,endfunction
function y=A_1(x), y=[-1,-1,-1]*x+1;,endfunction
//list_cntr is a reserved name
list_cntr=list(list(g_1),list(),list(),list(),list(),list(A_1));

//gradient fcts
function yd=grf_1(x)
  fa=2*(x(1)+3*x(2)+x(3));
  fb=8*(x(1)-x(2));
  yd=[fa+fb,3*fa-fb,fa];
endfunction
//list_grobj is a reserved name
list_grobj=list(list(grf_1),list());
function yd=grg_1(x), yd=[3*x(1)^2,-6,-4],endfunction
function yd=grA_1(x), yd=[-1,-1,-1],endfunction
//list_grcn is a reserved name
list_grcn=list(list(grg_1),list(),list(),list(),list(),list(grA_1));


x0=[0.1;0.7;0.2];

//fsqp parameters obtained by findparam:
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

//integer fsqp parameters not implicitly defined by the fcts.
modefsqp=100; miter=500; iprint=0;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
srpar=[nfsr,ncsrl,ncsrn];

//real fsqp parameters not implicitly defined by the fcts.
bigbnd=1.e10; eps=1.e-8; epsneq=0.e0; udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=[0;0;0];
bu=[bigbnd;bigbnd;bigbnd];
// list_obj, list_cntr, list_grobj, list_grcn are used as semi global variables

f=f_1(x0)
C=[g_1(x0);A_1(x0)]
mprintf('%s\n',['---------------------------Quadratic problem with srfsqp(list)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Value of the constraints :       '+sci2exp(C,0)])
timer();
x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu], obj,cntr,grob,grcn)

t=timer();
f=f_1(x)
C=[g_1(x);A_1(x)]
mprintf('%s\n',['Final values'
		'   Value of the  solution:          '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Value of the constraints :       '+sci2exp(C,0)
                '   Time:                            '+sci2exp(t,0)
	       ])


