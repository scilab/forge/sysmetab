// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_c()


    // PutLhsVar managed by user in sci_sum and in sci_sub
    // if you do not this variable, PutLhsVar is added
    // in gateway generated (default mode in scilab 4.x and 5.x)
    WITHOUT_AUTO_PUTLHSVAR = %f;
    tbx_build_gateway("spset", ..
    ["spset","sparse_set"], ..
    ["sparse_set.c"], ..
    get_absolute_file_path("builder_gateway_c.sce"), ..
    [""], ..
    "", ..
    "");

endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
