function [M,M_t,uij]=gather(M_m,nrow,ncol,storage,M_s)
    // group matrix contributions to same (i,j) term
    if storage==1 // compressed row storage (Scilab)    
        [ij,ind]=lex_sort(M_m(:,1:2));  // lexicographic sort of rows
        [uij,k]=lex_sort(ij,'unique'); // unique (row,col) pairs        
    else
      error('Julia storage not implemented')
    end
    
    m=size(M_m,1);
    if ~exists('M_s','local')
        M_s=ones(m,1);
    end
    M_i=zeros(m,1);
    n=length(k);
    for i=1:n-1
        M_i(k(i):k(i+1)-1)=i;
    end
    if n>0
      M_i(k(n):$)=n
    end
    M=sparse(uij,ones(n,1),[nrow,ncol]);
    M_t=sparse([M_i ind],M_s(ind),[n,m]);
    if n==1 | m==1
        M_t=full(M_t);
    end

endfunction

function R = convexSampler(A,b,me,m,x)
    // Sampling of uniformly distributed vectors x satisfying the linear system
    // of inequalities/equalities A.x <= b. It is assumed that the convex region described
    // by A.x <= b is not unbounded.
    // Derived from M. Weitzel Matlab code in Mathworks Central. 
    // The code has been adapted for mixed equality/inequality constraints
    // and a (fatal) bug in distToConstraints has been fixed...
    n=size(A,2);
    Q=eye(n,n);
    if me>0 then
        Q=kernel(A(1:me,:));
    end
    p=size(Q,2);
    R = zeros(n,m);
    if ~exists('x','local')
        [x,lagr]=qld(-eye(n,n),zeros(n,1),A,b,[],[],me); // admissible starting vector
    end
    for k=1:m // Hit and Run Sampling        
        dmin=0;
        dmax=0;
        while (dmin==0) & (dmax==0)
            r=rand(p,1,'normal');
            d=Q*r;
            [dmin,dmax] = distToConstraints(A(me+1:$,:),b(me+1:$),x,d);
        end
        rho = dmin+(dmax-dmin)*rand(1);
        x=x+rho*d;
        R(:,k)=x;
    end
endfunction
	
function [d_min,d_max] = distToConstraints(A,b,x,r)
    d_min = -%inf;
    d_max = %inf;
    for i=1:size(A,1)
        den=A(i,:)*r;
        if abs(den)>1e-6
            rho=(b(i)-A(i,:)*x)/den;
            if den>0
                d_max=min(d_max,rho);
            else
                d_min=max(d_min,rho);
            end
        end
    end
endfunction

function a=s_full(ij,v,n,m)
    // It is a function to build full matrix with the same description of sparse
    // matrix, i.e. an array (ij) contains pairs of row indecies and column 
    // also the array of corresponding values (v).
    // Attention : this is not equivalent to :
    // a=full(sparse(ij,v,dims))
    // because here it is assumed that there are no multiple entries (i,j)
    a=full(sparse(ij,v,[n;m]))
endfunction


function a=s_sparse(ij,v,n,m)
    a=sparse(ij,v,[n;m])
endfunction

function spm=spdiag(v)
    // return the components of a column array in a diagonal sparse matrix
    sv=size(v);
    spm=sparse([(1:sv(1))',(1:sv(1))'],v)
endfunction

function %fluxes_p(f)
    // User friendly display of a fluxes set
    nfluxes=size(f.names,'*')/2;
    maxlength=max(length(f.names));
    printf("%"+string(maxlength)+"s : %23s, %23s\n\n",'flux','net, [sigma]     ','xch, [sigma]     ');
    for i=1:nfluxes
        fname=strsubst(f.names(i),'_n','');
        fdnet=ftype(i)
        fdxch=ftype(i+nfluxes);
        printf("%"+string(maxlength)+"s : %+10.5f [%6.3f] (%s), %10.5f [%6.3f] (%s)\n",...
        fname,f.values(i),f.sigma(i),fdnet,f.values(i+nfluxes),f.sigma(i+nfluxes),fdxch)
    end
endfunction

function f = %s_i_fluxes(ind1,ind2,val,f)
    // initial fluxes and type
    f0=f.values;
    ind20=ind2;
    // Test if the flux is net or xch
    if ind2=="net" then
        ind2='f';
    elseif ind2=="xch" then
        ind2='b';
    else
        printf('""%s"" is not accepted, please choose a value between ""net"" or ""xch""',ind2);
        abort;
    end
    // Get the indice of the flux
    i=find(flux.names==strcat([ind1,"_",ind2]));
    if (i>=1 & i<=size(W,1))
        f.values(i) = val;
    else
        printf('The flux ""%s"" is not valid.',ind1);
        abort;
    end
    // Test for the type of the flux, if it's a free flux we continue otherwise we stop
    if ( ftype(i)=='c' | ftype(i)=='d' ) then
        printf('The flux ""%s"" is not a free flux, you must choose a free flux.',ind1);
        abort;
    end
    qf=f.values(ff);
    f.values=W*qf+w0;
    // Test if the fluxes verify the constraints inequality
    if (max(C(me+1:$,:)*f.values-b(me+1:$))>0) then
        f.values=f0;
        printf('The new constraints does not verify the inequality constraints\n');
        printf('Please choose another value for the flux ""%s_%s""',ind1,ind20);
        abort;
    end
endfunction

function %scale_p(scale)
    // User friendly display of a scale set
    maxlength=max(length(scale.names))+2;
    printf("%"+string(maxlength)+"s : %6s, %7s\n\n","group","scale","[sigma]");    
    for i=1:length(scale.values)
        printf("%"+string(maxlength)+"s : %7.4f [%6.3f]\n",scale.names(i),scale.values(i),scale.sigma(i));
    end
endfunction

function %meas_p(m)
    // User friendly display of a measurement vector
    maxlength=max(length(m.names))+2;
    nmeas=size(m.values,'*')
    printf("%"+string(maxlength)+"s : %7s\n\n","isotopomer","value");
    for i=1:nmeas
        printf("%"+string(maxlength)+"s : %7.5f\n",m.names(i),m.values(i));
    end
endfunction

function [W,w0,free,ftype,flux_adm,wadm]=freefluxes2(C,d,me,ffmat)
    // Determination of "free fluxes" and of setting of affine subspace define 
    // by A*w=b, as w=W*q+w0, where q is a subset of w component, the "free fluxes"

    W=[]
    free=[]
    A=C(1:me,:)
		b=d(1:me)
    n=size(A,2)
    n1=rank(A)
    user_ff=%f
    
    if exists("ffmat","local")  
      user_ff=%t
      free=ffmat(:,1)
      others=(1:n)'
      others(free)=[]
      p=[others;free]
      if rank(A(:,others))<>n1 | size(ffmat,1)<>n-n1
        disp('The choice of free fluxes is incoherent, falling back to automatic freefluxes determination')
        user_ff=%f
        free=[]
      end  
    end
    
    if isempty(free)        
      // We use QR factorisation of A : A*P=QR with P of size (n,n), R of size (n1,n)
      // and Q of size (n1,n1). We use here the fact that the permutation P ensures
      // that the principal submatrix (n1,n1) of A*P has full rank if rank(A)=n1.
      [Q,R,P]=qr(A)
      [p,j]=find(P)
      free=p(n1+1:$)
      others=p(1:n1)
    end
    
    A1=A(:,others)
    A2=A(:,free)
    
    W=zeros(n,n-n1)
    w0=zeros(n-n1,1)
    W(p,:)=[-A1\A2;eye(n-n1,n-n1)]
    w0(p)=[A1\b;zeros(n-n1,1)]
    
    ftype=string(zeros(n,1))
    ftype(:)='d'
    ftype(free)='f'
    ftype(find(clean(sum(abs(W),'c'))==0))='c'
    
    // To verify that the user does not impose incoherent constraints, we try to
    // solve a quadratic program whose cost function is the squared norm 
    // of the residual, restricted to the flux measurements, plus a
    // regularization term, and a random perturbation on the linear term,
    // in order to randomize the solution.
    // The flux values that have been proposed in the fml file are
    // added as equality constraints and allow to test if
    // the admissible set {[C(1:me,:),Flux_M]*w=[b(1:me),b_Flux]} 
    // and {C(me+1:$,:)*w<=b(me+1,:)} are non-empty.
    
    try
      Q=full(E'*E)+1e-6*eye(n,n)
      p=-full(E'*wmeas)+1e-6*rand(n,1)
      [wadm,lagr]=qld(Q,p,C,d,[],[],me)
      if user_ff
        M=zeros(n-n1,n)
        M(:,free)=eye(n-n1,n-n1)
        [wadm,lagr]=qld(M'*M,-M'*ffmat(:,2),C,d,[],[],me)
        residual=norm(M*wadm-ffmat(:,2));
        if residual>1e-6
           printf("Residual=%f, user free fluxes have been projected on admissible domain\n",residual) 
        end
      end
    catch
      error('The constraints are inconsistent');
    end
    flux_adm=tlist(['fluxes' 'names' 'values' 'sigma'],flux_ids,wadm,zeros(n,1));
        
endfunction

function [W,ff,ftype]=freefluxes(C,d,me)
    // Determination of "free fluxes" and of setting of affine subspace define 
    // by A*w=b, as w=W*q+w0, where q is a subset of w component, the "free fluxes"
    W=[];
    ff=[];
    A=C(1:me,:);
		b=d(1:me);
    [n1,n]=size(A);
    // We use QR factorisation of A : A*P=QR with P of size (n,n), R of size (n1,n)
    // and Q of size (n1,n1). We use here the fact that the permutation P ensures
    // that the principale submatrix (n1,n1) of A*P has full rank if rank(A)=n1.
    // Thus, we have, by putting B=A*P and E=P'
    // Aw=b <==> B*E*w=b <==> [B1 B2]*[E1*w;E2*w]=b <==> B1*E1*w = -B2*E2*w
    // if we add to this, the trivial equation E2*w=E2*w, we obtain
    // Aw=b <==> [B1*E1;E2]*w = [b;0]-[B2;I]*E2*w <==> w = inv([B1*E1;E2])*[b;0]-inv([B1*E1;E2])*[B2;I]*E2*w
    // which give the affine relation between fluxes and free fluxes, represented
    // here by E2*w. In the sequel, we set W=-inv([B1*E1;E2])*[B2;I]
    // This works too when A is not full rank, but this has no interest unless 
    // when b is the image of A, which is verified above when solving the linear program.
    [Q,R,P]=qr(A);
    E=P';
    n1=rank(A);
    n2=size(A,2)-n1;
    E1=E(1:n1,:);
    E2=E(n1+1:$,:);
    C1=[A*E1'*E1;E2];
    C2=[-A*E2';eye(n2,n2)];
    W=clean(C1\C2);
    [s,i]=gsort(-E2*(1:n1+n2)'); // On remet les free fluxes dans l'ordre.
    ff=-s;
    W=W(:,i);
    ftype=string(zeros(n,1));
    ftype(:)='d';
    ftype(ff)='f';
    ftype(find(sum(abs(W),'c')==0))='c';
endfunction

function [f]=qld_test(C,b,me,Flux_M,b_Flux)
    // To verify that the user does not impose incoherent constraints, we try to
    // solve a quadratic program whose cost function is the squared norm 
    // of the residual, restricted to the flux measurements, plus a
    // regularization term, and a random perturbation on the linear term,
    // in order to randomize the solution.
    // The flux values that have been proposed in the fml file are
    // added as equality constraints and allow to test if
    // the admissible set {[C(1:me,:),Flux_M]*w=[b(1:me),b_Flux]} 
    // and {C(me+1:$,:)*w<=b(me+1,:)} are non-empty.
    n=size(C,2);
    Q=full(E'*E)+1e-6*eye(n,n);
    p=-full(E'*wmeas)+1e-6*rand(n,1,'normal');
    try
        [w,lagr]=qld(Q,p,C,b,[],[],me);
        if argn(2)>3 then
            Q=full(Flux_M'*Flux_M);
            p=-full(Flux_M'*b_Flux);
            [w,lagr]=qld(Q,p,C,b,[],[],me);
            residual=norm(Flux_M*w-b_Flux);
            if residual>1e-6
               printf("Residual=%f, user free fluxes will be projected on admissible domain\n",residual) 
            end
//        else
//            wadm=convexSampler(C,b,me,100,w);
//            w=wadm(:,$);
        end
    catch
        error('The constraints are inconsistent');
    end
    f=tlist(['fluxes' 'names' 'values' 'sigma'],flux_ids,w,zeros(w));
endfunction

function y=phi(x,e)
    // A smooth function near (0,0)
    y=((x>-e & x<e).*(x+e).^2)/(4*e)+(x>=e).*x;
endfunction

function y=phiprime(x,e)
    // The derivative of the smooth function near (0,0)
    y=(x>-e & x<e).*(x+e)/(2*e)+(x>=e);
endfunction

function [v,Phiprime]=Phi(w,eps)
    // "mollification" of transformation (net,xch)->(forward,backward)
    n=length(w)/2;
    v_net=w(1:n,:);
    v_xch=w(n+1:$);
    I=speye(n,n);
    if (eps==0) then
        v=[v_xch+max(0,v_net)
           v_xch-min(0,v_net)];
           s=sign(v_net);
        Phiprime=[spdiag((1+s)/2),I
                 -spdiag((1-s)/2),I];
    else
        v=[v_xch+phi( v_net,eps)
           v_xch+phi(-v_net,eps)];
        Phiprime=[spdiag(phiprime( v_net,eps)),I
                 -spdiag(phiprime(-v_net,eps)),I];
    end
endfunction

function [C,b]=Add_constraints(w,method)
    // Add constraints when using zero crossing strategy with non negative 
    // threshold for net fluxes
    if (method=='zc') then
        n=length(w)/2;
        for i=1:n
            if ((ftype(i)=='f') | (ftype(i)=='d')) then
                New_C_Constraint=zeros(1,2*n);
                if (sign(w(i))>=0) then
                    New_C_Constraint(i)=-1;
                    b=[b;-1e-4];
                end
                if (sign(w(i))<0) then
                    New_C_Constraint(i)=1;
                    b=[b;1e-4];

                end
                C=[C;New_C_Constraint];
            end
        end
    end
endfunction

function [q,omega,f,info,mess]=optimize(algorithm,qstart,qmin,qmax,omegastart,omegamin,omegamax,eps_grad,eps_reg,eps_phi,miter,iprint,task,ntasks)

    // Monte-Carlo method 

    function [_q,_omega,_f,_info,_iter]=optimize_mc(i)
        ymeas=ymeastab(:,i);
        wmeas=wmeastab(:,i);        
        [_q,_omega,_f,_info,_iter]=optimize_single(algorithm,qstart,qmin,qmax,...
        omegastart,omegamin,omegamax,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%3d, info=%d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)         
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    // multistart

    function [_q,_omega,_f,_info,_iter]=optimize_multi(i)
        [_q,_omega,_f,_info,_iter]=optimize_single(algorithm,qstarttab(:,i),qmin,qmax,...
            omegastart,omegamin,omegamax,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%3d, info=%d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    time=0;

  if exists('task','local')

    if task=='mc'
        ymeastab=ymeas;
        wmeastab=wmeas;
        for i=2:ntasks
            ymeastab(:,i)=max(ymeas+rand(ymeas,'normal')./sqrt(Sypm2),0)
            wmeastab(:,i)=max(wmeas+rand(wmeas,'normal')./sqrt(Svpm2),0);
        end

        printf("%d tasks started, please wait.\n\n",ntasks)    

        [q,omega,f,info,iter]=parallel_run(1:ntasks,optimize_mc,[length(qstart) length(omegastart) 1 1 1]',init_param('dynamic_scheduling',1));	
        
        //if getos()=="Darwin"
        //    k=find(or(q,'r')); 
        //    q=q(:,k);omega=omega(:,k);f=f(k);info=info(k);
        //    if length(k)<>ntasks
        //        printf('\n%d task(s) have been dropped (Scilab OSX bug)\n',ntasks-length(k))
        //    end
        //end
    
        k=find(info==0); // keep only absolutely successfull optimization runs
        q=q(:,k);omega=omega(:,k);f=f(k);info=info(k);
        
    elseif task=="multi"
        
        w=convexSampler(C,b,me,ntasks,wadm);
        qstarttab=w(ff,:);

        printf("%d tasks started, please wait\n\n",ntasks)

        [q,omega,f,info,iter]=parallel_run(1:ntasks,optimize_multi,[length(qstart) length(omegastart) 1 1 1]');

        if getos()=="Darwin" // MacOSX parallel_run bug
            k=find(or(q,'r')); 
            q=q(:,k);omega=omega(:,k);f=f(k);info=info(k);
        end
    
        k=find(info==0 | info==8 | info==3); // keep only semi-successfull optimization runs
        q=q(:,k);omega=omega(:,k);f=f(k);info=info(k);
        [bc,bk]=min(f);
        q=q(:,bk);
        omega=omega(:,bk);
        f=f(bk);
        info=info(bk);
        printf("\n\n Best residual = %f\n",bc); 
        // write best free fluxes set in original file ?
    end   
  else
    [q,omega,f,info,iter,mess,time]=optimize_single(algorithm,qstart,qmin,qmax,omegastart,omegamin,omegamax,eps_grad,eps_reg,eps_phi,miter,iprint);
  end

  mess.iter=iter(1);
  mess.time=time;
  infoStrings=['Normal termination'
  'Cannot find a feasible initial guess'
  'Cannot find a feasible initial guess'
  'Maximum number of iteration has been reached'
  'Step size smaller than machine precision'
  'Failure in constructing d0'
  'Failure in constructing d1'
  ''
  'New iterate is equal to previous iterate to machine precision'];
  if info($)==-1
    mess.info="Error during optimization"
  else
      mess.info=infoStrings(info($)+1);
  end
endfunction

function [q,omega,f,inform,iter,mess,time]=optimize_single(algorithm,qstart,qmin,qmax,omegastart,omegamin,omegamax,eps_grad,eps_reg,eps_phi,miter,iprint)
    // w0=flux.values verify the equality constrains (CS) and the inequality
    // CS that why we reduce our problem by neglecting equality CS and working
    // with only inequality CS. In the other hand, we remove all evident 
    // inequality and zeros line of C(me+1:$,:)*W from inequality CS. 
    
    global lastobj iteration lastqomega
    
    lastobj=%inf
    iteration=1
    mess=[];
    
	nff=length(qstart);
	group_nb=length(omegastart);
    
    Cineq=C(me+1:$,:); bineq=b(me+1:$);
    Cq=Cineq*W;
    bq=bineq-Cineq*w0;
    ineq_constr=find(clean(sum(abs(Cq),'col'))~=0);
    Cq=Cq(ineq_constr,:);
    bq=bq(ineq_constr,:);

    // detection of pure bound constraints to treat as such

    bc=[];
    for i=1:size(Cq,1);
        j=find(Cq(i,:));
        if length(j)==1
            bc=[bc i];
            if Cq(i,j)<0 then
                qmin(j)=max(qmin(j),bq(i)/Cq(i,j));
            else
                qmax(j)=min(qmax(j),bq(i)/Cq(i,j));
            end
        end
    end
    
    Cq(bc,:)=[];
    bq(bc)=[];

    nineq=size(Cq,1); // is the number of linear inequalities
    Cq=[Cq zeros(nineq,group_nb)];

    lower=[qmin;omegamin]; // lower bounds
    upper=[qmax;omegamax]; // upper bounds 

    if algorithm=='fsqp'

        nf=1; // is the number of cost function
        nineqn=0; // is the number of nonlinear inequalities
        neqn=0; // is the number of nonlinear equalities
        neq=0; // is the number of linear equalities
        modefsqp=100; // see documentation of fsqp
        ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,1*(iprint>0)];
        bigbnd=1.e10; // play the role of infinity
        epsneq=0.e0; // Maximum violation of nonlinear equality constraints allowed 
                     // by the user at an optimal point
        udelta=0.e0; // The perturbation size the user suggests to use in 
                     // approximating gradients by finite difference
        rpar=[bigbnd,eps_grad,epsneq,udelta];
        // the input _obj,_cntr,_grobj and _grcntr must be define as a function
        // inform is a parameter indicating the status of the execution of fsqp
        // f is a value of the cost function at q at the end of the execution
        // lambda are the values of the Lagrange multipliers at q 
        // at the end of execution
        // g are the values of all constraints at q at the end of execution
    
        q=qstart;
        omega=omegastart;
        inform=-1;
        f=0;
        try
            tic();
            [qomega,inform,f]=fsqp([qstart;omegastart],ipar,rpar,[lower,upper],_obj,_cntr,_grobj,_grcntr);
            iter=iteration;
            time=toc();
        catch
            [str,n,line,func]=lasterror();
            errclear(0);
            mess.str=str;
            mess.n=n;
            mess.line=line;
            mess.func=func;
            iter=iteration;
            qomega=lastqomega;
            return
        end

                                    
     elseif algorithm=='ipopt'
    
       [sp_dcntr,sp_dcntr_vect]=SparseMatrixIndices(Cq); // return the indices of the sparse matrix dcntr
        // the input ipopt_obj,ipopt_cntr,ipopt_grobj and ipopt_dcntr must be define as a function
        // vector linear type for the variable in qomegastart 
        // = 0 for linear or = 1 for nonlinear
        var_lin_type=ones(nff+group_nb,1);
        // vector linear type for the constraints (equality or inequality)
        // = 0 for linear or = 1 for nonlinear
        constr_lin_type=zeros(nineq,1);
        constr_rhs=zeros(nineq,1); // upper bound for the constraints
        constr_lhs=-%inf*ones(nineq,1); // lower bound for the constraints
        // Ipopt parameters
        params = init_param();
        params = add_param(params,'jac_d_constant','yes');
        params = add_param(params,'hessian_approximation','limited-memory'); // quasi-Newton approximation of hessian

        params = add_param(params,'journal_level',(iprint>0)*5);
        params = add_param(params,'derivative_test','first-order')
        params = add_param(params,'max_iter',miter);
        params = add_param(params,'tol',eps_grad);
        
        [qomega,f,extra]=ipopt([qstart;omegastart],ipopt_obj,ipopt_grobj,...
                               ipopt_cntr,ipopt_dcntr,sp_dcntr,[],[],...
                               var_lin_type,constr_lin_type,constr_rhs,constr_lhs,lower,upper,params);
        // extra is a parameter indicating the status of the execution of ipopt
        // f is the value of the cost function at q at the end of the execution
    end
    q=qomega(1:nff);
    omega=qomega(nff+1:$);
endfunction

function [value,gradient]=all_stuff(_qomega,_algorithm)

    global lastobj iteration lastqomega
    
    lastqomega=_qomega
    
    // We build in the function all cost function and its gradient
    
    _q=_qomega(1:nff);
    _omega=_qomega(nff+1:$);

    // Computation of a net/xch flux vector w=W*q+w0 using free fluxes.

    w=W*_q+w0;

		// Computation of flux observation residual

    e_flux=E*w-wmeas;
    flux_error=Svpm2.*e_flux.^2;

		// Computation of a fwd/rev flux vector

    [v,dv_dw]=Phi(w,eps_phi);

		// Solve the state equation and compute the label observation residual and gradient

    [label_error,gradv,gradomega]=solveCumomersAndGrad(v,_omega);

		// Overall cost and gradient final computation

    value=sum(label_error)+sum(flux_error)+eps_reg*_q'*_q;
    gradient=[W'*dv_dw'*gradv+2*W'*E'*(Svpm2.*e_flux)+2.*eps_reg*_q;gradomega];

		// Iteration report

    if value<=lastobj
      lastobj=value;
      iteration=iteration+1;
      if iprint>0 & _algorithm=='fsqp'
          printf("iter=%5d, obj=%14.7e\r",iteration,value);
      end
    end
   
endfunction

function [flux,scale,meas,label_error,flux_error,X]=measurements_and_stats(q,omega,eps_phi,info)
    
	// This function return the standard deviation of w=W*q+w0 and scaling parameters omega.
	// The actual number of "free" parameters depend on the saturation of constraints
	// at the optimum.

	nff=size(q,1);
	group_nb=size(omega,1);

	w=W*q(:,1)+w0; 	// get the net/xch flux vector

	e_flux=E*w-wmeas;
	flux_error=Svpm2.*e_flux.^2;

	// Computation of the fwd/rev flux vector

	[v,dv_dw]=Phi(w,eps_phi);

	// Solve the state equation, then compute the label observation residual and gradient

	[label_error,X,y,dy_dv,dy_domega]=solveCumomersAndDerivative(v,omega(:,1));
	meas=tlist(['meas' 'names' 'values'],measurement_ids,y);

    flux=tlist(['fluxes','names','values','sigma'],flux_ids,w,zeros(w));
    scale=tlist(['scale','names','values','sigma'],scaling_ids,omega,zeros(omega));    
	
    if info<0
        return
    elseif size(q,2)>1 // in case of Monte Carlo stats
		flux.sigma=stdev(W*q,'c');
  	scale.sigma=stdev(omega(:,1),'c');
	else
		// Compute orthogonal basis of subspace with saturated constraints
    // for scaling parameters  

    basis_omega=eye(group_nb,group_nb);
    basis_omega(:,find(omega==omega_min | omega==omega_max))=[];

    // Here, we denote by Y the full observation (labeling and fluxes) [y;E*v]
    // The derivative below is made with respect to free parameters q, omega

    dYdq=[dy_dv*dv_dw*W;E*dv_dw*W];
    dYdomega=[dy_domega*basis_omega;zeros(size(E,1),size(basis_omega,2))];
    [U,S,V]=svd(dYdq);
    r=rank(dYdq);
    
    [i1,i2]=find(abs(V(:,r+1:$))>1e-6)
    nonId=unique(i1);
    //if i1<>[]
    //    printf('The Jacobian is singular and the following free fluxes are (locally) non-identifiable : ')
    //    printf('%s,',flux_ids(ff(nonId)));
    //    printf('\nConsider increasing the regularization parameter if the optimization was not making good progress\n\n')
    //end
    V=V(:,1:r);
    dYdqomega=[dYdq*V dYdomega];
    F=dYdqomega'*diag([Sypm2;Svpm2])*dYdqomega;

    try
        // Covariance matrix is the inverse of Fisher matrix
        C=inv(F);
        flux.sigma=sqrt(diag(W*V*C(1:r,1:r)*V'*W'));            
        flux.sigma(ff(nonId))=%inf;
        if basis_omega==[]
            scale.sigma=zeros(omega);
        else        
            scale.sigma=sqrt(diag(basis_omega*C(r+1:$,r+1:$)*basis_omega'));
        end
    catch
        printf('Fisher matrix is singular, cannot compute standard deviations !\n')
    end	
	
	end
		
endfunction

function y=sprintfnumber(number)
    if floor(number)==number then
        y=sprintf('%g',number);
    else
        y=sprintf('%.16g',clean(number));
    end
endfunction

function writeFreeFluxes(flux,_file)
    doc=xmlRead(_file);
    variables=xmlXPath(doc,'//f:variables',['f','http://www.13cflux.net/fluxml']);
    xmlRemove(variables(1).children);
    for i=1:nff
        f = xmlElement(doc,"fluxvalue");
        f.attributes.flux=part(flux.names(ff(i)),1:$-2);
        if part(flux.names(ff(i)),$-1:$)=='_n'
            f.attributes.type='net';
        else
            f.attributes.type='xch';
        end
        f.content=sprintf('%.16g',clean(flux.values(ff(i))))
        xmlAppend(variables(1),f)
    end
    xmlWrite(doc)
    xmlDelete(doc)
endfunction

function writeMessage(message,outputname)
	doc=xmlRead(outputname+'.params.xml');
	params=xmlXPath(doc,'//smtb:params',['smtb','http://www.utc.fr/sysmetab']);
    if isfield(message,'str')
    	param=xmlElement(doc,"param");
    	param.attributes.name="error"
    	param.attributes.type="string";
    	param.content=sprintf("%s (function %s at line %d)",message.str,message.func,message.line);
    	xmlAppend(params(1),param)
	end
    xmlRemove(xmlXPath(doc,'//smtb:params/smtb:param[@name=""iterations""]',['smtb','http://www.utc.fr/sysmetab']))
    xmlRemove(xmlXPath(doc,'//smtb:params/smtb:param[@name=""time""]',['smtb','http://www.utc.fr/sysmetab']))
    xmlRemove(xmlXPath(doc,'//smtb:params/smtb:param[@name=""info""]',['smtb','http://www.utc.fr/sysmetab']))   
    param=xmlElement(doc,"param");
	param.attributes.name="iterations"
	param.attributes.type="number";
	param.content=string(message.iter);
	xmlAppend(params(1),param)
	param.attributes.name="time"
	param.attributes.type="number";
	param.content=string(message.time);
	xmlAppend(params(1),param)
	param.attributes.name="info"
	param.attributes.type="string";
	param.content=string(message.info);
	xmlAppend(params(1),param)
    xmlWrite(doc)
    xmlDelete(doc)
endfunction

function gr=groupResidual(residual)
    // sums the squared residual vector among each measurement group
    for i=1:max(dy_domega_ij(:,2))
        j=find(dy_domega_ij(:,2)==i)
        gr(i)=sum(residual(j));
    end
endfunction

function [out]=results_fwd(f,o,m,X,residual,message,filename,outputname,SYSMETAB)
    // This script modify step3.xml file in order to save results obtain 
    // with scilab after optimization and transfer them to a fwd file wich
    // contains all details.
    ftypetext.f='free';
    ftypetext.d='dependent';
    ftypetext.c='constraint';
    
  	doc=xmlRead(outputname+'.params.xml');
  	params=xmlXPath(doc,'//smtb:params/smtb:param[@name=''scaling'']',['smtb','http://www.utc.fr/sysmetab']);
    scaling=params(1).content;
    xmlDelete(doc)
    //
    doc=xmlRead(filename+'.step3.xml');
    // add stoichiometry paragraph which will contain the type and the value of
    // fluxes id.
    nfluxes=size(f.names,'*')/2;
    fluxml=xmlXPath(doc,'//f:fluxml',['f','http://www.13cflux.net/fluxml']);
    stoich=xmlElement(doc,"stoichiometry");
    doc1 = xmlReadStr("<root xmlns=""http://www.13cflux.net/fwdsim""/>");
    NS=xmlGetNsByHref(doc1.root, "http://www.13cflux.net/fwdsim")
    for i=1:nfluxes
        flux=xmlElement(doc,"flux");
        flux.attributes.id=part(f.names(i),1:$-2);
        //
        flux_net= xmlElement(doc,"net");
        xmlSetAttributes(flux_net,['type' ftypetext(ftype(i))
                                   'value' sprintfnumber(f.values(i))
                                   'stddev' sprintfnumber(f.sigma(i))] )
        if ftype(i)=='d'
            vect=find(clean(W(i,:)));
            form='';
            if clean(w0(i))<>0
                form=string(w0(i));
            end
            for j=1:length(vect)
                coef=W(i,vect(j));
                coefs='';
                if coef>0 & form<>''
                    coefs='+'
                elseif coef<0
                    coefs='-'
                end
                if clean(abs(coef)-1)
                    coefs=coefs+string(abs(coef))+'*';
                end
                form=form+coefs+part(f.names(ff(vect(j))),1:$-2)+'.n';
            end
        else
          form=part(f.names(i),1:$-2)+'.n';;
        end
        flux_net.content=form;
        xmlAppend(flux,flux_net);
        //
        flux_xch= xmlElement(doc,"xch");
        xmlSetAttributes(flux_xch,['type' ftypetext(ftype(nfluxes+i))
                                   'value' sprintfnumber(f.values(nfluxes+i))
                                   'stddev' sprintfnumber(f.sigma(nfluxes+i))] )


       if ftype(nfluxes+i)=='d'
           vect=find(clean(W(nfluxes+i,:)));
           form='';
           if clean(w0(nfluxes+i))<>0
               form=string(w0(nfluxes+i));
           end
           for j=1:length(vect)
               coef=W(nfluxes+i,vect(j));
               coefs='';
               if coef>0 & form<>''
                   coefs='+'
               elseif coef<0
                   coefs='-'
               end
               if clean(abs(coef)-1)
                   coefs=coefs+string(abs(coef))+'*';
               end
               form=form+coefs+part(f.names(ff(vect(j))),1:$-2)+'.x';
           end
       else
         form=part(f.names(nfluxes+i),1:$-2)+'.x';;
       end
       flux_xch.content=form;

        
        xmlAppend(flux,flux_xch);
        xmlAppend(stoich,flux);
    end
    xmlAddNs(stoich,NS);
    xmlAppend(fluxml(1),stoich);
    
    // adding value for each smtb:cumomer in smtb:listOfIntermediateCumomers 
    listOfIntermediateCumomers=xmlXPath(doc,'//smtb:listOfIntermediateCumomers/smtb:listOfCumomers',...
                                        ['smtb','http://www.utc.fr/sysmetab']);
    number_listOfCumomers=size(listOfIntermediateCumomers,'*');
    for w=1:number_listOfCumomers
        number_cumomer=size(listOfIntermediateCumomers(w).children,'*');
        for i=1:number_cumomer
            value=listOfIntermediateCumomers(w).children(i);
            number=evstr(listOfIntermediateCumomers(w).children(i).attributes.number);
            value.content=sprintfnumber(X(number));
        end
    end
    // adding scaling value, residual and measurement values for each f:group
    norm_residu=groupResidual(residual);
    labelingmeasurement=xmlXPath(doc,'//f:labelingmeasurement',['f','http://www.13cflux.net/fluxml']);
    xmlSetAttributes(labelingmeasurement(1),['residual' sprintfnumber(sum(norm_residu))]);
    k=0;
    for i=1:size(labelingmeasurement(1).children,'*') // for each group
        mgroup=labelingmeasurement(1).children(i);
        fact=1;
        if (mgroup.attributes.scale=='auto')
          if scaling<>'no'
            fact=evstr(mgroup.attributes.sum);
          end
        elseif ~isempty(mgroup.attributes.scale)
          fact=evstr(mgroup.attributes.scale);
        end
        xmlSetAttributes(mgroup,...
        ['scale' sprintfnumber(o.values(i)*fact)
         'norm'  sprintfnumber(norm_residu(i))]);
        number_measurement=size(mgroup.children,'*')-1;
        for j=1:number_measurement // for each value in the group
            value=mgroup.children(j+1);
            value.content=sprintfnumber(m.values(j+k)*fact);
        end
        k=k+number_measurement;
    end
    fluxmeasurement=xmlXPath(doc,'//f:fluxmeasurement',['f','http://www.13cflux.net/fluxml']);
    xmlSetAttributes(fluxmeasurement(1),['residual' sprintfnumber(sum(norm_fm))]);
    netflux=xmlXPath(doc,'//f:fluxmeasurement/f:netflux',['f','http://www.13cflux.net/fluxml']);
    for i=1:size(wmeas,'*')
        xmlSetAttributes(netflux(i),...
        ['norm'  sprintfnumber(norm_fm(i))
         'value' sprintfnumber(E(i,:)*f.values)]);
    end
    xmlWrite(doc,outputname+'.step5.xml')
    xmlDelete(doc)
    unix_g('xsltproc --stringparam output '+outputname+' '+SYSMETAB+'/XSL/fmlsys_gen/fmltofwd.xsl '+outputname+'.step5.xml'+' > '+outputname+'.fwd')
    out=1;
endfunction
