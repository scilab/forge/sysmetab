<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Tue Oct 08 17:00:00 CET 2013
    Projet  :   PIVERT/Metalippro-PL1
-->

<!--
    This stylesheet transform a sbml file into a fml file    
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func">
  
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <xsl:key name="lblinpmeas" match="sbml:species/sbml:notes" use="substring-before(normalize-space(translate(.,'&#10;&#13;','')),' ')"/>

  <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sbml:sbml">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </fluxml>
  </xsl:template>

  <xsl:template match="sbml:model">
    <info xmlns="http://www.13cflux.net/fluxml">
      <name>
        <xsl:value-of select="@id"/>
      </name>
    </info>
    <reactionnetwork xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="sbml:listOfSpecies"/>
      <xsl:apply-templates select="sbml:listOfReactions"/>
    </reactionnetwork>	
    <constraints xmlns="http://www.13cflux.net/fluxml">
      <xsl:for-each select="sbml:listOfRules">  
        <net xmlns="http://www.13cflux.net/fluxml">
          <math xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:for-each select="sbml:algebraicRule/m:math">
              <xsl:choose>
                <xsl:when test="m:apply[m:minus and not(m:cn)]">
                  <apply>
                    <eq/>
                    <xsl:copy-of select="*"/>
                    <cn>0</cn>
                  </apply>
                </xsl:when>
                <xsl:when test="normalize-space(m:apply/m:cn)!='0'">
                  <apply>
                    <eq/>
                    <xsl:copy-of select="m:apply/*[2]"/>
                    <xsl:copy-of select="m:apply/*[3]"/>
                  </apply>
                </xsl:when>
              </xsl:choose>
            </xsl:for-each>
          </math> 
        </net>
      </xsl:for-each>
      <xch>
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:for-each select="sbml:listOfReactions/sbml:reaction[@reversible='false']">
            <apply>
              <eq/>
              <ci>
              <xsl:value-of select="@id"/>
              </ci>
              <cn>0</cn>
            </apply>
          </xsl:for-each>
        </math>
      </xch>
    </constraints>
    <configuration xmlns="http://www.13cflux.net/fluxml">
      <xsl:for-each select="key('lblinpmeas','LABEL_INPUT')">
        <input pool="{../@id}">
          <xsl:for-each select="str:split(substring-after(normalize-space(translate(.,'&#10;&#13;','')),' '),',')">
            <label cfg="{.}" type="isotopomer">0</label>
          </xsl:for-each>
        </input>
      </xsl:for-each>
      <measurement>
        <model>
          <labelingmeasurement>
            <xsl:for-each select="key('lblinpmeas','LABEL_MEASUREMENT')">
              <xsl:variable name="counter" select="position()"/>
              <xsl:variable name="ID" select="../@id"/>
              <xsl:for-each select="str:split(substring-after(normalize-space(translate(.,'&#10;&#13;','')),' '),';')">
                <xsl:variable name="counter2" select="concat($ID,'_',position())"/>
                <group scale="auto">
                  <xsl:choose>
                    <xsl:when test="contains(.,'#')">
                      <xsl:attribute name="id">
                        <xsl:value-of select="concat('ms_group_',$counter2)"/>
                      </xsl:attribute>
                      <textual><xsl:value-of select="$ID"/><xsl:value-of select="normalize-space(translate(.,'&#10;&#13;',''))"/></textual>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="id">
                        <xsl:value-of select="concat('lm_group_',$counter2)"/>
                      </xsl:attribute>
                      <textual><xsl:value-of select="$ID"/>#<xsl:value-of select="normalize-space(translate(.,'&#10;&#13;',''))"/></textual>
                    </xsl:otherwise>
                  </xsl:choose>
                </group>
              </xsl:for-each>
            </xsl:for-each>
          </labelingmeasurement>  
          <fluxmeasurement>
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction/sbml:annotation/celldesigner:extension/celldesigner:reactionType[.='KNOWN_TRANSITION_OMITTED']">
              <netflux id="{concat('fm_',position())}">
                <textual><xsl:value-of select="../../../@id"/></textual>
              </netflux>
            </xsl:for-each>
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction/sbml:annotation/celldesigner:extension/celldesigner:reactionType[.='TRANSPORT']">
              <netflux id="{concat('fi_',position())}">
                <textual><xsl:value-of select="../../../@id"/></textual>
              </netflux>
            </xsl:for-each>
          </fluxmeasurement>
        </model>
      </measurement>
    </configuration>
  </xsl:template>

  <xsl:template match="sbml:listOfSpecies">
    <metabolitepools xmlns="http://www.13cflux.net/fluxml">	
      <xsl:apply-templates/>
    </metabolitepools>		
  </xsl:template>
  
  <xsl:template match="sbml:species[key('reactants',@id)]">
    <pool id="{@id}" xmlns="http://www.13cflux.net/fluxml"/>
  </xsl:template>

  <xsl:template match="sbml:listOfReactions">
    <xsl:apply-templates select="sbml:reaction"/>
  </xsl:template>
  
  <xsl:template match="sbml:reaction">
    <reaction id="{@id}" xmlns="http://www.13cflux.net/fluxml">	
      <xsl:for-each select="sbml:listOfReactants/sbml:speciesReference">
        <xsl:variable name="pos" select="position()"/>
        <xsl:choose>
          <xsl:when test="../../sbml:notes">
	        <reduct cfg="{normalize-space(translate(str:split(translate(substring-before(../../sbml:notes,' &gt; '),'&#10;&#13;',''),'+')[$pos],'&#10;&#13;',''))}" id="{@species}" xmlns="http://www.13cflux.net/fluxml"/>
          </xsl:when>
          <xsl:otherwise>
            <reduct id="{@species}" xmlns="http://www.13cflux.net/fluxml"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
      <xsl:for-each select="sbml:listOfProducts/sbml:speciesReference">
        <xsl:variable name="pos" select="position()"/>
        <xsl:if test="key('reactants',@species)">
          <rproduct cfg="{normalize-space(translate(str:split(translate(substring-after(../../sbml:notes,' &gt; '),'&#10;&#13;',''),'+')[$pos],'&#10;&#13;',''))}" id="{@species}" xmlns="http://www.13cflux.net/fluxml"/>
        </xsl:if>
      </xsl:for-each>
    </reaction>
  </xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>