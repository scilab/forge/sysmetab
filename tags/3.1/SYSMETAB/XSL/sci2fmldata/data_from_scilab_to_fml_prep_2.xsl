<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:ftbl="http://ftbl.org"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func ftbl">
  
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()">    
      <xsl:copy>
          <xsl:apply-templates select="*|@*|text()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="fml">
    <fml>
      <xsl:apply-templates />
    </fml>
  </xsl:template>  

  <xsl:template match="row[@sec]">
    <xsl:variable name="posnext" select="following-sibling::row[@sec][1]/@position"/>
    <section>
      <xsl:attribute name="id">
        <xsl:value-of select="@sec"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="string-length($posnext)=0">
          <xsl:copy-of select="following-sibling::row/."/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="following-sibling::row[@position &lt;$posnext]">
            <row position="{position()}">
              <xsl:copy-of select="text()"/>
            </row>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
    </section>
  </xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>  