<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:ftbl="http://ftbl.org"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func ftbl">
  
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()">    
      <xsl:copy>
          <xsl:apply-templates select="*|@*|text()"/>
      </xsl:copy>
  </xsl:template>
  
  <xsl:template match="fml">
    <fml>
      <section id="Ids">
        <xsl:apply-templates select="section[@id='Ids']/row"/>
      </section>
      <section id="Values">
        <xsl:apply-templates select="section[@id='Values']/row"/>
      </section>
    </fml>
  </xsl:template>  

  <xsl:template match="section/row">
    <row position="{position()}">
      <xsl:copy-of select="text()"/>
    </row>
  </xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>  