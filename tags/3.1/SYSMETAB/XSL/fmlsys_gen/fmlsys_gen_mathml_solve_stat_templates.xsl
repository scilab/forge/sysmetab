<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
  
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- File name -->
  <xsl:param name="file"/>

  <!-- Solve cumomers, cost function and gradient of the cost function for Direct and Adjoint method -->
  <xsl:template name="solveCumomersAndGrad">
    <xsl:param name="method"/>
    <function xmlns="http://www.utc.fr/sysmetab">
      <!-- functions name -->
      <xsl:choose>
        <xsl:when test="$method='none'">
          <ci xmlns="http://www.w3.org/1998/Math/MathML">solveCumomers</ci>
        </xsl:when>
        <xsl:otherwise>
          <ci xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:value-of select="concat('solveCumomersAndGrad',$method)"/>
          </ci>
        </xsl:otherwise>
      </xsl:choose>
      <!-- functions input -->
      <input>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <ci>v</ci>
          <ci>omega</ci>
          <xsl:for-each select="f:reactionnetwork/smtb:listOfInputCumomers/smtb:listOfCumomers">
            <ci>
              <xsl:value-of select="concat('x',@weight,'_input')"/>
            </ci>
          </xsl:for-each>
        </list>
      </input>
      <!-- functions output -->
      <output>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:if test="$method!='none'">
            <ci>residual</ci>
            <ci>gradv</ci>
            <ci>gradomega</ci>
          </xsl:if>
          <ci>X</ci>
          <ci>y</ci>
          <xsl:if test="$method='Direct'">
            <ci>dy_dv</ci>
            <ci>dy_domega</ci>
          </xsl:if>
          <xsl:if test="$method='Direct'">
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <ci>
                <xsl:value-of select="concat('dx',@weight,'_dv')"/>
              </ci>
            </xsl:for-each>
          </xsl:if>
        </list>
      </output>
      <!-- functions body -->
      <body>

        <comment>Solve the state equation cascade</comment>

        <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
          <xsl:variable name="current_weight" select="@weight"/>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <list xmlns="http://www.w3.org/1998/Math/MathML" separator=",">
              <ci>
                <xsl:value-of select="concat('x',@weight)"/>
              </ci>
              <ci>
                <xsl:value-of select="concat('M',@weight,'_handle')"/>
              </ci>
              <xsl:if test="$method!='none'">
                <ci>
                  <xsl:value-of select="concat('dg',@weight,'_dv')"/>
                </ci>
                <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
                  <xsl:sort select="@weight" order="ascending" data-type="number"/>
                  <ci>
                    <xsl:value-of select="concat('db',$current_weight,'_dx',@weight)"/>
                  </ci>
                </xsl:for-each>
              </xsl:if>
            </list>
            <apply>
              <fn>
                <ci>
                  <xsl:value-of select="concat('solve_weight_',@weight)"/>
                </ci>
              </fn>
              <ci>v</ci>
              <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
                <xsl:sort select="@weight" order="ascending" data-type="number"/>
                <ci>
                  <xsl:value-of select="concat('x',@weight)"/>
                </ci>
              </xsl:for-each>
              <xsl:if test="../../smtb:listOfInputCumomers/smtb:listOfCumomers[@weight=current()/@weight]">
                <ci>
                  <xsl:value-of select="concat('x',@weight,'_input')"/>
                </ci>
              </xsl:if>
            </apply>
          </apply>
        </xsl:for-each>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>X</ci>
          <list separator=";">
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">                
              <ci>
                <xsl:value-of select="concat('x',@weight)"/>
              </ci>
            </xsl:for-each>
          </list>
        </apply>

        <comment>Compute the simulated measurements</comment>

        <!-- Numeric observation  -->
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>y_unscale</ci>
          <apply>
            <plus/>
            <ci>C0</ci>
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <apply>
                <times/>
                <ci>
                  <xsl:value-of select="concat('C',@weight)"/>
                </ci>
                <ci>
                  <xsl:value-of select="concat('x',@weight)"/>
                </ci>
              </apply>
            </xsl:for-each>
          </apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>Omega</ci>
          <apply>
            <selector/>
						<ci type="vector">omega</ci>
						<apply>
							<selector/>
							<ci type="matrix">dy_domega_ij</ci>
							<cn>:</cn>
							<cn>2</cn>
            </apply>
					</apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>y</ci>
          <apply>
            <times type="array"/>
            <ci>Omega</ci>
            <ci>y_unscale</ci>    
          </apply>
        </apply>

        <comment>Compute the residual</comment>

        <!-- e_label=y-ymeas; -->
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>e_label</ci>
          <apply>
            <minus/>
            <ci>y</ci>
            <ci>ymeas</ci>
          </apply>
        </apply>
        <!-- Sypm2.*e_label -->
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>Sypm2_e_label</ci>
            <apply>
              <times type="array"/>
              <ci>Sypm2</ci>
              <ci>e_label</ci>
            </apply>
          </apply>
        <!-- residual=Sypm2.*e_label.^2 -->
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>residual</ci>
            <apply>
              <times type="array"/>
              <ci>Sypm2_e_label</ci>
              <ci>e_label</ci>
            </apply>
          </apply>

          <!-- Gradient computation -->
        <xsl:choose>
          <xsl:when test="$method='Direct'">
            
            <comment>Compute the state derivatives</comment>

            <!-- System resolution for dx_weight/dv -->
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <xsl:variable name="current_weight" select="@weight"/>
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>
                  <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                </ci>
                <apply>
                  <minus/>
                  <apply>
                    <fn><ci>umf_lusolve</ci></fn>
                    <ci>
                      <xsl:value-of select="concat('M',@weight,'_handle')"/>
                    </ci>
                    <apply xmlns="http://www.w3.org/1998/Math/MathML">
                      <plus/>
                      <xsl:choose>
                        <xsl:when test="position()=1">
                          <apply>
                            <fn><ci>full</ci></fn>
                            <ci>
                              <xsl:value-of select="concat('dg',@weight,'_dv')"/>
                            </ci>
                          </apply>
                        </xsl:when>
                        <xsl:otherwise>
                          <ci>
                            <xsl:value-of select="concat('dg',@weight,'_dv')"/>
                          </ci>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
                        <apply>
                          <times/>
                          <ci>
                            <xsl:value-of select="concat('db',$current_weight,'_dx',@weight)"/>
                          </ci>
                          <ci>
                            <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                          </ci>
                        </apply>
                      </xsl:for-each>
                    </apply>
                  </apply>
                </apply>
              </apply>
            </xsl:for-each>
            
            <comment>Compute the gradient (forward method)</comment>

            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>dy_dv</ci>
              <apply>
                <times/>
                <apply>
                  <fn><ci>spdiag</ci></fn>
                  <apply>
                    <ci>Omega</ci>
                  </apply>
                </apply>
                <apply>
                  <plus/>
                  <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
                    <apply>
                      <times/>
                      <ci>
                        <xsl:value-of select="concat('C',@weight)"/>
                      </ci>
                      <ci>
                        <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                      </ci>
                    </apply>
                  </xsl:for-each>
                </apply>
              </apply>
            </apply>
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>gradv</ci>            
              <apply>
                <times/>
                <cn>2</cn>
                <apply>
                  <transpose/>
                  <ci>dy_dv</ci>
                </apply>
                <ci>Sypm2_e_label</ci>
              </apply>
            </apply>
          </xsl:when>
          <xsl:when test="$method='Adjoint'">
                 
            <comment>Compute the adjoint states</comment>

            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>Omega_Sypm2_e_label_t</ci>
              <apply>
                <transpose/>
                <apply>
                  <times type="array"/>
                  <ci type="matrix">Omega</ci>
                  <ci type="matrix">Sypm2_e_label</ci>
                </apply>
              </apply>
            </apply>
            <!-- System resolution for p_weight -->
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <xsl:sort select="@weight" order="descending" data-type="number"/>
              <xsl:variable name="current_weight" select="@weight"/>
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>
                  <xsl:value-of select="concat('p',@weight)"/>
                </ci>
                <apply>
                  <fn><ci>umf_lusolve</ci></fn>
                  <ci>
                    <xsl:value-of select="concat('M',@weight,'_handle')"/>
                  </ci>
                  <apply xmlns="http://www.w3.org/1998/Math/MathML">
                    <transpose/>
                    <apply>
                      <apply>
                        <xsl:if test="following-sibling::smtb:listOfCumomers">
                          <minus/>
                        </xsl:if>
                        <apply>
                          <times/>
                          <ci type="matrix">Omega_Sypm2_e_label_t</ci>
                          <ci>
                            <xsl:value-of select="concat('C',@weight)"/>
                          </ci>
                        </apply>
                        <xsl:if test="following-sibling::smtb:listOfCumomers">
                          <apply>
                            <plus/>
                            <xsl:for-each select="following-sibling::smtb:listOfCumomers">
                              <apply>
                                <times/>
                                <apply>
                                  <transpose/>
                                  <ci type="hypermatrix">
                                    <xsl:value-of select="concat('p',@weight)"/>
                                  </ci>
                                </apply>
                                <ci>
                                  <xsl:value-of select="concat('db',@weight,'_dx',$current_weight)"/>
                                </ci>
                              </apply>
                            </xsl:for-each>
                          </apply>
                        </xsl:if>
                      </apply>
                    </apply>
                  </apply>
                  <string xmlns="http://www.utc.fr/sysmetab">A&apos;&apos;x=b</string>
                </apply>
              </apply>
            </xsl:for-each>
            
            <comment>Compute the gradient (adjoint method)</comment>
            
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>gradv</ci>            
              <apply>
                <times/>
                <cn>-2</cn>
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <transpose/>
                  <apply>  
                    <apply>
                      <plus/>
                      <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
                        <apply>
                          <times/>
                          <apply>
                            <transpose/>
                            <ci>
                              <xsl:value-of select="concat('p',@weight)"/>
                            </ci>
                          </apply>
                          <ci>
                            <xsl:value-of select="concat('dg',@weight,'_dv')"/>
                          </ci>                        
                        </apply>
                      </xsl:for-each>
                    </apply>
                  </apply>
                </apply>
              </apply>
            </apply>
          </xsl:when>
        </xsl:choose>
    
        <xsl:if test="$method!='none'"> <!-- gradient computation for all but solveCumomers() function -->
        					
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
						<eq/>
						<ci>dy_domega</ci>
						<apply>
							<fn>
								<ci>s_full</ci>
							</fn>
							<ci>dy_domega_ij</ci>
							<ci>y_unscale</ci>
								<cn>
									<xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement)"/>
								</cn>
								<cn>
									<xsl:value-of select="count(f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group)"/>
								</cn>
						</apply>
					</apply>
        
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>gradomega</ci>            
            <apply>
              <times/>
              <cn>2</cn>
              <apply>
                <times/>
                <apply>
                  <transpose/>
                  <ci>dy_domega</ci>
                </apply>
                <ci>Sypm2_e_label</ci>
              </apply>
            </apply>
          </apply>

        </xsl:if>

        <comment>Free the memory occupied by the LU factors</comment>

        <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
          <xsl:sort select="@weight" order="descending" data-type="number"/>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>err</ci>
            <apply>
              <fn><ci>umf_ludel</ci></fn>
              <ci>
                <xsl:value-of select="concat('M',@weight,'_handle')"/>
              </ci>
            </apply>
          </apply>
        </xsl:for-each>
      </body>
    </function>
  </xsl:template>

</xsl:stylesheet>