<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- 
    System resolution for x_weight
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">

  <xsl:param name="verb_level">1</xsl:param>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

  <!-- find all f:reduct from f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfInputCumomers -->
  <xsl:key name="INPUT_CUMOMER" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfIntermediateCumomers -->
  <xsl:key name="INTERMEDIATE_CUMOMER" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from f:pool -->
  <xsl:key name="CUMOMERS" match="f:pool/smtb:cumomer" use="@id"/>

  <!-- find all f:datum -->
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <!-- find all f:group -->
  <xsl:key name="GROUP" match="f:group" use="@id"/>

  <!-- Definition of all templates using in this style sheet -->
  <xsl:include href="fmlsys_gen_mathml_solve_stat_templates.xsl"/>

  <xsl:template match="/">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>Generating state equation code</xsl:message>
    </xsl:if>
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <!-- Solve cumomers, cost function and gradient of the cost function -->
    <xsl:call-template name="solveCumomersAndGrad">
      <xsl:with-param name="method">Direct</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="solveCumomersAndGrad">
      <xsl:with-param name="method">Adjoint</xsl:with-param>
    </xsl:call-template>
    <xsl:call-template name="solveCumomersAndGrad">
      <xsl:with-param name="method">none</xsl:with-param>
    </xsl:call-template>
    <!-- System resolution for x_weight -->
    <xsl:call-template name="solve_functions"/>
    <!-- Building observation matrices (in fml_common_gen_mathml.xsl) -->
    <xsl:call-template name="construct_matrix_C"/>
    <!-- Building index matrix for dy_domega -->
    <xsl:call-template name="groups"/>
  </xsl:template>
  
  <!-- All functions to find the x_weight -->
  <xsl:template name="solve_functions">
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <xsl:sort select="@weight" order="ascending" data-type="number"/>
      <xsl:variable name="current_weight" select="@weight"/>
      <function xmlns="http://www.utc.fr/sysmetab">
        <!-- functions name -->
        <ci xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:value-of select="concat('solve_weight_',@weight)"/>
        </ci>
        <!-- functions input -->
        <input>
          <list xmlns="http://www.w3.org/1998/Math/MathML">
            <ci>v</ci>
            <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
              <xsl:sort select="@weight" order="ascending" data-type="number"/>
              <ci>
                <xsl:value-of select="concat('x',@weight)"/>
              </ci>
            </xsl:for-each>
            <ci>
              <xsl:value-of select="concat('x',@weight,'_input')"/>
            </ci>
          </list>
        </input>
        <!-- functions output -->
        <output>
          <list xmlns="http://www.w3.org/1998/Math/MathML">
            <ci>
              <xsl:value-of select="concat('x',@weight)"/>
            </ci>
            <ci>
              <xsl:value-of select="concat('M',@weight,'_handle')"/>
            </ci>
            <ci>
              <xsl:value-of select="concat('dg',@weight,'_dv')"/>
            </ci>
            <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
              <xsl:sort select="@weight" order="ascending" data-type="number"/>
              <ci>
                <xsl:value-of select="concat('db',$current_weight,'_dx',@weight)"/>
              </ci>
            </xsl:for-each>
          </list>
        </output>
        <!-- functions body -->
        <body>
          <xsl:variable name="nc" select="count(smtb:cumomer)"/>
          <optimize xmlns="http://www.utc.fr/sysmetab">
            <matrix-open type="sparse" id="{concat('M',@weight)}" rows="{$nc}" cols="{$nc}"/>
            <matrix-open type="sparse" id="{concat('b',@weight)}" rows="{$nc}" cols="1"/>
            <matrix-open type="sparse" id="{concat('dg',@weight,'_dv')}" rows="{$nc}" cols="{count(../../f:reaction)}" />
            <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
              <xsl:sort select="@weight" order="ascending" data-type="number"/>
              <matrix-open type="sparse" id="{concat('db',$current_weight,'_dx',@weight)}" rows="{$nc}" cols="{count(smtb:cumomer)}"/>
            </xsl:for-each>

            <comment>Weight <xsl:value-of select="@weight"/> cumomers</comment>

            <!-- For each cumomer, we call the template generating various assignments, to the matrix in the 
            right hand side of the system whose solution is the cumomer vector of the current weight.
            The template "iteration" is in the style sheet fml_common_gen_mathml.xsl -->
						
            <xsl:for-each select="smtb:cumomer">
              <xsl:call-template name="iteration"/>
            </xsl:for-each>
            
						<matrix-close id="{concat('M',@weight)}" prepost="yes"/>
            <matrix-close id="{concat('b',@weight)}" prepost="yes"/>
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>
                <xsl:value-of select="concat('M',@weight,'_handle')"/>
              </ci>
              <apply>
                <fn>
                  <ci>umf_lufact</ci>
                </fn>
                <ci>
                  <xsl:value-of select="concat('M',@weight)"/>
                </ci>
              </apply>
            </apply>
            <!-- System resolution for x_weight -->
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci><xsl:value-of select="concat('x',@weight)"/></ci>
              <apply>
                <fn>
                  <ci>umf_lusolve</ci>
                </fn>
                <ci>
                  <xsl:value-of select="concat('M',@weight,'_handle')"/>
                </ci>
                <apply>
                  <minus/>
                  <ci>
                    <xsl:value-of select="concat('full(b',@weight,')')"/>
                  </ci>
                </apply>
              </apply>
            </apply>
            <matrix-close id="{concat('dg',@weight,'_dv')}" prepost="yes"/>
            <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
              <matrix-close id="{concat('db',$current_weight,'_dx',@weight)}" prepost="yes"/>
            </xsl:for-each>
          </optimize>
        </body>
      </function>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="iteration">
    <!-- Beware the context node is an element <smtb:cumomer>.
    The variable "carbons" contains the carbons 13 of the current pool -->
    <xsl:variable name="carbons">
      <xsl:copy-of select="key('CUMOMERS',@id)/smtb:carbon"/>
    </xsl:variable>
    <xsl:variable name="weight" select="../@weight"/>
    <xsl:variable name="position" select="@number"/>
    <!-- influx rule : the most difficult, but also the most interesting, it is here that truly creates 
    supplementary information compared to the simple stoichiometry.
    We loop all the "rproduct" elements that have the current cumomer pool as a product, so in the for-each the
    contextual node is of reaction/rproduct type. -->
    <xsl:for-each select="key('RPRODUCT',@pool)">
      <!-- Now, We try to find occurrences marked carbons of reactant of the current reaction: it needs work ...
      -->
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="reaction" select="../@position"/>
      <xsl:variable name="occurrence" select="count(preceding-sibling::f:rproduct)+1"/>
      <xsl:variable name="influx">
        <!-- This is where the serious stuff begins. We loop all the reactants which have carbons atoms which 
        "point" to the pool. -->
        <xsl:for-each select="../f:reduct[smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]]">
          <xsl:variable name="somme">
            <!-- So now, we loop all the carbons of reactant. If a carbon of reactant point to a carbon 13 of 
            cumomer of product, we note its number in an element <token/> -->
            <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]">
              <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                <token>
                  <xsl:value-of select="@position"/>
                </token>
              </xsl:if>
            </xsl:for-each>
            <!-- At the end of this loop, the "somme" variable contains a number, possibly zero, of elements
            <token> which identify without ambiguity the concerned cumomer. -->
          </xsl:variable>
          <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
            <!-- We sum up the <token>, this is a way to know if there is at least one ... -->
            <!-- We generate a new element <token> with a weight attribut specifying the weight of concerned 
            cumomer (the number of <token> in $somme), the type of the concerned pool (intermediate or input) 
            and its identifier, which is obtained by the sum of <token>. -->
            <token weight="{count(exslt:node-set($somme)/token)}" type="{key('POOL',@id)/@type}">
              <xsl:value-of select="concat(@id,'_',sum(exslt:node-set($somme)/token))"/>
            </token>
          </xsl:if>
        </xsl:for-each>
        <!-- At the end of this loop, the "influx" variable conteins a number of <token>. According to the type
        and the weight of concerned cumomers. It is either unknown, if the weight is the current weight $weight,
        or involved quantities in the right hand side as product form, whose the sum of weight is equal to the 
        current weight $weight. -->
      </xsl:variable>
      <!-- Here, we genrate the code which forms the matrix and the right hand side of the system which allows 
      to obtain the cumomers of weight $weight -->
      <xsl:choose>
        <xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
          <!-- We only have one term of the current weight, so the matrix is assembled so that its derivative -->
          <matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{key('INTERMEDIATE_CUMOMER',$influx)/@number}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <selector/>
              <ci type="vector">v</ci>
              <cn> <!-- reaction id -->
                <xsl:value-of select="$reaction"/>
              </cn>
            </apply>
          </matrix-assignment>
          <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <selector/>
              <ci type="vector">
                <xsl:value-of select="concat('x',$weight)"/>
              </ci>
              <cn>
                <xsl:value-of select="key('INTERMEDIATE_CUMOMER',$influx)/@number"/>
              </cn>
            </apply>
          </matrix-assignment>
        </xsl:when>
        <xsl:when test="count(exslt:node-set($influx)/token)&gt;=1">
          <!-- We have a product with severals terms, therefore lower weight, thus we assembles the right hand 
          side and the Jacobien ofthe right hand side in comparison of cumomers of lower weight. -->
          <xsl:variable name="influx-factors">
            <xsl:call-template name="iterate-influx">
              <xsl:with-param name="tokens">
                <xsl:copy-of select="$influx"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
          <matrix-assignment id="{concat('b',$weight)}" row="{$position}" col="1" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <times type="array"/>
              <apply>
                <selector/>
                <ci type="vector">v</ci>
                <cn> <!-- reaction id -->
                  <xsl:value-of select="$reaction"/>
                </cn>
              </apply>
              <xsl:copy-of select="$influx-factors"/>
            </apply>
          </matrix-assignment>
          <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <times type="array"/>
              <xsl:copy-of select="$influx-factors"/>
            </apply>
          </matrix-assignment>
          <xsl:call-template name="iterate-influx-jacobian-x">
            <xsl:with-param name="reaction" select="$reaction"/>
            <xsl:with-param name="weight" select="$weight"/>
            <xsl:with-param name="tokens">
              <xsl:copy-of select="$influx"/>
            </xsl:with-param>
            <xsl:with-param name="position" select="$position"/>
            <xsl:with-param name="i">1</xsl:with-param>
            <xsl:with-param name="n" select="count(exslt:node-set($influx)/token)"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
    <!-- outflux rule : the easiest part to generate (cf Wiechert paper) -->
    <matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{$position}" xmlns="http://www.utc.fr/sysmetab">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <minus/>
        <xsl:call-template name="bilan-outflux">
          <xsl:with-param name="id" select="@pool"/>
        </xsl:call-template>
      </apply>
    </matrix-assignment>
    <xsl:for-each select="key('REDUCT',@pool)">
      <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{../@position}" xmlns="http://www.utc.fr/sysmetab">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <minus/>
          <apply>
            <selector/>
            <ci type="vector">
              <xsl:value-of select="concat('x',$weight)"/>
            </ci>
            <cn>
              <xsl:value-of select="$position"/>
            </cn>
          </apply>
        </apply>
      </matrix-assignment>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="bilan-outflux">
    <xsl:param name="id"/>
    <!-- outflux rule -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <plus/>
      <xsl:for-each select="key('REDUCT',$id)">
        <apply>
          <selector/>
          <ci type="vector">v</ci>
          <cn> <!-- reaction id -->
            <xsl:value-of select="../@position"/>
          </cn>
        </apply>
      </xsl:for-each>
    </apply>
  </xsl:template>

  <xsl:template name="iterate-influx">
    <xsl:param name="tokens"/>
    <xsl:variable name="id" select="exslt:node-set($tokens)/token[1]"/>
    <xsl:choose>
      <xsl:when test="key('INTERMEDIATE_CUMOMER',$id)">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="vector">
            <xsl:value-of select="concat('x',key('INTERMEDIATE_CUMOMER',$id)/@weight)"/>
          </ci>
          <cn>
            <xsl:value-of select="key('INTERMEDIATE_CUMOMER',$id)/@number"/>
          </cn>
        </apply>
      </xsl:when>
      <xsl:when test="key('INPUT_CUMOMER',$id)">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="vector">
            <xsl:value-of select="concat('x',key('INPUT_CUMOMER',$id)/@weight,'_input')"/>
          </ci>
          <cn>
            <xsl:value-of select="key('INPUT_CUMOMER',$id)/@number"/>
          </cn>
        </apply>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="count(exslt:node-set($tokens)/token)&gt;1">
      <xsl:call-template name="iterate-influx">
        <xsl:with-param name="tokens">
          <xsl:copy-of select="exslt:node-set($tokens)/token[position()&gt;1]"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="iterate-influx-jacobian-x">
    <!-- Jacobien (derivative) generation dbi/dxj -->
    <xsl:param name="reaction"/>
    <xsl:param name="weight"/>
    <xsl:param name="tokens"/>
    <xsl:param name="position"/>
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:variable name="id" select="exslt:node-set($tokens)/token[position()=$i]"/>
    <xsl:if test="key('INTERMEDIATE_CUMOMER',$id)">
      <matrix-assignment id="{concat('db',$weight,'_dx',key('INTERMEDIATE_CUMOMER',$id)/@weight)}" row="{$position}" col="{key('INTERMEDIATE_CUMOMER',$id)/@number}" xmlns="http://www.utc.fr/sysmetab">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <times type="array"/>
          <xsl:call-template name="iterate-influx">
            <!-- The derivative is equal to whatever is in the variable factor -->
            <xsl:with-param name="tokens">
              <xsl:copy-of select="exslt:node-set($tokens)/token[position()!=$i]"/>
            </xsl:with-param>
          </xsl:call-template>
          <apply>
            <selector/>
            <ci type="vector">v</ci>
            <cn>
              <xsl:value-of select="$reaction"/>
            </cn>
          </apply>
        </apply>
      </matrix-assignment>
    </xsl:if>
    <xsl:if test="$i&lt;$n">
      <xsl:call-template name="iterate-influx-jacobian-x">
        <xsl:with-param name="reaction" select="$reaction"/>
        <xsl:with-param name="weight" select="$weight"/>
        <xsl:with-param name="tokens">
          <xsl:copy-of select="$tokens"/>
        </xsl:with-param>
        <xsl:with-param name="position" select="$position"/>
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Building of matrices C-->
  <xsl:template name="observation-matrices-open">
    <xsl:param name="number-of-observations"/>
    <matrix-open id="C0"
                 rows="{$number-of-observations}" 
                 cols="1" 
                 type="sparse" 
                 assignments="unique"
                 xmlns="http://www.utc.fr/sysmetab"/>
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <matrix-open id="C{@weight}" 
                   rows="{$number-of-observations}" 
                   cols="{count(smtb:cumomer)}" 
                   type="sparse" 
                   assignments="unique"
                   xmlns="http://www.utc.fr/sysmetab"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="observation-matrices-close">
    <xsl:param name="number-of-observations"/>
    <matrix-close id="C0" xmlns="http://www.utc.fr/sysmetab"/>
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <matrix-close id="C{@weight}" xmlns="http://www.utc.fr/sysmetab"/>
    </xsl:for-each>    
  </xsl:template>

  <xsl:template name="construct_matrix_C">

    <comment xmlns="http://www.utc.fr/sysmetab">Observation matrices (cumomers)</comment>

    <optimize xmlns="http://www.utc.fr/sysmetab">
      <!-- We open an item "optimize" in which we will gradually build these matrices -->
      <xsl:call-template name="observation-matrices-open">
        <xsl:with-param name="number-of-observations" select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement)"/>
      </xsl:call-template>
      <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement">
        <xsl:sort select="@id" data-type="text"/>
        <xsl:variable name="position" select="position()"/>
        <xsl:for-each select="smtb:cumomer-contribution">
          <xsl:choose>
            <xsl:when test="@weight=0">
              <matrix-assignment id="C0" row="{$position}" col="1">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
              </matrix-assignment>
            </xsl:when>
            <xsl:otherwise>  
              <matrix-assignment id="C{@weight}" row="{$position}" col="{key('INTERMEDIATE_CUMOMER',concat(../../@id,'_',@subscript))/@number}">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">
                  <xsl:value-of select="@sign"/>
                </cn>
              </matrix-assignment>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:call-template name="observation-matrices-close"/>
    </optimize>

  </xsl:template>
	
	<xsl:template name="groups">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
			<ci>dy_domega_ij</ci>
			<matrix>
				<xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement">
        	<xsl:sort select="@id" data-type="text"/>
        	<matrixrow>
						<cn>
							<xsl:value-of select="position()"/>
						</cn>
						<cn>
							<xsl:value-of select="key('GROUP',@id)/@pos"/>
						</cn>
					</matrixrow>
      </xsl:for-each>
    </matrix>
	</apply>
	</xsl:template>


</xsl:stylesheet>
