<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Projet  :   PIVERT/Metalippro-PL1
-->

<!-- Cette feuille de style a pour but de générer différentes fonctions spécifiques
  d'un modèle donné sous forme SBML, édité dans CellDesigner. On prend ici l'exemple
  du "Branching Network" de l'article de N. Isermann, W. Wiechert :
  
  Metabolic isotopomer labeling systems. Part II: structural flux identifiability
  analysis.


          A_out
           |
           |v6
           |
           v
  _________A _______
  |        |       |
  |        |v2     |
  |v3      |       |
  |        v       |
  |        D       |v1
  |        |       |
  |        |v4     |
  |        |       |
  |        v       |
  _______> F <______
           |
           |v5
           |
           v
           G


  Ici on fait abstraction des identificateurs originaux des cumomères et des flux. On a dans cet
  exemple (les cumomères des métabolites d'entrée et de sortie ne font pas partie de l'état) :
  
  x1=[A$01;A$10;D$1;F$01;F$10];
  x2=[A$11;F$11];
  
  x1_input=[A_out$01;A_out$10];
  x2_input=[A_out$11];
  
  v=[v1;v2;v3;v4;v5;v6];
  
  Ici on génére en fait des fonctions décrites en MathML, il faut enchainer avec
  une dernière transformation pour avoir le code Scilab (sbml_sys_text.xsl) :


*** 1 - une fonction Scilab permettant
  calculer les différentes fractions des cumomères en fonction des flux
  et des fractions des cumomères des entrées. 
  On génère aussi le code des blocs non nuls de la jacobienne des états par rapport à v
  
  
  Exemple pour le "branching network"
  du papier "part II" de Wiechert :
  
  function [x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input)

    n1=5;

    // Cumomères de poids 1

    M1_ijv=[1,1,-(v(1)+v(2)+v(3))
    2,2,-(v(1)+v(2)+v(3))
    3,2,v(2)
    3,1,v(2)
    3,3,-(v(4)+v(4))
    4,1,v(1)
    4,2,v(3)
    4,3,v(4)
    4,4,-v(5)
    5,2,v(1)
    5,1,v(3)
    5,3,v(4)
    5,5,-v(5)];
    M1=sparse(M1_ijv(:,1:2),M1_ijv(:,3),[n1,n1]);

    b1_ijv=[1,1,v(6).*x1_input(1,:)
    2,1,v(6).*x1_input(2,:)];
    b1_1=s_full(b1_ijv(:,1:2),b1_ijv(:,3),[n1,1]);

    [M1_handle,M1_rank]=lufact(M1);
    x1=lusolve(M1_handle,-[b1_1]);

    dg1_dv_ijv=[1,6,x1_input(1,:)
    1,1,-x1(1,:)
    1,2,-x1(1,:)
    1,3,-x1(1,:)
    2,6,x1_input(2,:)
    2,1,-x1(2,:)
    2,2,-x1(2,:)
    2,3,-x1(2,:)
    3,2,x1(2,:)
    3,2,x1(1,:)
    3,4,-x1(3,:)
    3,4,-x1(3,:)
    4,1,x1(1,:)
    4,3,x1(2,:)
    4,4,x1(3,:)
    4,5,-x1(4,:)
    5,1,x1(2,:)
    5,3,x1(1,:)
    5,4,x1(3,:)
    5,5,-x1(5,:)];
    dg1_dv_1=s_full(dg1_dv_ijv(:,1:2),dg1_dv_ijv(:,3),[n1,6]);
    dx1_dv(:,:,1)=lusolve(M1_handle,-dg1_dv_1);

    ludel(M1_handle);

    n2=2;

    // Cumomères de poids 2

    M2_ijv=[1,1,-(v(1)+v(2)+v(3))
    2,1,v(1)
    2,1,v(3)
    2,2,-v(5)];
    M2=sparse(M2_ijv(:,1:2),M2_ijv(:,3),[n2,n2]);

    b2_ijv=[1,1,v(6).*x2_input(1,:)
    2,1,v(4).*x1(3,:).*x1(3,:)];
    b2_1=s_full(b2_ijv(:,1:2),b2_ijv(:,3),[n2,1]);

    [M2_handle,M2_rank]=lufact(M2);
    x2=lusolve(M2_handle,-[b2_1]);

    dg2_dv_ijv=[1,6,x2_input(1,:)
    1,1,-x2(1,:)
    1,2,-x2(1,:)
    1,3,-x2(1,:)
    2,1,x2(1,:)
    2,3,x2(1,:)
    2,4,x1(3,:).*x1(3,:)
    2,5,-x2(2,:)];
    dg2_dv_1=s_full(dg2_dv_ijv(:,1:2),dg2_dv_ijv(:,3),[n2,6]);
    db2_dx1_ijv=[2,3,x1(3,:).*v(4)
    2,3,x1(3,:).*v(4)];
    db2_dx1_1=sparse(db2_dx1_ijv(:,1:2),db2_dx1_ijv(:,3),[n2,n1]);
    dx2_dv=zeros(n2,6,1);
    dx2_dv(:,:,1)=lusolve(M2_handle,-(dg2_dv_1+db2_dx1_1*dx1_dv(:,:,1)));
    ludel(M2_handle);
  endfunction

2 *** - Une fonction permettant de calculer la fonction cout du problème d'identification
  ainsi que son gradient (sans utiliser d'état adjoint; on utilise Directement les jacobiennes
  des états par rapport à v)


  function [cost,grad]=costAndGrad(v)
    [x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input);
    e_label=(C1*x1+C2*x2)-ymeas;
    e_flux=E*v-vmeas;
    cost=0.5*(sum(Svpm2.*e_flux.^2)+sum(Sypm2(:,1).*e_label(:,1).^2));
    grad=(Svpm2.*e_flux)'*E+(Sypm2(:,1).*e_label(:,1))'*(C1*dx1_dv(:,:,1)+C2*dx2_dv(:,:,1));
  endfunction


3 *** - Les "matrices d'observation" correspondant aux cumomères et aux fluxs observés

  1) isotopomères en entrée ->contributions aux cumomères en entrée, matrices D1,D2, etc.

  Exemple ici pour A_out (seul métabolite d'entrée), on précise dans les notes (dans CellDesigner)
  
  LABEL_INPUT 01,10,11

  ce qui signifie qu'on donne en entrée les trois isotopomères dans un vecteur A_out_input
  qui contient en fait [A_out#01;A_out#10;A_out#11]. Pour le calcul des cumomères, on a besoin
  de connaitre les contributions aux vecteurs d'entrées x1_input (cumomères de poids 1) et
  x2_input (cumomères de poids 2). On génère donc les matrices D1 et D2 telles que
  
  x1_input=D1*A_out_input;
  x2_input=D2*A_out_input;


  // Matrices d'entrée
  D1_ijv=[1,1,1
  1,3,1
  2,2,1
  2,3,1];
  D1=sparse(D1_ijv(:,1:2),D1_ijv(:,3),[2,3]);
  D2_ijv=[1,3,1];
  D2=sparse(D2_ijv(:,1:2),D2_ijv(:,3),[1,3]);
  
  2) cumomères calculés ->isotopomères observés

  Exemple ici pour F, dont on observe les cumomères F$10,F$01,F$11, on a dans les notes

  LABEL_MEASUREMENT 1x,x1,11
  
  avec la convention du logiciel 13CFlux (1x=cumomère 10, x1=cumomère 01), on a besoin de
  pouvoir calculer ces observations en fonction des vecteurs des cumomères calculés x1,x2,
  on génère donc les matrices C1 et C2 telles que
  
  y=C1*x1+C2*x2

  et ici y contient finalement [F$10;F$01;F$11] car il n'y a pas d'autre observation dans 
  le modèle.

  // Matrices d'observation (cumomères)
  C1_ijv=[1,5,1
  2,4,1];
  C1=sparse(C1_ijv(:,1:2),C1_ijv(:,3),[3,5]);
  C2_ijv=[3,2,1];
  C2=sparse(C2_ijv(:,1:2),C2_ijv(:,3),[3,2]);


  3) flux ->flux observés
  
  Dans le Branching Network, l'exemple utilisé, on observe v5,v6. Dans le modèle CellDesigner
  ces deux réactions/flux sont identifiés comme TRANSPORT ou KNOWN_TRANSITION_OMMITED


  // Matrice d'observation (flux)
  E_ijv=[1,5,1
  2,6,1];
  E=sparse(E_ijv(:,1:2),E_ijv(:,3),[2,6]);

  
  3 - une fonction Scilab [A,b]=fluxSubspace(_fluxes) calculant le paramétrage du
  sous-espace des flux v vériant
  
    Nv=0 
    Hv=w

  N est la matrice stoechiométrique et Hv=w exprime des contraintes d'égalité
  sur les flux, typiquement utilisée lorsque des flux ont des valeurs connues.

  La partie Hv=w peut etre changee dans l'interface. La matrice _fluxes a deux colonnes
  dont la deuxième précise si on impose un flux ou pas. La valeur est par défaut -1 sinon
  on met une valeur positive ou nulle imposée. La matrice H et le vecteur w sont ainsi récupérés
  après appel à la fonction fluxConstraints (faisant partie de la bibliothèque de macros
  SYSMETAB).
    

  Ici on a finalement 
  
    A=[N;H] et b=[0;w]

  function [A,b,N,H,w]=fluxSubspace(_fluxes)
    // Construction des matrices (dont la matrice de stoechiométrie) permettant
    // de calculer le changement de variable affine des flux.
    N=zeros(3,6);
    // A
    N_ijv=[1,1,-1
    1,2,-1
    1,3,-1
    1,6,1
    2,2,1
    2,2,1
    2,4,-1
    2,4,-1
    3,1,1
    3,3,1
    3,4,1
    3,5,-1];
    // Flux imposés dans l'interface
    [H,w]=fluxConstraints(_fluxes);
    b=[zeros(3,1);w];
    A=[N;H];
  endfunction
  
-->

<xsl:stylesheet 
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings"
  xmlns:math="http://exslt.org/math"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb f">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
  <xsl:param name="file"/>
  <xsl:param name="verbose">no</xsl:param>
  <xsl:param name="type">stationnaire</xsl:param>

  <xsl:variable name="dim" select="count(f:fluxml/f:reactionnetwork/f:reaction)"/>

  <xsl:variable name="dimdiv2" select="count(f:fluxml/f:reactionnetwork/f:reaction) div 2"/>

  <xsl:variable name="dimscale" select="count(f:fluxml/f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale])"/>

  <xsl:variable name="experiences" select="count(f:fluxml/f:configuration)"/>

  <xsl:variable name="quote">'</xsl:variable>
  
  <xsl:include href="fml_common_gen_mathml.xsl"/><!-- templates communes stationnaire/dynamique -->

  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">
    
  <!-- Construction de la fonction solveCumomers (calcul des cumomères connaissant les cumomères en entrée et les flux) -->

  <xsl:call-template name="solveCumomersAndGrad">
    <xsl:with-param name="method">Direct</xsl:with-param>
  </xsl:call-template>

  <!--><xsl:call-template name="solveCumomersAndGrad">
    <xsl:with-param name="method">Adjoint</xsl:with-param>
  </xsl:call-template>

  <xsl:call-template name="solveCumomersAndGrad">
    <xsl:with-param name="method">none</xsl:with-param>
  </xsl:call-template>-->
  
    <!-- Fonction de calcul de A et B pour les contraintes sur les fluxs Av=b (dans common_gen_mathml.xsl)-->
  
    <xsl:call-template name="fluxSubspace"/>
  
    <!-- Construction des matrices d'observation (dans common_gen_mathml.xsl)-->
  
    <xsl:call-template name="construct_matrix_C"/>
    <xsl:call-template name="construct_matrix_E"/>
  
    <!-- Construction des matrices d'entrée, permettant d'obtenir la contribution sur les cumomères des isotopomères en entrée. -->
  
    <xsl:call-template name="construct_matrix_D"/>
  
    <!-- Matrice des noms des fluxs, Matrice des ids des fluxs -->
  
    <xsl:call-template name="names_and_ids_fluxes"/>

    <!-- Script pour le calcul Direct des cumomères. Ce script est exécuté dans l'application XMLlab, dès qu'un flux est changé ou dès que l'on 
  sort de l'optimisation avec des nouvelles valeurs de fluxs. -->

    <script href="{$file}_direct.sce" xmlns="http://www.utc.fr/sysmetab">
          <ci xmlns="http://www.w3.org/1998/Math/MathML">clear;&#10;clearglobal;&#10;</ci>

          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>path</ci>
            <apply>
              <fn>
                <ci>get_absolute_file_path</ci>
              </fn>
              <string xmlns="http://www.utc.fr/sysmetab">
                <xsl:value-of select="concat($file,'_direct.sce')"/>
              </string>
            </apply>
          </apply>

          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>err</ci>
            <apply>
              <fn>
                <ci>chdir</ci>
              </fn>
              <ci xmlns="http://www.utc.fr/sysmetab">path</ci>
            </apply>
          </apply>


        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>err</ci>
          <apply>
          <fn>
            <ci>exec</ci>
          </fn>
        <string xmlns="http://www.utc.fr/sysmetab">sysmetab_lib.sci</string>
          </apply>
    </apply>

        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>err</ci>
          <apply>
          <fn>
            <ci>exec</ci>
          </fn>
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="concat($file,'.sci')"/>
        </string>
          </apply>
    </apply>
  
      <comment>Cumomères en entrée, calculés à partir des isotopomères des métabolites d'entrée.</comment>

     <xsl:for-each select="f:configuration/f:input">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
          <list separator=";">
          <xsl:for-each select="f:label">
            <cn>
              <xsl:value-of select="."/>
            </cn>
          </xsl:for-each>
        </list>
      </apply>    
    </xsl:for-each>

      <xsl:for-each select="f:reactionnetwork/smtb:listOfInputCumomers/smtb:listOfCumomers">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>
            <xsl:value-of select="concat('x',@weight,'_input')"/>
          </ci>
          <apply>
            <times/>
            <ci>
              <xsl:value-of select="concat('D',@weight)"/>
            </ci>
            <list separator=";">
              <xsl:for-each select="../../f:metabolitepools/f:pool[smtb:input]">
                <ci>
                  <xsl:value-of select="concat(@id,'_input')"/>
                </ci>
              </xsl:for-each>
            </list>
          </apply>
        </apply>
      </xsl:for-each>
      
      <comment>Calcul d'un v admissible</comment>
      
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>me</ci>
        <cn>
          <xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']) + count(f:constraints/f:net/m:math/m:apply[m:eq]) + count(f:constraints/f:xch/m:math/m:apply[m:eq])"/>
        </cn>
      </apply>
      
      <comment>Regularization term for the cost function Obj=cost+.5*epsilon*|q|^2</comment>
      
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>epsilon</ci>
        <cn>2e-5</cn>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>eps_phi</ci>
        <cn>1e-6</cn>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>W</ci>
          <ci>w0</ci>
          <ci>ff</ci>
          <ci>ftype</ci>
        </list>
        <apply>
          <fn><ci>freefluxes</ci></fn>
          <apply>
            <selector/>
            <ci type="matrix">C</ci>
            <cn>1:me</cn>
            <cn>:</cn>
          </apply>
          <apply>
            <selector/>
            <ci type="vector">b</ci>
            <cn>1:me</cn>
          </apply>  
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>flux</ci>
        </list>
        <xsl:choose>
          <xsl:when test="f:configuration/f:simulation/f:variables/f:fluxvalue">
            <apply>
              <fn><ci>qld_test</ci></fn>
              <ci>C</ci>
              <ci>b</ci>
              <ci>me</ci>
              <ci>Flux_M</ci>
              <ci>b_Flux</ci>
            </apply>
          </xsl:when>
          <xsl:otherwise>
            <apply>
              <fn><ci>qld_test</ci></fn>
              <ci>C</ci>
              <ci>b</ci>
              <ci>me</ci>
            </apply>
          </xsl:otherwise>
        </xsl:choose>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>w0</ci>
        <ci>flux.values</ci>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>q</ci>
        <apply>
          <fn><ci>zeros</ci></fn>
          <apply>
          	<fn><ci>size</ci></fn>
          	<ci>ff</ci>
          	<cn>1</cn>
          </apply>
          <cn>1</cn>
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>f</ci>
          <ci>w</ci>
        </list>
        <apply>
          <fn><ci>optimize</ci></fn>
          <ci>q</ci>
        </apply>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>V</ci>
        </list>
        <apply>
          <fn><ci>variance</ci></fn>
          <ci>w</ci>
        </apply>
      </apply>

      <!--
      <comment>To save data from costAndGrad</comment>
      
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>cost</ci>
          <ci>grad</ci>
          <ci>meas</ci>
        </list>
        <apply>
          <fn><ci>costAndGradSaveData</ci></fn>
          <ci>w</ci>
        </apply>
      </apply>
      -->
    </script>
  </xsl:template>

  <xsl:template name="solveCumomersAndGrad">
    <xsl:param name="method"/>

    <function xmlns="http://www.utc.fr/sysmetab">
      <xsl:choose>
        <xsl:when test="$method='none'">          
            <ci xmlns="http://www.w3.org/1998/Math/MathML">solveCumomers</ci>
      </xsl:when>
      <xsl:otherwise>
          <ci xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:value-of select="concat('solveCumomersAndGrad',$method)"/>
        </ci>
      </xsl:otherwise>
    </xsl:choose>
    <input>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <ci>v</ci>
          <xsl:for-each select="f:reactionnetwork/smtb:listOfInputCumomers/smtb:listOfCumomers">
            <ci>
              <xsl:value-of select="concat('x',@weight,'_input')"/>
            </ci>
          </xsl:for-each>
        </list>
      </input>
      
    <output>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:if test="$method!='none'">
            <ci>cost</ci>
            <ci>grad</ci>
          </xsl:if>
          <ci>y</ci>
          <xsl:if test="$method='Direct'">
            <ci>dy_dv</ci>
            <xsl:if test="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale='auto']">
              <ci>dy_domega</ci>
            </xsl:if>
          </xsl:if>
          <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
            <ci>
              <xsl:value-of select="concat('x',@weight)"/>
            </ci>
          </xsl:for-each>
          <xsl:if test="$method='Direct'">
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <ci>
                <xsl:value-of select="concat('dx',@weight,'_dv')"/>
              </ci>
            </xsl:for-each>
          </xsl:if>
          <ci xmlns="http://www.w3.org/1998/Math/MathML">Timers</ci>
          <ci xmlns="http://www.w3.org/1998/Math/MathML">TimersID</ci>
        </list>
      </output>
    
      <body>

          <comment>Solve the state equation</comment>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>t_cmv</ci>
        <ci>0</ci>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>t_css</ci>
        <ci>0</ci>
      </apply>
                
      <xsl:apply-templates select="f:reactionnetwork/smtb:listOfIntermediateCumomers">
        <xsl:with-param name="method" select="$method"/>
      </xsl:apply-templates>
      
      <comment>Compute the simulated measurements</comment>      
      
      <xsl:if test="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale='auto']">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>y_unweighted</ci>
          <apply>
            <plus/>
            <ci>C0</ci>
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <apply>
                <times/>
                <ci>
                  <xsl:value-of select="concat('C',@weight)"/>
                </ci>
                <ci>
                  <xsl:value-of select="concat('x',@weight)"/>
                </ci>
              </apply>
            </xsl:for-each>
          </apply>
        </apply>
      </xsl:if>
      
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>y</ci>
        <xsl:choose>
          <xsl:when test="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale='auto']">
            <apply>
              <times/>
              <apply>
                <plus/>
                <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale='auto']">
                  <apply>
                    <times/>
                    <apply xmlns="http://www.w3.org/1998/Math/MathML">
                      <selector/>
                      <ci type="vector">omega</ci>
                      <cn>
                        <xsl:value-of select="@pos"/>
                      </cn>
                    </apply>
                    <ci>
                      <xsl:value-of select="concat('J',@pos)"/>
                    </ci>
                  </apply>
                </xsl:for-each>
              </apply>
              <ci>y_unweighted</ci>
            </apply>
          </xsl:when>
          <xsl:otherwise>
            <apply>
              <plus/>
              <ci>C0</ci>
              <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
                <apply>
                  <times/>
                  <ci>
                    <xsl:value-of select="concat('C',@weight)"/>
                  </ci>
                  <ci>
                    <xsl:value-of select="concat('x',@weight)"/>
                  </ci>
                </apply>
              </xsl:for-each>
            </apply>
          </xsl:otherwise>
        </xsl:choose>
      </apply>
      
      <xsl:if test="$method='Direct'">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>dy_dv</ci>
          <apply>
            <plus/>
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <apply>
                <times/>
                <ci>
                  <xsl:value-of select="concat('C',@weight)"/>
                </ci>
                <ci>
                  <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                </ci>
              </apply>
            </xsl:for-each>
          </apply>
        </apply>
        <xsl:if test="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale='auto']">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>dy_domega</ci>
            <list separator=";">
              <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group">
                <apply>
                  <times/>
                  <ci>
                    <xsl:value-of select="concat('J',@pos)"/>
                  </ci>
                  <ci>y_unweighted</ci>
                </apply>
              </xsl:for-each>
            </list>
          </apply>
        </xsl:if>
      </xsl:if>      
      
      <xsl:if test="$method!='none'">
            
      <comment><xsl:value-of select="concat($method,' computation of gradient')"/></comment>
        <timer name="t_cost" xmlns="http://www.utc.fr/sysmetab">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>e_label</ci>
            <apply>
              <minus/>
              <ci>y</ci>
              <ci>ymeas</ci>
            </apply>
          </apply>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>e_flux</ci>
            <apply>
              <minus/>
              <apply>
                <times/>
                <ci>E</ci>
                <ci>v</ci>
              </apply>
              <ci type="matrix">vmeas</ci>
            </apply>
          </apply>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>flux_error</ci>
            <apply>
              <plus/>
              <apply>  <!-- contribution des observations des flux -->
                <times/>
                <apply>
                  <fn>
                    <ci>sum</ci>
                  </fn>
                  <apply>
                    <times type="array"/>
                    <ci>Svpm2</ci>
                    <apply>
                      <power type="array"/>
                      <ci type="matrix">e_flux</ci>
                      <cn>2</cn>
                    </apply>
                  </apply>
                </apply>
              </apply>
            </apply>
          </apply>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>label_error</ci>
            <apply>
              <plus/>
              <xsl:call-template name="iterate-cost"/>  <!-- contributions des observations  des marquages -->
            </apply>
          </apply>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>cost</ci>
            <apply>
              <times/>
              <cn>0.5</cn>
              <apply>
                <plus/>
                <ci>flux_error</ci>
                <ci>label_error</ci>
              </apply>
            </apply>
          </apply>
        </timer>
        <xsl:if test="$method='Direct'">
          <timer name="t_grad" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>grad</ci>
              <apply>
                <plus/>
                <apply>  <!-- contribution des observations des flux -->
                  <times/>
                  <apply>
                    <transpose/>
                    <apply>
                      <times type="array"/>
                      <ci>Svpm2</ci>
                      <ci>e_flux</ci>
                    </apply>
                  </apply>
                  <ci>E</ci>
                </apply>
                <xsl:call-template name="iterate-grad"/>  <!-- contributions des observations  des marquages -->
              </apply>
            </apply>
          </timer>
        </xsl:if>
        <xsl:if test="$method='Adjoint'">
          <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
            <xsl:sort select="@weight" order="descending" data-type="number"/>    
            <!-- Résolution du système pour p weight -->
            <hypermatrix id="{concat('p',@weight)}" rows="{concat('n',@weight)}" cols="1" versions="{$experiences}"/>
            <xsl:call-template name="solve-p"/>
              </xsl:for-each>
              <timer name="t_grad" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>grad</ci>
              <apply>
                <plus/>
                <apply>  <!-- contribution des observations des flux -->
                  <times/>
                  <apply>
                    <transpose/>
                    <apply>
                      <times type="array"/>
                      <ci>Svpm2</ci>
                      <ci>e_flux</ci>
                    </apply>
                  </apply>
                  <ci>E</ci>
                </apply>
                <xsl:call-template name="iterate-grad-Adjoint"/>  <!-- contributions des observations  des marquages -->
              </apply>
            </apply>
         </timer>
          </xsl:if>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>grad</ci>
            <apply>
              <transpose/>
              <ci>grad</ci>
            </apply>
          </apply>
      </xsl:if>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Timers</ci>
        <list>
          <ci>t_cmv</ci>
          <ci>t_css</ci>
          <xsl:choose>
              <xsl:when test="$method='none'">
              </xsl:when>
              <xsl:otherwise>
              <ci>t_cost</ci>
              <ci>t_grad</ci>
              </xsl:otherwise>
          </xsl:choose>
        </list>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>TimersID</ci>
        <list>
          <string xmlns="http://www.utc.fr/sysmetab">t_cmv</string>
          <string xmlns="http://www.utc.fr/sysmetab">t_css</string>
          <xsl:choose>
              <xsl:when test="$method='none'">
              </xsl:when>
              <xsl:otherwise>
              <string xmlns="http://www.utc.fr/sysmetab">t_cost</string>
              <string xmlns="http://www.utc.fr/sysmetab">t_grad</string>
              </xsl:otherwise>
          </xsl:choose>
        </list>
      </apply>
      </body>
    </function>
</xsl:template>

  <xsl:template name="iterate-cost">
    <xsl:param name="i" select="'1'"/>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>sum</ci>
      </fn>
      <apply>
        <times type="array"/>
        <apply>
          <selector/>
          <ci type="matrix">Sypm2</ci>
          <cn>:</cn>
          <cn>
            <xsl:value-of select="$i"/>
          </cn>
        </apply>
        <apply>
          <power type="array"/>
          <apply>
            <selector/>
            <ci type="matrix">e_label</ci>
            <cn>:</cn>
            <cn><xsl:value-of select="$i"/></cn>
          </apply>
          <cn>2</cn>
        </apply>
      </apply>
    </apply>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-cost">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="iterate-grad">
    <xsl:param name="i" select="'1'"/>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <times/>
      <apply>
        <transpose/>
        <apply>
          <times type="array"/>
          <apply>
            <selector/>
            <ci type="matrix">Sypm2</ci>
            <cn>:</cn>
            <cn>
              <xsl:value-of select="$i"/>
            </cn>
          </apply>
          <apply>
            <selector/>
            <ci type="matrix">e_label</ci>
            <cn>:</cn>
            <cn><xsl:value-of select="$i"/></cn>
          </apply>
        </apply>
      </apply>
      <xsl:choose>
        <xsl:when test="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale='auto']">
          <apply>
            <times/>
            <apply>
              <apply>
                <plus/>
                <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[@scale='auto']">
                  <apply>
                    <times/>
                    <apply>
                      <plus/>
                      <apply xmlns="http://www.w3.org/1998/Math/MathML">
                        <selector/>
                        <ci type="vector">omega</ci>
                        <cn>
                          <xsl:value-of select="@pos"/>
                        </cn>
                      </apply>
                      <cn>1</cn>
                    </apply>
                    <ci>
                      <xsl:value-of select="concat('J',@pos)"/>
                    </ci>
                  </apply>
                </xsl:for-each>
              </apply>
            </apply>
            <apply>
              <plus/>
              <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
                <apply>
                  <times/>
                  <ci>
                    <xsl:value-of select="concat('C',@weight)"/>
                  </ci>
                  <apply>
                    <selector/>
                    <ci type="hypermatrix">
                      <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                    </ci>
                    <cn>:</cn>
                    <cn>:</cn>
                    <cn><xsl:value-of select="$i"/></cn>
                  </apply>
                </apply>
              </xsl:for-each>
            </apply>
          </apply>
        </xsl:when>
        <xsl:otherwise>
          <apply>
            <plus/>
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <apply>
                <times/>
                <ci>
                  <xsl:value-of select="concat('C',@weight)"/>
                </ci>
                <apply>
                  <selector/>
                  <ci type="hypermatrix">
                    <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                  </ci>
                  <cn>:</cn>
                  <cn>:</cn>
                  <cn><xsl:value-of select="$i"/></cn>
                </apply>
              </apply>
            </xsl:for-each>
          </apply>
        </xsl:otherwise>
      </xsl:choose>
    </apply>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-grad">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="iterate-grad-Adjoint">
    <xsl:param name="i" select="'1'"/>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <plus/>
        <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
          <apply>
            <times/>
            <apply>
              <transpose/>
              <apply>
                <selector/>
                <ci type="hypermatrix">
                  <xsl:value-of select="concat('p',@weight)"/>
                </ci>
                <cn>:</cn>
                <cn>:</cn>
                <cn><xsl:value-of select="$i"/></cn>
              </apply>
            </apply>
            <apply>
              <ci>
                <xsl:value-of select="concat('dg',@weight,'_dv_',$i)"/>
              </ci>
            </apply>
          </apply>
        </xsl:for-each>
    </apply>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-grad-Adjoint">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="smtb:listOfIntermediateCumomers">
    <xsl:param name="method"/>
    <xsl:for-each select="smtb:listOfCumomers">
    <xsl:sort select="@weight" order="ascending" data-type="number"/>    
    <!-- Déclaration et initialisation matrice et vecteur -->
    
      <xsl:variable name="current_weight" select="@weight"/>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>
          <xsl:value-of select="concat('n',@weight)"/>
        </ci>
        <cn>
          <xsl:value-of select="count(smtb:cumomer)"/>
        </cn>
      </apply>
    
      <optimize xmlns="http://www.utc.fr/sysmetab">

        <matrix-open type="sparse" id="{concat('M',@weight)}" rows="{concat('n',@weight)}" cols="{concat('n',@weight)}"/>
        <matrix-open type="s_full" id="{concat('b',@weight)}" rows="{concat('n',@weight)}" cols="1" versions="{$experiences}"/>
    
    <xsl:if test="$method!='none'">
    
        <matrix-open type="s_full" id="{concat('dg',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../f:reaction)}" 
          versions="{$experiences}" />
        <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
          <matrix-open type="sparse" id="{concat('db',$current_weight,'_dx',@weight)}" rows="{concat('n',$current_weight)}" cols="{concat('n',@weight)}"
            versions="{$experiences}"/>
        </xsl:for-each>
      
    </xsl:if>
        
        <comment>Weight <xsl:value-of select="@weight"/> cumomers</comment>
        
        <!-- Pour chaque cumomère on appelle la template générant les diverses affectations, à la matrice ou au second membre du système ayant 
        pour solution le vecteur des cumomères du poids courant -->
        
        <xsl:for-each select="smtb:cumomer">
          <xsl:call-template name="iteration"/>            
        </xsl:for-each>
      
    <timer name="t_cmv">
           <matrix-close id="{concat('M',@weight)}"/>
          <matrix-close id="{concat('b',@weight)}"/>
        </timer>
         
        <!-- Résolution des systèmes -->
        <!-- Résolution du système pour x_weight -->

    <timer name="t_css">
          <solve matrix="{concat('M',@weight)}">
            <lhs>
              <xsl:value-of select="concat('x',@weight)"/>
            </lhs>
            <rhs>
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <minus/>
                <list separator=",">
                  <xsl:call-template name="concatenate-rhs">
                    <xsl:with-param name="id" select="concat('b',@weight)"/>
                  </xsl:call-template>
                </list>
              </apply>
            </rhs>
          </solve>
      </timer>
    <xsl:if test="$method!='none'">
      
      <timer name="t_cmv">
        <matrix-close id="{concat('dg',@weight,'_dv')}"/>
            <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
                <matrix-close id="{concat('db',$current_weight,'_dx',@weight)}" />
            </xsl:for-each>
      </timer>
      
          <!-- Résolution du système pour dx_weight/dv -->
          
      <xsl:if test="$method='Direct'">        
            <hypermatrix id="{concat('dx',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../f:reaction)}" versions="{$experiences}"/>
           <timer name="t_css">
              <xsl:call-template name="solve-dx-dv"/>
          </timer>
          </xsl:if>
                        
    </xsl:if>
      </optimize>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="concatenate-rhs">
    <xsl:param name="i">1</xsl:param>
    <xsl:param name="id"/>
    <ci type="vector" xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:value-of select="concat($id,'_',$i)"/>
    </ci>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="concatenate-rhs">
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="id" select="$id"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="solve-dx-dv">
    <xsl:param name="i">1</xsl:param>
    <xsl:variable name="current_weight" select="@weight"/>
    <solve matrix="{concat('M',@weight)}" xmlns="http://www.utc.fr/sysmetab">
      <lhs>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="hypermatrix">
            <xsl:value-of select="concat('dx',@weight,'_dv')"/>
          </ci>
          <cn>:</cn>
          <cn>:</cn>
          <cn><xsl:value-of select="$i"/></cn>
        </apply>
      </lhs>
      <rhs>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <minus/>
          <apply>
            <plus/>
            <ci>
              <xsl:value-of select="concat('dg',@weight,'_dv_',$i)"/>
            </ci>
            <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
              <apply>
                <times/>
                <ci>
                  <xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_',$i)"/>
                </ci>
                <apply>
                  <selector/>
                  <ci type="hypermatrix">
                    <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                  </ci>
                  <cn>:</cn>
                  <cn>:</cn>
                  <cn><xsl:value-of select="$i"/></cn>
                </apply>
              </apply>
            </xsl:for-each>
          </apply>
        </apply>
      </rhs>
    </solve>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="solve-dx-dv">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="solve-p">
    <xsl:param name="i">1</xsl:param>
    <xsl:variable name="current_weight" select="@weight"/>
    <timer name="t_css" xmlns="http://www.utc.fr/sysmetab">
      <solve matrix="{concat('M',@weight)}" transpose="yes" xmlns="http://www.utc.fr/sysmetab">
        <lhs>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <selector/>
            <ci type="hypermatrix">
              <xsl:value-of select="concat('p',@weight)"/>
            </ci>
            <cn>:</cn>
            <cn>:</cn>
            <cn><xsl:value-of select="$i"/></cn>
          </apply>
        </lhs>
        <rhs>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:if test="following-sibling::smtb:listOfCumomers">
              <minus/>
            </xsl:if>
            <apply>
              <times/>
              <apply>
                <transpose/>
                <ci>
                  <xsl:value-of select="concat('C',@weight)"/>
                </ci>
              </apply>
              <apply>
              <apply>
                <times type="array"/>
            <apply>
              <selector/>
              <ci type="matrix">Sypm2</ci>
              <cn>:</cn>
              <cn>
                <xsl:value-of select="$i"/>
              </cn>        
            </apply>
                <apply>
                  <selector/>
                  <ci type="matrix">e_label</ci>
                  <cn>:</cn>
                  <cn><xsl:value-of select="$i"/></cn>
                </apply>
              </apply>
              </apply>
            </apply>
            <apply>
              <plus/>  
              <xsl:for-each select="following-sibling::smtb:listOfCumomers">
                <apply>
                  <times/>
                  <ci>
                    <xsl:value-of select="concat('db',@weight,'_dx',$current_weight,'_',$i,$quote)"/>
                  </ci>
                  <apply>
                    <selector/>
                    <ci type="hypermatrix">
                      <xsl:value-of select="concat('p',@weight)"/>
                    </ci>
                    <cn>:</cn>
                    <cn>:</cn>
                    <cn><xsl:value-of select="$i"/></cn>
                  </apply>
                </apply>
              </xsl:for-each>
            </apply>
          </apply>
        </rhs>
      </solve>
    </timer>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="solve-p">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
