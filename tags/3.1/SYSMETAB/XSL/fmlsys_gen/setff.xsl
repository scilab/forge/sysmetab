<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Authors :   Stéphane Mottelet
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt fwd">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
    
  <xsl:strip-space elements="*"/>

	<xsl:param name="fwdfile"/>

  <xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

  <xsl:template match="f:simulation">
		<simulation xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
  		<variables>
  			<xsl:for-each select="document($fwdfile,/)/fwd:fwdsim/fwd:stoichiometry/fwd:flux/*[@type='free']">
  				<fluxvalue flux="{../@id}" type="{name()}" xmlns="http://www.13cflux.net/fluxml">
  					<xsl:value-of select="@value"/>
  				</fluxvalue>
  			</xsl:for-each>
  		</variables>
    </simulation>
	</xsl:template>

  <xsl:template match="f:group[@scale='auto']">
		<group id="{@id}" scale="{document($fwdfile,/)/fwd:fwdsim/fwd:measurements/fwd:mgroup[@id=current()/@id]/@scale}"
      xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </group>
	</xsl:template>


  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>


</xsl:stylesheet>
