<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Fri Mar 14 20:00:00 CET 2014
    Projet  :   PIVERT/Metalippro-PL1
-->

<!--
    fml2ftbl
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:str="http://exslt.org/strings"   
  exclude-result-prefixes="xsl fwd str">

  <!--<xsl:variable name="tab" select="'&#09;'"/>-->
  <xsl:variable name="tab" select="';'"/>
  <xsl:variable name="cr" select="'&#10;'"/>
  <xsl:variable name="quot" select="'&quot;'"/>

  <xsl:output method="text" indent="yes" encoding="ISO-8859-1"/>
  
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="fwd:fwdsim">
    <xsl:apply-templates select="fwd:stoichiometry"/>
    <xsl:apply-templates select="fwd:measurements"/>
    <xsl:apply-templates select="fwd:simulation"/>
  </xsl:template>

  <xsl:template match="fwd:stoichiometry">
    <xsl:value-of select="concat('FLUXES',$cr)"/>
    <xsl:value-of select="concat($tab,'NET',$tab,'NAME',$tab,'FCD',$tab,'VALUE',$tab,'STDDEV',$cr)"/>
    <xsl:for-each select="fwd:flux/fwd:net">
      <xsl:value-of select="concat($tab,$tab,../@id,$tab,@type,$tab,translate(@value,'.',','),$tab,translate(@stddev,'.',','),$tab,$quot,.,$quot,$cr)"/>
    </xsl:for-each>
    <xsl:value-of select="concat($cr,$tab,'XCH',$tab,'NAME',$tab,'FCD',$tab,'VALUE',$tab,'STDDEV',$cr)"/>
    <xsl:for-each select="fwd:flux/fwd:xch">
      <xsl:value-of select="concat($tab,$tab,../@id,$tab,@type,$tab,translate(@value,'.',','),$tab,translate(@stddev,'.',','),$tab,$quot,.,$quot,$cr)"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="fwd:measurements">
    <xsl:value-of select="concat($cr,'MASS_SPECTROMETRY',$cr,$tab,'META_NAME',$tab,'FRAGMENT',$tab,'WEIGHT',$tab,'VALUE',$cr)"/>
    <xsl:for-each select="fwd:mgroup[starts-with(@id,'ms_group')]">
      <xsl:value-of select="concat($cr,$tab,substring-before(fwd:spec,'['),$tab,$quot,substring-before(substring-after(fwd:spec,'['),']'),$quot,$tab)"/>
      <xsl:for-each select="fwd:value">
        <xsl:value-of select="concat(number(position()-1),$tab,translate(.,'.',','),$cr)"/>
        <xsl:if test="position()&lt;last()">
          <xsl:value-of select="concat($tab,$tab,$tab)"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>    
      
    <xsl:value-of select="concat($cr,'LABEL_MEASUREMENTS',$cr,$tab,'META_NAME',$tab,'CUM_GROUP',$tab,'VALUE',$tab,'CUM_CONSTRAINTS',$cr)"/>
    <xsl:for-each select="fwd:mgroup[starts-with(@id,'lmcg')]">
      <xsl:sort data-type="text" select="substring-before(fwd:spec,'#')"/>
      <xsl:value-of select="concat($cr,$tab,substring-before(fwd:spec,'#'),$tab,$quot,substring-before(substring-after(@id,'_'),'_'),$quot,$tab)"/>
      <xsl:for-each select="fwd:value">
        <xsl:variable name="pos" select="position()"/>
        <xsl:value-of select="concat(translate(.,'.',','),$tab,$quot,str:split(../fwd:spec,';')[$pos],$quot,$cr)"/>
        <xsl:if test="position()&lt;last()">
          <xsl:value-of select="concat($tab,$tab,$tab)"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>

  </xsl:template>

  <xsl:template match="fwd:simulation">
    <xsl:value-of select="concat('CUMOMERS',$cr)"/>
    <xsl:value-of select="concat($tab,'NAME',$tab,'CFG',$tab,'VALUE',$cr)"/>
    <xsl:for-each select="fwd:pool">
      <xsl:value-of select="$cr"/>
      <xsl:for-each select="fwd:value">
        <xsl:value-of select="concat($tab,../@id,$tab,$quot,@cfg,' ',$quot,$tab,translate(.,'.',','),$cr)"/>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="node()"/>

</xsl:stylesheet>
