<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de r�cup�rer les informations sur les transitions
    entre atomes de carbone dans le champ sbml:notes des r�actions et de les mettre dans
	un attribut "carbons". La convention est que les patterns sont indiqu�s dans le meme
	ordre que les esp�ces participant � la r�action.
	
	Par exemple pour une r�action :
    
    <reaction id="re15" name="Transc�tolase 1">
        <notes>
            <html xmlns="http://www.w3.org/1999/xhtml">
                <body>
                    ABCDE + FGHIJ &gt; ABFGHIJ + CDE
                </body>
            </html>
        </notes>
        <listOfReactants>
            <speciesReference species="s15"/>
            <speciesReference species="s16/">
        </listOfReactants>
        <listOfProducts>
            <speciesReference species="s19/">
            <speciesReference species="s21/">
        </listOfProducts>
    </reaction>

    on obtient en sortie :
    
    <reaction id="re15" name="Transc�tolase 1">
        <listOfReactants>
            <speciesReference species="s15" carbons="ABCDE"/>
            <speciesReference species="s16" carbons="FGHIJ"/>
        </listOfReactants>
        <listOfProducts>
            <speciesReference species="s19" carbons="ABFGHIJ"/>
            <speciesReference species="s21" carbons="CDE"/>
        </listOfProducts>
    </reaction>
    
-->

<xsl:stylesheet 
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml">
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:key name="dynamicSpecies" match="sbml:species[(@constant='false') or not(@constant)]" use="@id"/>
    <xsl:key name="boundarySpecies" match="sbml:species[@boundaryCondition='true']" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>

        
    <xsl:template match="sbml:reaction">
        <reaction xmlns="http://www.sbml.org/sbml/level2/version4">
            <xsl:copy-of select="@*"/>	<!-- On recopie tous les attributs -->
            <xsl:apply-templates/>
        </reaction>
    </xsl:template>
    
    <xsl:template match="sbml:listOfReactants">
        <listOfReactants xmlns="http://www.sbml.org/sbml/level2/version4">
            <xsl:call-template name="carbons">
                <xsl:with-param name="CR" select="str:split(substring-before(../sbml:notes/xhtml:html/xhtml:body,'>'),'+')"/>
            </xsl:call-template>
        </listOfReactants>
    </xsl:template>

    <xsl:template match="sbml:listOfProducts">
        <listOfProducts xmlns="http://www.sbml.org/sbml/level2/version4">
            <xsl:call-template name="carbons">
                <xsl:with-param name="CR" select="str:split(substring-after(../sbml:notes/xhtml:html/xhtml:body,'>'),'+')"/>
            </xsl:call-template>
        </listOfProducts>
    </xsl:template>

    <xsl:template name="carbons">
        <xsl:param name="CR"/>
        <xsl:for-each select="sbml:speciesReference">
            <xsl:variable name="i" select="position()"/>
            <speciesReference species="{@species}" xmlns="http://www.sbml.org/sbml/level2/version4">
                <xsl:if test="exslt:node-set($CR)[$i]">
                    <xsl:attribute name="carbons">
                        <xsl:value-of select="normalize-space(exslt:node-set($CR)[$i])"/>
                    </xsl:attribute>
                </xsl:if>
            </speciesReference>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="sbml:notes"/>

</xsl:stylesheet>
