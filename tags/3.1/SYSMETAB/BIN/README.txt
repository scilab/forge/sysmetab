Auteur  :   St�phane Mottelet
Date    :   Tue Mar 25 10:01:05 CET 2008
Projet  :   SYSMETAB/Carnot


R�pertoire SYSMETAB/BIN

Commandes shell 
----------------

genstat : 	g�n�re les �quations stationnaires des cumom�res pour un poids maximum donn�.
			exemple (se mettre dans le r�pertoire CUMOMER/Branching) :
			
			genstat branching_orig 2
			
			// A : A, input
			// D : D, intermediate
			0=(v2+v2)-(v4+v4);
			0=(A_2*v2+A_1*v2)-D_1*(v4+v4);
			// F : F, intermediate
			0=(v1+v3+v4)-v5;
			0=(A_1*v1+A_2*v3+D_1*v4)-F_1*v5;
			0=(A_2*v1+A_1*v3+D_1*v4)-F_2*v5;
			0=(A_3*v1+A_3*v3+D_1*D_1*v4)-F_3*v5;
			// G : G, output		 
			
gendyn	:	g�n�re les �quations non stationnaires des cumom�res pour un poids maximum donn�.
			exemple (se mettre dans le r�pertoire CUMOMER/Branching) :
			
			gendyn branching_orig 2

			// A : A, intermediate
			0=v6-(v1+v2+v3);
			A*d(A_1)/dt=A_out_1*v6-A_1*(v1+v2+v3);
			A*d(A_2)/dt=A_out_2*v6-A_2*(v1+v2+v3);
			A*d(A_3)/dt=A_out_3*v6-A_3*(v1+v2+v3);
			// D : D, intermediate
			0=(v2+v2)-(v4+v4);
			D*d(D_1)/dt=(A_2*v2+A_1*v2)-D_1*(v4+v4);
			// F : F, intermediate
			0=(v1+v3+v4)-v5;
			F*d(F_1)/dt=(A_1*v1+A_2*v3+D_1*v4)-F_1*v5;
			F*d(F_2)/dt=(A_2*v1+A_1*v3+D_1*v4)-F_2*v5;
			F*d(F_3)/dt=(A_3*v1+A_3*v3+D_1*D_1*v4)-F_3*v5;
			// G : G, output
			// A_out : A_out, input


gensolvestat :  g�n�re le code scilab permettant de faire l'identification au sens des moindres
			carr�s. On g�n�re un certains nombre de fichiers Scilab, et un fichier xml au format
			XMLlab qui permet de d�crire l'interface et les diverses interactions :
			exemple (se mettre dans le réertoire CUMOMER/Sysmetab_CT_complet)

			gensolvestat Sysmetab_CT_complet 3
			
			le deuxi�me param�tre (ici 3) est le nombre d'exp�riences utilis�es pour l'identification.
			
			Archive: Sysmetab_CT_complet_stat_3.xmllab
    		testing: content.xml             
    		testing: Sysmetab_CT_complet.sci 
    		testing: Sysmetab_CT_complet_direct.sce
    	 	testing: sysmetab4.sci           

			Pour avoir une description de ces fichiers jeter un oeil � la feuille de style 
			
 			sbmlsys_gen_mathml_solve_stat_4.xsl (g�n�ration des *.sci et *.sce)
 			sbmlsys_gen_xmllab4.xsl (g�n�ration de content.xml)
			
			Pour ex�cuter le fichier XMLlab obtenu :
			
			xmllab -run -c -w notebook Sysmetab_CT_complet_stat_3.xmllab


Commandes obsol�tes, peu ou pas utilis�es
--------------------------------------


genC 	:	commande utilis�e pour g�n�rer le bout de code C permettant de remplir les matrices creuses
			de la cascade de syst�mes pour Vincent Martin. Utilise une feuille de style obsol�te.

genmaple :	commande g�n�rant les �quation stationnaires au format Maple pour les tests d'identifiabilit�
			de Lilianne.

