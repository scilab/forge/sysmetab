mode(-1);
function m=join_mess(str)
	m=[];
	for i=1:size(str,1)-1;
		m=m+str(i)+ascii(13);
	end
	m=m+str($);
endfunction

function [stat,varargout]=get_params(names,vars,values)
  stat=0;
	TCL_EvalStr('set ok 0; toplevel .dial');
	for i=1:size(names,1)
		TCL_EvalStr('label .dial.l'+string(i)+' -text {'+names(i)+'}');
		TCL_EvalStr('entry .dial.e'+string(i)+' -textvariable '+vars(i)+' -width 3 -background white');
		TCL_EvalStr('set '+vars(i)+' '+string(values(i)));
		TCL_EvalStr('grid .dial.l'+string(i)+' .dial.e'+string(i)'+' -row '+string(i-1)+' -padx 20 -pady 10');
	end
	TCL_EvalStr('button .dial.b -text OK -command {bind .dial <Destroy> {};destroy .dial;set ok 1}')
	TCL_EvalStr('grid .dial.b -row '+string(size(names,1))+' -columnspan 2 -pady 10')
	TCL_EvalStr('bind .dial  <Destroy> {set ok 2}')
	realtimeinit(1/10000);
	while TCL_GetVar('ok')=='0' 
		realtime(0);
	end
	if TCL_GetVar('ok')=='1'
		for i=1:size(names,1)
			varargout(i)=evstr(TCL_GetVar(vars(i)));
		end
	else
		stat=1;
	end
endfunction


function sysmetab_menu(k)
global oldpath
if oldpath==[]
   oldpath=pwd();
end
select k
	case 1 then
		_filename=uigetfile("*.xml",oldpath,"S�lectionner le fichier SBML");
		if (_filename~="")
			_oldpwd=pwd();
			_dirname=dirname(_filename);
			_basename=basename(_filename);
			cd(_dirname);
			[ok,nexp]=getvalue(['Nombre d''exp�riences'],['nexp'],list('vec',1),'1');
      if ok
  			[rep,stat]=unix_g(%SYSMETAB+'BIN/gensolvestat '+_basename+' '+string(nexp)+' 2>&1')
  			if ~stat
  				messagebox('G�n�ration de '+_basename+'_stat_'+string(nexp)+'.xmllab r�ussie')
  			else			
  				messagebox(join_mess(rep))
  			end
      end
			cd(_oldpwd);
			oldpath=_dirname;
		end
	case 2 then
		_filename=uigetfile("*.xml",oldpath,"S�lectionner le fichier SBML");
		if (_filename~="")
			_oldpwd=pwd();
			_dirname=dirname(_filename);
			_basename=basename(_filename);
			cd(_dirname);
			[ok,nexp,nmes]=getvalue(['Nombre d''exp�riences';'nombre de mesures'],['nexp';'nmes'],list('vec','1','vec','1'),['1';'10']);
      if ok
  			[rep,stat]=unix_g(%SYSMETAB+'BIN/gensolvedyn '+_basename+' '+string(nexp)+' '+string(nmes)+' 2>&1')
  			if ~stat
         messagebox('G�n�ration de '+_basename+'_dyn_'+string(nexp)+'_'+string(nmes)+'.xmllab r�ussie')
  			else			
   				messagebox(join_mess(rep))
  			end
			end
      cd(_oldpwd);
			oldpath=_dirname;
		end
	case 3 then
		_filename=uigetfile("*.xmllab",oldpath,"S�lectionner l''application");
		if (_filename~="")
			xmllab('-run','-w','notebook',_filename)
           oldpath=dirname(_filename);
        end
	case 4 then
		if %SVN~=[]
			_oldpwd=pwd();
			cd(%SYSMETAB);
			[rep,stat]=unix_g(%SVN+ " update");
			messagebox('SYSMETAB : '+join_mess(rep))
			cd(%xmllab);
			[rep,stat]=unix_g(%SVN+ " update");
			messagebox('XMLLAB : '+join_mess(rep))
			cd(_oldpwd);
		end
	case 5 then
		TCL_EvalFile(%SYSMETAB+"TCL/uninstall.tcl")
		messagebox('D�sinstallation r�ussie')
		delmenu("SYSMETAB");
end

endfunction

delmenu("SYSMETAB");
addmenu("SYSMETAB",["G�n�ration stationnaire";"G�n�ration dynamique";"Ex�cuter une application XMLlab";"Mise � jour";"D�sinstaller Sysmetab"],list(2,"sysmetab_menu"));



global LANGUAGE
// Default language is the one used at installation time
mode(-1)
lines(0)
%SYSMETAB=get_absolute_file_path("loader.sce");

if TCL_EvalStr('file exists /sw/bin/svn')=="1"
	%SVN="/sw/bin/svn";
elseif TCL_EvalStr('file exists /usr/bin/svn')=="1"
	%SVN="/usr/bin/svn";
else
	%SVN=[];
    disp('Loading SYSMETAB')
	TCL_EvalStr('mess {Impossible de trouver svn, il ne sera pas possible de faire de mise � jour.}')
end

if %SVN~=[]
	[rep,stat]=unix_g(%SVN+" info "+%SYSMETAB);
    printf('\n %s\n','Loading SYSMETAB ('+rep(4)+')')
	printf(' %s\n\n',rep(9));
end

TCL_SetVar('SCIHOME',SCIHOME)
