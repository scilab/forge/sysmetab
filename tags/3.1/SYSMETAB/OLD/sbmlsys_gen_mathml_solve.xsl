<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer un fichier XML avec une structure
    permettant de g�n�rer les �quations dans un langage quelconque. Il r�sume les
    informations sur les diff�rents flux (connus ou pas), et sur les esp�ces (connues
    ou pas + �quations des bilans au format "Content MathML" :
    
    <carbon-labeling-system>
        <listOfReactions>
            <reaction id="re1" known="yes"/>
            .
            .
            .
        </listOfReactions>
        <listOfSpecies>
            <species id="s1" type="inout" known="yes"/>
            <species id="s2" name="Glucose6P" type="intermediate">
                <equations>
                    <m:apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </m:apply>
                    <m:apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </m:apply>
                    .
                    .
                    .
                </equations>
            </species>
    </carbon-labeling-system>
    
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
        version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="weight">2</xsl:param>

    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="cumomer" match="smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <carbon-labeling-system>
          <xsl:apply-templates/>
        </carbon-labeling-system>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:model">
            <xsl:apply-templates select="smtb:listOfIntermediateCumomers"/>
    </xsl:template>

    <xsl:template match="smtb:listOfIntermediateCumomers">
        
        <xsl:for-each select="smtb:listOfCumomers[@weight&lt;=$weight]">    
			<!-- D�claration matrice -->     
            <xsl:for-each select="smtb:cumomer">
                <equation weight="{@weight}" cumomer="{@id}">
                    <xsl:call-template name="iteration"/>                                    
                </equation>
            </xsl:for-each>
			<!-- r�solution du syst�me -->
			
        </xsl:for-each>
		
    </xsl:template>
   

<!-- Template de fabrication de l'�quation de bilan stoechiom�trique
     de l'esp�ce @id -->

<xsl:template name="bilan">

    <xsl:variable name="name" select="@id"/>

    <m:apply>
        <m:eq/>
        <m:cn>0</m:cn>
        <m:apply>
            <m:minus/>   
            
            <!-- influx rule -->

            <m:apply>
                <m:plus/>
                <xsl:for-each select="key('products',@id)">
                    <m:ci>
                        <xsl:value-of select="../../@id"/>
                    </m:ci>
                </xsl:for-each>
            </m:apply>
            
            <xsl:call-template name="bilan-outflux">
                <xsl:with-param name="id" select="@id"/>
            </xsl:call-template>
                 
        </m:apply>
    
    </m:apply>

</xsl:template>

<xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

    <!-- outflux rule -->

    <m:apply>
        <m:plus/>
        <xsl:for-each select="key('reactants',$id)">
            <m:ci>
                <xsl:value-of select="../../@id"/>
            </m:ci>
        </xsl:for-each>
    </m:apply>

</xsl:template>


<!-- Template de fabrication du second membre de l'�quation diff�rentielle
     correspondant � l'enrichissement positionnel num�ro i de l'esp�ce @id.
     Cette template s'appelle r�cursivement, puisque l'on ne peut faire de
     boucle en xsl... -->


<xsl:template name="iteration">
    
    <!-- Attention le noeud contextuel est un �lement <smtb:cumomer> -->

    <xsl:variable name="name" select="../@id"/>
    
    <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>
	
    <xsl:variable name="weight" select="@weight"/>

    <!-- Suivant que l'on est en stationnaire ou en dynamique, cela change 
         le membre de gauche : -->

    <m:apply>
        <m:eq/>
        <xsl:choose>
            <xsl:when test="$type='stationnaire'">
                <m:cn>0</m:cn>
            </xsl:when>
            <xsl:when test="$type='dynamique'">
                <m:apply>
                    <m:times/>
                    <m:ci>
                        <xsl:value-of select="@species"/>
                    </m:ci>
                    <m:apply>
                        <m:diff/>
                        <m:ci>
                            <xsl:value-of select="@id"/> 
                        </m:ci>
                    </m:apply>
                </m:apply>
            </xsl:when>
        </xsl:choose>

        <m:apply>
            <m:minus/>   

            <!-- influx rule : le plus chiant, mais aussi le plus int�ressant. C'est l� que se cr�e v�ritablement
                 l'information suppl�mentaire par rapport � la stoechiom�trie simple. 

                 On boucle sur tous les �l�ments <speciesReference> qui ont l'esp�ce courante comme produit,
                 donc dans le for-each le noeud contextuel est de type reaction/listOfProducts/speciesReference. -->

            <m:apply>
                <m:plus/>
                <xsl:for-each select="key('products',@species)"> 

                    <!-- Maintenant, on essaye de trouver des occurences des carbones marqu�s
                         du reactant de la r�action courante : c'est du boulot... -->

                    <xsl:variable name="species" select="@species"/>
                    <xsl:variable name="occurence" select="count(preceding-sibling::sbml:speciesReference)+1"/>

					
                    <xsl:variable name="influx">

                        <!-- C'est ici que les choses s�rieuses commencent. On boucle sur tous les r�actants : -->

                        <xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[smtb:carbon[(@species=$species) and (@occurence=$occurence)]]">

                            <xsl:variable name="somme">
                                <xsl:for-each select="smtb:carbon[(@species=$species) and (@occurence=$occurence)]">
                                    <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                                        <token>
                                            <xsl:value-of select="@position"/>
                                        </token>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:variable>

                            <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
                                <token weight="{count(exslt:node-set($somme)/token)}" type="{key('species',@species)/@type}">
                                    <xsl:value-of select="concat(@species,'_',sum(exslt:node-set($somme)/token))"/>
                                </token>
                            </xsl:if>

                        </xsl:for-each>

					</xsl:variable>
					
					<xsl:choose>
						<xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
							<m:apply>
								<m:times/>
								<m:ci>
                            		<!-- id de la r�action -->
                            		<xsl:value-of select="../../@id"/>
                        		</m:ci>
								<m:ci>
				    				<xsl:value-of select="concat('x',$weight,'(',key('cumomer',$influx)/@position,')')"/>
								</m:ci>
							</m:apply>
						</xsl:when>
						<xsl:otherwise>
							<m:apply>
								<m:times/>
								<m:ci>
                            		<!-- id de la r�action -->
                            		<xsl:value-of select="../../@id"/>
                        		</m:ci>
								<xsl:for-each select="exslt:node-set($influx)/token">
									<m:ci>
										<xsl:value-of select="."/>
									</m:ci>
								</xsl:for-each>
							</m:apply>						
						</xsl:otherwise>
					</xsl:choose>

                </xsl:for-each>
				
                <!-- outflux rule : partie la plus simple � g�n�rer (voir papier Wiechert) -->

			</m:apply>				
			
            <m:apply>
              <m:times/>
              <m:ci>
                   <!--<xsl:value-of select="@id"/>-->
				   <xsl:value-of select="concat('x',@weight,'(',position(),')')"/>
              </m:ci>

            	<xsl:call-template name="bilan-outflux">
                	<xsl:with-param name="id" select="@species"/>
            	</xsl:call-template>

            </m:apply>
        </m:apply>
        
    </m:apply>
    
</xsl:template>

</xsl:stylesheet>
