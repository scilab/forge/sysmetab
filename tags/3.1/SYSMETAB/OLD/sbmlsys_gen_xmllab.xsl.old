<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer le fichier
    XMLlab d�crivant l'interface du logiciel de simulation/optimisation
    sp�cifique � un r�seau donn�.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:math="http://exslt.org/math" version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" doctype-system="http://www.xmllab.org/dtd/1.5/fr/simulation.dtd" doctype-public="-//UTC//DTD XMLlab V1.5//FR" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="file"/>
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="experiences">1</xsl:param>

	<xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

	<xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <simulation>
          <xsl:apply-templates/>
        </simulation>
    </xsl:template>

    <xsl:template match="sbml:model">
        <xsl:variable name="all-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="unknown-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[not(@known)]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="known-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[@known='yes']">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="input-cumomers">
            <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="cumomers">
            <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        
        <header>
            <title>R�seau m�tabolique <xsl:value-of select="@id"/></title>
			<author>St�phane Mottelet</author>
			<date>
				<xsl:value-of select="concat(date:date(),', at ',date:time())"/>
			</date>
            <script href="{$file}.sci"/>
            <script href="sysmetab.sci"/>
        </header>
        <parameters>
            <section>
                <title>Flux</title>
                <xsl:apply-templates select="sbml:listOfReactions/sbml:reaction"/>
            </section>

			<xsl:call-template name="iterate-experiences"/>
		
            <actions>
                <title>Simulation</title>
                <action>
                    <xsl:attribute name="update">
                        <xsl:value-of select="$unknown-fluxes"/>
                    </xsl:attribute>
                    <title>Admissibilit� des flux connus</title>
                    <script>
						<!-- vecteur de pond�ration pour le calcul d'un flux admissible -->
						<xsl:variable name="p">
							<xsl:for-each select="str:split($all-fluxes)">
								<xsl:value-of select="concat(.,'_coeff')"/>
								<xsl:if test="position()&lt;last()">
									<xsl:text>;</xsl:text>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
                        <xsl:value-of select="concat('[',translate($all-fluxes,' ',','),']=admissibilityTest([',translate($known-fluxes,' ',';'),'],[',$p,'])')"/>
                    </script>
                </action>
                <action>
                    <xsl:attribute name="update">
                        <xsl:for-each select="sbml:listOfSpecies/sbml:species/smtb:measurement">
                            <xsl:value-of select="@id"/>
                            <xsl:if test="position()&lt;last()">
                                <xsl:text> </xsl:text>
                            </xsl:if>    
                        </xsl:for-each>
                    </xsl:attribute>
                    <title>Calcul des cumom�res</title>
                    <script href="{$file}_script.sci"/>
 					<xsl:document href="{$file}_script.sci" method="text" encoding="ISO-8859-1">
                    	<xsl:text>[</xsl:text>
                    	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
                        	<xsl:value-of select="concat('x',position())"/>
                        	<xsl:if test="position()&lt;last()">
                            	<xsl:text>,</xsl:text>
                        	</xsl:if>
                    	</xsl:for-each>
                    	<xsl:text>]=solveCumomers([</xsl:text>
                    	<xsl:value-of select="$input-cumomers"/>
                    	<xsl:text>],</xsl:text>
                    	<xsl:value-of select="concat('[',$all-fluxes,']);&#xA;')"/>
						<xsl:text>y=</xsl:text>
						<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
							<xsl:value-of select="concat('C',position(),'*x',position())"/>
							<xsl:if test="position()&lt;last()">
								<xsl:text>+</xsl:text>
							</xsl:if>
						</xsl:for-each>
						<xsl:text>;&#xA;</xsl:text>
                    	<xsl:for-each select="sbml:listOfSpecies/sbml:species[@type='intermediate']/smtb:measurement">
                        	<xsl:value-of select="concat(@id,'=y(',position(),');&#xA;')"/>
                    	</xsl:for-each>
					</xsl:document>
                </action>
            </actions>    
        </parameters>
    </xsl:template>

	<xsl:template name="iterate-experiences">
		<xsl:param name="i">1</xsl:param>
        <section>
            <title>Cumom�res (<xsl:value-of select="concat('exp�rience ',$i)"/>)</title>
            <xsl:apply-templates select="sbml:listOfSpecies/sbml:species[@type='input']">
				<xsl:with-param name="experience" select="$i"/>
			</xsl:apply-templates>
            <xsl:apply-templates select="sbml:listOfSpecies/sbml:species[(@type='intermediate') and smtb:measurement]">
				<xsl:with-param name="experience" select="$i"/>
			</xsl:apply-templates>
        </section>
		
		<xsl:if test="$i&lt;$experiences">
			<xsl:call-template name="iterate-experiences">
				<xsl:with-param name="i" select="($i)+1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

    <xsl:template match="sbml:species[(@type='intermediate') and smtb:measurement]">
		<xsl:param name="experience"/>
        <xsl:variable name="id" select="@id"/>
        <subsection>
            <title>
                <xsl:value-of select="@name"/>
            </title>
            <xsl:for-each select="smtb:measurement">
                <xsl:variable name="cid" select="concat($id,'_',@string)"/>
                <scalar label="{@id}_{$experience}" state="disabled">
                    <name>
                        <xsl:value-of select="$cid"/>
                    </name>
                </scalar>
            </xsl:for-each>
        </subsection>
    </xsl:template>

    <xsl:template match="sbml:species[@type='input']">
		<xsl:param name="experience"/>
        <xsl:variable name="id" select="@id"/>
        <subsection>
            <title>
                <xsl:value-of select="@name"/>
            </title>
            <xsl:for-each select="str:split(@measurement,',')">
                <xsl:variable name="cid" select="concat($id,'_',.)"/>
                <scalar label="{concat($cid,'_',$experience)}">
                    <name>
                        <xsl:value-of select="$cid"/>
                    </name>
                </scalar>
            </xsl:for-each>
        </subsection>
    </xsl:template>

    <xsl:template match="sbml:reaction[@known='yes']">
		<group>		
        	<scalar label="{@id}">
            	<name>
                	<xsl:value-of select="concat(@id,', ',@name)"/>
            	</name>
            	<value>100</value>
        	</scalar>
			<scalar label="{@id}_coeff">
				<name>Coeff.</name>
				<value>0</value>
			</scalar>
		</group>
    </xsl:template>

    <xsl:template match="sbml:reaction">
		<group>
        	<scalar label="{@id}" state="disabled">
            	<name>
                	<xsl:value-of select="concat(@id,', ',@name)"/>
            	</name>
        	</scalar>
			<scalar label="{@id}_coeff">
				<name>Coeff.</name>
				<value>0</value>
			</scalar>
		</group>
    </xsl:template>

</xsl:stylesheet>
