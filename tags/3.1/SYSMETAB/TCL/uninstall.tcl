proc insert {start pattern string} {
    set newstart $start.new

    if {![file exists $start]} {
        if {![catch {set fp [open $start a]}]} {
    		if {$string!=""} {
	        	puts $fp $string
   		     }
        } else {
            mess "You don't have write access to directory [file dirname $start]. Please check permissions."
            exit 1;
        }
        close $fp
        return
    } 
    if {![catch {set fpnew [open $newstart w]}]} {
        set fp [open $start r]
        set matched 0
        while {![eof $fp]} {
            set line [gets $fp]
            if {![string match $pattern $line] && ![eof $fp]} {
                puts $fpnew $line
            } else {
                if {!$matched} {
                    if {$string!=""} { 
	                    puts $fpnew $string
                    }
                    set matched 1
                }
            }
      }
      close $fp
      close $fpnew
    } else {
        mess "You don't have write access to directory [file dirname $start]. Please check permissions."
        exit 1;
    }
    catch {file rename -force $newstart $start}
}
proc mess { mess } {

    set reply [tk_dialog .dial "" $mess  "" 0 " OK "]
 }

insert [file join $SCIHOME .scilab] "exec*SYSMETAB*loader.sce*" ""
insert [file join $SCIHOME .scilab] "exec*OPTIM*loader.sce*" ""
