#include "stack-c.h"
#include "cfsqpusr.h"
#include <string.h>
#include "FTables0.h"
#include "FTables.h"
#include "machine.h"
#include "localization.h"

extern int C2F(gettype)(int *);
extern int C2F(scifunction)(int *,int *,int *,int*) ;
extern int C2F(dcopy)(int *,double *,int*,double *,int*);
extern int SearchInDynLinks(char *op, void (**realop) ());


/***************************************************
 * deal with errors in scilab functions 
 ***************************************************/

#include <setjmp.h>

static  jmp_buf fsqpenv; 

#define ExecSciFunction(n,mx,nx,lx,name) \
  if(! C2F(scifunction)((c_local=n,&c_local),mx,nx,lx))\
{ sciprint("Error in function %s\r\n",name);  longjmp(fsqpenv,-1); }


/** like CreateVar but with return for void functions */

#define CreateVar1(n,ct,mx,nx,lx) if(! C2F(createvar)((c_local=n,&c_local),ct,mx,nx,lx, 1L))\
        { return ;  }


/***************************************************
 * data for interface 
 ***************************************************/

#define a_chain 10
#define a_function 13

static int sci_obj, lhs_obj, rhs_obj;
static int sci_cntr, lhs_cntr, rhs_cntr;
static int sci_grob, lhs_grob, rhs_grob;
static int sci_grcntr, lhs_grcntr, rhs_grcntr;

/***************************************************
 * Function used by fsqp 
 ***************************************************/

static void sciobj (ARGS_ffsqpobj);
static  ffsqpobjf objective;

static void scicntr (ARGS_ffsqpcntr);
static  ffsqpcntrf constraints;

static void scigrob (ARGS_ffsqpgrob);
static  ffsqpgrobf objectiveGrad;

static void scigrcn (ARGS_ffsqpgrcn);
static  ffsqpgrcnf constraintsGrad;

static voidf GetFunctionPtr(char *,int,FTAB *,voidf,int *,int*,int*);
extern void set_x_is_new(int n);
extern int get_x_is_new();

/***************************************************
/* function fname=get_x_is_new */
/***************************************************/
int cintfsqp1(char *fname)
{ 
  int minlhs=1, minrhs=0, maxlhs=1, maxrhs=0;
  int un=1, xisnew;
  /*   Check rhs and lhs   */  
  CheckRhs(minrhs,maxrhs) ;
  CheckLhs(minlhs,maxlhs) ;

  CreateVar( 1, "d", &un, &un, &xisnew);
  stk(xisnew)[0]=(double) get_x_is_new();
  LhsVar(1) = 1;
  return 0;
}
/***************************************************
/* function fname=set_x_is_new */
/***************************************************/
int cintfsqp0( char *fname)
{   
  /*    Define minlhs=1, maxlhs=1, minrhs, maxrhs   */
  static int minlhs=1, minrhs=1, maxlhs=1, maxrhs=1;
  int un=1, arg;
  /*   Check rhs and lhs   */  
  CheckRhs(minrhs,maxrhs) ;
  CheckLhs(minlhs,maxlhs) ;

  GetRhsVar(1, "i", &un,&un,&arg);
  if ( istk(arg)[0]==1 ) 
    set_x_is_new(TRUE);
  else
     set_x_is_new(FALSE);
  LhsVar(1) = 1;
  return 0;

}

/***************************************************
 * cintsqp2 : interface for fsqp 
 ***************************************************/

int cintfsqp2(char *fname) 
{ 
  int returned_from_longjump ;
  int nparam,nf,nfsr,neqn,nineqn,nineq,neq,ncsrl,ncsrn,mode;
  int iprint,miter,mesh_pts[1];
  double  bigbnd, eps, epsneq, udelta;

  static int un=1;

  static int x0, m_x0, n_x0;
  static int ipar, m_ipar, n_ipar;
  static int rpar, m_rpar, n_rpar;
  static int bl, bu, m_blbu, n_blbu;
  static int cd, m_cd, n_cd;
  static int inform, f, g, lambda, pipo;
  static int dimf, dimg, dimlambda;

  /*    Define minls=1, maxlhs, minrhs, maxrhs   */
  static int minlhs=1, minrhs=8, maxlhs=5, maxrhs=9;

  /*   Check rhs and lhs   */  
  CheckRhs(minrhs,maxrhs) ;
  CheckLhs(minlhs,maxlhs) ;

  /* RhsVar: fsqp(x0,ipar,rpar,[bl,bu],obj,cntr,grob,grcntr [,cd]) */
  /*               1,  2,  3,     4,    5,  6,   7,     8,   9     */

  /*   Variable 1 (x0)   */
  GetRhsVar(1, "d", &m_x0, &n_x0, &x0);
  nparam=m_x0*n_x0;

  /*   Variable 2 (ipar)   */
  GetRhsVar(2, "i", &m_ipar, &n_ipar, &ipar);
  if (! ( m_ipar*n_ipar == 8 )) 
    {
      sciprint("Wrong parameter in fsqp (number 2: should be of size 8!)\r\n");
      Error(999);
      return 0;
    }
  
  nf=*istk(ipar);
  nineqn=*istk(ipar+1);
  nineq=*istk(ipar+2);
  neqn=*istk(ipar+3);
  neq=*istk(ipar+4);
  mode=*istk(ipar+5);
  miter=*istk(ipar+6);
  iprint=*istk(ipar+7);

  /*   Variable 3 (rpar)   */
  GetRhsVar(3, "d", &m_rpar, &n_rpar, &rpar);
  if (! ( m_rpar*n_rpar == 4 )) 
    {
      sciprint("Wrong parameter in fsqp (number 3: should be of size 4!)\r\n");
      Error(999);
      return 0;
    }

  bigbnd=*stk(rpar);
  eps=*stk(rpar+1);
  epsneq=*stk(rpar+2);
  udelta=*stk(rpar+3);

  /* Variable 4 ( [bl,bu] )  */  

  GetRhsVar(4, "d", &m_blbu, &n_blbu, &bl);
  
  if ( ( m_blbu != nparam ) || ( n_blbu !=2) ) {
    sciprint("Wrong parameter in fsqp (number 4)\r\n");
    Error(999);
    return 0;}
  bu=bl+nparam;

  /*   Variable 5 (objective function)   */

  objective = (ffsqpobjf) GetFunctionPtr("fsqp",5,FTab_ffsqpobj,(voidf) sciobj,&sci_obj,&lhs_obj,&rhs_obj);
  if ( objective == (ffsqpobjf) 0 ) return 0;
  
  /*  Variable 6 (constraints function)  */

  constraints = (ffsqpcntrf) GetFunctionPtr("fsqp",6,FTab_ffsqpcntr,(voidf) scicntr,&sci_cntr,&lhs_cntr,&rhs_cntr);
  if ( constraints == (ffsqpcntrf) 0 ) return 0;

  /*  Variable 7 (gradient of objective function)  */

  objectiveGrad = (ffsqpgrobf) GetFunctionPtr("fsqp",7,FTab_ffsqpgrob,(voidf) scigrob,&sci_grob,&lhs_grob,&rhs_grob);
  if ( objectiveGrad == (ffsqpgrobf) 0 ) return 0;

  /*  Variable 8 (gradient of constraints function)  */

  constraintsGrad = (ffsqpgrcnf) GetFunctionPtr("fsqp",8,FTab_ffsqpgrcn,(voidf) scigrcn,&sci_grcntr,&lhs_grcntr,&rhs_grcntr);
  if ( constraintsGrad == (ffsqpgrcnf) 0 ) return 0;

  /*  Variable 9 (optional variable cd)  */

  if (Rhs == 9)
    {
      GetRhsVar(9, "d", &m_cd, &n_cd, &cd);
    }
  else
    {
      CreateVar(9, "d", &un, &un, &cd);
    }

  ncsrl=ncsrn=nfsr=mesh_pts[0]=0;

  /* Internal variables: inform, f, g, lambda */

  CreateVar(10, "i", &un, &un, &inform);

  dimf=nf;
  if (dimf <1) dimf=1;
  CreateVar(11, "d", &dimf, &un, &f);

  dimg=nineq+neq+1;
  if (dimg<1) dimg=1;
  CreateVar(12, "d", &dimg, &un, &g);

  dimlambda=nineq+neq+nf+nparam+1;
  CreateVar(13, "d", &dimlambda, &un, &lambda);

  /** just to fill the 14th and 15th  variable which are unused here **/
  CreateVar(14, "d", &un, &un, &pipo);
  CreateVar(15, "d", &un, &un, &pipo);

  if (( returned_from_longjump = setjmp(fsqpenv)) != 0 )
    {
      x_is_new=TRUE;
      Error(999);
      return 0;
    }
  x_is_new=TRUE;
  cfsqp(nparam,nf,nfsr,nineqn,nineq,neqn,neq,ncsrl,ncsrn,mesh_pts,
	mode,iprint,miter,
	istk(inform),
	bigbnd,eps,epsneq,udelta,
	stk(bl),stk(bu),stk(x0),
	stk(f),stk(g),stk(lambda),
	objective,constraints,objectiveGrad,constraintsGrad,
	stk(cd));

  /* LhsVar: [x, inform, f, g, lambda] = fsqp(...) */
  LhsVar(1) = 1;
  LhsVar(2) = 10;
  LhsVar(3) = 11;
  LhsVar(4) = 12;
  LhsVar(5) = 13;
  return 0;
}

int cintfsqp3(char *fname) 
{ 
  int *Mesh, Mesh0[1];
  int returned_from_longjump ;
  int nparam,nf,nfsr,neqn,nineqn,nineq,neq,ncsrl,ncsrn,mode;
  int iprint,miter;
  double  bigbnd, eps, epsneq, udelta;

  static int un=1;

  static int x0, m_x0, n_x0;
  static int ipar, m_ipar, n_ipar;
  static int rpar, m_rpar, n_rpar;
  static int srpar, m_srpar, n_srpar;
  static int mesh, m_mesh, n_mesh;
  static int bl, bu, m_blbu, n_blbu;
  static int cd, m_cd, n_cd;
  static int inform, f, g, lambda;
  static int dimf, dimg, dimlambda;
  static int icount;

  /*    Define minls=1, maxlhs, minrhs, maxrhs   */
  static int minlhs=1, minrhs=10, maxlhs=5, maxrhs=11;

  /*   Check rhs and lhs   */  
  CheckRhs(minrhs,maxrhs) ;
  CheckLhs(minlhs,maxlhs) ;

  /* RhsVar: fsqp(x0,ipar,rpar,[bl,bu],obj,cntr,grob,grcntr [,cd]) */
  /*               1,  2,  3,     4,    5,  6,   7,     8,   9     */

  /*   Variable 1 (x0)   */
  GetRhsVar(1, "d", &m_x0, &n_x0, &x0);
  nparam=m_x0*n_x0;

  /*   Variable 2 (ipar)   */
  GetRhsVar(2, "i", &m_ipar, &n_ipar, &ipar);
  if (! ( m_ipar*n_ipar == 8 )) 
    {
      sciprint("Wrong parameter in srfsqp (number 2: should be of size 8!)\r\n");
      Error(999);
      return 0;
    }
  
  nf=*istk(ipar);
  nineqn=*istk(ipar+1);
  nineq=*istk(ipar+2);
  neqn=*istk(ipar+3);
  neq=*istk(ipar+4);
  mode=*istk(ipar+5);
  miter=*istk(ipar+6);
  iprint=*istk(ipar+7);

  /*   Variable 3 (srpar)  */
  GetRhsVar(3, "i", &m_srpar, &n_srpar, &srpar);
  if (! ( m_srpar*n_srpar == 3 )) 
    {
      sciprint("Wrong parameter in srfsqp (number 3: should be of size 3!)\r\n");
      Error(999);
      return 0;
    }

  nfsr=*istk(srpar);
  ncsrl=*istk(srpar+1);
  ncsrn=*istk(srpar+2);

  /*   Variable 4 (mesh_pts)  */
  GetRhsVar(4, "i", &m_mesh, &n_mesh, &mesh);
  if (! ( m_mesh*n_mesh == nfsr + ncsrn +ncsrl )) 
    {
      sciprint("Wrong parameter in srfsqp (number 4)!)\r\n");
      Error(999);
      return 0;
    }
  if ( m_mesh*n_mesh != 0 ) 
    Mesh = istk(mesh);
  else 
    {
      Mesh0[0]=0;
      Mesh = Mesh0;
    }
  /*   Variable 5 (rpar)   */
  GetRhsVar(5, "d", &m_rpar, &n_rpar, &rpar);
  if (! ( m_rpar*n_rpar == 4 )) 
    {
      sciprint("Wrong parameter in srfsqp (number 5: should be of size 4!)\r\n");
      Error(999);
      return 0;
    }

  bigbnd=*stk(rpar);
  eps=*stk(rpar+1);
  epsneq=*stk(rpar+2);
  udelta=*stk(rpar+3);

  /* Variable 6 ( [bl,bu] )  */  

  GetRhsVar(6, "d", &m_blbu, &n_blbu, &bl);
  
  if ( ( m_blbu != nparam ) || ( n_blbu !=2) ) {
    sciprint("Wrong parameter in srfsqp (number 6)\r\n");
    Error(999);
    return 0;}
  bu=bl+nparam;

  /*   Variable 7 (objective function)   */

  objective = (ffsqpobjf) GetFunctionPtr("fsqp",7,FTab_ffsqpobj,(voidf) sciobj,&sci_obj,&lhs_obj,&rhs_obj);
  if ( objective == (ffsqpobjf) 0 ) return 0 ;
  
  /*  Variable 8 (constraints function)  */

  constraints = (ffsqpcntrf) GetFunctionPtr("fsqp",8,FTab_ffsqpcntr,(voidf) scicntr,&sci_cntr,&lhs_cntr,&rhs_cntr);
  if ( constraints == (ffsqpcntrf) 0 ) return 0;

  /*  Variable 9 (gradient of objective function)  */

  objectiveGrad = (ffsqpgrobf) GetFunctionPtr("fsqp",9,FTab_ffsqpgrob,(voidf) scigrob,&sci_grob,&lhs_grob,&rhs_grob);
  if ( objectiveGrad == (ffsqpgrobf) 0 ) return 0 ;

  /*  Variable 10 (gradient of constraints function)  */

  constraintsGrad = (ffsqpgrcnf) GetFunctionPtr("fsqp",10,FTab_ffsqpgrcn,(voidf) scigrcn,&sci_grcntr,&lhs_grcntr,&rhs_grcntr);
  if ( constraintsGrad == (ffsqpgrcnf) 0 ) return 0 ;

  /*  Variable 11 (optional variable cd)  */

  if (Rhs == 11)
    {
      GetRhsVar(11, "d", &m_cd, &n_cd, &cd);
    }
  else
    {
      CreateVar(11, "d", &un, &un, &cd);
    }

  /* Internal variables : inform, f, g, lambda */

  CreateVar(12, "i", &un, &un, &inform);

   dimf = 0;
    for (icount = 0; icount < nfsr ; ++icount) {
	dimf += Mesh[icount];
    }
    dimf = dimf + nf - nfsr;
    if (dimf < 1) dimf = 1; 

  CreateVar(13, "d", &dimf, &un, &f);

  dimg=0;
    for (icount = 0; icount < ncsrn ; ++icount) {
	dimg += Mesh[icount+nfsr];
    }
    for (icount = 0; icount < ncsrl ; ++icount) {
	dimg += Mesh[icount+nfsr+ncsrn];
    }
    dimg=dimg+nineq+neq - (ncsrl+ncsrn)+dimg;
    if (dimg < 1) dimg = 1;

  CreateVar(14, "d", &dimg, &un, &g);

  dimlambda=dimf+dimg+nparam;

  CreateVar(15, "d", &dimlambda, &un, &lambda);
  
  if (( returned_from_longjump = setjmp(fsqpenv)) != 0 )
    {
      x_is_new=TRUE;
      Error(999);
      return 0;
    }
  x_is_new=TRUE;
  cfsqp(nparam,nf,nfsr,nineqn,nineq,neqn,neq,ncsrl,ncsrn,Mesh,
	mode,iprint,miter,
	istk(inform),
	bigbnd,eps,epsneq,udelta,
	stk(bl),stk(bu),stk(x0),
	stk(f),stk(g),stk(lambda),
	objective,constraints,objectiveGrad,constraintsGrad,
	stk(cd));

  /* LhsVar: [x, inform, f, g, lambda] = srfsqp(...) */
  LhsVar(1) = 1;
  LhsVar(2) = 12;
  LhsVar(3) = 13;
  LhsVar(4) = 14;
  LhsVar(5) = 15;
  return 0;
}


void set_x_is_new(int n) 
{
  x_is_new = n;
}

int get_x_is_new()
{
  return x_is_new;  
} 


static void sciobj(int nparam,int  j,double *x,double *fj,void *cd)
{
  int scilab_j,scilab_x;
  int un=1;
  /* Inputs (j,x) at positions 16,17 */
  CreateVar1(16,"d",&un,&un,&scilab_j);
  stk(scilab_j)[0] = (double) j;

  CreateVar1(17,"d",&nparam,&un,&scilab_x);
  C2F(dcopy)(&nparam,x,&un,stk(scilab_x),&un);

  ExecSciFunction(16, &sci_obj,&lhs_obj,&rhs_obj,"sciobj");
  /* One output at position of first input (16) */
  /* scilab_fj=scilab_j */
  fj[0]=stk(scilab_j)[0];
}


static void scicntr(int nparam,int j, double *x, double *gj,void *cd)
{
  int scilab_j,scilab_x;
  int un=1;

  CreateVar1(16,"d",&un,&un,&scilab_j);
  stk(scilab_j)[0] = (double) j;

  CreateVar1(17,"d",&nparam,&un,&scilab_x);
  C2F(dcopy)(&nparam,x,&un,stk(scilab_x),&un);

  ExecSciFunction(16, &sci_cntr,&lhs_cntr,&rhs_cntr,"scicntr");
  gj[0] = stk(scilab_j)[0];
}


static void scigrob(int nparam,int j,double *x,double *gradfj,void (* dummy)(),void *cd)
{
  int scilab_j,scilab_x;
  int un=1;

  CreateVar1(16,"d",&un,&un,&scilab_j);
  stk(scilab_j)[0] = (double) j;

  CreateVar1(17,"d",&nparam,&un,&scilab_x);
  C2F(dcopy)(&nparam,x,&un,stk(scilab_x),&un);

  ExecSciFunction(16, &sci_grob,&lhs_grob,&rhs_grob,"scigrob");
  C2F(dcopy)(&nparam,stk(scilab_j),&un,gradfj,&un);
}

static void scigrcn(int nparam,int j,double *x,double *gradgj,void (* dummy)(),void *cd)
{
  int scilab_j,scilab_x;
  static int un=1;

  CreateVar1(16,"d",&un,&un,&scilab_j);
  stk(scilab_j)[0] = (double) j;

  CreateVar1(17,"d",&nparam,&un,&scilab_x);
  C2F(dcopy)(&nparam,x,&un,stk(scilab_x),&un);

  ExecSciFunction(16, &sci_grcntr,&lhs_grcntr,&rhs_grcntr,"scigrcn");
  C2F(dcopy)(&nparam,stk(scilab_j),&un,gradgj,&un);
}   


/***********************************
 * dealing with scilab externals
 ***********************************/

static voidf SetFunction(char *name, int *rep, FTAB *table);  
static int SearchComp(FTAB *Ftab, char *op, void (**realop) ( ));  
static void Emptyfunc  (void) {} ;

voidf GetFunctionPtr(char *name,int n,FTAB *Table,voidf scifun,int *ifunc,int *lhs,int *rhs) 
{
  int type,rep,mm,nn;
  voidf f;
  type=VarType(n);
  switch ( type) 
    {
    case a_chain : 
      GetRhsVar(n, "c", &mm, &nn, ifunc);
      f = SetFunction(cstk(*ifunc),&rep,Table);
      if ( rep == 1 )
	{
	  Error(999);
	  return (voidf) 0;
	}
      return f ;
    case  a_function : 
      GetRhsVar(n, "f", lhs,rhs, ifunc);
      return (voidf) scifun ;
    default: 
      sciprint("Wrong parameter in %s ! (number %d)\r\n",name,n);
      Error(999);
      return (voidf) 0 ;
    }
}

/*******************************************
 * General function 
 *******************************************/

static voidf SetFunction(char *name,int *rep,FTAB *table) 
{
  void (*loc)();
  char *s;
  strncpy(buf,name,MAXNAME);
  s=buf ; while ( *s != ' ' && *s != '\0') { s++;};
  *s= '\0';
  if ( SearchComp(table,buf,&loc) == OK) 
    {
      *rep = 0;
      return(loc);
    }
  if ( SearchInDynLinks(buf,&loc) >= 0 )
    {
      *rep = 0;
      return(loc);
    }
  loc = Emptyfunc;
  *rep = 1;
  sciprint(" Function %s not found\r\n",name);
  return(loc);
}


/*******************************************
 * Attention trier la table 
 * cherche un operateur dans une table : 
 * a ameliorer en utilisant bsearch 
 *******************************************/

static int SearchComp(FTAB *Ftab, char *op,void (**realop)()) 
{
  int i=0;
  while ( Ftab[i].name != (char *) 0) 
     {
       int j;
       j = strcmp(op,Ftab[i].name);
       if ( j == 0 )
         {
           *realop = Ftab[i].f;
	   return(OK);
	 }
       else
         { 
	   if ( j <= 0)
             {
               /* sciprint("\nUnknow function <%s>\r\n",op); */
               return(FAIL);
             }
	   else i++;
         }
     }
  /* sciprint("\n Unknow function <%s>\r\n",op); */
  return(FAIL);
}

