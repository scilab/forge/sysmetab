// linking the fsqp library 
//===================================================
if ~c_link('libcfsqp') then exec('../loader.sce') ; end 
exdir=get_absolute_file_path('myexample.sce');

modefsqp=111;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=0.e0;
bigbnd=1.e10; 
eps=1.e-8; 
epsneq=0.e0; 
udelta=0.e0;

rpar=[bigbnd,eps,epsneq,udelta];

// a column vector 
t1=linspace(-1,1,5)';

// example ou on cherche aussi a maximiser en sig
//=======================================================

getf(exdir+'/listutils.sci');

M=10;
SIG=20;

function y=f_1(x)
  y=- x(2).^2;
endfunction

function y=gradf_1(x)
  y=[0,-2.0*x(2)]
endfunction

function y=f(z); y=z.^3 + (z-1).^2 ;endfunction

function y=gradf(z); y=3*z.*z + 2*(z-1) ; endfunction

function g=G_1(x)
  g= - f(x(1)+t1*x(2)) +M - SIG 
endfunction

function g=G_2(x)
  g= f(x(1)+t1*x(2)) - M  - SIG 
endfunction

function w=grG_1(x)
  w=[-gradf(x(1)+t1*x(2)),-gradf(x(1)+t1*x(2)).*t1 ];
endfunction

function w=grG_2(x)
  w=[gradf(x(1)+t1*x(2)),gradf(x(1)+t1*x(2)).*t1];
endfunction

//f regular objective R-valued
//G_1 first SR constraint non linear inequality 
//G_2 2nd   SR constraint non linear inequality
//G_3 3rd   SR constraint non linear inequality

list_obj=list(list(f_1),list()); 

list_cntr=list(list(),list(G_1,G_2),list(),list(),list(),list());

list_grobj=list(list(gradf_1),list());

list_grcn=list(list(),list(grG_1,grG_2),list(),list(),list(),list());

x0=[2;0.1];

[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-7;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)

xx=-5:0.01:5;
yy=f(xx);
A=min(yy);
plot2d(xx,yy);
plot2d([xx;xx]',[(M+SIG)*ones(xx);(M-SIG)*ones(xx)]',strf='000');
plot2d([xx;xx]',[(f(x(1)-x(2)))*ones(xx);(f(x(1)-x(2)))*ones(xx)]',strf='000');
xxx=[1;1]*[x(1)-x(2),x(1)+x(2)],
plot2d([xxx],[A,A;f(x(1)-x(2)),f(x(1)+x(2))],strf='000');



