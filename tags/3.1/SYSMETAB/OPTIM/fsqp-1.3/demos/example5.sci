function fj=obj(j,x)
  fj=(x(1)^2)/3+x(2)^2+x(1)/2
endfunction

function gr=grob(j,x)
  gr=[2/3*x(1)+0.5,2*x(2)];
endfunction

function ct=cntr(j,x)
  y=(j-1)/100;
  ct=(1-(x(1)*y)^2)^2-x(2)^2+x(2)-x(1)*y^2;
endfunction

function gj=grcn(j,x)
  y=(j-1)/100;y2=y*y;
  gj=[-4*y2*(1-(y*x(1))^2)^2*x(1)-y2, -2*x(2)+1];
endfunction

function f=allobj(x)
  f=(x(1)^2)/3+x(2)^2+x(1)/2;
endfunction

function gr=allgrob(x)
  gr=[2/3*x(1)+0.5,2*x(2)];
endfunction

function c=allcntr(x)
//(t=0:0.01:1)';
//t2=t.*t;
  c=(1-(t*x(1))^2)^2-x(1)*t2+x(2)-x(2)^2;
endfunction


function g=allgrcn(x)
//(t=0:0.01:1)';
//t2=t.*t;
  g=[-4*t2.*(1-(t*x(1))^2)^2*x(1)-t2,-2*x(2)+ones(t)];
endfunction

function fj=obj2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    fj=all_objectives(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    fj=all_objectives(j);
  end
endfunction


function cj=cntr2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    cj=all_constraints(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    cj=all_constraints(j);
  end
endfunction

function gj=grob2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    gj=all_gradobjectives(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradobjectives(j,:);
  end
endfunction

function gj=grcn2(j,x)
  if x_is_new() then
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    gj=all_gradconstraints(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradconstraints(j,:);
  end
endfunction


