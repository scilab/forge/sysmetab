function f=f_1(x)
//Evaluate all objectives in vector f
  pi=cd(1);
  f=1/15+2/15*(cos(7*pi*sintheta)+cos(2*pi*sintheta*x')*un) 
endfunction

function yd=grf_1(x)
  yd=-2/15*2*pi*diag(sintheta)*sin( sintheta*2*pi*x')
endfunction
 
function g=c_1(x)
//ss=cd(2);
//A=[-1,0,0,0,0,0;
//   1,-1,0,0,0,0;
//   0,1,-1,0,0,0;
//   0,0,1,-1,0,0;
//   0,0,0,1,-1,0;
//   0,0,0,0,1,-1;
//   0,0,0,0,0,1;];
  g=A*x+cd(2);g(7)=g(7)-3.5;
endfunction

function yd=grc_1(x)
  yd=A;
endfunction

