// linking the fsqp library 
//===================================================
if ~c_link('libcfsqp') then exec('../loader.sce') ; end 
exdir=get_absolute_file_path('myexample.sce');

modefsqp=111;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=0.e0;
bigbnd=1.e10; 
eps=1.e-8; 
epsneq=0.e0; 
udelta=0.e0;

rpar=[bigbnd,eps,epsneq,udelta];

// parametrisation of the box 
N=3
x1=linspace(-1,1,N);
x2=linspace(-1,1,N);
t1=x1'*ones(1,3); t1=t1(:);
t2=ones(3,1)*x2;t2=t2(:);
// vecteur colonne 
t=(1:(N*N))';

// example ou on cherche aussi a maximiser en sig
//=======================================================

getf(exdir+'/listutils.sci');

M=10;
SIG=20;

function y=f_1(x)
// y=- x(2).^2 - x(4).^2 
  y=- x(2).^2*x(4).^2 
endfunction

function y=gradf_1(x)
  y=[0,-2.0*x(2)*x(4).^2,0,-2.0*x(4)*x(2).^2]
endfunction

function y=f(x1,x2); y= x1.^2 + x2.^2 -3*x1.*x2 ;endfunction
function y=gradfx1(x1,x2); y= 2*x1-3*x2  ; endfunction
function y=gradfx2(x1,x2); y= 2*x2-3*x1  ; endfunction

function g=G_1(x)
  g= - f(x(1)+t1*x(2),x(3)+t2*x(4)) +M - SIG 
endfunction

function g=G_2(x)
  g= f(x(1)+t1*x(2),x(3)+t2*x(4)) - M  - SIG 
endfunction

function w=grG_1(x)
  w=[-gradfx1(x(1)+t1*x(2),x(3)+t2*x(4)),-gradfx1(x(1)+t1*x(2),x(3)+t2*x(4)).*t1,...
     -gradfx2(x(1)+t1*x(2),x(3)+t2*x(4)),-gradfx2(x(1)+t1*x(2),x(3)+t2*x(4)).*t2];
endfunction

function w=grG_2(x)
  w=- [-gradfx1(x(1)+t1*x(2),x(3)+t2*x(4)),-gradfx1(x(1)+t1*x(2),x(3)+t2*x(4)).*t1,...
       -gradfx2(x(1)+t1*x(2),x(3)+t2*x(4)),-gradfx2(x(1)+t1*x(2),x(3)+t2*x(4)).*t2];
endfunction

//f regular objective R-valued
//G_1 first SR constraint non linear inequality 
//G_2 2nd   SR constraint non linear inequality
//G_3 3rd   SR constraint non linear inequality

list_obj=list(list(f_1),list()); 

list_cntr=list(list(),list(G_1,G_2),list(),list(),list(),list());

list_grobj=list(list(gradf_1),list());

list_grcn=list(list(),list(grG_1,grG_2),list(),list(),list(),list());

x0=[3;1;4;1];

[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-7;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)

// x(2) et x(4) peuvent etre n�gatifs
x(2)= abs(x(2));
x(4)= abs(x(4));

xx=linspace(x(1)-3*x(2),x(1)+3*x(2),30);
yy=linspace(x(3)-3*x(4),x(3)+3*x(4),30);
zz=feval(xx,yy,f);
zz1=min(M+SIG,max(M-SIG,zz));
zz1(find(zz1==M+SIG))=%nan;
zz1(find(zz1==M-SIG))=%nan;
//plot3d(xx,yy,zz1);
xbasc(); contour(xx,yy,zz,[M+SIG,M-SIG]);

xrect(x(1)-x(2),x(3)+x(4),2*x(2),2*x(4));




