function fj=obj1(j,x)
  fj=x(10);
endfunction

function gradfj=grob1(j,x)
  gradfj=[0,0,0,0,0,0,0,0,0,1];
endfunction

//  r,z, t defined in example4.sce

function gj=cntr1(j,x)
if j<= r
  gj=(1-x(10))^2-z(t(j),x);
elseif j<= 2*r 
  gj=z(t(j),x)-(1+x(10))^2;
elseif j<= 3.5*r
  gj=z(t(j),x)-(x(10))^2;
end
endfunction

function w=grcn1(j,x)
if j<= r
  w=[-grz(t(j),x),-2*(1-x(10))]
elseif j<= 2*r
  w=[grz(t(j),x),-2*(1+x(10))]
elseif j<= 3.5*r
  w=[grz(t(j),x),-2*x(10)]
end
endfunction


function gj=cntr3(j,x)
//Scilab function which calls a C function
//See link('example4.o',....) in file example4.sce 
  gj=fort('cntr3',j,1,'i',x,2,'d','out',[1,1],3,'d');
endfunction

function fj=obj2(j,x)
  if x_is_new() then
    //    Common part  
    all_objectives=x(10);
    all_constraints=[(1-x(10))^2-z(t1,x);z(t2,x)-(1+x(10))^2;z(t3,x)-x(10)^2];
    all_gradobjectives=[0,0,0,0,0,0,0,0,0,1];
    all_gradconstraints=[[-grz(t1,x);grz(t2,x);grz(t3,x)],[-2*(1-x(10))*ones(t1);-2*(1+x(10))*ones(t2);-2*x(10)*ones(t3)]];
    //
    fj=all_objectives(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    fj=all_objectives(j);
  end
endfunction

function cj=cntr2(j,x)
  if x_is_new() then
    //    Common part
    all_objectives=x(10);
    all_constraints=[(1-x(10))^2-z(t1,x);z(t2,x)-(1+x(10))^2;z(t3,x)-x(10)^2];
    all_gradobjectives=[0,0,0,0,0,0,0,0,0,1];
    all_gradconstraints=[[-grz(t1,x);grz(t2,x);grz(t3,x)],[-2*(1-x(10))*ones(t1);-2*(1+x(10))*ones(t2);-2*x(10)*ones(t3)]];
    //
    cj=all_constraints(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    cj=all_constraints(j);
  end
endfunction

function gj=grob2(j,x)
  if x_is_new() then
    //    Common part
    all_objectives=x(10);
    all_constraints=[(1-x(10))^2-z(t1,x);z(t2,x)-(1+x(10))^2;z(t3,x)-x(10)^2];
    all_gradobjectives=[0,0,0,0,0,0,0,0,0,1];
    all_gradconstraints=[[-grz(t1,x);grz(t2,x);grz(t3,x)],[-2*(1-x(10))*ones(t1);-2*(1+x(10))*ones(t2);-2*x(10)*ones(t3)]];
    //
    gj=all_gradobjectives(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradobjectives(j,:);
  end
endfunction

function gj=grcn2(j,x)
  if x_is_new() then
    //    Common part
    all_objectives=x(10);
    all_constraints=[(1-x(10))^2-z(t1,x);z(t2,x)-(1+x(10))^2;z(t3,x)-x(10)^2];
    all_gradobjectives=[0,0,0,0,0,0,0,0,0,1];
    all_gradconstraints=[[-grz(t1,x);grz(t2,x);grz(t3,x)],[-2*(1-x(10))*ones(t1);-2*(1+x(10))*ones(t2);-2*x(10)*ones(t3)]];
    //
    gj=all_gradconstraints(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradconstraints(j,:);
  end
endfunction





