// This is the builder.sce 
// must be run from this directory 

//========================Ncfsqp.c================================
// we create Ncfsqp.c
// make some modification in cfsqp.c file (replace fprintf by sciprint)

ierr=execstr('txt=mgetl(''cfsqp.c'',-1)','errcatch');
if ierr==241 then
  write(%io(2),'In order to make this toolbox run') ;
  write(%io(2),'You need to get cfsqp.c file from Prof. Andre Tits');
  error('File cfsqp.c is missing, take a look at the readme file')
  exit
end

txt=strsubst(txt,'fprintf(io,','sciprint(');
txt=strsubst(txt,'fprintf(stderr,','sciprint(');
txt=strsubst(txt,'fprintf(glob_prnt.io,','sciprint(');
txt=strsubst(txt,'\n','\r\n');
mputl(txt,'Ncfsqp.c');

//========================builder for libcfsqp.so =================

ilib_name  = 'libcfsqp' ;		// interface library name 

// objects files 

files=['Ncfsqp';'cintfsqp2.o';'cintfsqp3.o';'qld.o'];

libs  = [] ;				// other libs needed for linking

// table of (scilab_name,interface-name) 
// for fortran coded interface use 'C2F(name)'

table1 =['fsqp'  	'cintfsqp2';
	'srfsqp'	'cintfsqp3';
	'qld' 	 	'cintfsqp4'];

table2 =['x_is_new'	'cintfsqp1';
	'set_x_is_new'	'cintfsqp0'];

table=list(table1,table2);

ldflags = "";
cflags ="";
fflags ="";

// do not modify below
// ----------------------------------------------
ilib_build(ilib_name,table,files,libs,'Makelib',ldflags,cflags,fflags)



