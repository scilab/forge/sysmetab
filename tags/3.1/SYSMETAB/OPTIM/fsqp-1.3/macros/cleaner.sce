mode(-1);
curdir=pwd()
chdir(get_absolute_file_path('cleaner.sce'))
tokeep=['allcntr.sci'
	'allgrcn.sci'
	'allgrob.sci'
	'allobj.sci'
	'cleaner.sce'
	'cntr.sci'
	'findparam.sci'
	'fsqp_dem.sci'
	'grcn.sci'
	'grob.sci'
	'obj.sci'];
files=listfiles('*')';
for k=tokeep',files(files==k)=[];end
for f=files,mdelete(f),end
chdir(curdir)
clear files f mdelete listfiles curdir tokeep
