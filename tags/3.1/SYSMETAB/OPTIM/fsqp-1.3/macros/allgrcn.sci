function ctp=allgrcn(x)
  ctp=[];
  for k=1:ng0;
    grg_k=null();
    grg_k=list_grcn(1)(k);
    ctp=[ctp;grg_k(x)];
  end
  for k=1:ncsrn
    grG_k=null();
    grG_k=list_grcn(2)(k);
    ctp=[ctp;grG_k(x)];
  end
  for k=1:nc0
    grc_k=null();
    grc_k=list_grcn(3)(k);
    ctp=[ctp;grc_k(x)];
  end
  for k=1:ncsrl
    grC_k=null();
    grC_k=list_grcn(4)(k);
    ctp=[ctp;grC_k(x)];
  end
  for k=1:nh0
    grh_k=null();
    grh_k=list_grcn(5)(k);
    ctp=[ctp;grh_k(x)];
  end
  for k=1:na0
    grA_k=null();
    grA_k=list_grcn(6)(k);
    ctp=[ctp;grA_k(x)];
  end

endfunction



