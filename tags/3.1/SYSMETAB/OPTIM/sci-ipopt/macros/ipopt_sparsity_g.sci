// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sparse_dg = ipopt_sparsity_g ( nv , nc )
  // Computes the sparsity pattern of the dense gradient of the constraints. 
  //
  // Calling Sequence
  //   sparse_dg = ipopt_sparsity_g ( nv , nc )
  //
  // Parameters
  //   nv : a 1 x 1 matrix of floating point integers, the number of variables
  //   nc : a 1 x 1 matrix of floating point integers, the number of constraints
  //   sparse_dg : a (nv*nc) x 2 matrix of floating point integers, the sparsity pattern
  //
  // Description
  //   This function should be used to fill the sparse_dg argument of the 
  //   ipopt solver.
  //
  // Examples
  //   // 2 variables, 5 constraints
  //   nv = 2
  //   nc = 5
  //   sparse_dg = ipopt_sparsity_g ( nv , nc )
  //   expected = [
  //     1    1  
  //     1    2
  //     2    1
  //     2    2
  //     3    1
  //     3    2
  //     4    1
  //     4    2
  //     5    1
  //     5    2
  //   ]
  //
  // Authors
  // Michael Baudin, DIGITEO, 2010

  for ic = 1 : nc
    for iv = 1 : nv
      k = iv + (ic-1)*nv
      sparse_dg(k,1:2) = [ic iv]
    end
  end
endfunction

