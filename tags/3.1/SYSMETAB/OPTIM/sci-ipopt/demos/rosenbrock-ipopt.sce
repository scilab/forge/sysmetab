// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// TODO : fix and complete this !

// Test IPOPT In Scilab on Rosenbrock

function f = rosenbrock_f ( x , x_new )
    f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
    plot(x(1),x(2),"bo")
endfunction

function df = rosenbrock_df ( x , x_new )
    df(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
    df(2) = 200. * ( x(2) - x(1)**2 )
endfunction

// Nonlinear inequality constraints
function g = rosenbrock_g ( x , x_new )
  g = x(1)^2 + x(2)^2 - 1.5;
endfunction

function dg = rosenbrock_dg ( x , x_new )
  dg(1) = 2 * x(1)
  dg(2) = 2 * x(2)
endfunction

function Hf = rosenbrock_Hf ( x , x_new )
  Hf = zeros(2,2)
  Hf(1,1) = diag(-400*x(2) + 1200*x(1).^2 + 2)
  Hf(2,2) = 200
  Hf = Hf - diag(400*x(1),1) - diag(400*x(1),-1)
endfunction

function Hg = rosenbrock_Hg ( x , x_new )
  Hg = [
    2 0
    0 2
    ];
endfunction

if ( %f ) then
// Check derivatives of F
x = [-1.9 2.0]'
[df1 , Hf1] = derivative( list(rosenbrock_f,%t),x,[],[],"blockmat" )
df2 = rosenbrock_df ( x )'
Hf2 = rosenbrock_Hf ( x )
norm(df1-df2)
norm(Hf1-Hf2)


// Check derivatives of G
x = [-1.9 2.0]';
[dg1 , Hg1] = derivative(list(rosenbrock_g,%t),x,[],[],"blockmat" )
dg2 = rosenbrock_dg ( x )'
Hg2 = rosenbrock_Hg ( x )
norm(dg1-dg2)
norm(Hg1-Hg2)

end


// Plot contours of F
function f = rosenbrock_fC ( x1 , x2 )
    x = [x1 x2]
    f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
x = linspace ( -2 , 2 , 100 );
y = linspace ( -1 , 3 , 100 );
contour ( x , y , rosenbrock_fC , [1 10 100 500 1000] )

// Plot contours of G
function g = rosenbrock_gC ( x1 , x2 )
  x = [x1 x2]
  g = x(1)^2 + x(2)^2 - 1.5;
endfunction
x = linspace ( -2 , 2 , 100 );
y = linspace ( -1 , 3 , 100 );
contour ( x , y , rosenbrock_gC , [0 1 5 10 20] )

// The sparsity structure of the constraints
sparse_dg = [
  1 1
  1 2
];

// The Hessian of the Lagrangian
function y = rosenbrock_hessian ( x , lambda , obj_weight , x_new , lambda_new )
  Hf = rosenbrock_Hf ( x )
  Hg = rosenbrock_Hg ( x )
  y = obj_weight * Hf + lambda(1) * Hg
endfunction

// The sparsity structure of the Lagrangian
sparse_dh = [
  1 1
  1 2
  2 1
  2 2
];

upper = [%inf %inf]';
lower = [-%inf -%inf]';
// Not Feasible starting point
x0  = [-1.9 2.0]' 
nb_constr = 1;
var_lin_type = [1 1]'; // Non-Linear
constr_lin_type = [1 1]'; // Non-Linear
constr_rhs = [0 0]';
constr_lhs = [-%inf -%inf]';

////////////////////////////////////////////////////////////////////////

params = init_param();
// We use the given Hessian
params = add_param(params,"hessian_approximation","exact");
// We use a limited-bfgs approximation for the Hessian.
//params = add_param(params,"hessian_approximation","limited-memory");

[x_sol, f_sol, extra] = ipopt(x0, rosenbrock_f, rosenbrock_df, ..
  rosenbrock_g, rosenbrock_dg, sparse_dg, rosenbrock_hessian, sparse_dh, var_lin_type, ..
  constr_lin_type, constr_rhs, constr_lhs, lower, upper, params);

// Expected solution : 0.9072,0.8228

extra("it_count")
extra("fobj_eval")

