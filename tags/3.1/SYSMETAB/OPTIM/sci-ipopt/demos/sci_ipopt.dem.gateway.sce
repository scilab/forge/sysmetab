// ====================================================================
// Copyright Yann COLLETTE 2009
// This file is released into the public domain
// ====================================================================
demopath = get_absolute_file_path("sci_ipopt.dem.gateway.sce");

subdemolist = ["demo of ipopt", "ipopt_demo.sce"];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
