;##############################################################################################################
; Inno Setup Install script for Scicoinor Toolbox
; http://www.jrsoftware.org/isinfo.php
; Yann COLLETTE
; This file is released into the public domain
;##############################################################################################################
; modify this path where is toolbox_skeleton directory
#define BinariesSourcePath "e:\toolboxes\sci-ipopt"
;
#define SCIIPOPT_Toolbox_version "1.0"
#define CurrentYear "2010"
#define SCIIPOPT_ToolboxDirFilename "sci_ipopt_1.0"
;##############################################################################################################
[Setup]
; Debut Donn�es de base � renseigner suivant version
SourceDir={#BinariesSourcePath}
AppName=SciIPOpt Toolbox
AppVerName=SciIPOOpt Toolbox version 1.0
DefaultDirName={pf}/{#SCIIPOPT_ToolboxDirFilename}
InfoAfterfile=readme.txt
LicenseFile=license.txt
WindowVisible=true
AppPublisher=Home
BackColorDirection=lefttoright
AppCopyright=Copyright � {#CurrentYear}
Compression=lzma/max
InternalCompressLevel=normal
SolidCompression=true
VersionInfoVersion={#SCIIPOPT_Toolbox_version}
VersionInfoCompany=Home
;##############################################################################################################
[Files]
; Add here files that you want to add
Source: loader.sce; DestDir: {app}
Source: license.txt; DestDir: {app}
Source: readme.txt; DestDir: {app}
Source: changelog.txt; DestDir: {app}
Source: etc\sci_ipopt.quit; DestDir: {app}\etc
Source: etc\sci_ipopt.start; DestDir: {app}\etc
Source: macros\buildmacros.sce; DestDir: {app}\macros
Source: macros\lib; DestDir: {app}\macros
Source: macros\names; DestDir: {app}\macros
Source: macros\*.sci; DestDir: {app}\macros
Source: macros\*.bin; DestDir: {app}\macros
Source: sci_gateway\loader_gateway.sce; DestDir: {app}\sci_gateway
Source: sci_gateway\cpp\loader.sce; DestDir: {app}\sci_gateway\cpp
Source: sci_gateway\cpp\sci_ipopt.dll; DestDir: {app}\sci_gateway\cpp
Source: demos\*.*; DestDir: {app}\demos
Source: jar\*.*; DestDir: {app}\jar
Source: tests\unit_tests\*.*; DestDir: {app}\tests\unit_tests
; Source: tests\nonreg_tests\*.*; DestDir: {app}\tests\nonreg_tests
;
;##############################################################################################################

