mode(-1)
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("Example2.sce");
  methods=['Scilab Code','C code with fsqp','C code with srfsqp',..
	   'Using lists']
sz=[427 363];
fsqp_demo_gui(demopath,'example2',sz,methods)
