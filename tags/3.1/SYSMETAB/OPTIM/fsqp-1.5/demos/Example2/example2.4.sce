 mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 2 of the fsqp documentation


//[2] fsqp with  lists 
//=======================================================
//f_1 returns all the objectives (vector with 163 componenets)
function f=f_1(x)
//Evaluate all objectives in vector f
  pi=cd(1);
  f=1/15+2/15*(cos(7*pi*sintheta)+cos(2*pi*sintheta*x')*un) 
endfunction

//the associated gradient
function yd=grf_1(x),
  yd=-2/15*2*pi*diag(sintheta)*sin( sintheta*2*pi*x')
endfunction


//c_1 returns all the constraints in a vector with 7 components.
function g=c_1(x)
  g=A*x+cd(2);g(7)=g(7)-3.5;
endfunction
//the associated gradient
function yd=grc_1(x)
  yd=A;
endfunction




// Variables of the problem:
I=(1:163)';
sintheta=sin(%pi*(8.5+I*0.5)/180);
un=ones(6,1);
cd=[%pi,0.425];

pi=cd(1);ss=cd(2);
A=[-1,0,0,0,0,0;
   1,-1,0,0,0,0;
   0,1,-1,0,0,0;
   0,0,1,-1,0,0;
   0,0,0,1,-1,0;
   0,0,0,0,1,-1;
   0,0,0,0,0,1;];


// list of objectives (one objective considerd as regular or SR).
list_obj=list(list(),list(f_1))    //0 regular objective, 1 SR-objective (f_1)
//Use list_obj=list(list(f_1),list())  to consider f_1 as a set of 163 regular objs.

//list of constraints  (one linear SR inequality constraint R^7 valued)
list_cntr=list(list(),list(),list(c_1),list(),list(),list())

list_grobj=list(list(),list(grf_1))
//list_grobj=list(list(grf_1),list())

list_grcn=list(list(),list(),list(grc_1),list(),list(),list())

x0=[0.5;1;1.5;2;2.5;3]; 

//fsqp parameters obtained by findparam:
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=111;iprint=0;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-8;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

mprintf('%s\n',['---------------------------MinMax problem with srfsqp(list)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)
		'   Value of the objective function: '+sci2exp(max(f_1(x0)),0)
		'   Value of the constraints :       '+sci2exp(c_1(x0),0)])
timer();
x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)
t=timer();
mprintf('%s\n',['Final values'
		'   Value of the solution:           '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(max(f_1(x)),0)
		'   Value of the constraints :       '+sci2exp(c_1(x),0)
		'   Time :                           '+sci2exp(t,0)
	       ])




