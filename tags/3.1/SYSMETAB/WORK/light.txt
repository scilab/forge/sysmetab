re1 : 
re2 : 
re3 : Hexokinase
re4 : 
re5 : 
re6 : Gluconate Kinase
re7 : Glucose-6-phosphate Déshydrogénase
re8 : Glucose déshydrogénase
re9_f : Phosphoglucose Isomérase (forward)
re9_b : Phosphoglucose Isomérase (backward)
re16 : 6-phosphogluconate Déshydrogénase
re18 : Phosphofructo Kinase
re19 : Fructose 1,6 BisPhosphatase
re26 : Pyruvate Kinase
re30 : Pyruvate Déshydrogénase
re31 : PHB synthase
re34 : a-cétoglutarate déshydrogénase
re38_f : Malate déshydrogénase (forward)
re38_b : Malate déshydrogénase (backward)
re39 : Citrate Synthase
re40_f : Pyruvate Carboxylase (forward)
re40_b : Pyruvate Carboxylase (backward)
re41_f : Enzyme Malique-NAD(P)+ dépendante (forward)
re41_b : Enzyme Malique-NAD(P)+ dépendante (backward)
re44 : 
re45 : 
re47 : 
re48 : 
re49 : 
re50 : 
re51 : 
re52 : 
re53 : 
re54 : 
re56 : 
re57 : 
re58 : Entner-Doudoroff
re59 : 
re60_f : Transcétolase 1 (forward)
re60_b : Transcétolase 1 (backward)
re61_f : Transcétolase 2 (forward)
re61_b : Transcétolase 2 (backward)
re62_f : Transaldolase (forward)
re62_b : Transaldolase (backward)
re63_f : Fructose-1,6-diphosphate Aldolase (forward)
re63_b : Fructose-1,6-diphosphate Aldolase (backward)
re64_f : PhosphoGlycérate Kinase (forward)
re64_b : PhosphoGlycérate Kinase (backward)
re65_f : Enolase (forward)
re65_b : Enolase (backward)
re66 : Isocitrate Déshydrogénase
re67_f : Fumarase 1 (forward)
re67_b : Fumarase 1 (backward)
re68_f : Fumarase 2 (forward)
re68_b : Fumarase 2 (backward)

s1 : Glucose (6 carbones) : input metabolite

s2 : Glucose6P (6 carbones)
0=-re7-re9_f-re45-re56-re57+re3+re9_b
d(s2_1)/dt=-re7*s2_1-re9_f*s2_1-re45*s2_1-re56*s2_1-re57*s2_1+re3*s77_1+re9_b*s5_1
d(s2_2)/dt=-re7*s2_2-re9_f*s2_2-re45*s2_2-re56*s2_2-re57*s2_2+re3*s77_2+re9_b*s5_2
d(s2_3)/dt=-re7*s2_3-re9_f*s2_3-re45*s2_3-re56*s2_3-re57*s2_3+re3*s77_3+re9_b*s5_3
d(s2_4)/dt=-re7*s2_4-re9_f*s2_4-re45*s2_4-re56*s2_4-re57*s2_4+re3*s77_4+re9_b*s5_4
d(s2_5)/dt=-re7*s2_5-re9_f*s2_5-re45*s2_5-re56*s2_5-re57*s2_5+re3*s77_5+re9_b*s5_5
d(s2_6)/dt=-re7*s2_6-re9_f*s2_6-re45*s2_6-re56*s2_6-re57*s2_6+re3*s77_6+re9_b*s5_6

s3 : Gluconate (6 carbones) : output metabolite
d(s3_1)/dt=+re5*s73_1
d(s3_2)/dt=+re5*s73_2
d(s3_3)/dt=+re5*s73_3
d(s3_4)/dt=+re5*s73_4
d(s3_5)/dt=+re5*s73_5
d(s3_6)/dt=+re5*s73_6

s4 : 6PGluconate (6 carbones)
0=-re16-re58+re6+re7
d(s4_1)/dt=-re16*s4_1-re58*s4_1+re6*s78_1+re7*s2_1
d(s4_2)/dt=-re16*s4_2-re58*s4_2+re6*s78_2+re7*s2_2
d(s4_3)/dt=-re16*s4_3-re58*s4_3+re6*s78_3+re7*s2_3
d(s4_4)/dt=-re16*s4_4-re58*s4_4+re6*s78_4+re7*s2_4
d(s4_5)/dt=-re16*s4_5-re58*s4_5+re6*s78_5+re7*s2_5
d(s4_6)/dt=-re16*s4_6-re58*s4_6+re6*s78_6+re7*s2_6

s5 : Fructose6P (6 carbones)
0=-re9_b-re18-re44-re61_b-re62_b+re9_f+re19+re61_f+re62_f
d(s5_1)/dt=-re9_b*s5_1-re18*s5_1-re44*s5_1-re61_b*s5_1-re62_b*s5_1+re9_f*s2_1+re19*s28_1+re61_f*s14_1+re62_f*s19_1
d(s5_2)/dt=-re9_b*s5_2-re18*s5_2-re44*s5_2-re61_b*s5_2-re62_b*s5_2+re9_f*s2_2+re19*s28_2+re61_f*s14_2+re62_f*s19_2
d(s5_3)/dt=-re9_b*s5_3-re18*s5_3-re44*s5_3-re61_b*s5_3-re62_b*s5_3+re9_f*s2_3+re19*s28_3+re61_f*s25_1+re62_f*s19_3
d(s5_4)/dt=-re9_b*s5_4-re18*s5_4-re44*s5_4-re61_b*s5_4-re62_b*s5_4+re9_f*s2_4+re19*s28_4+re61_f*s25_2+re62_f*s21_1
d(s5_5)/dt=-re9_b*s5_5-re18*s5_5-re44*s5_5-re61_b*s5_5-re62_b*s5_5+re9_f*s2_5+re19*s28_5+re61_f*s25_3+re62_f*s21_2
d(s5_6)/dt=-re9_b*s5_6-re18*s5_6-re44*s5_6-re61_b*s5_6-re62_b*s5_6+re9_f*s2_6+re19*s28_6+re61_f*s25_4+re62_f*s21_3

s14 : Ribulose5P (5 carbones)
0=-re59-0.5*re60_f-re61_f+re16+re60_b+re61_b
d(s14_1)/dt=-re59*s14_1-re60_f*(s14_1)^2-re61_f*s14_1+re16*s4_2+re60_b*s19_1+re61_b*s5_1
d(s14_2)/dt=-re59*s14_2-re60_f*(s14_2)^2-re61_f*s14_2+re16*s4_3+re60_b*s19_2+re61_b*s5_2
d(s14_3)/dt=-re59*s14_3-re60_f*(s14_3)^2-re61_f*s14_3+re16*s4_4+re60_b*s21_1+re61_b*s21_1
d(s14_4)/dt=-re59*s14_4-re60_f*(s14_4)^2-re61_f*s14_4+re16*s4_5+re60_b*s21_2+re61_b*s21_2
d(s14_5)/dt=-re59*s14_5-re60_f*(s14_5)^2-re61_f*s14_5+re16*s4_6+re60_b*s21_3+re61_b*s21_3

s19 : Sedohept7P (7 carbones)
0=-re60_b-re62_f+re60_f+re62_b
d(s19_1)/dt=-re60_b*s19_1-re62_f*s19_1+re60_f*s14_1+re62_b*s5_1
d(s19_2)/dt=-re60_b*s19_2-re62_f*s19_2+re60_f*s14_2+re62_b*s5_2
d(s19_3)/dt=-re60_b*s19_3-re62_f*s19_3+re60_f*s14_1+re62_b*s5_3
d(s19_4)/dt=-re60_b*s19_4-re62_f*s19_4+re60_f*s14_2+re62_b*s25_1
d(s19_5)/dt=-re60_b*s19_5-re62_f*s19_5+re60_f*s14_3+re62_b*s25_2
d(s19_6)/dt=-re60_b*s19_6-re62_f*s19_6+re60_f*s14_4+re62_b*s25_3
d(s19_7)/dt=-re60_b*s19_7-re62_f*s19_7+re60_f*s14_5+re62_b*s25_4

s21 : GA3P (3 carbones)
0=-re48-re60_b-re61_b-re62_f-0.5*re63_b-re64_f+re58+re60_f+re61_f+re62_b+re63_f+re64_b
d(s21_1)/dt=-re48*s21_1-re60_b*s21_1-re61_b*s21_1-re62_f*s21_1-re63_b*(s21_1)^2-re64_f*s21_1+re58*s4_4+re60_f*s14_3+re61_f*s14_3+re62_b*s5_4+re63_f*s28_3+re64_b*s35_1
d(s21_2)/dt=-re48*s21_2-re60_b*s21_2-re61_b*s21_2-re62_f*s21_2-re63_b*(s21_2)^2-re64_f*s21_2+re58*s4_5+re60_f*s14_4+re61_f*s14_4+re62_b*s5_5+re63_f*s28_2+re64_b*s35_2
d(s21_3)/dt=-re48*s21_3-re60_b*s21_3-re61_b*s21_3-re62_f*s21_3-re63_b*(s21_3)^2-re64_f*s21_3+re58*s4_6+re60_f*s14_5+re61_f*s14_5+re62_b*s5_6+re63_f*s28_1+re64_b*s35_3

s25 : Erythrose4P (4 carbones)
0=-re47-re61_f-re62_b+re61_b+re62_f
d(s25_1)/dt=-re47*s25_1-re61_f*s25_1-re62_b*s25_1+re61_b*s5_3+re62_f*s19_4
d(s25_2)/dt=-re47*s25_2-re61_f*s25_2-re62_b*s25_2+re61_b*s5_4+re62_f*s19_5
d(s25_3)/dt=-re47*s25_3-re61_f*s25_3-re62_b*s25_3+re61_b*s5_5+re62_f*s19_6
d(s25_4)/dt=-re47*s25_4-re61_f*s25_4-re62_b*s25_4+re61_b*s5_6+re62_f*s19_7

s28 : Fruc1,6biP (6 carbones)
0=-re19-re63_f+re18+re63_b
d(s28_1)/dt=-re19*s28_1-re63_f*s28_1+re18*s5_1+re63_b*s21_3
d(s28_2)/dt=-re19*s28_2-re63_f*s28_2+re18*s5_2+re63_b*s21_2
d(s28_3)/dt=-re19*s28_3-re63_f*s28_3+re18*s5_3+re63_b*s21_1
d(s28_4)/dt=-re19*s28_4-re63_f*s28_4+re18*s5_4+re63_b*s21_1
d(s28_5)/dt=-re19*s28_5-re63_f*s28_5+re18*s5_5+re63_b*s21_2
d(s28_6)/dt=-re19*s28_6-re63_f*s28_6+re18*s5_6+re63_b*s21_3

s35 : 3Pglycérate (3 carbones)
0=-re49-re64_b-re65_f+re64_f+re65_b
d(s35_1)/dt=-re49*s35_1-re64_b*s35_1-re65_f*s35_1+re64_f*s21_1+re65_b*s37_1
d(s35_2)/dt=-re49*s35_2-re64_b*s35_2-re65_f*s35_2+re64_f*s21_2+re65_b*s37_2
d(s35_3)/dt=-re49*s35_3-re64_b*s35_3-re65_f*s35_3+re64_f*s21_3+re65_b*s37_3

s37 : PEP (3 carbones)
0=-re26-re50-re65_b+re65_f
d(s37_1)/dt=-re26*s37_1-re50*s37_1-re65_b*s37_1+re65_f*s35_1
d(s37_2)/dt=-re26*s37_2-re50*s37_2-re65_b*s37_2+re65_f*s35_2
d(s37_3)/dt=-re26*s37_3-re50*s37_3-re65_b*s37_3+re65_f*s35_3

s38 : Pyruvate (3 carbones)
0=-re30-re40_f-re41_b-re51+re26+re40_b+re41_f+re58
d(s38_1)/dt=-re30*s38_1-re40_f*s38_1-re41_b*s38_1-re51*s38_1+re26*s37_1+re40_b*s57_1+re41_f*s56_1+re58*s4_1
d(s38_2)/dt=-re30*s38_2-re40_f*s38_2-re41_b*s38_2-re51*s38_2+re26*s37_2+re40_b*s57_2+re41_f*s56_2+re58*s4_2
d(s38_3)/dt=-re30*s38_3-re40_f*s38_3-re41_b*s38_3-re51*s38_3+re26*s37_3+re40_b*s57_3+re41_f*s56_3+re58*s4_3

s47 : Acétyl-CoA (2 carbones)
0=-0.5*re31-re39-re52+re30
d(s47_1)/dt=-re31*(s47_1)^2-re39*s47_1-re52*s47_1+re30*s38_2
d(s47_2)/dt=-re31*(s47_2)^2-re39*s47_2-re52*s47_2+re30*s38_3

s48 : PHB (4 carbones) : output metabolite
d(s48_1)/dt=+re31*s47_1
d(s48_2)/dt=+re31*s47_2
d(s48_3)/dt=+re31*s47_1
d(s48_4)/dt=+re31*s47_2

s50 : Citrate (6 carbones)
0=-re66+re39
d(s50_1)/dt=-re66*s50_1+re39*s57_4
d(s50_2)/dt=-re66*s50_2+re39*s57_3
d(s50_3)/dt=-re66*s50_3+re39*s57_2
d(s50_4)/dt=-re66*s50_4+re39*s57_1
d(s50_5)/dt=-re66*s50_5+re39*s47_2
d(s50_6)/dt=-re66*s50_6+re39*s47_1

s52 : aKG (5 carbones)
0=-re34-re54+re66
d(s52_1)/dt=-re34*s52_1-re54*s52_1+re66*s50_1
d(s52_2)/dt=-re34*s52_2-re54*s52_2+re66*s50_2
d(s52_3)/dt=-re34*s52_3-re54*s52_3+re66*s50_3
d(s52_4)/dt=-re34*s52_4-re54*s52_4+re66*s50_5
d(s52_5)/dt=-re34*s52_5-re54*s52_5+re66*s50_6

s53 : Succinyl-CoA (4 carbones)
0=-re67_f-re68_f+re34+re67_b+re68_b
d(s53_1)/dt=-re67_f*s53_1-re68_f*s53_1+re34*s52_2+re67_b*s56_1+re68_b*s56_4
d(s53_2)/dt=-re67_f*s53_2-re68_f*s53_2+re34*s52_3+re67_b*s56_2+re68_b*s56_3
d(s53_3)/dt=-re67_f*s53_3-re68_f*s53_3+re34*s52_4+re67_b*s56_3+re68_b*s56_2
d(s53_4)/dt=-re67_f*s53_4-re68_f*s53_4+re34*s52_5+re67_b*s56_4+re68_b*s56_1

s56 : Malate (4 carbones)
0=-re38_f-re41_f-re67_b-re68_b+re38_b+re41_b+re67_f+re68_f
d(s56_1)/dt=-re38_f*s56_1-re41_f*s56_1-re67_b*s56_1-re68_b*s56_1+re38_b*s57_1+re41_b*s38_1+re67_f*s53_1+re68_f*s53_4
d(s56_2)/dt=-re38_f*s56_2-re41_f*s56_2-re67_b*s56_2-re68_b*s56_2+re38_b*s57_2+re41_b*s38_2+re67_f*s53_2+re68_f*s53_3
d(s56_3)/dt=-re38_f*s56_3-re41_f*s56_3-re67_b*s56_3-re68_b*s56_3+re38_b*s57_3+re41_b*s38_3+re67_f*s53_3+re68_f*s53_2
d(s56_4)/dt=-re38_f*s56_4-re41_f*s56_4-re67_b*s56_4-re68_b*s56_4+re38_b*s57_4+re41_b*s79_1+re67_f*s53_4+re68_f*s53_1

s57 : OAA (4 carbones)
0=-re38_b-re39-re40_b-re53+re38_f+re40_f
d(s57_1)/dt=-re38_b*s57_1-re39*s57_1-re40_b*s57_1-re53*s57_1+re38_f*s56_1+re40_f*s38_1
d(s57_2)/dt=-re38_b*s57_2-re39*s57_2-re40_b*s57_2-re53*s57_2+re38_f*s56_2+re40_f*s38_2
d(s57_3)/dt=-re38_b*s57_3-re39*s57_3-re40_b*s57_3-re53*s57_3+re38_f*s56_3+re40_f*s38_3
d(s57_4)/dt=-re38_b*s57_4-re39*s57_4-re40_b*s57_4-re53*s57_4+re38_f*s56_4+re40_f*s79_1

s72 : Glucose (6 carbones)
0=-re2-re8+re1
d(s72_1)/dt=-re2*s72_1-re8*s72_1+re1*s1_1
d(s72_2)/dt=-re2*s72_2-re8*s72_2+re1*s1_2
d(s72_3)/dt=-re2*s72_3-re8*s72_3+re1*s1_3
d(s72_4)/dt=-re2*s72_4-re8*s72_4+re1*s1_4
d(s72_5)/dt=-re2*s72_5-re8*s72_5+re1*s1_5
d(s72_6)/dt=-re2*s72_6-re8*s72_6+re1*s1_6

s73 : Gluconate (6 carbones)
0=-re4-re5+re8
d(s73_1)/dt=-re4*s73_1-re5*s73_1+re8*s72_1
d(s73_2)/dt=-re4*s73_2-re5*s73_2+re8*s72_2
d(s73_3)/dt=-re4*s73_3-re5*s73_3+re8*s72_3
d(s73_4)/dt=-re4*s73_4-re5*s73_4+re8*s72_4
d(s73_5)/dt=-re4*s73_5-re5*s73_5+re8*s72_5
d(s73_6)/dt=-re4*s73_6-re5*s73_6+re8*s72_6

s77 : Glucose (6 carbones)
0=-re3+re2
d(s77_1)/dt=-re3*s77_1+re2*s72_1
d(s77_2)/dt=-re3*s77_2+re2*s72_2
d(s77_3)/dt=-re3*s77_3+re2*s72_3
d(s77_4)/dt=-re3*s77_4+re2*s72_4
d(s77_5)/dt=-re3*s77_5+re2*s72_5
d(s77_6)/dt=-re3*s77_6+re2*s72_6

s78 : Gluconate (6 carbones)
0=-re6+re4
d(s78_1)/dt=-re6*s78_1+re4*s73_1
d(s78_2)/dt=-re6*s78_2+re4*s73_2
d(s78_3)/dt=-re6*s78_3+re4*s73_3
d(s78_4)/dt=-re6*s78_4+re4*s73_4
d(s78_5)/dt=-re6*s78_5+re4*s73_5
d(s78_6)/dt=-re6*s78_6+re4*s73_6

s79 : CO2 (1 carbone)
0=-re40_f-re41_b+re16+re30+re34+re40_b+re41_f+re66
d(s79_1)/dt=-re40_f*s79_1-re41_b*s79_1+re16*s4_1+re30*s38_1+re34*s52_1+re40_b*s57_4+re41_f*s56_4+re66*s50_4
