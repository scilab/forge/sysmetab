function a=s_full(ij,v,dims)
	a=full(sparse(ij,v,dims));
endfunction

function [V,v0,ff]=freefluxes(A,b)
// D�termination des "free fluxes"
	V=[];
	v0=[];
	ff=[];
	n=size(A,2);
	n1=size(A,1);
	if rank(A)~=n1
    	xmllab_error(sprintf('Attention : %d contrainte(s) redondante(s)',n1-rank(A)))		
	end
    ierr=execstr('v0=linpro(zeros(n,1),A,b,zeros(n,1),%inf*ones(n,1),n1)','errcatch');
 	if ierr~=0
	    xmllab_error('Pas de flux admissible, les flux impos�s sont incoh�rents.')
    else
		// On utilise la factorisation LU de A' : E*A'=LU avec E de taille (n,n),
		// L de taille (n,n1) et U de taille (n1,n1). On utilise ici le fait que
		// la permutation E permet de garantir que la sous matrice principale (n1,n1) 
		// de E*A' est de rang plein. On a donc, en posant B=A*E'
		//
		// Av=0 <==> B*E*v=0 <==> [B1 B2]*[E1*v;E2*v]=0 <==> B1*E1*v = -B2*E2*v
		// 
		// si on ajoute � ceci l'�quation triviale E2*v=E2*v, on obtient
		//
		// Av=0 <==> [B1*E1;E2]*v = [B2;I]*E2*v <==> v = inv([B1*E1;E2])*[B2;I]*E2*v
		//
		// ce qui donne la relation entre les flux et les free fluxes, repr�sent�s 
		// ici par E2*v
		//
		// Ceci fonctionne aussi quand A n'est pas de rang plein, mais cela n'a d'int�ret
		// que quand b est dans l'image de A, ce qui est v�rifi� plus haut lors de la r�solution
		// du programme lin�aire. 
		//
		
		[L,U,E]=lu(A');
		n1=rank(A);
		n2=size(A,2)-n1;
		E1=E(1:n1,:);
		E2=E(n1+1:$,:);
		[s,i]=sort(-E2*(1:n1+n2)'); // On remet les free fluxes dans l'ordre.
		ff=-s;
		E2=E2(i,:);
		E2=E(n1+1:$,:);
		C1=[A*E1'*E1;E2];
		C2=[-A*E2';eye(n2,n2)];
		V=clean(C1\C2);
		v0=clean(v0);
	end
endfunction

function [varargout]=admissibilityTest(w)
    [A,b]=fluxSubspace(w);
	V=kernel(A);
	v0=A\b;
    n=length(v0);
    ierr=execstr('[x,lagr,f]=linpro(-V''*zeros(n,1),-V,v0)','errcatch');
    if ierr~=0
        xmllab_error('Pas de flux admissible, les flux connus sont incoh�rents.')
	    v=zeros(n,1);
    else
	    v=clean(V*x+v0);
    end
   if argn(0)==1
	varargout(1)=v;
   else	
    for i=1:length(v)
        varargout(i)=v(i);
    end
   end
endfunction

function v=optimize()

	global x0_restart

	// Calcul d'un premier param�trage en utilisant les free fluxes. Pas 
	// forc�ment optimal num�riquement car V n'est pas orthogonale. De ce calcul
	// on ne va conserver que v0.

	[V,v0,ff]=freefluxes(A,b);

	 // On calcule diff�remment la base, pour qu'elle soit orthogonale, en pratique, cela permet
	 // une bien meilleure convergence pour les cas ou les fluxs ne sont pas identifiables et
	 // que l'on prend un petit param�tre de r�gularisation.

//	V=clean(kernel(A));

	// On identifie les contraintes d'�galit� a posteriori, car certaines peuvent apparaitre
	// en plus de celles sp�cifi�es a priori, par exemple lorsque l'on impose tous les fluxs
	// ext�rieurs (entrants et sortants) sauf un.

	eq_constr=find(clean(sum(V,'col'))==0);

	// Pareil pour les contraintes d'in�galit�, les autres donc.

	ineq_constr=find(clean(sum(V,'col'))~=0);
	
	// Tout cela ne change pas la dimension effective de l'espace dans lequel
	// va avoir lieu l'optimisation :
	
	n=size(V,2);
	
	// Pr�paration des param�tres pour l'appel � fsqp.
	
	nf=1;
	nineqn=0; 	
	nineq=size(V,1)-length(eq_constr); // nombre de contraintes d'in�galit�
	neqn=0; 
	neq=0; // nombre de contraintes d'in�galit�

	modefsqp=100; // faire un help fsqp pour en savoir plus !

	// Ces param�tres sont r�cup�r�s � partir de l'interface xmllab :
	//
	//	miter
	//	epsgrad 
	//	itmax

	iprint=1;
	ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
	bigbnd=1.e10; 


	epsneq=0.e0; udelta=0.e0;
	rpar=[bigbnd,epsgrad,epsneq,udelta];
	
	// Bornes inf/sup sur les variables. Ici, il n'y en a pas, elles
	// sont contenues dans les contraintes d'in�galit�.
	
	bl=-bigbnd*ones(n,1);
	bu=+bigbnd*ones(n,1);
		
	_inform=1;
	nit=0;

	// On boucle tant que l'optimiseur n'a pas termin� normalement

	if x0_restart~=[]
		x0=x0_restart;
	else
		x0=rand(n,1);
	end		

	while _inform & (nit<itmax)
		[x,_inform,f,g,_lambda]=fsqp(x0,ipar,rpar,[bl bu],_obj,_cntr,_grobj,_grcntr);
		nit=nit+1;
		if _inform
			x0=rand(n,1)
		end
	end	
	if _inform==0
		xmllab_error(sprintf('Convergence avec norme de la fonction cout = %f',f))
		v=clean(V*x+v0);
		x0_restart=x0;
	else
		xmllab_error(sprintf('Pas de convergence apr�s %d tirages de conditions initiales.',itmax))
		v=0*v0;
		x0_restart=[];
	end	
				
endfunction

function oj=_obj(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*x'*x;
		all_grobj=V'*grad'+epsilon*x;
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		oj=all_obj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		oj=all_obj;
	end
endfunction

function goj=_grobj(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*x'*x;
		all_grobj=V'*grad'+epsilon*x;
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		goj=all_grobj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		goj=all_grobj;
	end
endfunction

function cj=_cntr(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*x'*x;
		all_grobj=V'*grad'+epsilon*x;
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		cj=all_cntr(j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		cj=all_cntr(j);
	end
endfunction

function gcj=_grcntr(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*x'*x;
		all_grobj=V'*grad'+epsilon*x;
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		gcj=all_grcntr(:,j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		gcj=all_grcntr(:,j);
	end
endfunction

function _gdf=_grobjdf(j,x)
	_n=length(x);
	_h=1e-5;
	_gdf=zeros(_n,1);
	for i=1:length(x)
		_e=zeros(_n,1);
		_e(i)=_h;
		set_x_is_new(1);
		objh1=_obj(j,x+_e);
		set_x_is_new(1);
		objh2=_obj(j,x-_e);
		_gdf(i)=(objh1-objh2)/_h/2;
	end
endfunction

function _gcdf=_grcntrdf(j,x)
	_n=length(x);
	_h=1e-5;
	_gcdf=zeros(_n,1);
	for i=1:length(x)
		_e=zeros(_n,1);
		_e(i)=_h;
		set_x_is_new(1);
		gcjh1=_cntr(j,x+_e);
		set_x_is_new(1);
		gcjh2=_cntr(j,x-_e);
		_gcdf(i)=(gcjh1-gcjh2)/_h/2;
	end
endfunction


rand('seed',getdate('s'))
