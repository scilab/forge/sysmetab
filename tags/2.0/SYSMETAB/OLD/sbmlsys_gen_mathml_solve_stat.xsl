<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer 

    1 - une fonction Scilab permettant
    calculer les diff�rentes fractions des cumom�res en fonction des flux
    et des fractions des cumom�res des entr�es. 
	On g�n�re aussi le code des blocs non nuls de la jacobienne par
	rapport � x.
	
	
	Exemple pour le "branching network"
    du papier "part II" de Wiechert :
    
function [x1,x2]=solveCumomers(x_input,v)
    n1=3;
    M1=zeros(n1,n1);
    b1=zeros(n1,1);
    // Cumom�res de poids 1
    b1(1)=b1(1)+v(2)*x_input(2);
    b1(1)=b1(1)+v(2)*x_input(1);
    M1(1,1)=-(v(4)+v(4));
    b1(2)=b1(2)+v(1)*x_input(1);
    b1(2)=b1(2)+v(3)*x_input(2);
    M1(2,1)=v(4);
    M1(2,2)=-v(5);
    b1(3)=b1(3)+v(1)*x_input(2);
    b1(3)=b1(3)+v(3)*x_input(1);
    M1(3,1)=v(4);
    M1(3,3)=-v(5);
    x1=clean(-M1\b1);
    n2=1;
    M2=zeros(n2,n2);
    b2=zeros(n2,1);
    J2_1=zeros(n2,n1);
    // Cumom�res de poids 2
    b2(1)=b2(1)+v(1)*x_input(3);
    b2(1)=b2(1)+v(3)*x_input(3);
    b2(1)=b2(1)+v(4)*x1(1)*x1(1);
    J2_1(1,1)=J2_1(1,1)+x1(1)*v(4);
    J2_1(1,1)=J2_1(1,1)+x1(1)*v(4);
    M2(1,1)=-v(5);
    x2=clean(-M2\b2);
endfunction


    Ici on fait abstraction des identificateurs originaux des cumom�res et des flux.
    
    Ici on g�n�re en fait cette fonction d�crite en MathML, il faut enchainer avec
    une derni�re transformation pour avoir le code Scilab (sbml_sys_text.xsl)
    
    2 - une fonction Scilab [V,v0]=fluxSubspace(w) calculant le param�trage du
    sous-espace des flux v v�riant
    
        Nv=0 
        Hv=w
        
    o� N est la matrice stoechiom�trique et Hv=w exprime des contraintes d'�galit�
    sur les flux, typiquement utilis�e lorsque des flux ont des valeurs connues.
    
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
        version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="weight"/>

	<xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <carbon-labeling-system>
          <xsl:apply-templates/>
        </carbon-labeling-system>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:model">
		<smtb:function>
			<m:ci>solveCumomers</m:ci>
			<smtb:input>
				<m:list>
					<m:ci>x_input</m:ci>
					<m:ci>v</m:ci>
				</m:list>
			</smtb:input>
			<smtb:output>
                <m:list>
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				    <m:ci>
                            <xsl:value-of select="concat('x',@weight)"/>
                        </m:ci>
                    </xsl:for-each>
	            </m:list>
    		</smtb:output>
			<smtb:body>
	            <xsl:apply-templates select="smtb:listOfIntermediateCumomers"/>
			</smtb:body>
		</smtb:function>
        
        <smtb:function>
            <m:ci>fluxSubspace</m:ci>
			<smtb:input>
				<m:list>
					<m:ci>w</m:ci>
				</m:list>
			</smtb:input>
			<smtb:output>
				<m:list>
				    <m:ci>N</m:ci>
				    <m:ci>H</m:ci>
				    <m:ci>W</m:ci>
				</m:list>
			</smtb:output>
			<smtb:body>
                <smtb:comment>Construction des matrices (dont la matrice de stoechiom�trie) permettant</smtb:comment>
                <smtb:comment>de calculer le changement de variable affine des flux.</smtb:comment>
                <xsl:apply-templates select="sbml:listOfSpecies" mode="stoichiometric-matrix"/>
                <smtb:comment>Flux connus.</smtb:comment>
                <xsl:apply-templates select="sbml:listOfReactions" mode="stoichiometric-matrix"/>
                <m:apply>
                    <m:eq/>
                    <m:ci>W</m:ci>
                    <m:list>
                        <m:apply>
				            <m:fn><m:ci>zeros</m:ci></m:fn>
				            <m:cn>
					            <xsl:value-of select="count(sbml:listOfSpecies/sbml:species[@type='intermediate'])"/>
				            </m:cn>
				            <m:cn>1</m:cn>
			            </m:apply>
                        <m:ci>w</m:ci>
                    </m:list>
                </m:apply>
			</smtb:body>
        </smtb:function>  
                               
    </xsl:template>

    <xsl:template match="sbml:listOfReactions" mode="optimize">
        <m:apply>
            <m:eq/>
            <m:ci>w</m:ci>
            <m:list>				
                <xsl:for-each select="sbml:reaction[@known='yes']">
                    <m:ci>
                        <xsl:value-of select="@id"/>
                    </m:ci>
                </xsl:for-each>
            </m:list>
        </m:apply>
    </xsl:template>


    <xsl:template match="smtb:listOfInputCumomers" mode="optimize">
        <m:apply>
            <m:eq/>
            <m:ci>x_input</m:ci>
            <m:list>				
                <xsl:for-each select="smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                    <m:ci>
                        <xsl:value-of select="@id"/>
                    </m:ci>
                </xsl:for-each>
            </m:list>
        </m:apply>
    </xsl:template>

    <xsl:template match="smtb:listOfReactions" mode="optimize">
        <m:apply>
            <m:eq/>
            <m:ci>x_input</m:ci>
            <m:list>				
                <xsl:for-each select="smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                    <m:ci>
                        <xsl:value-of select="@id"/>
                    </m:ci>
                </xsl:for-each>
            </m:list>
        </m:apply>
    </xsl:template>



    <xsl:template match="sbml:listOfReactions" mode="stoichiometric-matrix">
        <m:apply>
			<m:eq/>
			<m:ci>H</m:ci>
			<m:apply>
				<m:fn><m:ci>zeros</m:ci></m:fn>
				<m:cn>
					<xsl:value-of select="count(sbml:reaction[@known='yes'])"/>
				</m:cn>
				<m:cn>
					<xsl:value-of select="count(sbml:reaction)"/>
				</m:cn>
			</m:apply>
		</m:apply> 
        <xsl:apply-templates select="sbml:reaction[@known='yes']" mode="stoichiometric-matrix"/>    
    </xsl:template>

    <xsl:template match="sbml:reaction" mode="stoichiometric-matrix">
<!--        <smtb:comment>
            <xsl:value-of select="@name"/>
        </smtb:comment>-->
        <m:apply>
            <m:eq/>
            <m:apply>
                <m:selector/>
			    <m:ci type="matrix">H</m:ci>
			    <m:cn>
                    <xsl:value-of select="position()"/>
                </m:cn>
			    <m:cn>
                    <xsl:value-of select="@position"/>
                </m:cn>
            </m:apply>
            <m:cn>1</m:cn>
        </m:apply>
    </xsl:template>


    <xsl:template match="sbml:listOfSpecies" mode="stoichiometric-matrix">
        <m:apply>
			<m:eq/>
			<m:ci>N</m:ci>
			<m:apply>
				<m:fn><m:ci>zeros</m:ci></m:fn>
				<m:cn>
					<xsl:value-of select="count(sbml:species[@type='intermediate'])"/>
				</m:cn>
				<m:cn>
					<xsl:value-of select="count(../sbml:listOfReactions/sbml:reaction)"/>
				</m:cn>
			</m:apply>
		</m:apply> 
        <xsl:apply-templates select="sbml:species[@type='intermediate']" mode="stoichiometric-matrix"/>
    </xsl:template>

    <xsl:template match="sbml:species" mode="stoichiometric-matrix">
        <xsl:variable name="position" select="position()"/>
		<smtb:comment><xsl:value-of select="@id"/></smtb:comment>
        <!-- influx rule -->
        <xsl:for-each select="key('products',@id) | key('reactants',@id)">
            <m:apply>
                <m:eq/>
                <m:apply>
                    <m:selector/>
			        <m:ci type="matrix">N</m:ci>
			        <m:cn>
                        <xsl:value-of select="$position"/>
                    </m:cn>
			        <m:cn>
                        <xsl:value-of select="../../@position"/>
                    </m:cn>
                </m:apply>
                <m:apply>
                    <xsl:choose>
                      <xsl:when test="parent::sbml:listOfProducts">
                            <m:plus/>
                        </xsl:when>
                        <xsl:when test="parent::sbml:listOfReactants">
                            <m:minus/>
                        </xsl:when>                   
                    </xsl:choose>
                     <m:apply>
                        <m:selector/>
			            <m:ci type="matrix">N</m:ci>
			            <m:cn>
                            <xsl:value-of select="$position"/>
                        </m:cn>
			            <m:cn>
                            <xsl:value-of select="../../@position"/>
                        </m:cn>
                    </m:apply>
                    <m:cn>1</m:cn>
                </m:apply>
            </m:apply>            
        </xsl:for-each>
    </xsl:template>
    

    <xsl:template match="smtb:listOfIntermediateCumomers">
        
        <xsl:for-each select="smtb:listOfCumomers[@weight&lt;=$weight]">    

			<!-- D�claration et initialisation matrice et vecteur -->
			
			<xsl:variable name="current_weight" select="@weight"/>
			

		<m:apply>
			<m:eq/>
			<m:ci>
                <xsl:value-of select="concat('n',@weight)"/>
			</m:ci>
			<m:cn>
				<xsl:value-of select="count(smtb:cumomer)"/>
			</m:cn>
		</m:apply>
        <m:apply>
			<m:eq/>
			<m:ci>
                <xsl:value-of select="concat('M',@weight)"/>
            </m:ci>
			<m:apply>
				<m:fn><m:ci>zeros</m:ci></m:fn>
				<m:cn>
					<xsl:value-of select="concat('n',@weight)"/>
				</m:cn>
				<m:cn>
					<xsl:value-of select="concat('n',@weight)"/>
				</m:cn>
			</m:apply>
		</m:apply> 

        <m:apply>
			<m:eq/>
			<m:ci>
                <xsl:value-of select="concat('b',@weight)"/>
            </m:ci>
			<m:apply>
				<m:fn><m:ci>zeros</m:ci></m:fn>
				<m:cn>
					<xsl:value-of select="concat('n',@weight)"/>
				</m:cn>
				<m:cn>1</m:cn>
			</m:apply>
		</m:apply> 
		
		<xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
        <m:apply>
			<m:eq/>
			<m:ci>
                <xsl:value-of select="concat('J',$current_weight,'_',@weight)"/>
            </m:ci>
			<m:apply>
				<m:fn><m:ci>zeros</m:ci></m:fn>
				<m:cn>
					<xsl:value-of select="concat('n',$current_weight)"/>
				</m:cn>
				<m:cn>
					<xsl:value-of select="concat('n',@weight)"/>
				</m:cn>
			</m:apply>
		</m:apply> 			
		</xsl:for-each>
            
            
            <smtb:comment>Cumom�res de poids <xsl:value-of select="@weight"/></smtb:comment>

            <xsl:for-each select="smtb:cumomer">
                    <xsl:call-template name="iteration"/>                                    
            </xsl:for-each>

			<!-- r�solution du syst�me -->

			<m:apply>
				<m:eq/>
				<m:ci>
					<xsl:value-of select="concat('x',@weight)"/>
				</m:ci>
				<m:apply>
					<m:fn><m:ci>clean</m:ci></m:fn>					
					<m:apply>
						<m:minus/>
						<m:apply>
						<m:backslash/>
						<m:ci type="matrix">
							<xsl:value-of select="concat('M',@weight)"/>
						</m:ci>
						<m:ci type="vector">
							<xsl:value-of select="concat('b',@weight)"/>
						</m:ci>
						</m:apply>
					</m:apply>
				</m:apply>
			</m:apply>
			
        </xsl:for-each>
		
    </xsl:template>
   

<!-- Template de fabrication de l'�quation de bilan stoechiom�trique
     de l'esp�ce @id -->

<xsl:template name="bilan">

    <xsl:variable name="name" select="@id"/>

    <m:apply>
        <m:eq/>
        <m:cn>0</m:cn>
        <m:apply>
            <m:minus/>   
            
            <!-- influx rule -->

            <m:apply>
                <m:plus/>
                <xsl:for-each select="key('products',@id)">
                    <m:ci>
                        <xsl:value-of select="../../@id"/>
                    </m:ci>
                </xsl:for-each>
            </m:apply>
            
            <xsl:call-template name="bilan-outflux">
                <xsl:with-param name="id" select="@id"/>
            </xsl:call-template>
                 
        </m:apply>
    
    </m:apply>

</xsl:template>

<xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

    <!-- outflux rule -->

    <m:apply>
        <m:plus/>
        <xsl:for-each select="key('reactants',$id)">
            <m:apply>
                <m:selector/>
                <m:ci type="vector">v</m:ci>
			    <m:cn>
                <!-- id de la r�action -->
	                <xsl:value-of select="key('reaction',../../@id)/@position"/>
                </m:cn>
            </m:apply>
        </xsl:for-each>
    </m:apply>

</xsl:template>


<xsl:template name="iteration">
    
    <!-- Attention le noeud contextuel est un �lement <smtb:cumomer> -->

    <xsl:variable name="name" select="../@id"/>
    
	<!-- La variable "carbons" contient les carbones 13 de l'esp�ce courante -->
	
    <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>
	
    <xsl:variable name="weight" select="@weight"/>
    <xsl:variable name="position" select="@position"/>

	<!-- influx rule : le plus chiant, mais aussi le plus int�ressant. C'est l� que se cr�e v�ritablement
     	l'information suppl�mentaire par rapport � la stoechiom�trie simple. 

     On boucle sur tous les �l�ments <speciesReference> qui ont l'esp�ce courante comme produit,
     donc dans le for-each le noeud contextuel est de type reaction/listOfProducts/speciesReference. -->

    <xsl:for-each select="key('products',@species)"> 

        <!-- Maintenant, on essaye de trouver des occurences des carbones marqu�s
             du reactant de la r�action courante : c'est du boulot... -->

        <xsl:variable name="species" select="@species"/>
        <xsl:variable name="reaction" select="key('reaction',../../@id)/@position"/>
        <xsl:variable name="occurence" select="count(preceding-sibling::sbml:speciesReference)+1"/>

        <xsl:variable name="influx">

            <!-- C'est ici que les choses s�rieuses commencent. On boucle sur tous les r�actants qui ont des atomes de carbones
                 qui "pointent" sur l'esp�ce -->

            <xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[smtb:carbon[(@species=$species) and (@occurence=$occurence)]]">
                <xsl:variable name="somme">
					<!-- On boucle donc maintenant sur les carbones du reactant. Si un carbone du reactant pointe vers
					     vers un carbone 13 du cumom�re du produit, on note son num�ro dans un �l�ment <token/> -->
                    <xsl:for-each select="smtb:carbon[(@species=$species) and (@occurence=$occurence)]">
                        <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                            <token>
                                <xsl:value-of select="@position"/>
                            </token>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
                    <token weight="{count(exslt:node-set($somme)/token)}" type="{key('species',@species)/@type}">
                        <xsl:value-of select="concat(@species,'_',sum(exslt:node-set($somme)/token))"/>
                    </token>
                </xsl:if>
            </xsl:for-each>

		</xsl:variable>
        
		<!-- Ici on g�n�re le code qui forme la matrice et le second membre du syst�me qui permet d'obtenir les
		     cumom�res de poids $weight -->
		
		<xsl:choose>
			<xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
				<!-- On n'a qu'un seul terme du poids courant, on assemble donc la matrice -->
				<m:apply>
						<m:eq/>
						<m:apply>
							<m:selector/>
							<m:ci type="matrix">
								<xsl:value-of select="concat('M',$weight)"/>
							</m:ci>
							<m:cn>
								<xsl:value-of select="$position"/>
							</m:cn>
							<m:cn>
								<xsl:value-of select="key('cumomer',$influx)/@position"/>
							</m:cn>
						</m:apply>
                        <m:apply>
                            <m:selector/>
                            <m:ci type="vector">v</m:ci>
						    <m:cn>
                            <!-- id de la r�action -->
	                            <xsl:value-of select="$reaction"/>
                            </m:cn>
                        </m:apply>
				</m:apply>
			</xsl:when>
			<xsl:otherwise>
				<!-- On a un produit de plusieurs termes, donc de poids inf�rieur, on assemble donc le second membre
				     ainsi que la jacobienne du second membre par rapport aux cumom�res de poids inf�rieur  -->
				<m:apply>
					<m:eq/>
					<m:apply>
						<m:selector/>
						<m:ci type="vector">
							<xsl:value-of select="concat('b',$weight)"/>
						</m:ci>
						<m:cn>
							<xsl:value-of select="$position"/>
						</m:cn>
					</m:apply>
					<m:apply>
						<m:plus/>
						<m:apply>
							<m:selector/>
							<m:ci type="vector">
								<xsl:value-of select="concat('b',$weight)"/>
							</m:ci>
							<m:cn>
								<xsl:value-of select="$position"/>
							</m:cn>
						</m:apply>
						<m:apply>
							<m:times/>
						    <m:apply>
                                <m:selector/>
                                <m:ci type="vector">v</m:ci>
						        <m:cn>
                                <!-- id de la r�action -->
	                                <xsl:value-of select="$reaction"/>
                                </m:cn>
                            </m:apply>
							<xsl:call-template name="iterate-influx">
								<xsl:with-param name="tokens">
									<xsl:copy-of select="$influx"/>
								</xsl:with-param>
							</xsl:call-template>
						</m:apply>						
					</m:apply>
				</m:apply>
				
				<xsl:call-template name="iterate-influx-jacobian-x">
					<xsl:with-param name="reaction" select="$reaction"/>
					<xsl:with-param name="weight" select="$weight"/>
					<xsl:with-param name="tokens">
						<xsl:copy-of select="$influx"/>
					</xsl:with-param>
					<xsl:with-param name="position" select="$position"/>
					<xsl:with-param name="i">1</xsl:with-param>
					<xsl:with-param name="n" select="count(exslt:node-set($influx)/token)"/>
				</xsl:call-template>
						
			</xsl:otherwise>
		</xsl:choose>

    </xsl:for-each>

    <!-- outflux rule : partie la plus simple � g�n�rer (voir papier Wiechert) -->

			
    <m:apply>

		<m:eq/>

		<m:apply>
			<m:selector/> 
			<m:ci type="matrix">
				<xsl:value-of select="concat('M',$weight)"/>
			</m:ci>
			<m:cn>
				<xsl:value-of select="$position"/>
			</m:cn>
			<m:cn>
				<xsl:value-of select="$position"/>
			</m:cn>
		</m:apply>`

		<m:apply>
			<m:minus/>
	        <xsl:call-template name="bilan-outflux">
    	        <xsl:with-param name="id" select="@species"/>
        	</xsl:call-template>
		</m:apply>

    </m:apply>
    
</xsl:template>


<xsl:template name="iterate-influx">
	<xsl:param name="tokens"/>
	<xsl:variable name="id" select="exslt:node-set($tokens)/token[1]"/>
	
	<xsl:choose>
		<xsl:when test="key('cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="vector">
					<xsl:value-of select="concat('x',key('cumomer',$id)/@weight)"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="key('cumomer',$id)/@position"/>
				</m:cn>
			</m:apply>
		</xsl:when>
		<xsl:when test="key('input-cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="vector">x_input</m:ci>
				<m:cn>
					<xsl:value-of select="key('input-cumomer',$id)/@position"/>
				</m:cn>
			</m:apply>
		</xsl:when>
	</xsl:choose>
	
	<xsl:if test="count(exslt:node-set($tokens)/token)&gt;1">
		<xsl:call-template name="iterate-influx">
			<xsl:with-param name="tokens">
				<xsl:copy-of select="exslt:node-set($tokens)/token[position()&gt;1]"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template name="iterate-influx-jacobian-x">
	<xsl:param name="reaction"/>
	<xsl:param name="weight"/>
	<xsl:param name="tokens"/>
	<xsl:param name="position"/>
	<xsl:param name="i"/>
	<xsl:param name="n"/>

	<xsl:variable name="id" select="exslt:node-set($tokens)/token[position()=$i]"/>
	
	<xsl:if test="key('cumomer',$id)">
		<xsl:variable name="J">
			<m:apply>
				<m:selector/>
				<m:ci type="matrix">
					<xsl:value-of select="concat('J',$weight,'_',key('cumomer',$id)/@weight)"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="$position"/>
				</m:cn>
				<m:cn>
					<xsl:value-of select="key('cumomer',$id)/@position"/>
				</m:cn>
			</m:apply>
		</xsl:variable>
		<m:apply>
			<m:eq/>
			<xsl:copy-of select="$J"/>
			<m:apply>
				<m:plus/>
				<xsl:copy-of select="$J"/>
				<m:apply>
					<m:times/>
					<xsl:call-template name="iterate-influx">
						<xsl:with-param name="tokens">
							<xsl:copy-of select="exslt:node-set($tokens)/token[position()!=$i]"/>
						</xsl:with-param>
					</xsl:call-template>
                    <m:apply>
                        <m:selector/>
                        <m:ci type="vector">v</m:ci>
						<m:cn>
	                        <xsl:value-of select="$reaction"/>
                        </m:cn>
                    </m:apply>				
				</m:apply>
			</m:apply>
		</m:apply>
	</xsl:if>

	<xsl:if test="$i&lt;$n">
		<xsl:call-template name="iterate-influx-jacobian-x">
			<xsl:with-param name="reaction" select="$reaction"/>
			<xsl:with-param name="weight" select="$weight"/>
			<xsl:with-param name="tokens">
				<xsl:copy-of select="$tokens"/>
			</xsl:with-param>
			<xsl:with-param name="position" select="$position"/>
			<xsl:with-param name="i" select="($i)+1"/>
			<xsl:with-param name="n" select="$n"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>


</xsl:stylesheet>
