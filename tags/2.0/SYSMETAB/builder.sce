mode(-1)
lines(0)
path=get_absolute_file_path("builder.sce");
chdir(path)
disp('Installing SYSMETAB')
if part(getlanguage(),1:2)=="en"
	TCL_SetVar('lang','english')
  install_message=["Installation of SYSMETAB was successful.";
                  "The next time you launch Scilab, the SYSMETAB menu ";
                  "will be loaded automatically"];
elseif part(getlanguage(),1:2)=="fr"
	TCL_SetVar('lang','french')
  install_message=["L''installation de SYSMETAB est réussie.";
                  "La prochaine fois que Scilab sera lancé, le menu SYSMETAB";
                  "sera chargé automatiquement"];
end
TCL_SetVar('SCI',SCI)
TCL_SetVar('SCIHOME',SCIHOME)
TCL_SetVar('SYSMETAB',path)
TCL_EvalFile('TCL/build.tcl')

exec('OPTIM/fsqp-1.5/builder.sce',-1)

if havewindow()
	messagebox(install_message);
	exec('loader.sce',-1)
else
	printf("\n\n"+install_message+"\n\n")	
    quit
end

