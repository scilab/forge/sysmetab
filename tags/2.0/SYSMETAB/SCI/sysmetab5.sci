//
//    Auteur  :   St�phane Mottelet
//    Date    :   Tue Mar 25 14:25:41 CET 2008
//    Projet  :   SYSMETAB/Carnot
//

function a=matrix_assemble(a_ijv,dims,_type,cast)
 
    if argn(2)==3
        cast=%t;
    end
    
    if _type=="sparse"
        if length(dims)==2
            a=sparse(a_ijv(:,1:2),a_ijv(:,3),dims);
        else
            error('matrix_assemble : sparse type only allowed for usual matrices')
        end
    elseif length(dims)>1
        k=a_ijv(:,1)+dims(1)*a_ijv(:,2)-dims(1);
        if length(dims)>2
            nmat=prod(dims(1:2));
            n=prod(dims);
            index=0:nmat:n-nmat;
            a=zeros(n,1);            
            a(k(:,ones(size(index,2),1))+index(ones(size(k,1),1),:))=a_ijv(1+2*size(a_ijv,1):$);
            if cast
                a=mlist(['hm','dims','entries'],dims,a);
            end 
        else
            a=zeros(dims(1),dims(2));
            a(k)=a_ijv(:,3);
        end
    end
    
endfunction

function a=s_full(ij,v,dims)

// Fonction permettant de construire une matrice pleine avec la 
// meme description que pour les matrices creuses, � savoir un 
// tableau (ij) contenant les paires d'indices de ligne et de colonne
// et un tableau de valeurs correspondantes (v).
//
// Attention : cela n'est pas �quivalent � 
//
// a=full(sparse(ij,v,dims))
//
// car ici on suppose qu'il n'y a pas d'entr�es (i,j) multiples.
//

    a=zeros(dims(1),dims(2));
    a(ij(:,1)+dims(1)*ij(:,2)-dims(1))=v;

endfunction

function [H,w]=fluxConstraints(_fluxes)

// On r�cup�re l'information sur les flux impos�s � partir du
// tableau des fluxs : valeur >=0 pour une valeur impos�e, n�gatif
// pour des valeurs libres. On renvoie une matrice H et un vecteur w
// tels que ces contraintes s'�crivent sous la forme
//
// H*v=w
//
// o� v est le vecteur des flux.
//


	_constr=find(_fluxes(:,2)>=0);
	_nc=length(_constr);
	_ij=[(1:_nc)' _constr'];
    if _ij==[]
        H=[];
        w=[];
    else
	    H=s_full(_ij,ones(_nc,1),[_nc size(_fluxes,1)]);
	    w=_fluxes(_constr,2);
    end
endfunction


function [result]=adjustFluxesTab(_fluxes)

// En fonction des valeurs des flux, les free fluxes peuvent changer, en g�n�ral c'est le cas quand
// le statut de certaines contraintes change (satur�es ou inactives). La fonction adjustFluxesTab
// (d�finie dans SYSMETAB/SCI/sysmetab4.sci) a pour but de rendre actives les cases du tableau des
// fluxs correspondant � des free fluxes. On peut alors les faire varier avec <ctrl>+fl�che droite
// ou gauche (incr�ment de 1 ou -1) ou <ctrl>+<shift>-fl�che (incr�ment de 10 ou -10).
//
// Attention, cela ne marche pas sur le panneau d�tachable.
//
	global V v0 ff A b v N H w

	result=0;

	// Construction des matrices (dont la matrice de stoechiom�trie) permettant
	// de calculer le changement de variable affine des flux. La fonction fluxSubspace
	// est g�n�r�e sp�cifiquement pour chaque r�seau.	
    
	disp avant
	[A,b,N,H,w]=fluxSubspace(_fluxes);
	disp apres
	
	// Calcul des free fluxes et du param�trage correspondant (la fonction freefluxes est d�finie
	// un peu plus bas dans ce fichier.

	[V,v0,ff]=freefluxes(A,b);

	if ff~=[]
		TCL_EvalStr('$fluxes_window tag delete free');
		TCL_EvalStr('$fluxes_window tag configure free -background #cceecc');
		for i=ff
			TCL_EvalStr('$fluxes_window tag cell free '+string(i)+',1');
		end
		TCL_EvalStr('$fluxes_window tag lower disabled');
		TCL_EvalStr('bind $fluxes_window <Control-Right> {..
						if [$fluxes_window tag includes free [$fluxes_window tag cell active]] {..
							ScilabEval changeFreeFlux([$fluxes_window tag cell active],1)  ..
						}..
					}');
		TCL_EvalStr('bind $fluxes_window <Control-Left> {..
						if [$fluxes_window tag includes free [$fluxes_window tag cell active]] {..
							ScilabEval changeFreeFlux([$fluxes_window tag cell active],-1)  ..
						}..
					}');
		TCL_EvalStr('bind $fluxes_window <Control-Shift-Right> {..
						if [$fluxes_window tag includes free [$fluxes_window tag cell active]] {..
							ScilabEval changeFreeFlux([$fluxes_window tag cell active],10)  ..
						}..
					}');
		TCL_EvalStr('bind $fluxes_window <Control-Shift-Left> {..
						if [$fluxes_window tag includes free [$fluxes_window tag cell active]] {..
							ScilabEval changeFreeFlux([$fluxes_window tag cell active],-10)  ..
						}..
					}');	
	end
endfunction

function changeFreeFlux(_row,_col,_dir)

	global V v0 ff A b v lock
		
	if lock
		return // On empeche les appels asynchrones si la macro est d�j� en train de travailler
	end

	lock=%t; 

	if v==[]
		v=fluxes(:,1);
	end
	
	free_fluxes=v(ff);
	selected=find(_row==ff);
	free_flux=v(ff(selected));
	
	if (free_flux+_dir<0)
		free_fluxes(selected)=0;
	else
		free_fluxes(selected)=free_flux+_dir;
	end
	
	newv=clean(V*free_fluxes+v0);
	neg=find(newv<0);
	
	if neg~=[]
		xmllab_error(sprintf('Modification impossible, %s deviendrait n�gatif',flux_ids(neg(1))));
	else
		v=newv;
		TCL_SetMatrix('fluxes',[clean(v);fluxes(:,2)]);
	end
	
	exec(xmllab_run,-1); // Ex�cute le script principal de calcul des marquages
	
	lock=%f;

endfunction


function [V,v0,ff]=freefluxes(A,b)

	// D�termination des "free fluxes",et du param�trage du sous-espace affine
	// d�fini par A*v=b, sous la forme v=V*x+v0, o� x est un sous-ensemble des
	// composantes de v, les "free fluxes". 
	
	V=[];
	v0=[];
	ff=[];
	n=size(A,2);
	n1=size(A,1);

	// Pour v�rifier que l'utilisateur n'impose pas des contraintes incoh�rentes on essaye
	// de r�soudre un programme lin�aire dont la fonction cout est identiquement nulle, ce
	// qui permet de tester si l'ensemble admissible {A*v=b, v>=0} est non-vide.

    ierr=execstr('v0=linpro(zeros(n,1),A,b,zeros(n,1),%inf*ones(n,1),n1)','errcatch');
 	if ierr~=0
	    xmllab_error('Pas de flux positifs admissibles, les flux impos�s sont incoh�rents.')
		if rank(A)~=n1
    		xmllab_error(sprintf('Attention : %d contrainte(s) redondante(s)',n1-rank(A)))		
		end    
	else
		// On utilise la factorisation QR de A : A*P=QR avec P de taille (n,n),
		// R de taille (n1,n) et Q de taille (n1,n1). On utilise ici le fait que
		// la permutation P permet de garantir que la sous matrice principale (n1,n1) 
		// de A*P est de rang plein si rang(A)=n1. On a donc, en posant B=A*P et E=P'
		//
		// Av=b <==> B*E*v=b <==> [B1 B2]*[E1*v;E2*v]=b <==> B1*E1*v = -B2*E2*v
		// 
		// si on ajoute � ceci l'�quation triviale E2*v=E2*v, on obtient
		//
		// Av=b <==> [B1*E1;E2]*v = [b;0]-[B2;I]*E2*v <==> v = inv([B1*E1;E2])*[b;0]-inv([B1*E1;E2])*[B2;I]*E2*v
		//
		// ce qui donne la relation affine entre les flux et les free fluxes, repr�sent�s 
		// ici par E2*v. Dans la suite on pose donc
		//
		// v0=inv([B1*E1;E2])*[b;0] et V=-inv([B1*E1;E2])*[B2;I].
		//
		// Ceci fonctionne aussi quand A n'est pas de rang plein, mais cela n'a d'int�ret
		// que quand b est dans l'image de A, ce qui est v�rifi� plus haut lors de la r�solution
		// du programme lin�aire. 
		//
		// Personnellement, je n'ai jamais trouv� de publication sur le sujet mentionnant ce type
		// d'approche syst�matique. Quand on regarde dans la doc de 13C-Flux par exemple, Wiechert
		// propose de d�terminer les free fluxes "� la main". A d�tailler dans l'annexe du papier
		// que l'on �crira sur Sysmetab.
		//
		
		[Q,R,P]=qr(A);
		E=P';
		n1=rank(A);
		n2=size(A,2)-n1;
		E1=E(1:n1,:);
		E2=E(n1+1:$,:);
		C1=[A*E1'*E1;E2];
		C2=[-A*E2';eye(n2,n2)];
		V=clean(C1\C2);
		[s,i]=sort(-E2*(1:n1+n2)'); // On remet les free fluxes dans l'ordre.
		ff=-s;
		V=V(:,i);
		
		// Attention, le v0 obtenu ici peut avoir des composantes n�gatives, cela est tout
		// � fait normal. 
		
		v0=clean(C1\[b;zeros(n2,1)]);
				
	end
endfunction

function [v]=optimize()

	global A b V v0 x0_restart v_old label_error_old l_tab 

	// Calcul d'un premier param�trage v=V*x+v0 en utilisant les free fluxes. Pas 
	// forc�ment optimal num�riquement car V n'est pas orthogonale. En pratique
	// cela a l'air de ne pas poser trop de probl�mes.
	//
	// Si on voulait un param�trage compl�tement "virtuel", sans connaissance a priori
	// sur le probl�me que l'on r�sout, on �crirait :
	//
	// V=kernel(A);v0=A\b;
	//
	// o� ici v0 est la solution de norme minimale de Av=b.

//	[V,v0,ff]=freefluxes(A,b);

	vreg=0*v0; // Le vreg sert dans le terme de r�gularisation. On peut prendre
			   // vreg=v0 si on veut.
	
	// On identifie les contraintes d'�galit� a posteriori, car certaines peuvent apparaitre
	// en plus de celles sp�cifi�es a priori, par exemple lorsque l'on impose tous les fluxs
	// ext�rieurs (entrants et sortants) sauf un.
	//
	// On rep�re ces contraintes en regardant chaque ligne de la matrice V. Si la ligne k
	// est nulle � la pr�cision machine, alors cela veut dire que v_k est fix�e.
	//

	eq_constr=find(clean(sum(abs(V),'col'))==0);

	// Pareil pour les contraintes d'in�galit�, les autres donc.

	ineq_constr=find(clean(sum(abs(V),'col'))~=0);
	
	// Tout cela ne change pas la dimension effective de l'espace dans lequel
	// va avoir lieu l'optimisation, � savoir la dimension du vecteur x du param�trage
	// 
	// v=V*x+v0
	//
	
	n=size(V,2);
	
	// Pr�paration des param�tres pour l'appel � fsqp.
	
	nf=1;
	nineqn=0; 	
	nineq=size(V,1)-length(eq_constr); // nombre de contraintes d'in�galit�
	neqn=0; 
	neq=0; // nombre de contraintes d'in�galit�
	
	modefsqp=100; // faire un help fsqp pour en savoir plus (mode pas trop bavard).

	// Ces param�tres sont r�cup�r�s � partir de l'interface xmllab :
	//
	//	miter : nombre de tirages al�atoires de condition initiale
	//	epsgrad : seuil sur la norme du gradient projet�
	//	itmax : nombre maximum d'appels � la fonction cout
	//

	iprint=1;
	ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
	ipar_relax=[nf,nineqn,nineq,neqn+1,neq+1,modefsqp,miter,iprint];
	bigbnd=1.e10; 

	epsneq=1e-8; udelta=0.e0;
	rpar=[bigbnd,epsgrad,epsneq,udelta];
	
	// Bornes inf/sup sur les variables. Ici, il n'y en a pas, elles
	// sont contenues dans les contraintes d'in�galit� (lin�aires).
	
	bl=-bigbnd*ones(n,1);
	bu=+bigbnd*ones(n,1);
		
	_inform=1;
	nit=0;

	// On boucle tant que l'optimiseur n'a pas termin� normalement
	// On repart du dernier vecteur obtenu � l'issue de l'optimisation, si possible.

	if x0_restart~=[] & length(x0_restart)==n
		x0=x0_restart;
	else
		x0=rand(n,1);
	end		

	while _inform & (nit<itmax)
		[x,_inform,f,g,_lambda]=fsqp(x0,ipar,rpar,[bl bu],_obj,_cntr,_grobj,_grcntr);
//
// Autres fonctions cout. Pas vraiment concluant.		
//
//		[x,_inform,f,g,_lambda]=fsqp(x0,ipar,rpar,[bl bu],_obj_reg_sum,_cntr_reg_sum,_grobj_reg_sum,_grcntr_reg_sum);
//		[x,_inform,f,g,_lambda]=fsqp(x0,ipar_relax,rpar,[bl bu],_obj_relax,_cntr_relax,_grobj_relax,_grcntr_relax);

		nit=nit+1;
		if _inform
			x0=rand(n,1)
		end

	end	

	if _inform==0 // L'optimisation s'est termin�e correctement

		xmllab_error(sprintf('Convergence avec norme de la fonction cout = %f',f))
		v=clean(V*x+v0);
		v_old=v;
		x0_restart=x;

		[label_error,grad]=costAndGrad(v); // label_error est affich� dans l'interface
		label_error_old=label_error;

		regul=.5*epsilon*(v-vreg)'*(v-vreg);

		// Tableau � utiliser �ventuellement pour une approche de type "L-curve" pour
		// la d�termination d'un epsilon optimal.

		l_tab=[l_tab;[epsilon label_error regul]];

	else // Echec de l'optimisation
	
		xmllab_error(sprintf('Pas de convergence apr�s %d tirages de conditions initiales.',itmax))
		if v_old~=[]
			v=v_old;
			label_error=label_error_old
		else
			v=v0;
			label_error="";
		end
		x0_restart=[];
	end	
				
endfunction

function oj=_obj(j,x)

// Calcul de la fonction cout : au terme classique de moindres carr�s, on ajoute
// un terme de r�gularisation
//
// 0.5*epsilon*norm(v-vreg)^2
//
// Note : on calcule aussi tout ce qui est n�cessaire � fsqp, � savoir la fonction
// donnant la satisfaction des contraintes et leurs gradients respectifs. Puisqu'en
// pratique on calcule simultan�ment la fonction cout et son gradient, cela permet 
// de ne pas faire de calculs inutiles, en utilisant la fonction x_is_new() de la
// toolbox fsqp, qui permet de savoir si le x courant a chang� ou pas.

	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*(_v-vreg)'*(_v-vreg);
		all_grobj=V'*grad'+epsilon*V'*(_v-vreg);
		all_cntr=-_v(ineq_constr)+vmin;
		all_grcntr=-V(ineq_constr,:)';
		oj=all_obj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		oj=all_obj;
	end
endfunction

function goj=_grobj(j,x)

// Calcul du gradient de la fonction cout.

	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*(_v-vreg)'*(_v-vreg);
		all_grobj=V'*grad'+epsilon*V'*(_v-vreg);
		all_cntr=-_v(ineq_constr)+vmin;
		all_grcntr=-V(ineq_constr,:)';
		goj=all_grobj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		goj=all_grobj;
	end
endfunction

function cj=_cntr(j,x)

// Calcul de la fonction donnant la satisfaction des contraintes.

	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*(_v-vreg)'*(_v-vreg);
		all_grobj=V'*grad'+epsilon*V'*(_v-vreg);
		all_cntr=-_v(ineq_constr)+vmin;
		all_grcntr=-V(ineq_constr,:)';
		cj=all_cntr(j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		cj=all_cntr(j);
	end
endfunction

function gcj=_grcntr(j,x)

// Calcul du gradient de la fonction donnant la satisfaction des contraintes.

	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*(_v-vreg)'*(_v-vreg);
		all_grobj=V'*grad'+epsilon*V'*(_v-vreg);
		all_cntr=-_v(ineq_constr)+vmin;
		all_grcntr=-V(ineq_constr,:)';
		gcj=all_grcntr(:,j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		gcj=all_grcntr(:,j);
	end
endfunction

//
// Calcul du gradient de la fonction cout et du gradient de la fonction de satisfaction
// des contraintes par diff�rences finies, pour v�rification uniquement !
//

function _gdf=_grobjdf(j,x)
	_n=length(x);
	_h=1e-5;
	_gdf=zeros(_n,1);
	for i=1:length(x)
		_e=zeros(_n,1);
		_e(i)=_h;
		set_x_is_new(1);
		objh1=_obj(j,x+_e);
		set_x_is_new(1);
		objh2=_obj(j,x-_e);
		_gdf(i)=(objh1-objh2)/_h/2;
	end
endfunction

function oj=_obj_relax(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=sum(_v)
		all_grobj=V'*ones(_v);
		all_cntr=[-_v(ineq_constr);cost-epsilon];
		all_grcntr=[-V(ineq_constr,:)' V'*grad' ];
		oj=all_obj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		oj=all_obj;
	end
endfunction

// 
// Deuxi�me jeu de fonctions pour l'optimisation : ici on r�gularise avec un terme �gal �
// la norme L1 de v. En fait le terme de r�gularisation est carr�ment �gal � la somme des
// composantes de v puisque les flux sont positifs.
//

function oj=_obj_reg_sum(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+epsilon*sum(_v);
		all_grobj=V'*grad'+epsilon*V'*ones(_v);
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		oj=all_obj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		oj=all_obj;
	end
endfunction

function goj=_grobj_reg_sum(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+epsilon*sum(_v);
		all_grobj=V'*grad'+epsilon*V'*ones(_v);
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		goj=all_grobj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		goj=all_grobj;
	end
endfunction

function cj=_cntr_reg_sum(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+epsilon*sum(_v);
		all_grobj=V'*grad'+epsilon*V'*ones(_v);
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		cj=all_cntr(j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		cj=all_cntr(j);
	end
endfunction

function gcj=_grcntr_reg_sum(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+epsilon*sum(_v);
		all_grobj=V'*grad'+epsilon*V'*ones(_v);
		all_cntr=-_v(ineq_constr);
		all_grcntr=-V(ineq_constr,:)';
		gcj=all_grcntr(:,j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		gcj=all_grcntr(:,j);
	end
endfunction


// 
// Troisi�me jeu de fonctions pour l'optimisation : ici on minimise la norme L1 de v 
// sous la contrainte que la norme de la fonction cout soit inf�rieure � epsilon en ajoutant.
// Approche dy type "relaxation Lagrangienne" pas vraiment concluante apr�s quelques essais. 
//

function goj=_grobj_relax(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=sum(_v)
		all_grobj=V'*ones(_v);
		all_cntr=[-_v(ineq_constr);cost-epsilon];
		all_grcntr=[-V(ineq_constr,:)' V'*grad' ];
		goj=all_grobj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		goj=all_grobj;
	end
endfunction

function cj=_cntr_relax(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=sum(_v)
		all_grobj=V'*ones(_v);
		all_cntr=[-_v(ineq_constr);cost-epsilon];
		all_grcntr=[-V(ineq_constr,:)' V'*grad' ];
		cj=all_cntr(j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		cj=all_cntr(j);
	end
endfunction

function gcj=_grcntr_relax(j,x)
	if x_is_new()
		_v=V*x+v0;
		[cost,grad]=costAndGrad(_v);
		all_obj=sum(_v)
		all_grobj=V'*ones(_v);
		all_cntr=[-_v(ineq_constr);cost-epsilon];
		all_grcntr=[-V(ineq_constr,:)' V'*grad' ];
		gcj=all_grcntr(:,j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		gcj=all_grcntr(:,j);
	end
endfunction

function _gdf=_grobjdf_relax(j,x)
	_n=length(x);
	_h=1e-5;
	_gdf=zeros(_n,1);
	for i=1:length(x)
		_e=zeros(_n,1);
		_e(i)=_h;
		set_x_is_new(1);
		objh1=_obj(j,x+_e);
		set_x_is_new(1);
		objh2=_obj(j,x-_e);
		_gdf(i)=(objh1-objh2)/_h/2;
	end
endfunction


function _gcdf=_grcntrdf(j,x)
	_n=length(x);
	_h=1e-5;
	_gcdf=zeros(_n,1);
	for i=1:length(x)
		_e=zeros(_n,1);
		_e(i)=_h;
		set_x_is_new(1);
		gcjh1=_cntr(j,x+_e);
		set_x_is_new(1);
		gcjh2=_cntr(j,x-_e);
		_gcdf(i)=(gcjh1-gcjh2)/_h/2;
	end
endfunction


function a=s_full3(ij,v,dims)
	a=hypermat(dims);
	for i=1:size(v,1)
	     a(ij(i,1),:)=a(ij(i,1),:)+v(i,:);
	end;
endfunction;

function HM=MtoHM(M,t)
	HM=matrix(ones(1,t).*.M,[size(M),t]);
endfunction


function [t,X]=CN_HM(t0,t1,s,M,b)
h=(t1-t0)/s;
d=size(b);
[l1,l2]=size(M);
x=hypermat([l1,d(2),s]);
t=t0:h:(t1-h);
I=speye(l1,l2);
R=I-(h/2)*M;
[R_handle,R_rank]=lufact(R);
for i=1:(s-1)
	tmp=(x(:,:,i)+(h/2)*(M*x(:,:,i)+b(:,:,i)+b(:,:,i+1)));
	x(:,:,i+1)=lusolve(R_handle,tmp);
end;
X=x;
endfunction

function x=flatten_exp(X,n)
	x=X(:,n,:);
	x=x(:,:);
endfunction

rand('seed',getdate('s'))

global V v0 ff A b v N H w

