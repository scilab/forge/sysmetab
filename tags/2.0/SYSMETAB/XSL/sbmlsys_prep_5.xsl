<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Lun jui  2 10:46:52 CEST 2008

    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but :

	1-de g�rer le probl�me des mol�cules sym�triques. Quand une mol�cule
	sym�trique est substrat d'une r�action, il y a une chance sur deux
	pour que l'une ou l'autre version participe � la r�action. Par exemple
	si on a 
	
	v1 : succinate -> Fumarate
   	          ABCD -> ABCD 
       
	on a aussi 


	v1' : succinate -> Fumarate
   	          DCBA  -> ABCD 

avec v1'=v1

	Donc pour chaque instance de type v1, on doit ajouter v1' et conserver l'information
	qu'il faut ajouter dans la suite v1=v1' comme contrainte.

-->

<xsl:stylesheet  version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:math="http://exslt.org/math"   
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="math sbml celldesigner m str exslt xhtml smtb">

    <xsl:param name="experiences">1</xsl:param>
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
	
    <xsl:key name="species" match="sbml:species" use="@id"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="sbml:listOfReactions">
		
		<sbml:listOfRules>
			<xsl:for-each select="sbml:reaction[key('species',sbml:listOfReactants/sbml:speciesReference/@species)/@symmetric='yes']">
				<sbml:algebraicRule>
					<m:math>
						<m:apply>
							<m:minus/>
							<m:ci>
								<xsl:value-of select="concat(@id,'_1')"/>
							</m:ci>
							<m:ci>
								<xsl:value-of select="concat(@id,'_2')"/>
							</m:ci>
						</m:apply>
					</m:math>
				</sbml:algebraicRule>
			</xsl:for-each>
		</sbml:listOfRules>
		
		<sbml:listOfReactions>
			<xsl:apply-templates/>
		</sbml:listOfReactions>
		
	</xsl:template>


    <xsl:template match="sbml:reaction">
		<xsl:variable name="pos" select="count(preceding-sibling::sbml:reaction[key('species',sbml:listOfReactants/sbml:speciesReference/@species)/@symmetric='yes'])
			                      +count(preceding-sibling::sbml:reaction)+1"/>
		<xsl:choose>
			<xsl:when test="key('species',sbml:listOfReactants/sbml:speciesReference/@species)/@symmetric='yes'">
    			<sbml:reaction id="{@id}_1" name="{@name} (1)" position="{$pos}">
        			<xsl:copy-of select="*"/>
    			</sbml:reaction>				
    			<sbml:reaction id="{@id}_2" name="{@name} (2)" position="{1+$pos}">
        			<sbml:listOfReactants>
						<xsl:for-each select="sbml:listOfReactants/sbml:speciesReference">
							<xsl:choose>
								<xsl:when test="key('species',@species)/@symmetric='yes'">
									<sbml:speciesReference species="{@species}">
										<xsl:for-each select="smtb:carbon">
											<xsl:sort select="@position" data-type="number" order="descending"/>
											<smtb:carbon position="{math:power(2,position()-1)}" 
														 destination="{@destination}"
														 occurence="{@occurence}"
														 species="{@species}">
												
											</smtb:carbon>
										</xsl:for-each>
									</sbml:speciesReference>
								</xsl:when>
								<xsl:otherwise>
									<xsl:copy-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</sbml:listOfReactants>
					<xsl:copy-of select="sbml:listOfProducts"/>
    			</sbml:reaction>										
			</xsl:when>

			<xsl:otherwise>
    			<sbml:reaction position="{$pos}">
        			<xsl:copy-of select="@*"/>
        			<xsl:copy-of select="*"/>
    			</sbml:reaction>				
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>

    <xsl:template match="smtb:measurement"> <!-- addition des contributions de chaque cumom�re -->
      <smtb:measurement>
        <xsl:copy-of select="@*"/>
        <xsl:for-each select="smtb:cumomer-contribution[not(@id=preceding-sibling::smtb:cumomer-contribution/@id)]">
          <xsl:sort select="@weight"/>
          <xsl:sort select="@string" order="descending"/>
          <smtb:cumomer-contribution>
            <xsl:copy-of select="@*[name()!='sign']"/>
            <xsl:attribute name="sign">
              <xsl:value-of select="sum(../smtb:cumomer-contribution[@id=current()/@id]/@sign)"/>
            </xsl:attribute>
          </smtb:cumomer-contribution>
        </xsl:for-each>
      </smtb:measurement>
    </xsl:template>
    


</xsl:stylesheet>
