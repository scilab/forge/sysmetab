exdir=get_absolute_file_path('example1.sce');

//fsqp parameters for example1 of doc.

nf=1; nineqn=1; nineq=1; neqn=0; neq=1; modefsqp=100; miter=500; iprint=1;
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

bigbnd=1.e10; eps=1.e-8; epsneq=0.e0; udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=[0;0;0];
bu=[bigbnd;bigbnd;bigbnd];

x0=[0.1;0.7;0.2];  

//[1] fsqp with Scilab functions
//=======================================================
//Loading scilab functions obj32,cntr32,grob32,grcn32
getf(exdir+'/example1.sci'); 

x=fsqp(x0,ipar,rpar,[bl,bu],obj32,cntr32,grob32,grcn32)

//[2] fsqp with  C functions 
//=======================================================
exec(exdir+'/example1-src/loader.sce') 

x=fsqp(x0,ipar,rpar,[bl,bu],'obj32','cntr32','grob32','grcn32')

//nineqn non linear inequality
for j=1:nineqn
	cntr32(j,x)
end

//nineq-nineqn linear inequality
for j=nineqn+1:nineqn+(nineq-nineqn)
	cntr32(j,x)
end

//neqn nonlinear equality
for j=nineqn+(nineq-nineqn)+1:nineqn+(nineq-nineqn)+neqn
	cntr32(j,x)
end

//neq-neqn linear equality
for j=nineqn+(nineq-nineqn)+neqn+1:nineqn+(nineq-nineqn)+neqn+neq-neqn
	cntr32(j,x)
end

//[2] fsqp with  lists 
//=======================================================
getf(exdir+'/listutils.sci')

getf(exdir+'/example1_list.sci');  //Loading scilab functions.

//list of objectives fcts: 1 reg 0 SR
list_obj=list(list(f_1),list());  

//list of constraints fcts: 1 non-lin-reg-ineq (g_1) one lin-equal (A_1) 
list_cntr=list(list(g_1),list(),list(),list(),list(),list(A_1));

//gradient fcts
list_grobj=list(list(grf_1),list());
list_grcn=list(list(grg_1),list(),list(),list(),list(),list(grA_1));

x0=[0.1;0.7;0.2];

//fsqp parameters obtained by findparam:
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

//integer fsqp parameters not implicitly defined by the fcts.
modefsqp=100; miter=500; iprint=1;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
srpar=[nfsr,ncsrl,ncsrn];

//real fsqp parameters not implicitly defined by the fcts.
bigbnd=1.e10; eps=1.e-8; epsneq=0.e0; udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=[0;0;0];
bu=[bigbnd;bigbnd;bigbnd];

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)






