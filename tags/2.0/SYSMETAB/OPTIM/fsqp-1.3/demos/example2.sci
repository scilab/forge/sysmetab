function fj=objmad(j,x)
  pi=cd(1);
  sintheta=sin(pi*(8.5+j*0.5)/180);
  fj=1/15+2/15*(cos(7*pi*sintheta)+sum(cos(2*pi*sintheta*x)) ) 
endfunction

 
function gj=cnmad(j,x)
  ss=cd(2);
  select j
   case 1
    gj=ss-x(1);
   case 2
    gj=ss+x(1)-x(2);
   case 3
    gj=ss+x(2)-x(3);
   case 4
    gj=ss+x(3)-x(4);
   case 5
    gj=ss+x(4)-x(5);
   case 6
    gj=ss+x(5)-x(6);
   case 7
    gj=ss+x(6)-3.5;
  end
endfunction









  
