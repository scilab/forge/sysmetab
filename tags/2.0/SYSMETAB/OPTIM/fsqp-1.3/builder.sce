mode(-1);
lines(0);
curdir=pwd()
// specific part
libname='fsqplib'


// generic part  
// get the absolute path 

DIR=get_absolute_file_path('builder.sce');
genlib(libname,DIR+'/macros/')


// [2] src directory 

if c_link('fsqplib') 
  write(%io(2),'please do not rebuild a shared library while it is linked')
  write(%io(2),'in scilab. use ulink to unlink first');
else 
  write(%io(2),'building shared library')
  chdir(DIR+'/C-src') 
  ierr=execstr('exec builder.sce','errcatch');
  if ierr<>0 then
    chdir(curdir)
    error("Library build error: "+lasterror())
  end 
  chdir(curdir);
end 

// [3] examples directory 
write(%io(2),'building demos')
chdir(DIR+'demos') 
exec('builder.sce');
chdir(curdir);


clear fd err nams DIR libname MACROS genlib SRC OLD cmd_make tab curdir


