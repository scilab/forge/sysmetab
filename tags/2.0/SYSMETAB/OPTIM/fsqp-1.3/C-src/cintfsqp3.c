#include "stack-c.h"
#include "cfsqpusr.h"
#include <string.h>
#include "machine.h"
#include "os_specific/link.h"


extern void set_x_is_new(int n);
extern int get_x_is_new();

/* function fname=get_x_is_new */

int cintfsqp1(char *fname)
{ 
  int minlhs=1, minrhs=0, maxlhs=1, maxrhs=0;
  int un=1, xisnew;
  /*   Check rhs and lhs   */  
  CheckRhs(minrhs,maxrhs) ;
  CheckLhs(minlhs,maxlhs) ;

  CreateVar( 1, "d", &un, &un, &xisnew);
  stk(xisnew)[0]=(double) get_x_is_new();
  LhsVar(1) = 1;
  return 0;
}

/* function fname=set_x_is_new */

int cintfsqp0( char *fname)
{   
  /*    Define minlhs=1, maxlhs=1, minrhs, maxrhs   */
  static int minlhs=1, minrhs=1, maxlhs=1, maxrhs=1;
  int un=1, arg;
  /*   Check rhs and lhs   */  
  CheckRhs(minrhs,maxrhs) ;
  CheckLhs(minlhs,maxlhs) ;

  GetRhsVar(1, "i", &un,&un,&arg);
  if ( istk(arg)[0]==1 ) 
    set_x_is_new(TRUE);
  else
     set_x_is_new(FALSE);
  LhsVar(1) = 1;
  return 0;

}

