mode(-1);
// specific part
mprintf("%s\n","Loading toolbox, FSQP')

libname='fsqplib'
libtitle='FSQP: Constrained minimax optimization problems solver';

mess=[' ';' ';libtitle+' loaded';
    'execute ""fsqp_dem()"" for demonstrations']

// generic 
DIR=get_absolute_file_path('loader.sce');
execstr(libname+'=lib(""'+DIR+'/macros/'+'"")')

//[2] loader for src 
exec(DIR+'/C-src/loader.sce');

add_help_chapter(libtitle,DIR+'/man/fsqp')
add_demo(libtitle,DIR+'demos/fsqp.dem')
mprintf("%s\n",' done')

clear  DIR libname libtitle mess add_help_chapter

