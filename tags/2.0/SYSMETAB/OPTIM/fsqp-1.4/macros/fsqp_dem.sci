function fsqp_dem()
// Copyright INRIA
  oldln=lines();
  lines(0)
  path=string(fsqplib);path=strsubst(path(1),'macros','examples')

  t1=['  min f(x)=(x1+3*x2+x3)^2+4*(x1-x2)^2'
      's.t.'
      '  0<= xi (i=1:3)'
      '  x1^3-6*x2-4*x3+3 <=0'
      '  1-x1-x2-x3 ==0'];

  t2=['min max |fi(x)|'
      'x   i '  
      'with'
      '-x1     + s <=0'
      'x1 - x2 + s <=0'
      'x2 - x3 + s <=0'
      ' ....'
      'x5 - x6 + s <=0'
      'x6 - 3.5+ s <=0'
      ' '
      'where'
      '                     ---6'
      '       1     2       \'
      'fi(x)=--- + ---  *      <cos(2*Pi*xj*sin(ti))+cos(t*Pi*sin(ti)))'
      '      15    15       /'
      '                     ---j=1'
      'ti = Pi/180*(8.5+0.5*i) ,i=1:163'
      's  = 0.425'];

  t3=[' min x1*x4*(x1+x2+x3)+x3'
      ' xi'
      ' '
      'with'
      '1<=xi<=5'
      'x1*x2*x3*x4-25 >=0'
      'x1^2+x2^2+x3^2+x4^2-40=0'];

  t4=['  min  x10'
      '  x'
      'Subject to'
      '  z(ti) - (1 - x10)^2 >=0       i=1:r'
      '  (1 + x10)^2 - z(ti) >=0       i=r+1:2*r'
      '  x10^2 -z(ti)        >=0       i=2*r+1:3.5*r'
      'Where'
      '         ---9                     ---9  '             
      '         \                  2     \               2'
      '  z(t)= ( < xk*cos(k*t)  )   +  ( < xk*sin(k*t)  )'
      '         /                        /              '    
      '         ---k=1                   ---k=1          '
      'and'
      ' '
      '  ti = Pi*(i-1)*0.025               i=1:r'
      '  ti = Pi*(i-1-r)*0.025             i=r+1:2*r'
      '  ti = Pi*(1.2+(i-1-2*r)*0.2)*0,25  i=2*r+1:3.5*r'
      ' '
      '  r=100'
      'Warning this problem is time consuming, Be patient!'];

  t5=['Example of discretized semi-infinite program'
      ' Solve'
      '   min 1/3*x1^2+x2^2+x1/2'
      ' s.t. '
      '   x2^2 - x2+x1*t^2 - (1 - x1^2*t^2)^2 >=0  t in [0,1]']


  pblist=string(1:5)

  while %t then
    num=x_choose(pblist',['Click to choose one of the problem';
		    'described in the fsqp documentation']);
    if num==0 then 
      lines(oldln(1))
      return
    else
      if x_message(evstr('t'+string(num)),['Ok','Cancel'])==1 then
	dir = getcwd()
	chdir(path)
	exec('example'+string(num)+'.sce')
	chdir(dir)
      end
    end
  end
endfunction
