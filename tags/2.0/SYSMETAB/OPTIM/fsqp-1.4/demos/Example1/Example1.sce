mode(-1)
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("Example1.sce");
  methods=['Scilab Code','C code',..
	   'Using lists']
sz=[244,114];
fsqp_demo_gui(demopath,'example1',sz,methods)
