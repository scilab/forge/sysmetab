mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 1 of the fsqp documentation

//The objective function
function fj=obj32(j,x)
  fj=(x(1)+3*x(2)+x(3))^2+4*(x(1)-x(2))^2;
endfunction

//inequality and equality constraints
function gj=cntr32(j,x)
  select j
   case 1
    gj=x(1)^3-6*x(2)-4*x(3)+3;
   case 2   
    gj=1-sum(x);
  end
endfunction

//gradient of the objective function
function gradf=grob32(j,x)
  fa=2*(x(1)+3*x(2)+x(3));
  fb=8*(x(1)-x(2));
  gradf=[fa+fb,3*fa-fb,fa];
endfunction

//gradient of the constraints
function gradgj=grcn32(j,x)
  select j
   case 1
    gradgj=[3*x(1)^2,-6,-4];
   case 2
    gradgj=[-1,-1,-1];
  end
endfunction

//state boundary constraint
bigbnd=1.e10; //no bound value
bl=[0;0;0];
bu=[bigbnd;bigbnd;bigbnd];

//fsqp parameters 
nf=1; nineqn=1; nineq=1; neqn=0; neq=1; 
modefsqp=100; miter=500; iprint=0;
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

 eps=1.e-8; epsneq=0.e0; udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];


x0=[0.1;0.7;0.2];  
f=obj32(1,x0)
C=[cntr32(1,x0);cntr32(2,x0)]
mprintf('%s\n',['---------------------------Quadratic problem with fsqp(Scilab code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Value of the constraints :       '+sci2exp(C,0)])
timer();
x=fsqp(x0,ipar,rpar,[bl,bu],obj32,cntr32,grob32,grcn32)
t=timer();
f=obj32(1,x)
C=[cntr32(1,x);cntr32(2,x)]
mprintf('%s\n',['Final values'
		'   Value of the  solution:          '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Value of the constraints :       '+sci2exp(C,0)
                '   Time:                            '+sci2exp(t,0)
	       ])

