function f=allobj(x)
  f=[];
  for k=1:nf0;
    f_k=null();
    f_k=list_obj(1)(k);
    f=[f;f_k(x)];
  end
  for k=1:nfsr;
    F_k=null();
    F_k=list_obj(2)(k)
    f=[f;F_k(x)];
  end
endfunction
