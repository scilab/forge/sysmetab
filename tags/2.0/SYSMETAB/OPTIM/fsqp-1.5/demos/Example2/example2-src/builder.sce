// builder for a shared library for example2.c functions 
//=======================================================
ex_path=get_absolute_file_path('builder.sce')
// functions to be added to the call table 
link_name =["objmad","cnmad"] ;  

// the next call generates files (Makelib,loader.sce) used
// for compiling and loading ext1c and performs the compilation
tbx_build_src(link_name,'example2.c','c',ex_path,[],"","-I"+ex_path+"../../../sci_gateway/c","","","example2")
clear ex_path get_absolute_file_path link_name tbx_build_src
