mode(-1)
//[2] fsqp with  C functions 
//=======================================================
exec(get_absolute_file_path('example5.3.sce')+'example5-src/loader.sce') 
modefsqp=100;
iprint=0;
miter=500;
bigbnd=1.e10;
eps=1.e-4;
epsneq=0.e0;
udelta=0.e0;
nf=1;
neqn=0;
nineqn=1;nineq=1;
ncsrn=1;
ncsrl=0;
mesh_pts=101;
neq=0;nfsr=0;
   
bl=[-bigbnd;-bigbnd];
bu=-bl;

x0=[-1;-2];

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];
srpar=[nfsr,ncsrl,ncsrn];


// All C functions
//=======================================================
format(6)
mprintf('%s\n',['---------------------------SIP problem with srfsqp(C code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)])
timer();
[x,info,f]=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj','cntr', ...
		  'grob','grcn');
tim=timer();
mprintf('%s\n',['Final values'
		'   Value of the solution:           '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Time :                           '+sci2exp(tim,0)
	       ])
format(10)
return
//With cntr a scilab function
function ct=cntr(j,x)
  y=(j-1)/100;
  ct=(1-(x(1)*y)^2)^2-x(2)^2+x(2)-x(1)*y^2;
endfunction

x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj',cntr,'grob','grcn')

