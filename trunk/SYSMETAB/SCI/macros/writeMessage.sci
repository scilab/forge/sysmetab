function writeMessage(message,outputname)
	doc=xmlRead(outputname+'.params.xml');
	params=xmlXPath(doc,'//smtb:params',['smtb','http://www.utc.fr/sysmetab']);
    if isfield(message,'str')
    	param=xmlElement(doc,"param");
    	param.attributes.name="error"
    	param.attributes.type="string";
    	param.content=sprintf("%s (function %s at line %d)",message.str,message.func,message.line);
    	xmlAppend(params(1),param)
	end
  xmlRemove(xmlXPath(doc,'//smtb:params/smtb:param[@name=""iterations""]',['smtb','http://www.utc.fr/sysmetab']))
  xmlRemove(xmlXPath(doc,'//smtb:params/smtb:param[@name=""time""]',['smtb','http://www.utc.fr/sysmetab']))
  xmlRemove(xmlXPath(doc,'//smtb:params/smtb:param[@name=""info""]',['smtb','http://www.utc.fr/sysmetab']))   
  param=xmlElement(doc,"param");
	param.attributes.name="iterations"
	param.attributes.type="number";
	param.content=string(message.iter);
	xmlAppend(params(1),param)
	param.attributes.name="time"
	param.attributes.type="number";
	param.content=string(message.time);
	xmlAppend(params(1),param)
	param.attributes.name="info"
	param.attributes.type="string";
	param.content=string(message.info);
	xmlAppend(params(1),param)
  xmlWrite(doc)
  xmlDelete(doc)
endfunction
