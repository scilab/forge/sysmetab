function [residual,X,y,h,omega,Sypm2_e_label]=solveStat(v,Xinp,M,b,cum)
    //
    // Solve the state equation cascade
    //
    h=list();
    X=Xinp;
    for i=1:length(cum)
        [X(cum(i)),h(i)]=solveStatWeight(v,X,M(i),b(i));
    end
    //
    // Compute the simulated measurements
    //
    y_unscaled=Cx*X;
    //
    // Compute optimal scaling factors (simple least squares problem)
    //
    omega=ones(nb_group,1)
    for i=auto_scales_ind // for groups with scale="auto"
      ind=meas_ind_group(i);
      yui=y_unscaled(ind)./Sy(ind);
      ymi=ymeas(ind)./Sy(ind);
      omega(i)=sum(yui.*ymi)/norm(yui)^2;
    end
    y=omega(omega_ind_meas).*y_unscaled;
    //
    // Compute the residual
    //
    e_label=y-ymeas;
    Sypm2_e_label=Sypm2.*e_label;
    residual=Sypm2_e_label.*e_label;
    //
endfunction

function [residual,gradq,X,y]=solveStatAndGradDirect(v,Xinp,M,b,cum,dv_dw,W)
    //
    [residual,X,y,h,omega,Sypm2_e_label]=solveStat(v,Xinp,M,b,cum);
    //
    // Compute the state derivatives
    //
    [dgdv_nz,db_dx_nz]=forwardStatDerivatives(v,X,dgdv,db_dx)
    dX_dq=zeros(size(X,1),size(W,2));
    dgdq=full(spset(dgdv.value,dgdv_nz))*dv_dw*W
    //
    dX_dq(cum(1),:)=-umf_lusolve(h(1),dgdq(cum(1),:));
    umf_ludel(h(1));
    for i=2:length(cum)
        dX_dq(cum(i),:)=-umf_lusolve(h(i),dgdq(cum(i),:)+spset(db_dx(i).value,db_dx_nz(i))*dX_dq);
        umf_ludel(h(i));
    end
    //
    // Compute the gradient (forward method)
    //
    Omega_Sypm2_e_label=omega(omega_ind_meas).*Sypm2_e_label;
    gradq=2*(Omega_Sypm2_e_label'*Cx*dX_dq)';
    //
endfunction

function [residual,X,y,dy_dq,omega]=solveStatAndDerivative(v,Xinp,M,b,cum,dv_dw,W)
    //
    [residual,X,y,h,omega]=solveStat(v,Xinp,M,b,cum)
    y_unscaled=Cx*X;
    // Compute the state derivatives
    //
    [dgdv_nz,db_dx_nz]=forwardStatDerivatives(v,X,dgdv,db_dx)
    dX_dq=zeros(size(X,1),size(W,2));
    dgdq=full(spset(dgdv.value,dgdv_nz))*dv_dw*W;
    //
    dX_dq(cum(1),:)=-umf_lusolve(h(1),dgdq(cum(1),:));
    umf_ludel(h(1));
    for i=2:length(cum)
        dX_dq(cum(i),:)=-umf_lusolve(h(i),dgdq(cum(i),:)+spset(db_dx(i).value,db_dx_nz(i))*dX_dq);
        umf_ludel(h(i));
    end
    //
    // Compute the derivative (forward method)
    //
    dy_unscaled_dq=Cx*dX_dq;
    dy_dq=spdiag(omega(omega_ind_meas))*dy_unscaled_dq;
    //
    // Finish the computation of dy_dq for observations belonging to groups with scale="auto"
    for i=auto_scales_ind
      indg=meas_ind_group(i);
      yui=y_unscaled(indg)./Sy(indg);
      domega_i_dq=-((2*y(indg)-ymeas(indg)).*Sypm2(indg))'*dy_unscaled_dq(indg,:)/norm(yui)^2;
      dy_dq(indg,:)=dy_dq(indg,:)+y_unscaled(indg)*domega_i_dq
    end
    //
endfunction 

function [residual,gradq,X,y]=solveStatAndGradAdjoint(v,Xinp,M,b,cum,dv_dw,W)
    //
    [residual,X,y,h,omega,Sypm2_e_label]=solveStat(v,Xinp,M,b,cum)
    //
    // Compute the state derivatives
    //
    [dgdvt_nz,db_dxt_nz]=adjointStatDerivatives(v,X,dgdvt,db_dxt)
    //
    Ct_Omega_Sypm2_e_label=Cxt*(omega(omega_ind_meas).*Sypm2_e_label);
    //
    p=zeros(X);
    ncum=length(cum);
    p(cum(ncum))=umf_lusolve(h(ncum),Ct_Omega_Sypm2_e_label(cum(ncum)),"A''x=b");
    umf_ludel(h(ncum));
    for i=ncum-1:-1:1
        p(cum(i))=umf_lusolve(h(i),Ct_Omega_Sypm2_e_label(cum(i))-spset(db_dxt(i).value,db_dxt_nz(i))*p,"A''x=b");
        umf_ludel(h(i));
    end
    //
    // Compute the gradient (adjoint method)
    //
    gradq=-2*(dv_dw*W)'*spset(dgdvt.value,dgdvt_nz)*p;
    //
endfunction

function [dgdv_nz,db_dx_nz]=forwardStatDerivatives(v,X,dgdv,db_dx)
    //
    // Derivatives needed for the forward computation of gradient
    //
    dgdv_nz=dgdv.t*(X(dgdv.m1).*X(dgdv.m2));
    db_dx_nz=list();
    for i=2:length(db_dx)
        db_dx_nz(i)=db_dx(i).t*(v(db_dx(i).m2).*X(db_dx(i).m1));
    end
    //
endfunction

function [dgdvt_nz,db_dxt_nz]=adjointStatDerivatives(v,X,dgdvt,db_dxt)
    //
    // Derivatives needed for the adjoint computation of gradient
    //
    dgdvt_nz=dgdvt.t*(X(dgdvt.m1).*X(dgdvt.m2));
    db_dxt_nz=list();
    for i=1:length(db_dxt)
        db_dxt_nz(i)=db_dxt(i).t*(v(db_dxt(i).m2).*X(db_dxt(i).m1));
    end
    //
endfunction

function [Xn,Mn_handle]=solveStatWeight(v,X,Mn,bn)
  //
  // Weight n cumomers
  //
  Mn_handle=umf_lufact(sparse(Mn.ij,Mn.t*v(Mn.m1),Mn.size));
  Xn=umf_lusolve(Mn_handle,-full(spset(bn.value,bn.t*(X(bn.m1).*X(bn.m2).*v(bn.m3)))));
endfunction

