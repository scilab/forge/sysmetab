function time_struct=time_discr(scheme,tmeas,q,m,nsteps,TOL) 
  nsteps=max(nsteps,length(tmeas))
  if exists("TOL","local")
    [t,err]=stepAdapt(scheme,Phi(W*q+w0,0),m,Xinp,M,b,cum,tmeas,pool_ind_weight,TOL,nsteps,tmeas(1));
  else
    t=[0;tmeas(:)]
  end
  t($+1)=t($)+t($)-t($-1) // one more step for the adjoint state (no measurement at terminal time)
  
  time_struct=time_discr_from_t(scheme,tmeas,t) 
endfunction

function time_struct=time_discr_from_t(scheme,tmeas,t) 
  m=length(tmeas)
  s=size(scheme.a,1) // number of RK stages
  shape=zeros(s,m)
  ind_meas=[]
  for k=1:length(t)-1
    theta=(tmeas(:)-t(k))/(t(k+1)-t(k))
    kmeas=find(theta>=0 & theta<1) 
    if ~isempty(kmeas)
      shape(:,kmeas)=scheme.d*cumprod(repmat(theta(kmeas)',scheme.order,1),1)
      ind_meas=[ind_meas; kmeas(1) kmeas($) k]
    end
  end
  time_struct=struct("t",t(1:ind_meas($)+1),"tmeas",tmeas,"ind",ind_meas,"shape",shape)
endfunction

function t2=dub(t)
    t2=zeros(1,2*size(t,"*")-1)
    t2(1:2:$)=t
    t2(2:2:$)=(t(1:$-1)+t(2:$))/2
endfunction
