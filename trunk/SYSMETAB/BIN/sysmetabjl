#!/bin/bash
#
# Author   :   Stephane Mottelet and Georges Sadaka
# Date     :   Lun 13 avr 2015 16:23:00 CEST
# Project  :   SYSMETAB/MetaLipPro-PL1/PL2
#
# Generation program of resolution/optimization, steady case
#
SYSMETAB=$( cd "$( dirname "$0" )/.." && pwd )
XSLSYSPREP=$SYSMETAB/XSL/fmlsys_prep
XSLSYSGEN=$SYSMETAB/XSL/fmlsys_gen
XSLCOMMON=$SYSMETAB/XSL/common
BIN=$SYSMETAB/BIN

function help()
{
    echo ""
    echo "Usage: sysmetabjl [options] filename (without extension)"
    echo ""
    echo "Description : Generation program of resolution/optimization Scilab code, stationary case"
    echo ""
    echo "Options (for multiple choice default value is the first)"
    echo ""
    echo "    --output=filename                output filename (without extension)"
    echo "    --nocalc                         generate Scilab code but do not execute it."
    echo "    --full                           no cumomer graph simplification"
    echo "    --nooptimize                     no optimization"
    echo "    --gradient=adjoint|direct        method for computing the gradient"    
    echo "    --freefluxes=user|auto|multi:n   freefluxes initialization"
    echo "    --stepsize                       stepsize value for discrete time integration (non-stationary configurations)"
    echo "    --stats=lin|mc:n                 linear or Monte Carlo statistics"
    echo "    --scaling=user|yes|no            scaling of measurement groups"
    echo "    --reg=REG                        set regularization parameter of cost function (default=1e-5)"
    echo "    --regphi=TOL                     set smoothing parameter for the (net,xch)->(f,b) transformation (default=0)"
    echo "    --tolgrad=TOL                    set upper bound for the norm of projected gradient (default=1e-5)"
    echo "    --maxiter=N                      set maximum number of iterations of optimizer (default=1000)"
    echo "    --maxxch=MAXXCH                  maximum value for xch flux (default=999)"
    echo "    --mininout=MININOUT              minimum value for input/output net flux (default=0)"
    echo "    --rebuild                        rebuild Scilab code from scratch"
    echo "    --verbose 1|0                    verbosity level"
    echo "    -h, --help                       show this help message and exit"
    echo ""
}

tab=( $@ ); # put all parameters entries in a vector

if [[ $1 == "-h" || $1 == "--help" || $1 == "" ]]
then
    help
    exit
	else
		file_name=${tab[$#-1]};
		output=${tab[$#-1]};
fi

timer="no"
maxweight="999"
verb_level="1"
gradient="adjoint"
minimize="yes"
optimize="yes"
scaling="user"
zc="no"
optmeth="fsqp"
max_xch="999"
min_inout="0"
calc="yes"
eps_grad="1e-6"
eps_reg="1e-6"
eps_phi="0"
max_iter="1000"
minstdev="-1"
stats="lin"
freefluxes="user"
stepsize="0"
rebuild="no"
file_name=${tab[$#-1]};
output=${tab[$#-1]};

#
# Preliminary transformation
#

for ((i=0; i<$#-1 ; i++));
do
	case ${tab[i]} in
        -h | --help)
            help
            exit
            ;;
        --maxweight=*)
          maxweight="${tab[i]#*=}"
          ;;
        --gradient=*)
            gradient="${tab[i]#*=}"
            ;;
        --full)
          minimize="no"
          ;;
        --timer)
          timer="yes"
          calc="no"
          ;;
        --nooptimize)
            optimize="no"
            ;;
        --scaling=*)
           scaling="${tab[i]#*=}"
            ;;
        --nocalc)
        calc="no"
		        ;;
        --rebuild)
		        rebuild="yes"
		        ;;
        --output=*)
            output="${tab[i]#*=}"
            ;;
        --algorithm=*)
            optmeth="${tab[i]#*=}"
            ;;
      --reg=*)
	          eps_reg="${tab[i]#*=}"
	          ;;
      --regphi=*)
	          eps_phi="${tab[i]#*=}"
	          ;;
      --tolgrad=*)
	          eps_grad="${tab[i]#*=}"
	          ;;
        --maxxch=*)
            max_xch="${tab[i]#*=}"
            ;;
        --mininout=*)
          min_inout="${tab[i]#*=}"
          ;;
        --maxiter=*)
            max_iter="${tab[i]#*=}"
            ;;
        --verbose=*)
            verb_level="${tab[i]#*=}"
            ;;
        --stats=*)
            stats="${tab[i]#*=}"
            ;;
        --zc)
            zc="yes"
            ;;
		--freefluxes=*)
		        freefluxes="${tab[i]#*=}"
						;;
        --stepsize=*)
          stepsize="${tab[i]#*=}"
          ;;    
        --minstdev=*)
  	        minstdev="${tab[i]#*=}"
  					;;
        *)
            echo "ERROR: unknown parameter \"${tab[i]}\""
            help
            exit 1
            ;;
    esac
#    shift
done


dev=/dev/stdout
if [ "$verb_level" == "0" ]
then
	dev="/dev/null"
fi
	
if [ ! -f $file_name.fml ]
then
	echo "$file_name.fml : file not found\n"
	exit
fi

# XML file of parameters

xsltproc -o $output.params.xml \
	--stringparam file `basename $file_name` \
	--stringparam sysmetab $SYSMETAB\
	--stringparam timer $timer\
  --stringparam minimize $minimize\
  --stringparam maxweight $maxweight\
  --stringparam verb_level $verb_level\
	--stringparam output $output\
	--stringparam eps_grad $eps_grad\
	--stringparam eps_reg $eps_reg\
	--stringparam eps_phi $eps_phi\
	--stringparam max_iter $max_iter\
  --stringparam max_xch $max_xch\
  --stringparam min_inout $min_inout\
  --stringparam min_stdev $minstdev\
	--stringparam zc $zc\
	--stringparam optimize $optimize\
	--stringparam optimize_method $optmeth\
  --stringparam scaling $scaling\
  --stringparam gradient $gradient\
	--stringparam computation $calc\
	--stringparam stats $stats\
	--stringparam freefluxes $freefluxes\
  --stringparam stepsize $stepsize\
	--stringparam date "$(date)"\
	$XSLSYSGEN/fmlsys_gen_params.xsl\
	$file_name.fml

if [ "$rebuild" == "yes" ]
then
	rm -f $file_name.old
fi

md5cmd="md5sum" # md5sum is named differently under MacOSX 
if [ $(uname) == "Darwin" ]
then
	md5cmd="md5"
fi

RN_LM=$(xsltproc $XSLSYSGEN/RN_LM.xsl $file_name.fml | $md5cmd)
if [ ! -f $file_name.old ]
then
	RN_LM_OLD=""
else
  RN_LM_OLD=$(xsltproc $XSLSYSGEN/RN_LM.xsl $file_name.old | $md5cmd)
fi

if [ "$RN_LM" != "$RN_LM_OLD" ]
	then
	cp $file_name.fml $file_name.old

	xsltproc --stringparam verb_level $verb_level\
	--maxdepth 8192 $XSLSYSPREP/fmlsys_prep_1.xsl\
	$file_name.fml | \
	xsltproc -o $file_name.step2.xml $XSLSYSPREP/fmlsys_prep_2.xsl -

	if test $minimize = yes # Cumomer graph simplification by "backtracing"
		then
		xsltproc -o $file_name.graph\
		--stringparam verb_level $verb_level\
		$XSLSYSGEN/fmlsys_gen_graph.xsl\
		$file_name.step2.xml
		# saxon is used instead of xsltproc because of its terminal recursion handling
		$BIN/saxon-xslt $file_name.graph $XSLSYSGEN/minimize_graph.xsl verb_level=$verb_level > $file_name.nodes

		xsltproc -o $file_name.step3.xml\
		--stringparam nodes $file_name.nodes\
		--stringparam verb_level $verb_level\
    --stringparam output $output\
		$XSLSYSPREP/fmlsys_prep_3.xsl $file_name.step2.xml 
	elif test $minimize = no
		then
		xsltproc -o $file_name.step3.xml\
		--stringparam verb_level $verb_level\
    --stringparam output $output\
		$XSLSYSPREP/fmlsys_prep_3.xsl\
		$file_name.step2.xml
	fi
	#
	# Final generation of specific Scilab functions for the network
	#
	xsltproc -o $file_name.state.xml\
	--stringparam verb_level $verb_level\
  --stringparam output $output\
  --stringparam storage 2\
	$XSLSYSGEN/fmlsys_gen_mathml_solve_stat_state_equation.xsl\
	$file_name.step3.xml

		xsltproc -o ${file_name}_solve_state_equation.jl\
		--stringparam verb_level $verb_level\
    --stringparam output $output\
		$XSLCOMMON/julia_sys_text.xsl\
		$file_name.state.xml
	fi

	xsltproc -o $output.step4.xml\
	--stringparam output $output\
	$XSLSYSGEN/fmlsys_gen_mathml_solve_stat.xsl\
	$file_name.step3.xml

	xsltproc -o $output.jl\
	--stringparam verb_level $verb_level\
  --stringparam output $output\
  $XSLCOMMON/julia_sys_text.xsl\
	$output.step4.xml

	if test $calc = yes
		then
		scilab -nwni -f ${output}_direct.sce > $dev 2>&1
		xsltproc $XSLSYSGEN/fwd_report.xsl $output.fwd > $dev 2>&1
	else 
		echo "Everything is OK."
		echo "Now you must run \"${output}_direct.sce\" in scilab in order to see all results."
		echo ""
	fi
