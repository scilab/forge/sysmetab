<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Wed Nov 13 16:00:00 CET 2013
    Projet  :   PIVERT/Metalippro-PL1
-->

<!--
    This stylesheet prepare the sbml file by taking into account the stoichiometry grater than n (\in N) and duplicated n times in the speciesReference before transform the hole file into a fml file.
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func">
  
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="sbml:speciesReference[@stoichiometry &gt; 1]">
		<xsl:variable name="speciesReference">
          <speciesReference species="{@species}" xmlns="http://www.sbml.org/sbml/level2/version4"/>
		</xsl:variable>
		<xsl:for-each select="str:split(str:padding(@stoichiometry,'.'),'')">
			<xsl:copy-of select="$speciesReference"/>
		</xsl:for-each>
  </xsl:template>
  
</xsl:stylesheet>