<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Tue Oct 08 17:00:00 CET 2013
    Projet  :   PIVERT/Metalippro-PL1
-->

<!--
    This stylesheet add atoms number to a pool and add zeros data for measurement
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:exslt="http://exslt.org/common"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"    
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m math exslt">
  
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->
  	
  <xsl:key name="matchREDUCTPRODUCT" match="f:reduct|f:rproduct" use="@id"/>
  
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="f:fluxml">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </fluxml>
  </xsl:template>
  
  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="f:pool">
    <pool atoms="{string-length(key('matchREDUCTPRODUCT',@id)[1]/@cfg)}"  id="{@id}" xmlns="http://www.13cflux.net/fluxml"/>
  </xsl:template>

  <xsl:template match="f:measurement">
    <measurement xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
      <data>
        <xsl:for-each select="f:model/f:fluxmeasurement/f:netflux">  
          <datum id="{@id}" stddev="1">0</datum> 
        </xsl:for-each>
        <xsl:for-each select="f:model/f:labelingmeasurement/f:group">
          <xsl:variable name="counter" select="position()"/>
          <xsl:variable name="ID" select="@id"/>
          <xsl:choose>
            <xsl:when test="contains(f:textual,']')">
              <xsl:for-each select="str:split(substring-after(normalize-space(translate(.,'&#10;&#13;','')),'#M'),',')">
                <datum id="{$ID}" stddev="1" weight="{.}">0</datum> 
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
              <datum id="{$ID}" stddev="1">0</datum> 
            </xsl:otherwise>        
          </xsl:choose>
        </xsl:for-each>
      </data>
    </measurement>
  </xsl:template>
  
</xsl:stylesheet>