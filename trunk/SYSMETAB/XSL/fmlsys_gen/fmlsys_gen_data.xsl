<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- Cette feuille de style a pour but de générer différentes fonctions spécifiques
  d'un modèle donné sous forme FML -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- Output file name -->
  <xsl:param name="output"/>
	
  <xsl:param name="language">scilab</xsl:param>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <xsl:variable name="freefluxes" select="exslt:node-set($params)[@name='freefluxes']"/>
  <xsl:variable name="nv" select="count(//f:reactionnetwork/f:reaction)"/>
  <xsl:variable name="np" select="count(//f:reactionnetwork/f:metabolitepools/f:pool)"/>

  <!-- Mathml generation of the code, this templates is common between stationnary case or dynamical one -->
  
  <!-- find all label input -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>
  
  <!-- find all f:pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>
  
  <!-- find all f:rproduct from forward f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

  <!-- find all f:reduct from forward f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:reaction -->
  <xsl:key name="REACTION" match="f:reaction" use="@id"/>
  
  <!-- find all f:datum -->
  <xsl:key name="DATUM_ROW_WEIGHT" match="f:datum" use="concat(@id,'_',@row,@weight)"/>
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <!-- find all Mass Spectrometry (MS) -->
  <xsl:key name="MS" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'[')"/>

  <!-- find all Label Measurement (LM) from f:configuration -->
  <xsl:key name="LM" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'#')"/>

  <!-- find all f:poolmeasurement/f:textual -->
  <xsl:key name="poolmeasurement" match="f:poolsize" use="f:textual"/>

  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <xsl:apply-templates/>
  </xsl:template>
  
  <!-- THIS STYLESHEET HAS TO BE CUT PIECES !!!!!!!!! -->
  
  <xsl:template match="f:reactionnetwork">
    <!-- Building matrices of fluxes names and ids -->
    <!-- Matrix of metabolite names -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>metabolite_names</ci>
      <list separator=";">
        <xsl:for-each select="f:metabolitepools/f:pool">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select='@id'/>
          </string>
        </xsl:for-each>
      </list>
    </apply>
    <!-- Matrix of fluxes names -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_names</ci>
      <list separator=";">
        <xsl:for-each select="f:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(@id,'.n')"/>
          </string>
        </xsl:for-each>
        <xsl:for-each select="f:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(@id,'.x')"/>
          </string>
        </xsl:for-each>
      </list>
    </apply>

    <!-- Matrix of fluxes ids -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_ids</ci>
      <ci>flux_names</ci>
    </apply>
  </xsl:template>
  
  <xsl:template match="f:configuration">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:measurement"> 
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:model"> 
    <xsl:apply-templates/>
    <xsl:if test="not(f:fluxmeasurement)">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>E</ci>
        <vector/>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>wmeas</ci>
        <vector/>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Sv</ci>
        <vector/>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Svpm2</ci>
        <vector/>
      </apply>
    </xsl:if>
    <xsl:if test="not(f:poolmeasurement)">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Em</ci>
        <vector/>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>mmeas</ci>
        <vector/>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Sm</ci>
        <vector/>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Smpm2</ci>
        <vector/>
      </apply>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="f:simulation">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>ffnames</ci>
      <list separator=";">
        <xsl:for-each select="f:variables/f:fluxvalue">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:choose>
              <xsl:when test="@type='net'">
                <xsl:value-of select="concat(@flux,'.n')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat(@flux,'.x')"/>
              </xsl:otherwise>
            </xsl:choose>
          </string>
        </xsl:for-each>
      </list>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>ffvalues</ci>
      <matrix>
        <xsl:for-each select="f:variables/f:fluxvalue">
          <matrixrow>
            <cn>
              <xsl:value-of select="."/>
            </cn>
          </matrixrow>
        </xsl:for-each>
      </matrix>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>mnames</ci>
      <list separator=";">
        
        <xsl:for-each select="f:variables/f:poolvalue">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="@pool"/>
          </string>
        </xsl:for-each>
        
        <!-- output reaction --> 
        <xsl:for-each select="//f:reaction[not(f:rproduct)]"> <!-- if the output metabolite is not measured (via labeling or poolsize)--> <!-- arbitrary value -->
          <xsl:variable name="metab" select="f:reduct/@id"/>
          <xsl:if test="(not(key('MS',$metab) or  key('LM',$metab) or key('poolmeasurement',$metab))) and (key('POOL',$metab)/@atoms &gt; 0) and not(key('REDUCT',$metab)[../@id!=current()/@id])">
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="key('POOL',f:reduct/@id)/@id"/>
            </string>      
          </xsl:if>
          </xsl:for-each>
        
        <xsl:for-each select="//f:pool[key('INPUT',@id)]">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="@id"/>
          </string>
        </xsl:for-each>
        
        <xsl:for-each select="//f:pool[@atoms='0']"> <!-- pool used for flux stoechiometry only  (not carying carbon atoms) -->
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="@id"/>
          </string>
        </xsl:for-each>
        
        <xsl:for-each select="//f:pool[@size]"> <!-- pool with fixed size -->
          <matrixrow>
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="@id"/>
            </string>
          </matrixrow>          
        </xsl:for-each>
        
      </list>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>mvalues</ci>
      <matrix>
        
        <xsl:for-each select="f:variables/f:poolvalue">
          <matrixrow>
            <cn>            
              <xsl:value-of select="text()"/>
            </cn>
            <xsl:choose>
              <xsl:when test="@lo">
                <cn><xsl:value-of select="@lo"/></cn>
              </xsl:when>
              <xsl:otherwise>
                <cn>
                  <xsl:value-of select="exslt:node-set($params)[@name='min_p']"/>
                </cn>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="@hi">
                <cn><xsl:value-of select="@hi"/></cn>
              </xsl:when>
              <xsl:otherwise>
                <cn>
                  <xsl:value-of select="exslt:node-set($params)[@name='max_p']"/>
                </cn>
              </xsl:otherwise>
            </xsl:choose>
          </matrixrow>
        </xsl:for-each>

        <!-- output reaction --> 
        <xsl:for-each select="//f:reaction[not(f:rproduct)]"> <!-- if the output metabolite is not measured (via labeling or poolsize)--> <!-- arbitrary value -->
          <xsl:variable name="metab" select="f:reduct/@id"/>
          <xsl:if test="(not(key('MS',$metab) or  key('LM',$metab) or key('poolmeasurement',$metab))) and (key('POOL',$metab)/@atoms &gt; 0) and not(key('REDUCT',$metab)[../@id!=current()/@id])">
             <matrixrow>
               <cn>1</cn>
               <cn>1</cn>
               <cn>1</cn>
             </matrixrow>
          </xsl:if>
        </xsl:for-each>
        
        <xsl:for-each select="//f:pool[key('INPUT',@id)]">
          <matrixrow>
            <cn>1</cn> <!-- arbitrary value -->
            <cn>1</cn>
            <cn>1</cn>
          </matrixrow>          
        </xsl:for-each>
        
        <xsl:for-each select="//f:pool[@atoms='0']"> <!-- pool used for flux stoechiometry only  (not carying carbon atoms) -->
          <matrixrow>
            <cn>1</cn>
            <cn>1</cn>
            <cn>1</cn>
          </matrixrow>          
        </xsl:for-each>

        <xsl:for-each select="//f:pool[@size]"> <!-- pool with fixed size -->
          <xsl:variable name="size">
            <xsl:choose>
              <xsl:when test="@size &lt; exslt:node-set($params)[@name='min_p']">
                <xsl:value-of select="exslt:node-set($params)[@name='min_p']"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@size"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <matrixrow>
            <cn><xsl:value-of select="$size"/></cn>
            <cn><xsl:value-of select="$size"/></cn>
            <cn><xsl:value-of select="$size"/></cn>
          </matrixrow>          
        </xsl:for-each>
      </matrix>
    </apply>
  </xsl:template>

<xsl:template match="f:labelingmeasurement[../../../@stationary='false']"> 
  <comment xmlns="http://www.utc.fr/sysmetab"> time vector</comment>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>tmeas</ci>
    <vector>
      <xsl:for-each select="smtb:times/smtb:time">
        <cn>
          <xsl:value-of select="."/>
        </cn>
      </xsl:for-each>
    </vector>
  </apply>

  <comment xmlns="http://www.utc.fr/sysmetab"> ymeas and Sy matrices (non-stationary measurements)</comment> 
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>ymeas</ci>
    <matrix>
      <xsl:for-each select="f:group">
        <xsl:for-each select="smtb:index">
          <xsl:variable name="keyvalue">
            <xsl:value-of select="concat(../@id,'_',.)"/>
          </xsl:variable>
          <matrixrow>
            <xsl:for-each select="../../smtb:times/smtb:time">
              <xsl:choose>
                <xsl:when test="key('DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]">
                  <cn>
                    <xsl:value-of select="key('DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]"/>
                  </cn>
                </xsl:when>
                <xsl:otherwise>
                  <cn>0</cn>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </matrixrow>
        </xsl:for-each>
      </xsl:for-each>
    </matrix>
  </apply>
  
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>Sy</ci>
    <matrix>
      <xsl:for-each select="f:group">
        <xsl:for-each select="smtb:index">
          <xsl:variable name="keyvalue">
            <xsl:value-of select="concat(../@id,'_',.)"/>
          </xsl:variable>
          <matrixrow>
            <xsl:for-each select="../../smtb:times/smtb:time">
              <xsl:choose>
                <xsl:when test="key('DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]">
                  <cn>
                    <xsl:value-of select="key('DATUM_ROW_WEIGHT',$keyvalue)[@time=current()/.]//@stddev"/>
                  </cn>
                </xsl:when>
                <xsl:otherwise>
                  <cn><infinity/></cn>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </matrixrow>
        </xsl:for-each>
      </xsl:for-each>
    </matrix>
  </apply>
      
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>Sypm2</ci>
    <apply>
      <power type="array"/>
      <ci>Sy</ci>
      <cn>-2</cn>
    </apply>
  </apply>
  
  <xsl:call-template name="groups"/>
</xsl:template>

<xsl:template match="f:labelingmeasurement[../../../@stationary='true']"> 
  <comment xmlns="http://www.utc.fr/sysmetab"> ymeas and Sy vectors (stationary measurements)</comment>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>ymeas</ci>
    <vector>
      <xsl:for-each select="f:group">
        <xsl:for-each select="smtb:index">
          <xsl:choose>
            <xsl:when test="key('DATUM_ROW_WEIGHT',concat(../@id,'_',.))">
                <cn>
                  <xsl:value-of select="key('DATUM_ROW_WEIGHT',concat(../@id,'_',.))"/>
                </cn>
            </xsl:when>
            <xsl:otherwise>
              <cn>0</cn>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:for-each>
    </vector>
  </apply>
  
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>Sy</ci>
    <vector>
      <xsl:for-each select="f:group">
        <xsl:for-each select="smtb:index">
          <xsl:choose>
            <xsl:when test="key('DATUM_ROW_WEIGHT',concat(../@id,'_',.))">
                <cn>
                  <xsl:value-of select="key('DATUM_ROW_WEIGHT',concat(../@id,'_',.))/@stddev"/>
                </cn>
            </xsl:when>
            <xsl:otherwise>
              <cn><infinity/></cn>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:for-each>
    </vector>
  </apply>
  
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>Sypm2</ci>
    <apply>
      <power type="array"/>
      <ci>Sy</ci>
      <cn>-2</cn>
    </apply>
  </apply>
  
  <xsl:call-template name="groups"/>
</xsl:template>

<xsl:template name="netflux_coeff">
  <xsl:param name="string"/>
  <xsl:choose>
    <xsl:when test="contains($string,'*')">
      <matrix-assignment id="E" row="{position()}" col="{key('REACTION',substring-after($string,'*'))/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:value-of select="substring-before($string,'*')"/>
        </cn>
      </matrix-assignment>
    </xsl:when>
    <xsl:otherwise>
      <matrix-assignment id="E" row="{position()}" col="{key('REACTION',$string)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="iterate_netflux">
  <xsl:param name="string"/>
  <xsl:choose>
    <xsl:when test="contains($string,'+')">
      <xsl:call-template name="netflux_coeff">
        <xsl:with-param name="string" select="substring-before($string,'+')"/>
      </xsl:call-template>
      <xsl:call-template name="iterate_netflux">
        <xsl:with-param name="string" select="substring-after($string,'+')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="netflux_coeff">
        <xsl:with-param name="string" select="$string"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="iterate_pool">
  <xsl:param name="string"/>
  <xsl:choose>
    <xsl:when test="contains($string,'+')">
      <matrix-assignment id="Em" row="{position()}" col="{key('POOL',substring-before($string,'+'))/@number}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
      <xsl:call-template name="iterate_pool">
        <xsl:with-param name="string" select="substring-after($string,'+')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <matrix-assignment id="Em" row="{position()}" col="{key('POOL',$string)/@number}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="f:fluxmeasurement"> 
  <comment xmlns="http://www.utc.fr/sysmetab"> (Sv)^-2 array</comment>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>Svpm2</ci>
    <apply>
      <power type="array"/>
      <vector>
        <xsl:for-each select="f:netflux">
					<xsl:choose>
						<xsl:when test="key('DATUM',@id)">
              <cn>
                <xsl:value-of select="key('DATUM',@id)/@stddev"/>
              </cn>
						</xsl:when>
						<xsl:otherwise>
							<cn>
								<infinity/>
							</cn>
						</xsl:otherwise>
					</xsl:choose>
        </xsl:for-each>
      </vector>
      <apply>
        <minus/>
        <cn>2</cn>
      </apply>
    </apply>
  </apply>

  <comment  xmlns="http://www.utc.fr/sysmetab"> wmeas array</comment>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>wmeas</ci>
    <vector>
      <xsl:for-each select="f:netflux">
				<xsl:choose>
					<xsl:when test="key('DATUM',@id)">
            <cn>
              <xsl:value-of select="key('DATUM',@id)"/>
            </cn>
					</xsl:when>
					<xsl:otherwise>
						<cn>0</cn>
					</xsl:otherwise>
				</xsl:choose>
      </xsl:for-each>
    </vector>
  </apply>
  
  <comment xmlns="http://www.utc.fr/sysmetab">Matrice d'observation (flux)</comment>

  <optimize xmlns="http://www.utc.fr/sysmetab">
    <!-- We open an item "optimize" in which we will gradually build the matrix E -->
    <matrix-open id="E" 
                 cols="{2*$nv}" 
                 rows="{count(f:netflux)}"
                 type="sparse"/>
    <xsl:for-each select="f:netflux">
      <xsl:call-template name="iterate_netflux">
        <xsl:with-param name="string" select="f:textual"/>
      </xsl:call-template>
    </xsl:for-each>  
    
    <matrix-close id="E"/>
    <!-- When we will found this element in the transformation which have a goal to produce the Scilab code, 
    we combine all assignments order to the matrix "E" and we build this matrix -->
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>fluxmeasurement_textual</ci>
      <vector>
        <xsl:for-each select="f:netflux">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="f:textual"/>
          </string>
        </xsl:for-each>
      </vector>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>fluxmeasurement_id</ci>
      <vector>
        <xsl:for-each select="f:netflux">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="@id"/>
          </string>
        </xsl:for-each>
      </vector>
    </apply>
    
  </optimize>
  
</xsl:template>

<xsl:template match="f:poolmeasurement"> 
  <comment xmlns="http://www.utc.fr/sysmetab"> (Sm)^-2 array</comment>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>Smpm2</ci>
    <apply>
      <power type="array"/>
      <vector>
        <xsl:for-each select="f:poolsize">
					<xsl:choose>
						<xsl:when test="key('DATUM',@id)">
              <cn>
                <xsl:value-of select="key('DATUM',@id)/@stddev"/>
              </cn>
						</xsl:when>
						<xsl:otherwise>
							<cn>
								<infinity/>
							</cn>
						</xsl:otherwise>
					</xsl:choose>
        </xsl:for-each>
      </vector>
      <apply>
        <minus/>
        <cn>2</cn>
      </apply>
    </apply>
  </apply>

  <comment  xmlns="http://www.utc.fr/sysmetab"> mmeas array</comment>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>mmeas</ci>
    <vector>
      <xsl:for-each select="f:poolsize">
				<xsl:choose>
					<xsl:when test="key('DATUM',@id)">
            <cn>
              <xsl:value-of select="key('DATUM',@id)"/>
            </cn>
					</xsl:when>
					<xsl:otherwise>
						<cn>0</cn>
					</xsl:otherwise>
				</xsl:choose>
      </xsl:for-each>
    </vector>
  </apply>
  
  <optimize xmlns="http://www.utc.fr/sysmetab">  
    <matrix-open id="Em" 
                 cols="{$np}" 
                 rows="{count(f:poolsize)}"
                 type="sparse"/>
    <xsl:for-each select="f:poolsize">        
      <xsl:call-template name="iterate_pool">
        <xsl:with-param name="string" select="f:textual"/>
      </xsl:call-template>
    </xsl:for-each>  
    <matrix-close id="Em"/>
  </optimize>  

  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>poolmeasurement_textual</ci>
    <vector>
      <xsl:for-each select="f:poolsize">
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="f:textual"/>
        </string>
      </xsl:for-each>
    </vector>
  </apply>

  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>poolmeasurement_id</ci>
    <vector>
      <xsl:for-each select="f:poolsize">
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="@id"/>
        </string>
      </xsl:for-each>
    </vector>
  </apply>


</xsl:template>

<xsl:template name="groups">
  
	<comment xmlns="http://www.utc.fr/sysmetab">omega_ind_meas contains the group number for each measurement</comment>
  
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
		<eq/>
		<ci>omega_ind_meas</ci>
		<vector>
		  <xsl:for-each select="f:group">
        <xsl:variable name="groupnumber" select="position()"/>
        <xsl:for-each select="smtb:index">
          <cn>
					  <xsl:value-of select="$groupnumber"/>
				  </cn>
        </xsl:for-each>
      </xsl:for-each>
    </vector>
  </apply>
  
	<comment xmlns="http://www.utc.fr/sysmetab">meas_in_group(i) contains measurement numers of group i</comment>
  
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>meas_ind_group</ci>
    <apply>
      <fn><ci>s_list</ci></fn>
      <cn>
        <xsl:value-of select="count(f:group)"/>
      </cn>
    </apply>
  </apply>
  
  <xsl:for-each select="f:group">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
  		<eq/>
      <apply>
        <selector/>
        <ci type="vector">meas_ind_group</ci>
        <cn><xsl:value-of select="position()"/></cn>
      </apply>
  		<vector>
  			<xsl:for-each select="smtb:index">
  			  <cn>
  				  <xsl:value-of select="1+count(preceding::smtb:index)"/>
  				</cn>
        </xsl:for-each>
      </vector>
    </apply>
  </xsl:for-each>
  

  <!-- Matrix of scaled measurement ids -->
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>group_ids</ci>
    <list separator=";">
      <xsl:for-each select="f:group">
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="@id"/>
        </string>
      </xsl:for-each>
    </list>
  </apply>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>group_textual</ci>
    <list separator=";">
      <xsl:for-each select="f:group">
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="f:textual"/>
        </string>
      </xsl:for-each>
    </list>
  </apply>
  <!-- Matrix of measurement ids -->
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>measurement_ids</ci>
    <list separator=";">
      <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:index">
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="."/>
        </string>
      </xsl:for-each>
    </list>
  </apply>
  
</xsl:template>

<xsl:template name="fluxSubspace">

  <comment xmlns="http://www.utc.fr/sysmetab">Building matrices (also the stoichiometry matrix) allows to compute the linear change of variable of the fluxes.</comment>
  
  <xsl:apply-templates select="../f:reactionnetwork/f:metabolitepools" mode="stoichiometric-matrix"/>

  <comment xmlns="http://www.utc.fr/sysmetab">Equality and inequality constraints</comment>

  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>C</ci>
    <matrix>
      <xsl:if test="f:net/m:math/m:apply[m:eq]">
        <matrixrow>
        <ci>A_eq_net</ci>
        <apply>
          <fn><ci>zeros</ci></fn>
          <cn>
            <xsl:value-of select="count(f:net/m:math/m:apply[m:eq])"/>
          </cn>
          <cn>
            <xsl:value-of select="$nv"/>
          </cn>
        </apply>
       </matrixrow>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:eq]">
        <matrixrow>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:xch/m:math/m:apply[m:eq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$nv"/>
            </cn>
          </apply>
          <ci>A_eq_xch</ci>            
        </matrixrow>
      </xsl:if>
      <matrixrow>
        <ci>N</ci>
        <apply>
          <fn><ci>zeros</ci></fn>
          <cn>
            <xsl:value-of select="count(../f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
          </cn>
          <cn>
            <xsl:value-of select="$nv"/>
          </cn>
        </apply>
      </matrixrow>
      <xsl:if test="f:net/m:math/m:apply[m:leq]">
        <matrixrow>
          <apply>
            <minus/>
            <ci>A_leq_net</ci>
          </apply>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:net/m:math/m:apply[m:leq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$nv"/>
            </cn>
          </apply>
        </matrixrow>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:leq]">
        <matrixrow>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:xch/m:math/m:apply[m:leq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$nv"/>
            </cn>
          </apply>
          <apply>
            <minus/>
            <ci>A_leq_xch</ci>
          </apply>            
        </matrixrow>
      </xsl:if>
    </matrix>
  </apply>
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
    <eq/>
    <ci>d</ci>
    <vector>
      <xsl:if test="f:net/m:math/m:apply[m:eq]">
        <ci>b_eq_net</ci>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:eq]">
        <ci>b_eq_xch</ci>            
      </xsl:if>
      <apply>
        <fn><ci>zeros</ci></fn>
        <cn>
          <xsl:value-of select="count(../f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
        </cn>
        <cn>1</cn>
      </apply>
      <xsl:if test="f:net/m:math/m:apply[m:leq]">
        <apply>
          <minus/>
          <ci>b_leq_net</ci>
        </apply>
      </xsl:if>
      <xsl:if test="f:xch/m:math/m:apply[m:leq]">
        <apply>
          <minus/>
          <ci>b_leq_xch</ci>
        </apply>            
      </xsl:if>
    </vector>
  </apply>

</xsl:template>

<xsl:template match="f:constraints">
  <optimize xmlns="http://www.utc.fr/sysmetab">
    <xsl:if test="f:net/m:math/m:apply[m:leq]">
      <matrix-open type="full" id="A_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="{$nv}"/>
      <matrix-open type="full" id="b_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="1"/>
    </xsl:if>
    <xsl:if test="f:net/m:math/m:apply[m:eq]">
      <matrix-open type="full" id="A_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="{$nv}"/>
      <matrix-open type="full" id="b_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="1"/>
    </xsl:if>
    <xsl:if test="f:xch/m:math/m:apply[m:leq]">
      <matrix-open type="full" id="A_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="{$nv}"/>
      <matrix-open type="full" id="b_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="1"/>
    </xsl:if>  
    <xsl:if test="f:xch/m:math/m:apply[m:eq]">
      <matrix-open type="full" id="A_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="{$nv}"/>
      <matrix-open type="full" id="b_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="1"/>
    </xsl:if>
    <xsl:apply-templates select="f:xch/m:math/m:apply[m:eq]"/>
    <xsl:apply-templates select="f:xch/m:math/m:apply[m:leq]"/>
    <xsl:apply-templates select="f:net/m:math/m:apply[m:eq]"/>
    <xsl:apply-templates select="f:net/m:math/m:apply[m:leq]"/>
    <xsl:if test="f:net/m:math/m:apply[m:leq]">
      <matrix-close id="A_leq_net"/>
      <matrix-close id="b_leq_net"/>
    </xsl:if>
    <xsl:if test="f:net/m:math/m:apply[m:eq]">
      <matrix-close id="A_eq_net"/>
      <matrix-close id="b_eq_net"/>
    </xsl:if>
    <xsl:if test="f:xch/m:math/m:apply[m:leq]">
      <matrix-close id="A_leq_xch"/>
      <matrix-close id="b_leq_xch"/>
    </xsl:if>  
    <xsl:if test="f:xch/m:math/m:apply[m:eq]">
      <matrix-close id="b_eq_xch"/>
      <matrix-close id="A_eq_xch"/>
    </xsl:if>
  </optimize>
  <xsl:call-template name="fluxSubspace"/>
</xsl:template>

<xsl:template match="f:constraints/*/m:math/m:apply">
  
  <xsl:if test="m:cn!='0'">
    <matrix-assignment id="{concat('b_',name(*[1]),'_',name(../..))}" row="{position()}" col="1" xmlns="http://www.utc.fr/sysmetab">
      <xsl:choose>
        <xsl:when test="name(*[1])='leq' and name(*[2])='ci' and name(*[3])='cn'">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">-(<xsl:value-of select="m:cn"/>)</cn>
        </xsl:when>
        <xsl:otherwise>
          <cn xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="m:cn"/></cn>
        </xsl:otherwise>
      </xsl:choose>
    </matrix-assignment>
  </xsl:if>
  <xsl:variable name="id" select="concat('A_',name(*[1]),'_',name(../..))"/>

  <xsl:choose>
    <xsl:when test="m:ci and not(m:apply)">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',m:ci)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="name(*[1])='leq' and name(*[2])='ci' and name(*[3])='cn'">
            <cn xmlns="http://www.w3.org/1998/Math/MathML">-1</cn>
          </xsl:when>
          <xsl:otherwise>
            <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
          </xsl:otherwise>
        </xsl:choose>
      </matrix-assignment>
    </xsl:when>
    <xsl:when test="m:apply">
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="m:apply"/>
        <xsl:with-param name="minus">
          <xsl:choose>
            <xsl:when test="m:apply[m:plus]">1</xsl:when>
            <xsl:when test="m:apply[m:minus]">-1</xsl:when>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="iteration_eq_leq">
  <xsl:param name="id"/>
  <xsl:param name="apply"/>
  <xsl:param name="minus" select="'1'"/>
  <xsl:choose>
    <xsl:when test="$apply[m:times]">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="$minus='-1'">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <minus/>
              <cn><xsl:value-of select="$apply/m:cn"/></cn>
            </apply>
          </xsl:when>
          <xsl:otherwise>
            <cn><xsl:value-of select="$apply/m:cn"/></cn>
          </xsl:otherwise>
        </xsl:choose>
      </matrix-assignment>
    </xsl:when>
    <xsl:when test="(count($apply/m:ci)='2') and not($apply/m:apply)">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci[1])/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
      </matrix-assignment>
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci[2])/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:value-of select="$minus"/>
        </cn>
      </matrix-assignment>
    </xsl:when>
    <xsl:when test="count($apply/m:apply)='1'">
      <matrix-assignment id="{$id}" row="{position()}" col="{key('REACTION',$apply/m:ci)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:choose>
            <xsl:when test="$apply[m:plus]">1</xsl:when>
            <xsl:when test="$apply[m:minus]">-1</xsl:when>
          </xsl:choose>
        </cn>  
      </matrix-assignment>
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="$apply/m:apply"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="count($apply/m:apply)='2'">
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="$apply/m:apply[1]"/>
      </xsl:call-template>
      <xsl:call-template name="iteration_eq_leq">
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="apply" select="$apply/m:apply[2]"/>
        <xsl:with-param name="minus">
          <xsl:choose>
            <xsl:when test="$apply[m:plus]">1</xsl:when>
            <xsl:when test="$apply[m:minus]">-1</xsl:when>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
  </xsl:choose>
</xsl:template> 

<xsl:template match="f:metabolitepools" mode="stoichiometric-matrix">
  <optimize xmlns="http://www.utc.fr/sysmetab">
    <matrix-open type="full" id="N" rows="{count(f:pool[@type='intermediate'])}" cols="{count(../f:reaction)}"/>
    <xsl:apply-templates select="f:pool[@type='intermediate']" mode="stoichiometric-matrix"/>
    <matrix-close id="N"/>
  </optimize>
</xsl:template>

<xsl:template match="f:pool" mode="stoichiometric-matrix">
  <xsl:variable name="position" select="position()"/>
  <xsl:for-each select="key('REDUCT',@id)">
	<matrix-assignment id="N" row="{$position}" col="{../@position}" xmlns="http://www.utc.fr/sysmetab">
		<xsl:choose>
			<xsl:when test="@stoichiometry"> <!-- used for non labeled pools or to simulate dilution effects-->
				<cn xmlns="http://www.w3.org/1998/Math/MathML">
				  <xsl:value-of select="concat('-',@stoichiometry)"/>
				</cn>
			</xsl:when>
			<xsl:otherwise>
				<cn xmlns="http://www.w3.org/1998/Math/MathML">-1</cn>
			</xsl:otherwise>
		</xsl:choose>
	</matrix-assignment>
  </xsl:for-each>
  <xsl:for-each select="key('RPRODUCT',@id)">
    <matrix-assignment id="N" row="{$position}" col="{../@position}" xmlns="http://www.utc.fr/sysmetab">
    	<cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
    </matrix-assignment>
  </xsl:for-each>
</xsl:template>

<xsl:template match="node()"/>

</xsl:stylesheet>