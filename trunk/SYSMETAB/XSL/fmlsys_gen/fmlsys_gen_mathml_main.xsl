<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- Cette feuille de style a pour but de générer différentes fonctions spécifiques
  d'un modèle donné sous forme FML -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- Output file name -->
  <xsl:param name="output"/>
	
  <xsl:param name="language">scilab</xsl:param>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <xsl:variable name="freefluxes" select="exslt:node-set($params)[@name='freefluxes']"/>

  <!-- find all f:datum -->
  
	<xsl:key name="DATUM" match="f:datum" use="@id"/>

  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">    

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>getd</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/macros')"/>
      </string>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>exec</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/spset/loader.sce')"/>
      </string>
      <cn>-1</cn>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>exec</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/fsqp/loader.sce')"/>
      </string>
      <cn>-1</cn>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>path</ci>
      <apply>
        <fn>
          <ci>get_absolute_file_path</ci>
        </fn>
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="concat(exslt:node-set($params)[@name='output'],'.main.sci')"/>
        </string>
      </apply>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>chdir</ci>
      </fn>
      <ci xmlns="http://www.utc.fr/sysmetab">path</ci>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>exec</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="concat(exslt:node-set($params)[@name='file'],'.network.sci')"/>
      </string>
      <cn>-1</cn>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>exec</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="concat(exslt:node-set($params)[@name='output'],'.data.sci')"/>
      </string>
      <cn>-1</cn>
    </apply>


    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>rand</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">seed</string>
      <cn>
      	<xsl:value-of select="exslt:node-set($params)[@name='seed']"/>
      </cn>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>stacksize</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">max</string>
    </apply>


    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>errcatch</ci>
      </fn>
      <cn>-1</cn>
      <string xmlns="http://www.utc.fr/sysmetab">stop</string>
    </apply>


    <comment xmlns="http://www.utc.fr/sysmetab">Input cumomers, computed from isotopomers of input metabolites.</comment>


    <xsl:for-each select="f:configuration/f:input">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
        <list separator=";">
          <xsl:for-each select="f:label">
            <cn>
              <xsl:value-of select="@value"/>
            </cn>
          </xsl:for-each>
        </list>
      </apply>    
    </xsl:for-each>
	
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Xinp</ci>
        <apply>
          <times/>
          <ci>D</ci>
          <vector>
						<ci>1</ci>
            <xsl:for-each select="f:configuration/f:input">
              <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
            </xsl:for-each>
          </vector>
        </apply>
      </apply>
          
    <comment xmlns="http://www.utc.fr/sysmetab">Computation of an eligible vector of fluxes wadm (net,xch)</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>me</ci>
      <cn>
        <xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']) + count(f:constraints/f:net/m:math/m:apply[m:eq]) + count(f:constraints/f:xch/m:math/m:apply[m:eq])"/>
      </cn>
    </apply>

    <!-- [W,ff,ftype]=freefluxes(C,b,me); -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <list separator=",">
        <ci>W</ci>
        <ci>w0</ci>
        <ci>ff</ci>
        <ci>ftype</ci>
        <ci>flux</ci>					
        <ci>wadm</ci>					
      </list>
      <apply>
        <fn><ci>freefluxes</ci></fn>
        <ci>C</ci>
        <ci>d</ci>
        <ci>me</ci>
        <xsl:if test="exslt:node-set($params)[@name='freefluxes'] = 'user' ">
          <ci>ffnames</ci>
          <ci>ffvalues</ci>
        </xsl:if>
      </apply>
    </apply>

    <!-- Admissible w : wadm=flux.values; -->
    <comment xmlns="http://www.utc.fr/sysmetab">Initial guesses to start the optimization over free fluxes</comment>

    <!-- the freefluxes qstart=wadm(ff); -->
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>q</ci>
      <apply>
        <fn><ci>wadm</ci></fn>
        <ci>ff</ci>
      </apply>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>auto_scales_ind</ci>
      <matrix>
        <matrixrow>
          <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group[key('DATUM',@id)]">
            <xsl:choose>
              <xsl:when test="exslt:node-set($params)[@name='scaling']='yes'">
                <cn><xsl:value-of select="position()"/></cn>
              </xsl:when>
              <xsl:when test="(exslt:node-set($params)[@name='scaling']='user') and (@scale='auto')">
                <cn><xsl:value-of select="position()"/></cn>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </matrixrow>
      </matrix>
    </apply>

    <comment xmlns="http://www.utc.fr/sysmetab">Lower and upper bound for free fluxes (other constraints can be taken into account via C and d matrices)</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>q_max</ci>
      <apply>
        <times/>
        <cn>1e10</cn>
        <apply>
          <fn><ci>ones</ci></fn>
          <ci>q</ci>
        </apply>
      </apply>
    </apply>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>q_min</ci>
      <apply>
        <minus/>
          <ci>q_max</ci>
      </apply>
    </apply>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>nb_group</ci>
      <cn><xsl:value-of select="count(f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group)"/></cn>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>nb_pool</ci>
      <cn><xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool)"/></cn>
    </apply>
    
    <comment xmlns="http://www.utc.fr/sysmetab">Verbosity level (iprint)</comment>
    
    <xsl:choose>
      <xsl:when test="exslt:node-set($params)[@name='optimize_method']='fsqp'">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>iprint</ci>
          <cn>1</cn>
        </apply>
      </xsl:when>
      <xsl:when test="exslt:node-set($params)[@name='optimize_method']='ipopt'">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>iprint</ci>
          <cn>5</cn>
        </apply>
      </xsl:when>
    </xsl:choose>
							                  
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>nb_cores</ci>
      <cn>
        <xsl:value-of select="exslt:node-set($params)[@name='nc']"/>
      </cn>                
    </apply>
    
    <xsl:if test="f:configuration/@stationary='false'">      
      <comment xmlns="http://www.utc.fr/sysmetab">Lower, upper bound and subspace parameters for pool sizes</comment>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">  
          <ci>m</ci>
          <ci>m_min</ci>
          <ci>m_max</ci>
          <ci>m_type</ci>
        </list>
        <apply>
          <fn>
            <ci>poolsizes</ci>
          </fn>
          <ci>mnames</ci>
          <ci>mvalues</ci>
          <ci>nb_pool</ci>                
          <cn>
            <xsl:value-of select="exslt:node-set($params)[@name='min_p']"/>
          </cn>                
          <cn>
            <xsl:value-of select="exslt:node-set($params)[@name='max_p']"/>
          </cn>                
        </apply>
      </apply>
			<comment xmlns="http://www.utc.fr/sysmetab">Choice of ODE scheme for direct simulation, derivatives and gradient</comment>  

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>scheme</ci>
        <apply>
          <fn>
          <ci>
            <xsl:value-of select="exslt:node-set($params)[@name='scheme']"/>
          </ci>
        </fn>
        </apply>                
      </apply>                           
      
      <comment xmlns="http://www.utc.fr/sysmetab">Discretization for time integration </comment>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>time_struct</ci>
        <apply>
          <fn><ci>time_discr</ci></fn>
          <ci>scheme</ci>
          <ci>tmeas</ci>
          <ci>q</ci>                
          <ci>m</ci>                
          <cn>
            <xsl:value-of select="exslt:node-set($params)[@name='steps']"/>
          </cn>                
          <xsl:if test="exslt:node-set($params)[@name='adapt']='yes'">
            <cn>
              <xsl:value-of select="exslt:node-set($params)[@name='odetol']"/>
            </cn>
          </xsl:if>
        </apply>
      </apply>

    </xsl:if>
      
    <xsl:choose>
      <xsl:when test="exslt:node-set($params)[@name='optimize']='yes'">
        <xsl:choose>
          <xsl:when test="f:configuration/@stationary='false'">
            
            <comment xmlns="http://www.utc.fr/sysmetab">Call the optimization function (fsqp)</comment>

            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <list separator=",">
                <ci>q</ci>
                <ci>m</ci>
                <ci>f</ci>
                <ci>info</ci>
                <ci>message</ci>
              </list>
              <apply>
                    <fn><ci>optimizeNonStat</ci></fn>
                <smtb:string>
                  <xsl:value-of select="exslt:node-set($params)[@name='optimize_method']"/>
                </smtb:string>
                <list separator=";">
                  <ci>q</ci>
                  <ci>m</ci>                
                </list>
                <list separator=";">
                  <ci>q_min</ci>
                  <ci>m_min</ci>                
                </list>
                <list separator=";">
                  <ci>q_max</ci>
                  <ci>m_max</ci>                
                </list>
                <ci>C</ci>
                <ci>d</ci>
                <ci>me</ci>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_x']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_grad']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_reg']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_phi']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='max_iter']"/></cn>
                <ci>iprint</ci>
                <xsl:choose>
                  <!--<xsl:when test="exslt:node-set($params)[@name='extrap']='yes'">
                    <smtb:string>extra</smtb:string>
                    <ci>5</ci>
                  </xsl:when>-->
                  <xsl:when test="(starts-with(exslt:node-set($params)[@name='stats'],'mc')) and (number(substring-after(exslt:node-set($params)[@name='stats'],':')) &gt; 0)">
                    <smtb:string>mc</smtb:string>
                    <ci>
                      <xsl:value-of select="round(number(substring-after(exslt:node-set($params)[@name='stats'],':')))"/>
                    </ci>
                  </xsl:when>
                  <xsl:when test="(starts-with($freefluxes,'multi') or starts-with($freefluxes,'auto')) and (number(substring-after($freefluxes,':')) &gt; 0)">
                    <smtb:string>multi</smtb:string>
                    <ci>
                      <xsl:value-of select="round(number(substring-after($freefluxes,':')))"/>
                    </ci>                  
                  </xsl:when>
                </xsl:choose>
              </apply>
            </apply>
            
          </xsl:when>
        
          <xsl:otherwise>

  					<comment xmlns="http://www.utc.fr/sysmetab">Choice of the method for gradient computation</comment>  

            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>solveStatAndGrad</ci>
                <xsl:choose>
                  <xsl:when test="exslt:node-set($params)[@name='gradient']='adjoint'">
                    <ci>solveStatAndGradAdjoint</ci>
                  </xsl:when>
                  <xsl:otherwise>
                    <ci>solveStatAndGradDirect</ci>
                  </xsl:otherwise>
                </xsl:choose>
            </apply>

            <comment xmlns="http://www.utc.fr/sysmetab">Call the optimization function (fsqp)</comment>

            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <list separator=",">
                <ci>q</ci>
                <ci>f</ci>
                <ci>info</ci>
                <ci>message</ci>
              </list>
              <apply>
                    <fn><ci>optimize</ci></fn>
                <smtb:string>
                  <xsl:value-of select="exslt:node-set($params)[@name='optimize_method']"/>
                </smtb:string>                  
                <ci>q</ci>
                <ci>q_min</ci>
                <ci>q_max</ci>  
                <ci>C</ci>
                <ci>d</ci>
                <ci>me</ci>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_x']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_grad']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_reg']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_phi']"/></cn>
                <cn><xsl:value-of select="exslt:node-set($params)[@name='max_iter']"/></cn>
                <ci>iprint</ci>
                <xsl:choose>
                  <xsl:when test="(starts-with(exslt:node-set($params)[@name='stats'],'mc')) and (number(substring-after(exslt:node-set($params)[@name='stats'],':')) &gt; 0)">
                    <smtb:string>mc</smtb:string>
                    <ci>
                      <xsl:value-of select="round(number(substring-after(exslt:node-set($params)[@name='stats'],':')))"/>
                    </ci>
                  </xsl:when>
                  <xsl:when test="(starts-with($freefluxes,'multi') or starts-with($freefluxes,'auto')) and (number(substring-after($freefluxes,':')) &gt; 0)">
                    <smtb:string>multi</smtb:string>
                    <ci>
                      <xsl:value-of select="round(number(substring-after($freefluxes,':')))"/>
                    </ci>                  
                  </xsl:when>
                </xsl:choose>
              </apply>
            </apply>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:when>

      <xsl:otherwise>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>info</ci>
          <ci>0</ci>
        </apply>  
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>message</ci>
          <vector/>
        </apply>  
      </xsl:otherwise>
    </xsl:choose>
    

    <xsl:if test="(exslt:node-set($params)[@name='extrap']='yes') and f:configuration/@stationary='false'">
      
			<comment xmlns="http://www.utc.fr/sysmetab">Halving time steps for Richardson extrapolation</comment>  
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>time_struct</ci>
        <apply>
          <fn><ci>time_discr_from_t</ci></fn>
          <ci>scheme</ci>
          <ci>tmeas</ci>
          <apply>
            <fn><ci>dub</ci></fn>
            <ci>time_struct.t</ci>
          </apply>
        </apply>
      </apply>
      
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>q2</ci>
          <ci>m2</ci>
          <ci>f2</ci>
          <ci>info2</ci>
          <ci>message2</ci>
        </list>
        <apply>
              <fn><ci>optimizeNonStat</ci></fn>
          <smtb:string>
            <xsl:value-of select="exslt:node-set($params)[@name='optimize_method']"/>
          </smtb:string>
          <list separator=";">
            <ci>q</ci>
            <ci>m</ci>                
          </list>
          <list separator=";">
            <ci>q_min</ci>
            <ci>m_min</ci>                
          </list>
          <list separator=";">
            <ci>q_max</ci>
            <ci>m_max</ci>                
          </list>
          <ci>C</ci>
          <ci>d</ci>
          <ci>me</ci>
          <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_x']"/></cn>
          <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_grad']"/></cn>
          <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_reg']"/></cn>
          <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_phi']"/></cn>
          <cn><xsl:value-of select="exslt:node-set($params)[@name='max_iter']"/></cn>
          <ci>iprint</ci>
        </apply>
      </apply>
      
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>q</ci>
        <list separator=","><ci>q</ci><ci>q2</ci></list>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>m</ci>
        <list separator=","><ci>m</ci><ci>m2</ci></list>
      </apply>
      
    </xsl:if>

    <!-- [flux_opt,scale_opt,meas,norm_residu,norm_fm,X]=measurements_and_stats(q,omega); -->

    <comment xmlns="http://www.utc.fr/sysmetab">Compute measurements, states and statistics</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <list separator=",">
        <ci>flux_opt</ci>
        <ci>pool_opt</ci>
        <ci>scale_opt</ci>
        <ci>label_meas</ci>
        <ci>label_error</ci>
        <ci>flux_meas</ci>
        <ci>flux_error</ci>
        <ci>pool_meas</ci>
        <ci>pool_error</ci>
        <ci>X</ci>
      </list>
      <apply>
        <fn><ci>measurements_and_stats</ci></fn>
        <ci>q</ci>
        <xsl:choose>
          <xsl:when test="f:configuration/@stationary='false'">
            <ci>m</ci>
          </xsl:when>
          <xsl:otherwise>
            <vector/>
          </xsl:otherwise>
        </xsl:choose>
        <cn><xsl:value-of select="exslt:node-set($params)[@name='eps_phi']"/></cn>
        <ci>info</ci>
        <xsl:choose>
          <xsl:when test="exslt:node-set($params)[@name='extrap']='yes'">
            <smtb:string>extra</smtb:string>
          </xsl:when>
          <xsl:when test="(starts-with(exslt:node-set($params)[@name='stats'],'mc')) and (number(substring-after(exslt:node-set($params)[@name='stats'],':')) &gt; 0)">
            <smtb:string>mc</smtb:string>
          </xsl:when>
          <xsl:otherwise>
            <smtb:string>normal</smtb:string>
          </xsl:otherwise>
        </xsl:choose>
        <cn><xsl:value-of select="exslt:node-set($params)[@name='conflevel']"/></cn>
      </apply>
    </apply>

    <xsl:if test="exslt:node-set($params)[@name='optimize']='yes'">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <fn><ci>writeMessage</ci></fn>
        <ci>message</ci>
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="exslt:node-set($params)[@name='output']"/>                
        </string>
      </apply>
    </xsl:if>

    <!-- results_fwd(flux_no_opt,"step3.xml","exslt:node-set($params)[@name='file']"); -->

    <comment xmlns="http://www.utc.fr/sysmetab">update step3.xml file with new results and save them in a fwd file</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn><ci>results_fwd</ci></fn>
      <ci>flux_opt</ci>
      <ci>pool_opt</ci>
      <ci>scale_opt</ci>
      <ci>label_meas</ci>
      <ci>label_error</ci>
      <ci>flux_meas</ci>
      <ci>flux_error</ci>
      <ci>pool_meas</ci>
      <ci>pool_error</ci>
      <ci>X</ci>
      <ci>message</ci>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="exslt:node-set($params)[@name='file']"/>                
      </string>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="exslt:node-set($params)[@name='output']"/>                
      </string>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="exslt:node-set($params)[@name='sysmetab']"/>                
      </string>
    </apply>
       
    <xsl:if test="exslt:node-set($params)[@name='plot']='yes'">
      <xsl:choose>
        <xsl:when test="f:configuration/@stationary='false'">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <fn><ci>plotNonStatMeasurements</ci></fn>
            <ci>flux_opt</ci>
            <ci>time_struct</ci>
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="exslt:node-set($params)[@name='output']"/>                
            </string>
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="exslt:node-set($params)[@name='sysmetab']"/>                
            </string>            
          </apply>
        </xsl:when>
        <xsl:otherwise>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <fn><ci>plotStatMeasurements</ci></fn>
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="exslt:node-set($params)[@name='output']"/>                
            </string>
            <string xmlns="http://www.utc.fr/sysmetab">
              <xsl:value-of select="exslt:node-set($params)[@name='sysmetab']"/>                
            </string>            
          </apply>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>        
	      
    <xsl:if test="exslt:node-set($params)[@name='computation']='yes'">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <fn><ci>quit</ci></fn>
      </apply>
    </xsl:if>
    			
  </xsl:template>

</xsl:stylesheet>