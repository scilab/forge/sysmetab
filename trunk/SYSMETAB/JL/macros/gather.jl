function gather(M_m,nrow,ncol,storage)
  # group matrix contributions to same (i,j) term
  if isa(M_m,String)
    try
      M_m=readdlm(M_m,Int)
    catch
      M_m=[]
    end
  end
  if isempty(M_m)
    Mstruct([nrow,ncol],spzeros(nrow,ncol),[],[],[],[],[])
  else 
    m=size(M_m,1)
    if storage==1 # CSR storage (Scilab)    
      ij=sortrows([M_m[:,1:2] [1:m]]) # lexicographic sort of rows
      ind=ij[:,3]
      (uij,k)=s_unique(ij[:,1:2]); # unique (row,col) pairs        
    elseif storage==2 # CSC storage (Julia)  
      ij=sortrows([M_m[:,2:-1:1] [1:m]]) # lexicographic sort of rows
      ind=ij[:,3]
      (uij,k)=s_unique(ij[:,2:-1:1]); # unique (row,col) pairs        
    end
    M_s=M_m[:,3]
    M_i=int(zeros(m))
    n=length(k)
    for i=1:n-1
      M_i[k[i]:k[i+1]-1]=i
    end
    if n>0
      M_i[k[n]:end]=n
    end
    M=sparse(uij[:,1],uij[:,2],ones(n),nrow,ncol)
    M_t=sparse(M_i,ind,M_s[ind],n,m);
    if n==1 | m==1
      M_t=full(M_t);
    end
    if size(M_m,2)==4
      Mstruct([nrow,ncol],M,uij,M_t,M_m[:,4],[],[])        
    elseif size(M_m,2)==5
      Mstruct([nrow,ncol],M,uij,M_t,M_m[:,4],M_m[:,5],[])        
    elseif size(M_m,2)==6
      Mstruct([nrow,ncol],M,uij,M_t,M_m[:,4],M_m[:,5],M_m[:,6])        
    end
  end
end

function s_unique(ij)
  # computes unique lines of ij. Index k contains first occurences of unique lines 
  n=size(ij,1)
  if n==0
    return ([],[])
  else
    k=zeros(n,1)
    ind=1;
    k[ind]=1
    for i=2:n
      if ij[i,:]!=ij[k[ind],:]
        ind=ind+1
        k[ind]=i
      end
    end
    k=k[1:ind]
    (ij[k,:],k)
  end
end

type Mstruct
  size
  value
  ij
  t
  m1
  m2
  m3
end