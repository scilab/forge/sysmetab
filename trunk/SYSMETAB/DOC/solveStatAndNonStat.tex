\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{amsfonts}	%\mathbb
%\usepackage{amsthm}
%\usepackage[english]{babel}
\usepackage[a4paper]{geometry}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc} 
\usepackage{graphicx} 

  \voffset=-1in
   \hoffset=-1in
   \topmargin=2cm
   \headheight=0cm
   \headsep=0cm
   \setlength{\paperheight}{29.7cm}%
   \setlength{\paperwidth}{21cm}%
   \setlength{\oddsidemargin}{2.5cm}%
   \setlength{\evensidemargin}{2.5cm}%
   \setlength{\marginparsep}{0cm}%
   \setlength{\marginparwidth}{0cm}%
   \setlength{\footskip}{1cm}%
   \setlength{\textheight}{24cm}%
   \setlength{\textwidth}{16cm}%


\begin{document}
\begin{center}
%\textbf{\huge Note de développement}\\[6mm]
\textbf{\huge Algorithm and implementation note}\\[2mm]
{\large Main functions from the files \texttt{solveStat.sci} \& \texttt{solveNonStat.sci}}
\end{center}

\section{Computing the state}
\subsection{solveNonStat}
\noindent\textbf{Discrete scheme}\\[2mm]
Denoting the approximations $x_k^n\simeq x_k(t_n)$ and $x_{\leq k}^n\simeq x_{\leq k}(t_n)$, the implicit Euler scheme gives the following  discrete version
%
\begin{equation}
\left\{
\begin{split}
x_k^0&=0,\\
X_k({m}) \left(x_k^{n+1}-x_k^n\right)&=h f_k(v,x_ {\leq k}^{n+1},x_{\leq k}^\leftarrow),
\end{split}
\label{eq:discrcasc}
\right.
\end{equation}
%
where the second row has to be considered for $0\leq n<N_T$, and the objective function is given by
%
$$
J(v)=\textstyle\frac{1}{2}\sum_{j=1}^M\Vert Cx^{n(j)} -y_j  \Vert,
$$
%
where $t_{n(j)}=\tau_j$, i.e. each measurement time is supposed to correspond to a sampling point of the time grid.\\[2mm]
\noindent\textbf{Algorithm}\\[2mm]
The state variable is computed for every discretization time and every related cumomer weight by an embedding loop over the cumomer weights
\begin{quote}
\emph{For every related cumomer weight, do}
\begin{quote}
$\bullet$~\emph{Compute the state variable for every discretization time}
\end{quote}
\emph{end do}
\end{quote}
As time state values are required to compute the derivatives, they are stored in a state matrix (\texttt{X}) with cumomer weight as first component and discretization time value as second component.\\[2mm]
\noindent\textbf{Implementation}\\[2mm]
The state matrix \texttt{X} is first built by the \texttt{repmat} command. Then, for every cumomer weight, the time state values are cumulated by the \texttt{solveNonStatWeight} function which contains the discrete time scheme.
\begin{quote}
\texttt{h=list();\\
    X=repmat(Xinp,1,length(t));\\
    for i=1:length(cum)\\
        \hspace*{4mm}[X(cum(i),:),h(i)]=solveNonStatWeight(..\\
	\hspace*{4mm}v,m,X,cum(i),M(i),b(i),t,pool\_ind\_weight(i));\\
    end
}
\end{quote}
\subsection{solveNonStatWeight}
\paragraph{Algorithm}~\\[2mm]
This function contains the discrete time scheme needed to compute the state variable at every discretization time. First, the time scheme matrix (\texttt{MnImp}) and the non homogeneous part of the EDO (\texttt{bmat\_sp}) are built. Then, the forward scheme is described by a forward time loop where the state variable at time $t_{n+1}$ is computed by solving the related linear system. We recall that, for the implicit Euler scheme, the time scheme matrix is given by
\begin{equation}
M_k=X_k(m)-h A_k
\notag
\end{equation}
The solution of the implicit Euler scheme, based on the \emph{full} time dependent state variable~\texttt{X}, is stored in a new variable \texttt{Xn} in order to use only adjacent memory boxes.

\paragraph{Implementation}~\\[2mm]
To build the time scheme matrix \texttt{MnImp} and the non homogeneous part \texttt{bmat\_sp} of the EDO, their sparsity are exploited in the Scilab code
\begin{quote}
\texttt{Mn\_sp=sparse(Mn.ij,Mn.t*v(Mn.m1),Mn.size);\\
  bmat\_sp=bn.t*spdiag(v(bn.m3))*(X(bn.m1,:).*X(bn.m2,:));
}
\end{quote}
The time step $h$ is computed from the difference of two adjacent points of the time grid
\begin{quote}
\texttt{stepsize=t(2)-t(1)\\
  n=length(t)
}
\end{quote}
and the time scheme matrix \texttt{MnImp} assembly is based on the cumomer weight matrix \texttt{Mn\_sp} and on the \texttt{stepsize} variable
\begin{quote}
\texttt{MnImp=sparse(spdiag(m(pool\_ind\_weightn))-stepsize*Mn\_sp);\\
  MnImp\_handle=umf\_lufact(MnImp);\\
  bmat\_sp=stepsize*bmat\_sp;
}
\end{quote}
The output, i.e. the state variable related to the cumomer weight under consideration, uses adjacent memory boxes thanks to the introduction of the \texttt{Xn} variable
\begin{quote}
\texttt{Xn=zeros(length(cum),n);\\
  for k=1:(n-1)\\
   \hspace*{4mm} Xn(:,k+1)=umf\_lusolve(MnImp\_handle,..\\
   \hspace*{4mm} m(pool\_ind\_weightn).*Xn(:,k)+spset(bn.value,bmat\_sp(:,k+1)));  \\
  end}
\end{quote}
which enables to speed up the computation. Note that, in the Scilab code below,  the discrete time  scheme is implemented explicitely with the \texttt{umf\_lusolve} command.
\section{Computing derivatives}
\subsection{solveNonStatAndGradAdjoint}
%\noindent\textbf{Modèle continu}\\[2mm]
%The adjoint equation is obtained by expanding $\partial_x\mathcal{L}(x,p,\lambda)=0$ which leads to the following backward in time ODE cascade 
%\begin{equation}
%\left.
%\begin{split}
%%X_N({m} ) \frac{d}{dt} \lambda_N&=A_N(v)^\top \lambda_N,\\
%X_k({m}) \frac{d}{dt}\lambda_k&=A_k(v)^\top \lambda_k\\
%-&\textstyle\sum_{i>k} \partial_{x_k}b_i(v,x_{<i},x_{\leq i}^{\leftarrow})^\top\lambda_i,
%\end{split}
%\right.
%\label{eq:timeadj}
%\end{equation}
%for $1\leq k\leq N$, where the second row vanishes for $k=N$ and the equalities read for $t\in[0,T[\setminus \{\tau_j\}_{1\leq j \leq M}$. The final condition is given for each~$k$ by $\lambda_k(T)=0$ and the following $j$ step conditions
%\begin{equation}
%X_k({m})[\lambda_k(\tau_j^+)-\lambda_k(\tau_j^-)]=C_k^\top (Cx(\tau_j)-y_j),
%\label{eq:iniadj}
%\end{equation}
%occur for $j=1\dots M$. 
%Considering \eqref{eq:timeadj}-\eqref{eq:iniadj} determines the functions $\lambda_k(t)$ which enter into the gradient of $J$ (with respect to $v$) as follows
%\begin{equation}
%\nabla J(v)=\sum_{k=1}^N \int_0^T\partial_v f_k(v,x_{\leq k}(t),x_{\leq k}^{\leftarrow})^\top \lambda_k\,dt.
%\label{eq:timeadjgrad}
%\end{equation}
\noindent\textbf{Discrete scheme}\\[2mm]
 The discrete adjoint scheme is given for $n=1\dots N_T-1$ by 
\begin{equation}
%\left.
%\begin{split}
X_k({m})({\lambda_k^n-\lambda_k^{n-1}})= h A_k(v)^\top \lambda_k^{n-1}
-h\sum_{i>k} \partial_{x_k}b_i(v,x_{<i}^n,x_{\leq i}^{\leftarrow})^\top\lambda_i^{n-1}+\delta_n,
%\end{split}
%\right.
\label{eq:discradjoint}
%\tag{ADJ1\_ns}
\end{equation}
with final condition $\lambda_k^{N_T-1}=0$ and
$$\delta_n=\left\{
\begin{array}{rl}
C_k^\top (Cx^{n}-y_j),&\mbox{if } n=n(j),\\
0,&\mbox{otherwise},
\end{array}
\right.$$
which determines~$(\lambda_k^n)_{0\leq n<N_T}$ for each weight $k$. The gradient of $J$ at $v$ is finally given by
\begin{equation}
\nabla J(v)=h\sum_{k=1}^N \sum_{n=0}^{N_T-1}\partial_v f_k(v,x^{n+1}_{\leq k},x_{\leq k}^{\leftarrow})^\top \lambda_k^n.
%\tag{ADJ2\_ns}
\label{eq:discradjgrad}
\end{equation}
%\newpage
\noindent\textbf{Algorithm}\\[2mm]
%Pour \emph{chaque} poids, la variable adjointe est calculée à \emph{tous} les instants de discrétisation. La structure de l'algorithme est la suivante
The adjoint variable is computed first for every discretization time and secondly for every related cumomer weight as follows
\begin{enumerate}
%\item \emph{Pour chaque poids de cumomère, faire}
\item \emph{For every related cumomer weight, do}
\begin{quote}
$\bullet$~\emph{Compute the adjoint variable for every discretization time}
\end{quote}
\emph{end do}
\item\emph{Gradient assembly}
\end{enumerate}
%La variable $\delta_n$ prend la forme d'un tableau (\texttt{bmeas}) constitué de valeurs nulles et des conditions terminales associées à chaque temps de la mesure. La variable adjointe est formée par une matrice (\texttt{p}) dont la seconde composante contient les différents instants de discrétisation.

The $\delta_n$ variable  takes the form of a null and final condition (related to time measurements) array (\texttt{bmeas}). The adjoint variable is represented by a matrix (\texttt{p}) whose second component comprises the different discretization times.

The second term in the right-hand side of \eqref{eq:discradjoint} is modeled by the dot product between 
\begin{itemize}
\item the derivative vector $(\partial_{x_k}b_i)_{1\leq i\leq n}$,
\item the adjoint vector $(\lambda_i)_{1\leq i\leq n}$, 
\end{itemize}
as
\begin{equation}
\left[\underbrace{\partial_{x_k}b_1^\top,\dots,\partial_{x_k}b_k^\top}_0,\partial_{x_k}b_{k+1}^\top,\dots,\partial_{x_k}b_N^\top\right]\cdot\left[\lambda_1,\dots,\lambda_N\right]^\top=\sum_{i>k}\partial_{x_k}b_i^\top \cdot\lambda_i
\notag
\end{equation}
In the same spirit, the gradient is computed as follows
\begin{equation}
\nabla J(v)=h\sum_{n=0}^{N_T-1}\left(\left[\partial_v f_1^\top,\dots,\partial_v f_k^\top,\partial_v f_{k+1}^\top,\dots,\partial_v f_N^\top\right]\cdot\left[\lambda_1^n,\dots,\lambda^n_N\right]^\top\right)
\notag
\end{equation}
\noindent\textbf{Implementation}\\[2mm]
The discrete time scheme is described in the function \texttt{solveNonStatWeightAdjoint}. The loop over the related cumomer weight is implemented as
\begin{quote}
\texttt{p=zeros(X);\\
    ncum=length(cum);\\    
    p(cum(ncum),:)=solveNonStatWeightAdjoint(..\\p,bmeas,cum(ncum),h(ncum),t,m,pool\_ind\_weight(ncum));    \\
    for i=ncum-1:-1:1\\
        \hspace*{4mm} p(cum(i),:)=solveNonStatWeightAdjoint(..\\\hspace*{4mm} p,bmeas,cum(i),h(i),t,m,pool\_ind\_weight(i),db\_dxt(i).value,db\_dxt\_nz(i));        \\
    end\\
}
\end{quote}
The gradient assembly is based on the \texttt{spset} command which builds the dot product for every discretization time
\begin{quote}
\texttt{stepsize=t(2)-t(1)\\
    n=length(t) \\   
    gradv=zeros(length(v),1);\\
    for k=1:n-1        \\
     \hspace*{4mm}gradv=gradv+spset(..\\\hspace*{4mm}dgdvt.value,dgdvt.t*(X(dgdvt.m1,k+1).*X(dgdvt.m2,k+1))..\\\hspace*{4mm})*p(:,k);\\
    end\\
    gradv=2*stepsize*gradv;\\
}
\end{quote}
\subsection{solveNonStatWeightAdjoint}
\noindent\textbf{Algorithm}\\[2mm]
This function is dedicated to the implementation of the adjoint discrete scheme, namely of~\eqref{eq:discradjoint}. For a given cumomer weight, the function \texttt{solveNonStatWeightAdjoint} computes the related adjoint variable at every discretization time. The result is stored in a matrix with first component size given by the cumomer weight dimension under consideration and with second component given by the time component of the adjoint state.

In order to speed up the computation, the adjoint state output, associated to the cumomer weight under consideration, is  stored in adjacent memory boxes  by the introduction of the matrix \texttt{pn}. In the script, \texttt{n} is the generic name for the cumomer weight considered. The input comprises the \emph{full} time dependent adjoint state (\texttt{p}), i.e. the adjoint state variable for every discretization time and every cumomer weight.

The implementation is able to handle both unit cumomer weight and larger cumomer weight, i.e. to handle wether the derivatives $(\partial_{x_k}b_i)_{1\leq i\leq n}$ enter into the computation of the adjoint state or not.\\[2mm]
\noindent\textbf{Implementation}\\[2mm]
The handling of every cumomer weight by a unique code is obtained by the following test
\begin{quote}
\texttt{if exists("db\_dx\_t","local")\\\vdots\\else\\\vdots\\end
}
\end{quote}
where the matrix \texttt{db\_dx\_t} contains the the derivatives $(\partial_{x_k}b_i^\top)_{1\leq i\leq n}$ .

When the condition is fulfilled, i.e. when the cumomer weight under consideration is larger than one, the adjoint state related to the considered cumomer weight is computed by a backward time loop involving the derivatives $(\partial_{x_k}b_i^\top)_{1\leq i\leq n}$
\begin{quote}
\texttt{for k=n:-1:2    \\
\hspace*{4mm}    pn(:,k-1)=umf\_lusolve(Mimp\_adj\_handle,..\\
\hspace*{4mm}    m(pool\_ind\_weightn).*pn(:,k)+bmeas(cum,k)+..\\
\hspace*{4mm}    stepsize*(spset(db\_dx\_t,db\_dx\_t\_mat(:,k))*p(:,k-1)));   \\
  end
}
\end{quote}
where \texttt{bmeas} is the matrix corresponding to the $\delta_n$ variable.

When the condition is not fulfilled, i.e. for unit cumomer weight, the related adjoint state does not involve the derivatives $(\partial_{x_k}b_i^\top)_{1\leq i\leq n}$, which leads to the following Scilab code
\begin{quote}
\texttt{for k=n:-1:2    \\
\hspace*{4mm}     pn(:,k-1)=umf\_lusolve(Mimp\_adj\_handle,..\\
\hspace*{4mm}    m(pool\_ind\_weightn).*pn(:,k)+bmeas(cum,k));      \\
end
}
\end{quote}

\subsection{solveNonStatAndGradDirect}

\noindent\textbf{Discrete scheme}\\[2mm]
In the direct approach, implicit derivation of scheme \eqref{eq:discrcasc} with respect to $v$ and application of the Euler scheme to the continuous time equation \eqref{eq:timedirect} provides the approximated derivatives $x'_k$ at sampling points $t_n$ by solving
%
\begin{equation}
\left.
\begin{split}
X_k({m} ) (x_k'^{\,n+1}-x_k'^{\,n})=&h A_k(v)x_k'^{\,n+1}+h\partial_v f_k(v,x_{\leq k}^{n+1},x_{\leq k}^{\leftarrow})\\
+h&\textstyle\sum_{i<k}  \partial_{x_i}b_k(v,x_{\leq i}^{n+1},x_{\leq i}^{\leftarrow}) x_i'^{\,n+1},\\[2mm]
\end{split}
\right.
\label{eq:discrdirect}
\end{equation}
%
combined with the initial condition $x_k'^{\,0}=0$ for $k=1\dots N$. From the time  dependent state sequence and the time dependent state derivative sequence, the gradient is obtained by
%
\begin{equation}
\nabla J(v)=\sum_{j=1}^M\sum_{k=1}^N (x_k'^{\,n(j)})^\top C_k^\top(Cx^{n(j)}-y_j) .
\label{eq:discrdirectgrad}
\end{equation}
%

\noindent\textbf{Algorithm}\\[2mm]
The \texttt{solveNonStatAndGradDirect} function computes for \emph{every} discretization time the sensibility $x'$ for related cumomer weights: the loop over the discretization time comprises the computation of $x'$ at each related weight %La structure est la suivante (au poids des cumomères près).
\begin{quote}
\emph{For every time measurement, do}
\begin{quote}
$\bullet$~\emph{Compute $x'$ up to the next time measurement}\\[2mm]
%\begin{quote}
%-\emph{Propagation  de la condition initiale $x'(0)=0$ par un schéma}
%\end{quote}
$\bullet$~\emph{Gradient assembly}
\end{quote}
\emph{end do}
\end{quote}
Time measurements are considered from the smallest to the largest. To be precise, the indice set of time measurements, i.e. the set comprising the indices  $n(j)$ for $j=1\dots M$,  is considered instead of the set of time measurements containing $\tau_j$. The gradient contribution of $x'$ from $n(j)$ is cumulated (successively) inside the loop in the following manner
%Les instants discrets contenus entre $n(j)$ et $n(j-1)$ sont considérés pour le calcul de la variable adjointe à ces instants. 
%La contribution au gradient de $x'$ en $n(j)$ est accumulée (successivement) à l'intérieur de la boucle.

%La variable adjointe associée au poids des cumomères est considérée dans l'ordre décroissant, i.e. du poids le plus élevé au poids unité, ce qui donne lieu à la structure suivante
\begin{quote}
$x_k'^{\,0}=0, k=1\dots N$\\
$n(0)=0$\\
$\nabla J(v)=0$\\
$j\leftarrow 0,\dots, M-1$
\begin{quote}
$\forall n\in[n(j);n(j+1)[$ 
\begin{quote}
$k\leftarrow 1,\dots, N$
\begin{quote}
\emph{Solve (in }$x_k'^{\,n+1}$)
\begin{equation}
\left.
\begin{split}
X_k({m} ) (x_k'^{\,n+1}-x_k'^{\,n})=&h A_k(v)x_k'^{\,n+1}+h\partial_v f_k(v,x_{\leq k}^{n+1},x_{\leq k}^{\leftarrow})\\
+h&\textstyle\sum_{i<k}  \partial_{x_i}b_k(v,x_{\leq i}^{n+1},x_{\leq i}^{\leftarrow}) x_i'^{\,n+1},\\[2mm]
\end{split}
\right.
%\label{eq:discrdirect}
\notag
\end{equation}
\end{quote}
\emph{fin }$k$
\end{quote}
\emph{fin }$n$
\end{quote}
\begin{quote}
$k\leftarrow 1,\dots, N$
\begin{quote}
\emph{Gradient assembly}
\begin{equation}
\nabla J(v)\leftarrow\nabla J(v)+(x_k'^{\,n(j)})^\top C_k^\top(Cx^{n(j)}-y_j) .
%\label{eq:discrdirectgrad}
\notag
\end{equation}
\end{quote}
\emph{fin }$k$
\end{quote}
\emph{fin }$j$
\end{quote}
As only the sequence of the $x_k'^{\,n(j)}$ values enters into the gradient computation, we do not have to keep all the iterates $x_k'^{\,n}$, for  $n=1,\dots,N_T$, in memory.
As a consequence, the current iteration may be overwrite by the next iteration and the sensitivity $x'$ can be viewed as a matrix (with related cumomer weight for the first component and flux component from which the sensitivity is computed for the second component).

The last term in the right-hand of \eqref{eq:discrdirect} is modeled as a product between
\begin{itemize}
\item the derivative vector of $(\partial_{x_i}b_k)_{1\leq i\leq N}$ values,
\item the sensibility matrix $x'$
\end{itemize}
as follows
\begin{equation}
\left[\partial_{x_1}b_k,\dots,\partial_{x_{k-1}}b_k,\underbrace{\partial_{x_k}b_k,\dots,\partial_{x_N}b_k}_0\right]\cdot x'=\sum_{i<k}\partial_{x_i}b_k(v,x_{\leq i}^{n+1},x_{\leq i}^{\leftarrow}) x_i'^{\,n+1}
\notag
\end{equation}

\noindent\textbf{Implementation}\\[2mm]
%On distingue le cas $\delta_{n(j)}=C_k^\top (Cx^{n(j)}-y_j)$ du cas $\delta_n=0$ (si $n\neq n(j)$) afin d'éviter de sommer des termes nuls. De sorte que dès qu'on entre dans la boucle en $j$, le calcul de la condition terminale est effectué pour tout $k=N,\dots,1$, ce qui correspond à la résolution (en $\lambda_k^{n-1}$) de
%\begin{equation}
%\begin{split}
%X_k({m})({\lambda_k^n-\lambda_k^{n-1}})&= h A_k(v)^\top \lambda_k^{n-1}
%+C_k^\top (Cx^{n(j)}-y_j)\\
%&-h\sum_{i>k} \partial_{x_k}b_i(v,x_{<i}^n,x_{\leq i}^{\leftarrow})^\top\lambda_i^{n-1}
%\end{split}
%\notag
%%\tag{ADJ1\_ns}
%\end{equation}
%En outre, comme la seconde ligne de cette équation disparaît pour $k=N$, le cas $k=N$ est traité à part. Pour $k<N$, la somme est évaluée à l'aide de la fonction \texttt{spset}. 
The implementation is based on the introduction of a new array (\texttt{ind\_meas0}) which differs from \texttt{ind\_meas} by adding the unity as first element: this model considers the null time measurement as a "virtual" first measurement.
\begin{quote}
\texttt{ ind\_meas0=[1;ind\_meas];}
\end{quote}
Considering integers into the interval $[n(j);n(j+1)[$ is equivalent to the following loop
\begin{quote}
\texttt{for k=ind\_meas0(ind):ind\_meas0(ind+1)-1\\
\hspace*{4mm} \vdots\\
end}
\end{quote}
One can note that for the first weight cumomer, i.e. for $k=1$, the sequence $\left(\partial_{x_i}b_k\right)_{i<k}$ does not enter in the calculus of the sensitivity. Hence, the special case $k=1$ is considered separetely by 
\begin{quote}
\texttt{dvX1=dX\_dv(cum(1),:);\\
dgdv\_kp1=spset(dgdv.value,dgdv\_nz(:,k+1));            \\
dvX1=umf\_lusolve(..\\
h\_handle(1),spdiag(m(pool\_ind\_weight(1)))*dvX1+..\\
stepsize*dgdv\_kp1(cum(1),:)..\\
);\\
dX\_dv(cum(1),:)=dvX1;}
\end{quote}
where the variable \texttt{dvX1} is introduced in order to manage adjacent memory boxes, which speeds up the computation. In this script, the discrete time scheme is implemented explicitely without refering to an embedded function. After $LU$ factorization, the linear system given by the discrete scheme is solve at each discretization time by the \texttt{umf\_lusolve} command.

For larger cumomer weight values, the computation is made in the same spirit, except that the sequence $\left(\partial_{x_i}b_k\right)_{i<k}$  enters into the calculation \emph{via} a matrix product by the command \texttt{spset}.

 This procedure produces the complete following Scilab code
\begin{quote}
\texttt{
for ind=1:length(ind\_meas)\\
\hspace*{4mm} for k=ind\_meas0(ind):ind\_meas0(ind+1)-1\\
\hspace*{8mm}  dvX1=dX\_dv(cum(1),:);\\
 \hspace*{8mm} dgdv\_kp1=spset(dgdv.value,dgdv\_nz(:,k+1));            \\
\hspace*{8mm}  dvX1=umf\_lusolve(..\\
\hspace*{8mm} h\_handle(1),spdiag(m(pool\_ind\_weight(1)))*dvX1+..\\
\hspace*{8mm} stepsize*dgdv\_kp1(cum(1),:)..\\
\hspace*{8mm} );\\
\hspace*{8mm} dX\_dv(cum(1),:)=dvX1;\\
\hspace*{8mm} for i=2:length(cum)\\
\hspace*{12mm} dvXi=dX\_dv(cum(i),:);\\
\hspace*{12mm} db\_dx\_mat=db\_dx\_nz(i);\\            
\hspace*{12mm} dvXi=umf\_lusolve(..\\
\hspace*{12mm} h\_handle(i),spdiag(m(pool\_ind\_weight(i)))*dvXi+..\\
\hspace*{12mm} stepsize*dgdv\_kp1(cum(i),:)+..\\
\hspace*{12mm} stepsize*(spset(db\_dx(i).value,db\_dx\_mat(:,k+1))*dX\_dv)..\\
\hspace*{12mm} );\\
\hspace*{12mm} dX\_dv(cum(i),:)=dvXi;\\
\hspace*{8mm} end\\
\hspace*{4mm} end\\             
\hspace*{4mm}  gradv=gradv+dX\_dv'*Ct\_Omega\_Sypm2\_e\_label(:,ind);\\
    end\\
%    for i=1:length(cum)\\
%\hspace*{4mm}  umf\_ludel(h\_handle(i))\\
%    end\\
%    gradv=2*gradv;
}
\end{quote}
\section{Computing statistics}
\end{document}