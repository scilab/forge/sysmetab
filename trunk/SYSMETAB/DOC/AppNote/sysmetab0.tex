\documentclass{bioinfo}
\copyrightyear{2005}
\pubyear{2016}
\usepackage{url}
\begin{document}
\firstpage{1}

\title[short Title]{Sysmetab: high-accuracy sofware for nonstationary metabolic flux analysis}
\author[Mottelet \textit{et~al}]{Stephane Mottelet\,$^{1}$\footnote{to whom correspondence should be addressed}\, and Gil Gaullier\,$^{2}$ }
\address{$^{1}$Department EA4297 TIMR, Sorbonne University, Universit\'e de technologie de Compi\`egne, rue du docteur Schweitzer, F-60203 Compi\`egne, France\\
$^{2}$Department EA2222 LMAC, Sorbonne University, Universit\'e de technologie de Compi\`egne}

\history{Received on XXXXX; revised on XXXXX; accepted on XXXXX}

\editor{Associate Editor: XXXXXXX}

\maketitle

\begin{abstract}
\section{Summary:}$^{13}$C metabolic flux analysis ($^{13}$C MFA) is a powerful technique to assess \emph{in~vivo} metabolic reaction rates. It is usually performed once $^{13}$C has been absorbed by microorganisms but transient experiments are also very informative. In the last case, the computational cost generated has prevented the development of efficient softwares for the classical cumulative istopomer (cumomer) model. Using the framework of nonlinear optimization and  the so-called adjoint method,  the software \texttt{sysmetab} shows that this nonstationary problem can be made tractable even when it is modeled by cumomers. The approach of \texttt{sysmetab} is efficient, flexible and provides, with low cost, accurate parameter identifications by high-order numerical schemes that can simulate dilution effects.
\section{Availability and implementation:} \texttt{sysmetab} is implemented in XSL and Scilab. The software package can run under Linux and Mac OS X and is distributed under an Open Source license at \url{http://forge.scilab.org/index.php/p/sysmetab/}.
\section{Contact:} stephane.mottelet@utc.fr
\section{Supplementary information:} Supplementary data are available  online.
\end{abstract}

\section{Motivation}
$^{13}$C isoptope labeling experiments (ILE) enable to assess the \emph{in~vivo} metabolic phenotype by the estimation of immesurable reaction rates (fluxes). This estimation relies on  the model introduced in~\citep{sauer,wiechert1,wiechert2} and derives from the optimization of the model parameters. Thanks to progress in measurements, this technique has been applied to the analysis of complex cells such as eukaryotes and mammalian cells, which makes involve thousands of equations once the carbon is  absorbed throughout the intracellular metabolism (stationary ILE). An other experimental setup that increases drastically the number of equation is given by transient experiments (nonstationary ILE),  namely when the carbon is being absorbed. This case considers time dependent measurements modeled by a cascade of equations having to be solved at each time measurement. In stationary ILE, the cascade of equations is given by algebraic equations, while it is constituted by ordinary differential equations (ODE) in nonstationary ILE.

Several publicly available softwares have been proposed to handle the large amount of equations and to overcome computational and modeling limitations: \texttt{13Cflux2}~\citep{13CFLUX2} for stationary ILE, \texttt{Inca}~\citep{young2014inca} and \texttt{influx\_si}~\citep{influxs} for both stationary and nonstationary ILE, to name a few. In these references, sophisticated strategies are considered to reduce the dimension of the topological network (by the  introduction of the elementary metabolites units - EMU) or the number of iterations in the optimization (by a linearization method in the spirit of the Gauss-Newton algorithm). However, if one considers the parameter identification in the general nonlinear optimization framework, the identification problem can profit from a suitable method, i.e. the so-called \emph{adjoint} method.
  
The software \texttt{sysmetab} is based on this method and offers the possibility to simulate transient ILE including dilution effects with high accuracy and low computational cost. From the flexible adjoint approach, it enables the identification of free flux and asymptotic intermediate metabolite concentration without requiring any topological network reduction strategy, as the EMU model provides.

\section{Approach}
\texttt{Sysmetab} is based on the general framework of optimization under equality and inequality constraints requiring only the computation of the (projected) gradient of a cost-function. In this framework, the equations linking the parameters to the labeled measurements can be interpreted as a \emph{state} equation and the gradient computation can profit from the \emph{adjoint} method~\citep{lionsmagenes}. This \emph{dual} formulation is available both for stationary and non stationary ILE. In the second case, the ODE integration is carried out by high order  and L-stable numerical schemes that determine their adjoint counterpart. ILE specification is performed by XSL transformations and metabolic flux analysis by Scilab code.
%\begin{itemize}
%\item General framework of optimization
%\item Adjoint method for gradient computation
%\item ODE solved by high order  and L-stable numerical schemes
%\end{itemize}

\section{Methods and implementation}
The sofware \texttt{sysmetab} runs on Linux and MAC OS X platforms and is available under an Open Source license. It is implemented in XSL and \texttt{scilab}. Both steady-state and transient ILE from MS (Mass Spectrometry) and/or NMR (Nuclear Magnetic Resonance) measurements can be simulated.
\subsection{ILE specification}
ILE specification includes the specification of the reaction network, constraints on free fluxes, possibly time dependent labeled data, measurements and initial values for the simulation. These elements are entered in a FluxML document, the standard format of \texttt{13Cflux2} introduced in~\citep{13CFLUX2}. The FluxML document is parsed by XSL transformation to generate automatically a code corresponding to the experiment specified. Therefore, \texttt{sysmetab} is not dependent from a specific ILE and can parse every experiment described by a FluxML document. The series of XSL transformations provides the required material to define the optimization problem of parameter identification. For example, the stoichiometry laws are automatically computed from the reaction network specified and are interpreted as an equality constraint in the general framework of nonlinear optimization.
%\begin{itemize}
%\item FluxML document
%\item balance equations are considered as equality constraints in the optimization process
%\item possibly time dependent labeled data and measurements 
%\item specification handled by XSL transformations
%\end{itemize}

\subsection{Metabolic flux analysis}
\texttt{Sysmetab} performs the parameter identification, namely the metabolic flux analysis (MFA)  calculation, by minimizing the sum-of-squared residuals between simulated and experimental measurements. Those parameters are free flux values - with free pool sizes in transient labeling experiments - that govern the state equation. In nonstationary MFA, the cascade of ODE (given by the state equation) is numerically integrated by high order schemes that can handle dilution effects by using 0-sized mixing pools,  which enables preventing numerical solutions from oscilations (see \texttt{e\_coli\_dil} and \texttt{e\_coli\_dil\_ns} files in the supplementary data). The suitable numerical schemes implemented to this purpose are singly implicit Runge-Kutta (SDIRK) schemes up to order four that share the L-stability property. In order to compute quickly the gradient, their adjoint counterpart are implemented. Both state foreward schemes and adjoint backward schemes are available in their constant time-step and variable time-step versions. In the second case, the time-step of the chosen scheme is adjusted by local error estimation. Since time measurements may not be contained into the time-grid, time measurement interpolation is used.
Once the gradient computation is performed, a Broyden-Fletcher-Goldfarb-Shanno (BFGS) algorithm is launched. First, quite coarse results are obtained by constant fixed time-step. Then, these results are refined by the time-step  adaptation  procedure. These supplementary stages of optimization are performed in few seconds since they start with the estimation results provided by the previous stage. The \texttt{fsqp} library~\citep{fsqp} is used to carry out every stage of the coarse-to-fine optimization process and the MFA calculation is handle by \texttt{scilab} codes.

Once the parameter identification has been performed, a goodness-of-fit can be carried out thanks to both uncertainty and error analysis. In the first case, the uncertainty analysis is based on a (possibly parallel on a multi-core architecture) Monte Carlo analysis or on linearized statistics to compute confidence intervals. In the second case, we have observed that the error made on the parameter estimations inherits the truncation error of the numerical scheme. This means that, for a given accuracy, the larger the order of the numerical scheme, the larger the time-step can be chosen. Therefore, using a high order scheme can speed up the parameter estimation, as  a larger time-step is required to achieve the same accuracy.
%heritage de l'ordre schema

\begin{figure}
\begin{center}
\includegraphics[scale=0.75]{WorkFlow}
\end{center}
\caption{Workflow related to the software \texttt{sysmetab}}
\end{figure}

\subsection{Experimental design}
\texttt{Sysmetab} offers the possibility to generate synthetic data from the parameter identification obtained in the stationary case. 
In particular, it enables (by simulation) to choose the number and the distribution of the time dependent labeled measurements that  minimize the uncertainty and/or the error analysis of a given parameter. Thus, by simulating synthetic data, \texttt{sysmetab} may be used to design time dependent experiments in real world in order to investigate a specific assumption on a metabolic network. 
%\begin{itemize}
%\item sum-of-squared residuals
%\item state and ajoint numerical schemes
%\item time step: adaption and time measurement interpolation
%\item MFA handled by scilab code and fsqp library
%\end{itemize}
\section{Conclusion}
Nonstationary Metabolic Flux Analysis increases drastically the number of equations, which makes the problem hardly tractable. The software \texttt{sysmetab}
addresses the challenges posed by exploiting the large number of equation in the basic model of cumulative istopomers (cumomers). Using the adjoint method in the general framework of nonlinear optimization  reduces the computational cost and enables  to implement high order numerical scheme even in the basic model of cumomers, i.e. without requiring the EMU model. Furthermore, the adjoint approach can be extended to EMU, which constitutes future prospects of implementation.

\section*{Acknowledgement}
This work was performed, in partnership with the SAS
PIVERT, within the frame of the French Institute for the
Energy Transition (Institut pour la Transition Energ´ etique
(ITE) P.I.V.E.R.T. (www.institut-pivert.com) selected as an
Investment for the Future (”Investissements d’Avenir”).
This work was supported, as part of the Investments for
the Future, by the French Government under the reference
ANR-001.


\bibliographystyle{natbib}
%\bibliographystyle{achemnat}
%\bibliographystyle{plainnat}
%\bibliographystyle{abbrv}
%\bibliographystyle{bioinformatics}
%
%\bibliographystyle{plain}
%
\bibliography{sysmetab}


%\begin{thebibliography}{}
%
%
%\end{thebibliography}
\end{document}
